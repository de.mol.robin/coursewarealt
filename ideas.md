# Ideas

## Next gen

Exercise for servlet or JAX-RS: make a fake authentication service, which allows you to log in and which returns a token.
The token should be signed in a verifiable way, so there must be an endpoint where the public key (+algo) can be found, so the token user can check the validity of the token.
=> next, we can make use of this when implementing security
- http auth mechanism extracts bearer token and redirects to login endpoint when there is none
- identity store verifies validity of token and returns user+roles

- JPA: also show update JPQL

### Option 1: JEE + Spring megablock

- DECOUPLED from education
    - ppt instead of courseware
    - NOT reusable for customers, but highly reusable for acaddemicts
    - Robin teaches the whole thing, Brecht focusses on maintenance of Spring material, Robin on JEE

(overview that there are plenty of frameworks, but that we focus on JEE and Spring)
- Dropwizard
- Javalin
- GWT
- Vert.x
- JEE
- Spring
- ...

Course concept: lots of topics, in low/medium detail.
The idea is that the acaddemicts know of all the options that exist, and that they know where to search for extra information, so they can learn the details of particular topics when required.

1. Preliminaries
    - inversion of control/dependency injection (JNDI)
    - managed objects/beans
    - web layer (including security?)
    - testing (unit vs integration)
    - web app (MVC)
    - ?mocking?
    - validation
    - aspect-oriented programming/proxy classes and cross-cutting concerns
    - ?container-based programming?
    - ?batch processing?
    - ?security?
2. JEE
3. Spring
4. Project exercise

#### Format

- (Math-like) exercises
    - pro: freedom to create exercise on specific topics, not bound to find a way to force it into a bigger picture
    - pro: good way to introduce the habit of writing unit tests to test functionality (cause we won't deploy these in a container, per se)
    - con: abstract, hard to see bigger picture
- Small project-exercise (optional, depending on feasibility and overlap with big project)
    - ties things together
    - could be running through the course (code as we go)
    - could be a single day at the end of the course
    - per block: one for JEE, one for Spring
    - perhaps multiple per block even (web service, soap service, web app)
- Big project (togethair)
    - think micro-services
    - different groups do 1-2 of the microservices
    - they need to communicate with each other
    - they could mock eachothers services during development
    - or we just do togethair... :)

#### Structure

##### Spring

core

- Introduction (what is Spring + history)
    - ApplicationContext (the Spring container: what is it)
    - Spring boot
    - template-driven (avoids boilerplate)
    - ?packaging? (-> development model/dev tools)
- Bean wiring (~ CDI)
- Configuration
- [risk] Web MVC (Spring dev tools)
- REST (includes REST client)
- AOP (+ transactions)
- Data (jdbc/jpa) + ?mongodb?
- data REST
- testing (web layer, repo...)
- [risk] security (BASIC, oauth2...)
- [small risk] messaging (jms (kafka, active mq), AMQP (rabbitmq))
- actuator (~ MP-fault tolerance)
- [small risk] scheduling (~ EJB timers)
- [small risk] caching

extra

- [risk] cloud (gateway, circuit breaker, ...)
- [risk] batch
- [small risk] containerization (spring app to docker image)

not present from old course

- groovy
- migration
- sockets
- soap
- email
-- extra remark: the (long) list of core topics can be thinned significantly, it is often in more detail than is needed, which is actually more confusing than informational


### Option 2: spring and JEE as independent courses

- full spring course = preliminaries + spring specific
- con: less integration with JEE
- pro: (more) reusable as standalone course

### Plans for JEE

- Start with block on API development
    - servlet (just hello world, explain this is base for API and JSF)
    - quickly move on to JAX-RS (drop JAX-WS?)
    - extract logic to EJB (introduce CDI yet?)
    - exercise
        - number guessing game?
        - small crud (include JPA?), perhaps interesting later in JSF exercise?
            - could be a token generating service... maybe for spring?
    - to be decided:
        - include security?

- Position of CDI?
    - with loose exercises, could move CDI up to somewhere in the start, to explain things like the container and managed beans
    - Follow closely with interceptors (~aspect oriented programming)
- JAX-RS
    - add extra info about resource lifecycle
        - per-request by default
        - can be coerced into requestscoped OR applicationscoped CDI bean
        - can also be coerced into stateless OR singleton EJB
- JPA: exercise on VERSION!

- Investigate how microservices work (separate databases? same database? share entities (jar) among microservices? forego all FKs? are microservices internally protected??)
    - only if I can answer all of these questions, migrate the exercise to microservices...
- "JPA first" only makes sense in context of long-running exercise, so it's OK to keep servlet/ejb at start!
- course format: no longer 1 long running exercise (merch shop) but rather smaller exercises. Merch shop can remain as JPA/JSF exercise. JPA is initially developed and tested through tests, not through servlet/ejb
    - servlet/ejb number guessing game
    - JPA/JSF merch shop (perhaps simplify a bit, like the user registration and such...)
    - some web service exercise, maybe a todo-list here?
- Investigate JMS, think of exercise
    - Ex ideas: guessing game with topic and queue?
        - post message to queue to start a game
        - game server (only subscriber to the queue) generates a number and a few guesses and posts to the topic that a game has started (broadcast)
        - subscribers of the topic (players) can reply by sending a guess to the queue
        - the server posts the results of the guess on the topic (higher, lower, winner, game over)
        - players keep replying until game over or there is a winner, the winner logs a victory message and the losers a "ggwp"
- Investigate Batch, think of exercise
    - Ex idea: write deployable which has an MDB subscribing to a topic, which does a batch job each time the topic produces a message

### Plans for Spring

- go through existing slides, removing unnecessary details (low-level specialisations, too many code snippets...) (together)
- do investigative work in some subjects (see risk analysis) (Brecht does study and prepares material)
- [large] do exercises and/or make new ones (Brecht makes exercises, Robin tests them by making them, based on the slides)
- migrate to PPT

## Courseware

The software is not very stable.
Often, especially when trying to combine code snippets and lists, the markdown is parsed incorrectly.
Layouting is limited (bullets and poor image support), and fumbling with html will have a noticeable, negative impact on the PDF rendering.
The trainer notes/invisible blocks are buggy (having bulleted lists in the notes can cause the PDF rendering to break is there unless there is a blank line above the end-of-block line).

On the side, the engine refers (hard coded) to phantomjs.bat (which does not exist on non-windows OSs, where phantomjs is simply "phantomjs").
Though this can easily by fixed by symlinking phantomjs.bat to phantomjs, this is just poor code, which has led to the decision to commit an entire node (and bower?) installation inside the git repo, making this one of the most painful projects I have ever cloned.

## Improvements

JAX-RS resource without EJB annotation: what kind of "Bean" is this? CDI? Servlet? Other??

### Data model

v Make email the ID for user entity?
v Get rid of manufacturer
v OrderLine could have a composite PK: orderid + merchid
v Embed address in order? (it's simpler, also for reusing the same address info)
- swap mugs for pens (enum color)

### Microservices

Towards microservices: the data model needs to be separated so that order is not directly linked to people or merch table.

- Perhaps we shouldn't do microservices?
- Make library containing dtos (jar)

### Security concepts in preliminaries

Since the concepts of security are so strongly tied to the container (rather than JEE code), maybe it would be better to introduce all the security concepts in the preliminaries.
We could explain:

- What are realms, users, groups
- What are roles and what is group to role mapping
- What authentication mechanisms are there? How do they work (session based or not) and how can one log out?

## Comments by students

- Hoodie size could be enum, think of different attributes for mug... (or different type of merch, like pens that write in a specific color ink?)

## Observations

### JDK

- Make sure everyone is using JDK 11 (also for wildfly!)
- maven enforcer plugin?

### Running exercise

- Security is (heavily) complicated by not using the email as person id... It results in having to have a utility somewhere to fetch the current user details by email (rather than being able to use `entityManager.find`), meaning we also can not use `entityManager.getReference`.

> natural vs artificial key, probably safe to use natural key in the exercise...

### JPA

- Explain default values (indicated in Java)

- JPQL does not explain join (and fetch join)??
    - After "fetching relations", should add slides on how to fetch LAZY associations

- Add part on JPA Converters

- (?) Clarify enum constants in DB

- JPA should be more interactive and accessible
    - Maybe more code-along instead of many slides with examples?
    - Maybe more exercises in between?
    > exercises in alternative way: test-driven

### JBV

- @Valid is not explained?

- Change exercise to not suggest they try to write their own **unique** validator (for email), instead point out that even the JBV spec suggests that this is unreliable and should be avoided.

### JSF

- Copy servlet logic to second-last slide to repeat programmatic access

### Security

- It is hard and confusing to maintain the user database for so long without using it
- Working with the container's security realms is also difficult, and more importantly, out of scope of JEE (it focusses on wildfly instead of JEE)
- Plain text passwords -> no, just no!
- The JEE8 Security spec concepts are hard to grok. Perhaps we should get rid of the users in our application, banking fully on container based security mechanisms, and focus on a different use case than a web shop...
> likely no longer a problem when abolishing the single, long-running exercise: servlet with AS user, JSF with own auth mechanism

- combination of JSF and servlet security?
> no longer an issue with separate exercises :)

- JAX-RS security?

### Interceptors

- Flesh this out a bit more, like delve into the details of the signature of around invoke and such (what happens on an error, ...)

### JAX-RS

- Illustrate integration with bean validation

### JSON-B

- Remove?

### JAX-B

- Remove?

### JMS

- The explanation is poor, there is much on the slides that I didn't know well enough to explain it.

### Batch

- See JMS

### Testing

- It would be nice to talk about this, too...
> junit and mockito

## Starting with JPA (or just: starting with the domain)

### Why

JPA is a long and quite theoretical part.
Natural approach to starting a project (subjective).
Fits the narrative (what is an enterprise application, which starts with the database setup in the slides).

### Pro

- Avoids having to introduce web tech.
- Avoids having to set up application server first.
- Could force test-driven initial code experience.
    - requires resource-managed persistence unit (which means no need to first define datasource in wildfly).
- Later introducing EJB (and the application server) more clearly highlights the benefits of EJB (and the container), like automatic transaction management etc.

### Con

- No prior introduction of web and EJB containers (as reference to compare the workings of the entity manager and persistence context to).
- JPA is not an original JEE spec, nor is it strictly required.
- Not starting with a WAR (no "hello world").

Initial code exercises could be written in the from of tests.
That way, the persistence unit and transaction management is explicit.
It can all be made to work without relying on the concept of containers.
The next step would be to add an accessibility layer, which can be through the use of a REST layer, leading to the introduction of a concept like Servlets.

## Keeping servlet/EJB first: rethink exercises to be more relevant to data model

ATM, the course starts with a hello world example, which is then expanded (grant parameter, auto capitalize name, add a ping-pong servlet (to illustrate annotation-driven metadata), add a guessing game servlet (to illustrate sessions)...).
- Hard to grasp what persistence context is
Later, EJB is added into the mix as a way to separate logic into its own components: the name cleaning goes into a separate (stateless) bean, the guessing game can go into a (stateful) bean (complicated exercise which requires meticulous manipulation of the http session and a programmatic JNDI lookup to obtain a unique reference for such a bean per unique session - injection in the servlet class will result in multiple sessions sharing the same stateful ejb), and one can think of extra EJBs (e.g.: as part of the guessing game exercise, an EJB can do the logic of parsing and validating the submitted guess parameter (is it null, it is an int, is it in the correct range?)).

**HOWEVER**: none of this is actually relevant to the merch shop, which is confusing.
It feels like the time on the earlier exercises is "wasted".
So the goal is: can we rethink the exercises to be relevant to the merch shop?

