# Using Courseware 2

## Bugs

### PDF rendering UI Scaling

#### Problem

PhantomJS seems to be affected by Windows's scaling factor (100% - 125%). This means that when rendering on a display that is scaled to 125%
the result is not the same.

One way to identify this problem is when the PDF's title page shows up with the R logo (using 'green' theme) on the second page.

Need to find a way to render PhantomJS without any UI scaling for stable effect. Not done any research on this yet.

#### Workaround

It is agreed upon for now that rendering a PDF for now __must__ be done from a 125% scaled UI, until a better solution presents itself

### Duplicate slide names

No two same slide title that are not consecutive must ever be used in the entire combination document for now.

This is because the double topic mrger enhancer has a bug that would combine _any_ slides with the same name, rather than
combining them only when they are in direct sequence of other.

### Other issues

- Svg (external, still inside <img>) works everywhere except for in the PDF render. (Maybe not all SVGS are broken. The one I used had internal png embedded).

Kevin confirms that a normal SVG (no embedded images) added as an external image (using <img> tag) work perfectly fine both in presentation
as handouts pdf.

- Some valid Java code blocks are not recognized correctly by the syntax highlighter/markdown parser

The following example demonstrates this bug:

```java
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = NotNullValidator.class)
public @interface NotNull {
  String message() default "{javax.validation.constraints.NotNull.message}";
  Class<?>[] groups() default {}; // to control the order of validation
  Class<? extends Payload>[] payload() default {}; // to associate meta-data
}
```

The final quote at the end of the message is interpreted as the beginning of a new String. The rest of the code block will not be highlighted!

---

- Nasty bug in the duplicate topic merger: whenever two slides exist with the same name in the entire presentation, not following each other, it removes the h2 and hr (this is normal)
but it should group them per occurring together (without gaps). Not a dealbreaker since normally you shouldn't have two topics with the same name. Fixing is not a huge problem, but requires some thinking (i'm short on thinking-juice at the moment).

---

- The Console outputs a series of 404 errors on images when the MarkDown DOM is loaded by Remark. Once the src attribute is set, loading immediately starts by the browser. Any enhancers (like the image relocator) come too late!

```javascript
// prevent loading of images once the DOM is ready? probably not...
<script type="text/javascript" charset="utf-8">
    $(document).ready( function() { $("img").removeAttr("src"); } );
</script>
```

Most solutions would require a change in Remark code.

One option is to replace the image's `src` with a temporary image url first (which is valid, and shows no 404 messages), but put the MarkDown image url in `data-src` attribute. Apply the relocate enhancer on the urls from `data-src`, then replace the `src` with the relocated `data-src`.

```javascript
$(function() {
    // Only modify the images that have 'data-src' attribute
    $('img[data-src]').each(function(){
        var src = $(this).data('src'); // fetch the 'data-src' attribute
        // relocate the src here
        $(this).prop('src', src); // set the correct on the image and start loading
    });
});
```

This technique is also used in the JQuery Lazy plugin (https://github.com/eisbehr-/jquery.lazy) and in the JQuery LazyLoad plugin (https://github.com/tuupola/jquery_lazyload), which also add a beforeLoad event.

You could also prevent images from being loaded, by wrapping them in a `<noscript>` tag. Browsers that have JavaScript enabled, will just ignore the tags and images embedded in it.

```html
<noscript class="img">
  <img src="my-image.jpeg" />
</noscript>
```

Do anything you want afterwards with some jQuery, like replacing the `<noscript>` tags with their inner content, which will start loading the images.

```javascript
$('noscript.img').replaceWith(function(){
    return $(this.innerText);
});
```

Also check http://nicolasgallagher.com/responsive-images-using-css3/. This lets you change the images shown depending on media queries using CSS only! Problems: Which media query would be right? And we will still need to update the ``img`` tag's ``src`` and ``data-src`` attributes by changing Remark code...

---

## Backlog ideas

### Undetailed ones

- Generate the information necessary to update the Education Website, especially the Table Of Contents of the created course.
- Add a HR inside the slides to add spacing __invisible__ spacing (can be done using `***` not `---` since this is already interpreted by remark.) Handouts should ignore this.
- Add exercises as supported feature in markdown somehow. Maybe add the exercises at the end of the handouts.pdf, as part of the file to print.
- May be able to calculate page numbers by checking the Y coordinate in the HTML document of each title, and converting pixels to multiples of 29.7cm (72pixels per inch ???), using some jQuery magic.
- Add handlebars template and enhancer to "feed" course title into each chapter (like the master slides in powerpoint did)
- Add support for twin slides left and right split (needs some extra semantics. @rd{twin} or so.)
- Add a proper goal slide (idea: add a marker like ??? but using something like @rd{twin} @rd{goal}, this can be easily filtered with an enhancer.
- Add presentation PDF as fallback plan for when the proverbial s**t hits the fan.
- Decouple from remark by injecting rd-* classes for the themes to act on (this is to my (Kevin) opinion useful because I think there is too much coupling with this framework at the moment, which impedes at least one thing: rendering PDF _presentations_)
- No need to artificially delay "page settle down period" when phantom can detect a "ready token" on the page (can you read console.log? or something we specially add for this in the DOM?).

### Dynamic Parameterizable Modules (Kevin)

So, in the future it'd be super cool to be able to do something like this on couse.json level:

```json
{
  "title": "Generic",
  "theme": "green",
  "disabled": true,
  "modules" : [
    "introduction",
    {"type": "goal", "params": ["Learn a thing or two about how cool this is"]},
    "module-one",
    "module-two",
    {"type": "chapter", "params": ["Chapter Three: A Story Of The Awesome"]},
    "module-four",
    "module-five",
    {"type": "exercise"}    
  ]
}
```

Why is this cool? Because there are currently several use cases that could benefit from this:

- Chapter title glue slides with only one title and that's it (see JEE710, JAXP010) are **DUMB**!
    - They are added b/c reusable modules with chapter titles in between don't reuse well I have discovered
- Goal slide is always the same with different text
- Nancy's generic exercise slide requires an identical (and not even parameterized) slide to be put at the beginning of each exercise
    - See HCJ010 course where they are used in trial

Implementation note: could be done by leveraging JavaScript's dynamic typing, and can be made totally transparent and compatible
with existing structure if the normal string based modules are just a "shorthand" for a `{type: "contents" param="module-one"}` or something similar

---

## Creating MarkDown Files

- Use one hashtag (#) for a module, two hashtags for a slide. Each slide should be separated by three dash signs (-).

```markdown
# Module 1

---

## Slide 1

---

## Slide 2

```

- For bullets, use a dash sign for each bullet. Use tabs for sub-bullets.

```markdown
- item 1
- item 2
  - item 2.1
- item 3
  - item 3.1

```

- Code blocks require the use of three backticks. Also add the language used, such as "java".

````java
```java
public class Sample {
  public Sample() {
    // constructor
  }
}
```
````

- Highlight individual lines in code blocks by prefixing the line with an asterix.

```java
public class Sample {
*  public Sample() {
    // constructor
  }
}
```

- To start a note-block, add three question marks. Make sure to start the note at the end of the slide, Everything after these question marks are part of the note.
- A noteblok that should be visible in the handouts, should have an empty line after ???
- A noteblok that should be invisible in the handouts, cannot have an empty line after ??? - layout in these is not possible

```markdown

??? This is a note
followed by a white line will be printed in the handouts (pdf)
without a white line will not be printed, but is visible in the presentation mode (no bullets can be used)

```



- You can embed HTML and SVG directly in the slides.

```html
<div style="color: blue">This is blue text</div>
<svg width="18cm" height="5cm">
	<circle cx="50" cy="50" r="40" stroke="green" stroke-width="4" fill="yellow" />
</svg>
```

- Images use a specific syntax as follows:

```markdown
![alt text](images/image.png "title text")
```

- To make images that do not have a border (the way the border turns out is up to the theme):

```markdown
![alt text](images/image.png#no-border "title text")
```

- Specifying border explicitly is the same as adding nothing:

```markdown
![alt text](images/image.png#border "title text")
```

## Presenting

- Use the presentation-template.html file from a browser
  - Use Shift-C to create a window for presenting on a second screen
  - Use Shift-P to change to presentation mode (this let's you see notes and the next slides)

- Kevin's note: this works on my machine by just doing `P` and `C`. Using Firefox, Windows 8.1.

TODO Handouts
- Test Serif font for the pdf handouts (since print should be serif and screen should be sans)

TODO Rebranding
- a secondary color can be used in the slides and in the handouts, this can be eg pink
    - possible colors can be found: https://portal.realdolmen.com/sharedservices/marketing/Documents/Realdolmen_Beknopt_Huisstijldocument.pdf

TODO Handouts
Nancy added a <br /> in some courses to make sure that titles would not be orphaned in handouts
needs to be solved in css with orphans: ?;