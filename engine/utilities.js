/**
 * RealDolmen namespace containing useful functions.
 * Requires and needs to be loaded after jQuery.
 */
var realdolmen = (function() {
    var exports = {};

    exports.enhancer = {};

    /**
     * Parses the request URL and extracts parameter with specified name or undefined if not present.
     * @param name The name of the request parameter to extract.
     * @param spec Spec object with properties 'default' for a default value if missing and type which is either 'string', 'boolean' or 'number'
     * @returns either the extracted request parameter value, a default if specified or undefined.
     */
    var getRequestParameterByNameOrDefault = function(name, spec) {
        var match = new RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        var rawValue = match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        if(rawValue === undefined || rawValue === null) {
            return spec.default;
        } else {
            var convertedValue = null;
            switch (spec.type) {
                case 'number':
                    convertedValue = Number(rawValue);
                    break;
                case 'boolean':
                    if (rawValue === 'true') {
                        convertedValue = true;
                    } else if (rawValue === 'false') {
                        convertedValue = false;
                    } else {
                        if (spec.default === undefined) {
                            throw new Error('Value "' + rawValue + '" of parameter "' + name + '" should be of type boolean (true or false)');
                        }
                    }
                    break;
                case 'string':
                    convertedValue = rawValue;
                    break;
                default:
                    throw new Error('Unrecognized parameter type: ' + spec.type);
            }
            return convertedValue;
        }
    };

    /**
     * Retrieves a map (object) of request parameter values or a default value if the parameter is missing.
     * @param parameters A map of parameter name/default value pairs.
     * @return An object of parameter name/value pairs.
     */
    exports.getRequestParameterMapOrDefault = function(parameters) {
        var values = {};
        for(var parameterName in parameters) {
            if(parameters.hasOwnProperty(parameterName)) {
                values[parameterName] = getRequestParameterByNameOrDefault(parameterName, parameters[parameterName]);
            }
        }
        return values;
    };

    /**
     * Injects the page with a custom theme (stylesheet) for presentations to be rendered in.
     * @param themeName The name of the theme (for for example: 'turing' would load theme 'themes/turing.css'.
     */
    exports.injectTheme = function(themeName) {
        console.log('Loading theme:', themeName);
        exports.addStylesheetToPage('engine/themes/' + themeName + '/style.css');
    };

    /**
     * Loads and evaluates a Handlebars template.
     * @param sourceUrl The template source URL to be loaded using XHR.
     * @param context The data context object to use as the model for evaluating {{handlebars}} expressions
     * @return A promise
     */
    exports.evaluateHandlebarsTemplate = function(sourceUrl, context) {
        return $.ajax(sourceUrl).then(function(source) {
            return Handlebars.compile(source)(context);
        });
    };

    exports.addStylesheetToPage = function(stylePath) {
        $('<link rel="stylesheet" type="text/css">').attr('href', stylePath).appendTo(document.head);
    };

    /**
     * Invokes all currently registered enhancers for the specified course.
     * 'this' will be mapped to the course object.
     * @param course The course to trigger enhancers for.
     * @param mode The mode to trigger enhacers for (handouts or presentation)
     * @param contextElements The context parameters passed as argument[0]
     */
    exports.invokeEnhancers = function(course, mode, contextElements) {
        if(mode !== 'presentation' && mode !== 'handouts') throw new Error('Invalid enhancement mode specified.');

        course.enhancers.forEach(function(e) {
            if(e[mode]) { // Looks like a duck
                console.log('Invoking enhancer for "' + mode + '": "' + e.name + '"');
                e[mode].call(course, contextElements);
            } else {
                console.log('Enhancer "' + e.name + '" does not have an enhancement for mode "' + mode +  '"');
            }
        });
    };

    return exports;
})();