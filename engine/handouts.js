realdolmen.handouts = (function() {
    var TITLE_PAGE_TEMPLATE = 'engine/title-page-template.html';

    var TABLE_OF_CONTENTS_CLASSIFIER = 'rd-table-of-contents';
    var BODY_CLASSIFIER = 'rd-body';
    var CONTENTS_CLASSIFIER = 'rd-contents';
    var EXERCISES_CLASSIFIER = 'rd-exercises';

    var exports = function(course, title, version, contents, $handouts, type) {
        console.log('Building handouts of course "' + course.title + '"');

        buildTitlePage(course, title, version).then(function(titlePage) {
            $handouts.append(titlePage);
            var $toc = $('<div>').addClass(TABLE_OF_CONTENTS_CLASSIFIER);
            $handouts.append($toc);

            var $contents = $('<div>').addClass(BODY_CLASSIFIER).addClass(type === 'exercises' ? EXERCISES_CLASSIFIER : CONTENTS_CLASSIFIER);
            $contents.append(contents);
            $handouts.append($contents);

            realdolmen.invokeEnhancers(course, 'handouts', $contents);
            buildTableOfContents($toc, $contents, course);
        });
    };

    var buildTitlePage = function(course, title, version) {
        console.log('Building title page for handouts of course "' + course.title + '"');
        return realdolmen.evaluateHandlebarsTemplate(TITLE_PAGE_TEMPLATE, {title: title, version: version}).then(function(html) {
            return $(html);
        });
    };

    var buildTableOfContents = function($toc, $contents, course) {
        console.log('Building table of contents for handouts of course "' + course.title + '"');

        var unwrapAnchors = function($toc) {
            $toc.find('a').replaceWith(function() {
                return $(this).text();
            });
        };

        $contents.planize({
            number_suffix: '. ',
            generate_toc: true,
            max_level: 2,
            //debug: true,
            toc_elem: $toc
        });

        unwrapAnchors($toc);
    };

    return exports;
})();
