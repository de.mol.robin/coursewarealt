/**
 * Enhances the DOM structure to enable nicer code highlighting formatting.
 */
realdolmen.enhancer.codeHighlighting = {
    name: 'Code highlighting',

    handouts: function(target) {
        // required for line highlighting, make every code line a div
        function wrapLines(block) {
            var lines = block.innerHTML.split(/\r?\n/).map(function(line) {
                return '<div class="code-line">' + line + '</div>';
            });

            // remove the last line introduced by the extra newline character
            if (lines.length && lines[lines.length - 1].indexOf('><') !== -1) {
                lines.pop();
            }

            block.innerHTML = lines.join('');
        }

        // add the CSS class to highlight the lines having the *
        function highlightBlockLines(block, lines) {
            lines.forEach(function(i) {
                $(block.childNodes[i]).addClass('highlighted');
            });
        }

        // search for lines starting with a *, these will be hightlighted
        function extractMetadata(block) {
            var highlightedLines = [];

            block.innerHTML = block.innerHTML.split(/\r?\n/).map(function(line, i) {
                if (line.indexOf('*') === 0) {
                    highlightedLines.push(i);
                    return line.replace(/^\*( )?/, '$1$1');
                }
                return line;
            }).join('\n');

            return {
                highlightedLines: highlightedLines
            };
        }

        target.find('pre code').each(function(i, block) {
            hljs.highlightBlock(block);
            var meta = extractMetadata(block);
            wrapLines(block);
            highlightBlockLines(block, meta.highlightedLines);
        });
    }
};