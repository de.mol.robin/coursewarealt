A course can be rendered in two ways:
    - As handouts
    - As a presentation

The canonical representation for both in in markdown format. However, sometimes this markdown is "limited" in expressiveness.
It is hard to differentiate styles for certain elements for example. This problem is solved by the enhancers.

In case of handouts, the course contents is maintained on the course object, and represents the contents of the <div id="rd-course-contents"> element
In case of presentation, the contents is first rendered by remark, and represents the contents of all <div class="rd-slide"> elements
Both are wrapped by jquery first, and then passed to the enhancers.

An enhancer is a simple object that has (optionally) two functions. The this reference is the course object.
{
    handouts: function($contents) {},   // this: course object, $contents: $('.rd-course-contents')
    presentation: function($slides) {}, // this: course object, $contents: $('.rd-slide')
}

All currently enabled enhancers are executed on either handouts or presentation depending on the rendering mode. Enhancers run when the renderer has already generated the DOM structure, and
can at this point freely be introspected and modified to suit the needs of the slide author.

Typical tasks for enhancers are:
    - Getting rid of unwanted dom elements
    - Fixing image src urls so they match the right path
    - Wrapping elements with additional semantics (classes) for easy differentiation with CSS.

In summary, the enhancers can be seen as a sort of "visitor" objects, that are plugging in their behaviour on the used DOM tree.