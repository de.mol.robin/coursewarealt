/**
 * Enhancer that wraps each image with a wrapper (for easy styling) and also fixes the location origin of the images.
 */
realdolmen.enhancer.images = (function() {
    var WRAPPER_CLASSIFIER = "rd-image-wrapper";
    var NO_BORDER_CLASSIFIER = 'rd-no-border';
    var BORDER_CLASSIFIER = 'rd-border';
        

    var self = {
        name: 'Image wrapper and relocator',

        handouts: function(target) {
            var $images = wrapAndReturnImages(target);
            correctImagesPath($images, this);
            addBorderClasses($images);

        },

        presentation: function($slides) {
            self.handouts.call(this, $slides);
        }
    };

    var correctImagesPath = function($images, course) {
        $images.each(function() {
            $(this).attr('src', course.path + $(this).attr('src'));
        });
    };

    /**
     * Adds extra classes 'rd-border' and 'rd-no-border' based on image URL bookmark 'no-border' to differentiate styling.
     * For example an image with URL: 'my_image.png#no-border' receives class 'rd-no-border'.
     * @param $images Sizzle for all images that should be enhanced.
     */
    var addBorderClasses = function($images) {
        $images.each(function() {
            if($(this).attr('src').match(/#no-border$/)) {  // Currently defaults to 'rd-border'
                $(this).addClass(NO_BORDER_CLASSIFIER);
            } else {                
                $(this).addClass(BORDER_CLASSIFIER);
            }
        });
    };

    var wrapAndReturnImages = function(target) {
        var $images = target.find('img');
        $images.wrap($('<div>').addClass(WRAPPER_CLASSIFIER));
        return $images;
    };

    return self;
})();
