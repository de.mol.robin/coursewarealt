/**
 * Enhancer that detects and wraps notes in a wrapper tag, so that it becomes trivial to attach styles on it.
 */
realdolmen.enhancer.notesWrapper = {
    name: 'Notes wrapper',

    handouts: function () {
        var trainerRegex = /(<p>\?{3}trainer)(.|\n)*?(\?{3}t<\/p>)/g;
        var invisibleRegex = /(<p>\?{3}invisible)(.|\n)*?(\?{3}i<\/p>)/g;
        var printRegex = /(<p>\?{3}print)(.|\n)*?(\?{3}p<\/p>)/g;

        var replace = function (selector) {
            selector.html(selector.html().replace(trainerRegex, ''));
            selector.html(selector.html().replace(invisibleRegex, ''));
            selector.html(selector.html().replace(printRegex, function (string) {
                return string.replace('???print\n', '<i>').replace('\n???p', '</i>');
            }));
        };

        var contentsSelector = $(".rd-contents");
        replace(contentsSelector);

        var exercisesSelector = $(".rd-exercises");
        if (exercisesSelector.html()) {
            replace(exercisesSelector);
        }
    }
};
