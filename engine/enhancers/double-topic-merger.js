/**
 * Enhancer that detects double topics and merges their contents under a single header in the handouts
 */
realdolmen.enhancer.doubleTopicMerger = (function() {
    return {
        name: 'Double topic merger',

        handouts: function(target) {
            var topics = [];
            target.find('h2').each(function() {
                var value = $(this).text();
                if(topics.indexOf(value) === -1) {
                    topics.push(value);
                } else {
                    $(this).prev('hr').remove();
                    $(this).remove();
                }
            });
        }
    };
})();
