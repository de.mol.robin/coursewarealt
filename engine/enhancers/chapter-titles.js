/**
 * Enhancer that removes the HR out of the chapter title slides.
 */
realdolmen.enhancer.chapterTitles = {
    name: 'Chapter titles',

    handouts: function($contents) {
        $contents.find('h1').next('hr').remove();
    }
};
