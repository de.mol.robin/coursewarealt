realdolmen.course = (function() {
    var ACTIVE_ENHANCERS = [
        realdolmen.enhancer.chapterTitles,
        realdolmen.enhancer.codeHighlighting,
        realdolmen.enhancer.notesWrapper,
        realdolmen.enhancer.images,
        realdolmen.enhancer.doubleTopicMerger
    ];

    var exports = {};

    var coursesRoot = 'courses/';
    var courseMetaFile = 'course.json';

    var handoutsClassifier = "rd-handouts";

    /**
     * Represents the core course object.
     * The contents of this course are to be injected asynchronously by means of markdown.
     * @param code The course code (e.g. SPR010)
     * @returns A newly created course object.
     */
    var course = function(code) {
        var self = {};

        self.code = code;
        self.path = coursesRoot + self.code + '/';
        self.enhancers = ACTIVE_ENHANCERS;

        var setDocumentTitle = function() {
            document.title = self.title;
        };

        self.handouts = function(contents, exercises) {
            setDocumentTitle();

            if(contents === true) {
                console.log('Rendering handouts with contents');
                $contents = $('<div>').addClass(handoutsClassifier);
                $contents.appendTo(document.body);
                realdolmen.handouts(self, self.title, self.version, self.contents, $contents, 'contents');
            }

            if(exercises === true) {
                console.log('Rendering handouts with exercises');
                $exercises = $('<div>').addClass(handoutsClassifier);
                $exercises.appendTo(document.body);
                realdolmen.handouts(self, self.title + ' Exercises', self.version, self.exercises, $exercises, 'exercises');
            }
        };

        self.presentation = function() {
            setDocumentTitle();
            realdolmen.presentation(self);
        };

        return self;
    };

    /**
     * Private helper method to load all the course modules asynchronously from markdown files.
     * @param course The course to load and inject modules for.
     * @param moduleNames Array of module names to retrieve contents for from markdown files.
     * @returns A deferred to chain further asynchronous tasks to. When this deferred is fulfilled, all modules are loaded.
     */
    var loadModules = function(course, moduleNames) {
        course.modules = moduleNames.map(function(moduleName) {
            return {
                name: moduleName,
                path: course.path + moduleName + '.md',
                exercisesPath:  course.path + moduleName + '.exercises.md'
            }
        });

        var sanitizeNewlinesInMarkdown = function(data) {
            return data.replace(/\r/g, '');
        };

        var convertMarkdownToJQuery = function(data) {
            return $(marked(sanitizeNewlinesInMarkdown(data)));
        };

        var tasks = [];
        course.modules.forEach(function(mod) {
            // Enqueue Deferred XHR for module contents that should always be fulfilled (otherwise there is an error in the course.json module list).
            tasks.push($.get(mod.path).then(function(data) {
                mod.markdownContents = sanitizeNewlinesInMarkdown(data);    // TODO: only needed for remark renderer (it does not work directly with the HTML nodes but with the MD text). factor this out when implementing spawnling-idea on >>>inversion of control on remark<<< (idea details uploaded in brains of Kevin; warning: slow storage mechanism!)
                mod.contents = convertMarkdownToJQuery(data);
                console.log('Loaded data for contents of module "' + mod.name + '"');
            }));

            // Enqueue Deferred XHR for module exercises that may be rejected (if no exercises are there for this module).
            var d = $.Deferred();
            $.get(mod.exercisesPath).complete(d.resolve);
            tasks.push(d.then(function(xhr) {
                if(xhr.status == 404) {
                    console.log('No data for exercises of module "' + mod.name + '"');
                } else {
                    mod.exercises = convertMarkdownToJQuery(xhr.responseText);
                    console.log('Loaded data for exercises of module "' + mod.name + '"');
                }
            }));
        });

        return $.when.apply($, tasks).then(function() {
            console.log('All modules loaded (-_-). Breaks out in fanatic excitation (>_<).');
            return course;
        });
    };

    /**
     * Fetch course data from metadata file and load course.
     * @param code Course code
     * @returns {*}
     */
    exports.load = function(code) {
        var c = course(code);
        var courseMetaDataPath = c.path + courseMetaFile;
        return $.getJSON(courseMetaDataPath).then(function(data) {
            // Assign title, version and theme from metadata file to course object.
            c.title = data.title;
            c.version = data.versionOfManual;
            if(!data.theme) throw new Error('Course ' + code  + ' does not have a theme set in ' + courseMetaDataPath);
            c.theme = data.theme;

            return loadModules(c, data.modules).then(function(course) {
                course.markdownContents = "";
                course.contents = $([]);
                course.exercises = $([]);
                course.modules.forEach(function(mod, index, modules) {
                    course.markdownContents += mod.markdownContents;
                    course.contents = course.contents.add(mod.contents);
                    course.exercises = course.exercises.add(mod.exercises);

                    // Add a <hr> or --- to the end of the module only if this is not the last module.
                    if(index + 1 !== modules.length) {
                        var separator = '\n\n---\n\n';
                        course.markdownContents += separator;
                        var selector = $(marked(separator));
                        course.contents = course.contents.add(selector);
                    }
                });
                return course;
            });
        }).then(function(course) {
            console.log('Course loaded:', course);
            return course;
        });
    };

    return exports;
})();
