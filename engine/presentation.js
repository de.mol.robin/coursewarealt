realdolmen.presentation = (function() {
    var presentationClassfier = "rd-presentation";
    var slideClassifier = "rd-slide";

    exports = function(course) {
        console.log('Building presentation of course "' + course.title + '"');
        injectSlideDeck(course);

        // Add a custom class marker for easier styling selectors (handouts vs presentation)
        var $slides = $(".remark-slide");
        $slides.addClass(slideClassifier);
        $(".remark-container").attr('id', presentationClassfier);

        realdolmen.invokeEnhancers(course, 'presentation', $('.' + slideClassifier));
    };

    /**
     * Creates and injects the slide decks using remark.
     * @param course The already loaded course.
     */
    
    // TODO: implement inversion of control, this would allow all canonical course stuff to be always the HTML. (simplifies a lot in the course object, and sets us free!)
    // this will require some work though.
    var injectSlideDeck = function(course) {
        var slideshow = remark.create({
            source: course.markdownContents, // TODO: <<---- not happy with this! Fix by means of IOC. (let us do the HTML rendering and pass that to Remark??? (experimental idea) 
            ratio: '16:9',
            slideNumberFormat: 'SLIDE %current%',
            navigation: {
                scroll: true,
                touch: true,
                click: false
            },
            highlightStyle: 'vs',
            highlightLines: true
        });

        //updateImageSrc(course.path);
    };

    //var updateImageSrc = function(coursePath) {
    //    $("img:not(.titlepage)").each(function () {
    //        var src = $(this).attr("src");
    //        $(this).attr("src", coursePath + src);
    //    });
    //};

    return exports;
})();