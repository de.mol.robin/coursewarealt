# Jakarta Security

???trainer
https://jakarta.ee/specifications/security/2.0/apidocs/jakarta/security/enterprise/package-summary.html

A decent read (author: Arjan Tijms): https://dzone.com/refcardz/getting-started-java-ee?chapter=1

???t

---

## Rundown

- Security pre JEE 8 was very container-based (or DIY)
    - Or you could use a 3rd party solution
- Since JEE 8, Jakarta Security introduces pluggable security
- Key features
    - Pluggable authentication mechanisms
    - Pluggable identity stores
    - Authorization
    - SecurityContext

???trainer
Declarative security will get you started quickly and easily, with a few annotations and minimal configuration.
Programmatic security gives you full control in case declarative security is not flexible enough.

???t

---

## Layers of security

- Application-layer
    - provided by containers
        - \+ fine-grained, application-specific settings
        - \+ uniquely suited to the needs of the application
        - \- not transferable (data is only protected in the container)
        - \- multiple protocols = multiple points of attack
- Transport-layer (TLS)
    - HTTPS
    - encrypted communication
        - \+ secures messages and attachments in transit
        - \- all-or-nothing (no selective security)
        - \- protection only during transit
- Message-layer
    - SOAP
        - \+ security stays with messages across hops
        - \+ independent of application environment or transport protocol

???trainer
TLS is point2point, not end2end

???t

---

## Vocabulary

- Realm
    - security policy domain of application server
    - collection of users and groups
- User
    - individual or other client identity
- Group
    - collection of similar users
    - scoped to the _container_
- Role
    - permission for a particular set of resources
    - scoped to an _application_

---

## Protecting resources

- Annotations
    - EJB: `@DeclareRoles` + `@RolesAllowed`/`@PermitAll`/`@DenyAll`
    - Servlets: `@ServletSecurity` with nested `@HttpConstraint`
 - Deployment descriptor
    - Servlets (`web.xml`)
        - `<security-role>` elements
        - one or more `<security-constraint>` elements

```xml
<security-role>
    <role-name>manager</role-name>
</security-role>
<security-role>
    <role-name>employee</role-name>
</security-role>
<security-constraint>
    <web-resource-collection>
        <web-resource-name>Protected Area</web-resource-name>
        <url-pattern>/security/protected/*</url-pattern>
   </web-resource-collection>
    <auth-constraint>
        <role-name>manager</role-name>
    </auth-constraint>
</security-constraint>
```

???trainer
For EJB, the (non-servlet) annotations are used

<img src="images/security-web.png#no-border" style="height: 12em"/>

???t

---
## Declarative security

- Application server
    - create user, realm, group in application server
    - `<login-config>` in `web.xml`
        - default group to role mapping
        - role mappings can be added in application server specific descriptor
- Programmatic
    - `HttpServletRequest`
        - `authenticate`: similar to security context
        - `login` attempts to log in with provided username + password
        - `logout` resets called identity
        - `getUserPrincipcal` for authentication
        - `isUserInRole` for authorization
    - Use filters to separate security from servlet logic
    - `SecurityContext` (new since JEE8)
        - `authenticate` triggers or continues authentication check
        - `getCallerPrincipcal` for authentication
        - `isCallerInRole` for authorization

???trainer
with `curl`: add `--user username:password` to automatically add auth header
This displays mostly a hybrid approach, which still requires an authentication mechanism and an identity store.
Full manual is also possible if you forego any annotation or xml

???t

---

## Pluggable security

- Requires JASPI(C): Java Authentication Service Provider Interface for Containers
- Relies less on application server
- `HttpAuthenticationMechanism` implementation
    - Defaults
        - `@BasicAuthenticationMechanismDefinition`
        - `@FormAuthenticationMechanismDefinition`
        - `@CustomFormAuthenticationMechanismDefinition`
    - DIY: provide exactly one CDI bean implementing the interface
        - AutoApplySession
        - RememberMe
        - LoginToContinue
- `IdentityStore` implementation (or `RememberMeIdentityStore`)
    - Defaults
        - `@LdapIdentityStoreDefinition`
        - `@DatabaseIdentityStoreDefinition`
            - requires `@Dependent` implementation of `PasswordHash`
    - DIY: same as auth mechanism

???trainer
In WILDFLY, you need to set the default security domain from the UNDERTOW (web) subsystem to "jaspitest" for the custom methods to work!
https://blogs.nologin.es/rickyepoderi/index.php?/archives/183-JavaEE-8-Security-API-in-Wildfly.html

???t

