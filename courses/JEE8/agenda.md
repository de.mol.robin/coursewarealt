# Agenda

---

???invisible
Ideas

- Hello world (servlet with security concepts)
- Number guessing game (servlet + EJB)
    - Guessing game revisited with CDI/JAXRS
- Inetum merch shop (JPA + JSF + CDI + JBV)
- Todolist (or some other CRUD API) (JAXRS + JPA)
- Small time stuff (JMS, Batch)


`docker run --detach --name pg -p 5432:5432 -v pgdata:/var/lib/postgresql/data -e POSTGRES_PASSWORD=acaddemicts -e POSTGRES_USER=acaddemicts postgres:alpine`

???i

## Mini-projects

1. merch shop
    - JPA
        - Start as JavaSE
        - Add services (EJB) -> write tests
    - JSF
        - Transition to application server
            - make services into EJBs
        - Add JBV?
2. todo list (modern take, try to avoid EJBs)
    - JDBC
    - JAX-RS
        - Heavily use CDI + `@Transactional`
3. extras...
    - Batch
    - JMS

???trainer

- first cup of JEE?
- tutorials from eclipse?

- Todo list
    - create items that can be marked as done
    - group them in lists
    - expose functionality as a "RESTful" API
- Merch webshop
    - several products
        - name, price, manufacturer...
    - customers
        - email, shipping address...
    - orders
        - who buys what, payment status...

???t

---

<img src="images/schema.png#no-border" style="margin-left: 1em; height: 80vh"/>

???trainer

TODO: provide a better indication of the relations between the entities, perhaps some logical examples? (person A places order G for merch X, Y and Z...)

TODO: manufacturers can be removed...

???t
