# Jakarta Enterprise Beans (EJB)

---

## Rundown

- Since JEE 1
- Separate + automatically enrich business logic
    - Achieved through proxy classes, managed by the EJB container
        - Obtain by JNDI lookup (or injection), _never_ write `new Foo()`!
- Key features
    - The EJB container
    - EJBs and their views
    - Message-driven beans (JMS interaction)
    - Persistence (later: Jakarta Persistence)
    - Interceptors (later: Jakarta Interceptors)
    - Transactions (later: Jakarta Transactions)
    - Asynchronicity (later: Jakarta Transactions)
    - Security management
    - Timer service

???trainer
As you can see, EJB is a pretty large spec, but it has been redesigned a lot through time
???t

---

## History

- Original spec was cumbersome and hard to use
    - No annotations, everything in xml (`META-INF/ejb-jar.xml`)
    - Bean class + "views": Remote interface + Local interface + No interface
    - No injection, pure JNDI lookup
        - This means errors often only became apparent at runtime, rather than at compile time
    - Separate packaging (business module, JAR)
        - Small changes (bugfixes) would propagate across many apps, cause a redeploy wave
    - Maybe a bit too ambitious, it tried to do it all
        - Precursor for later specs (Dependency Injection, JPA, Interceptors, ...)
- Modern EJB(-lite) spec is more streamlined and relatively easy to use
    - Annotation driven
    - No extra interfaces (but "views" still possible with annotations)
    - Injection support
    - Bundled deployment (inside WAR) possible

---

## Types of EJBs

- Session beans (do NOT confuse this with HTTP sessions!!)
    - `@Stateless`
      - Pooled instances, reused by multiple clients
      - **Should not contain state**
    - `@Stateful`
      - Single bean instance per reference (not shared): safe to keep state
      - Can be "passivated" and "activated" (= serialized to disk to free memory)
      - Clear up resources in `@Remove`, define timeout in `@StatefulTimeout`
    - `@Singleton`
      - The container will make sure that only one instance exists for the entire app
      - Concurrency management through `@Lock` and `@AccessTimeout` (or bean-managed)
      - Hot start with `@Startup`, startup order through `@DependsOn` (other singletons)
- `@MessageDriven` (MDBs)
    - Supports _receiving_ (async) messages using JMS
    - Implement `MessageListener`
    - Customize activation with `@ActivationConfigProperty`

???trainer
Note that modern web services aim to be stateless, so stateful beans are hardly used.
Similarly, singleton beans are rare, and MDB are also infrequent (JMS is not popular).
Stateless bean is the most relevant.
Mention that there is no support for JMS producers, only consumers
???t

---

## EJB definition

- Definition: just an annotation (nowadays)!
```java
@Stateless
public class Foo { /* ... */ }
```
    - Resulting JNDI names
```java
java:global/cdbookstore/Foo
java:app/cdbookstore/Foo
java:module/Foo
...
```
- Obtaining reference

```java
Foo f = (Foo) new InitialContext().lookup("java:global/cdbookstore/Foo");
```
```java
@Resource(lookup = "java:global/cdbookstore/Foo") Foo f;
```
```java
@EJB Foo f; // EJB-style injection
```
```java
@Inject Foo f; // CDI-style injection
```

???trainer
Do this in live coding:
make "NameCleaner" EJB (stateless)
don't inject yet, just deploy to show JNDI names
inject using @Resource + JNDI
replace inject with @EJB
???t

---

## EJB lifecycle hooks

<div style="float:right; display: flex; flex-direction: column; align-items: flex-end">
    <label>Stateless/Singleton EJB</label>
    <img src="images/stateless-singleton-ejb-lifecycle.png" style="height: 7em"/>
    <label>Stateful EJB</label>
    <img src="images/stateful-ejb-lifecycle.png" style="height: 18em"/>
</div>
- `@PostConstruct`
- `@PreDestroy`
- `@PostActivate`
- `@PrePassivate`
- `@AroundTimeout`
- see Jakarta Interceptors

???trainer
the point of the slide is to introduce the lifecycle callbacks
implement this in code to show
add post construct to name cleaner ejb
add sleep to business logic method
do 2 curl commands at the same time
observe 2 beans being created

The interceptor is a managed bean itself and its life cycle matches that of target bean.
???t

---

## EJB views (legacy)

- `@Remote`
    - EJB may run on different server than application client
    - Scalable but introduces network latency
    - Requires explicit interface defining exposed methods
- `@Local`
    - Only accessible from within same module (tighter coupling)
    - Increased performance
    - Requires explicit interface defining exposed methods
- "no-interface" view
    - Like local, but without interface

```java
@Remote
public interface Foo { }

@Stateless
public class FooBean implements Foo { }
```
```java
@EJB Foo fooRemote;
@EJB FooBean fooNoInterface;
```

???trainer
Remote EJB is actually a very strong feature, it allows you to do remote method invocation as if the bean was local.
On the client, you just inject a bean and do calls on it, and JEE will translate that to a network query behind the scenes, using the EJB container on a remote machine (with all its benefits of transactions, security and so on).
Originally, ALL EJB used the remote protocol, even if the EJB container was on the same server as the client.

Local EJB were introduced later to simplify things, producing a simpler flow at the cost that the EJB container had to be on the same server as the client.
But since this was quite often the case, there was virtually no price to pay in those cases so the transition was natural.

Both remote and local views require that a Java (business) interface is created, specifying the available contract to the client of the bean, which the bean must implement.
Either the interface specifies its view itself, or the bean explicitly makes the association.

Even later, the no-interface view was added as a convenience to make local views simpler to use.
It allows you to inject the bean directly, without having to specify an explicit, local interface.

REMOTE EJB ARE RARELY USED ANYMORE, WHY?
crazily tight coupling (any change in a class requires recompilation of all clients)
ties to JEE stack (can't call from non-Java client)
...?
???t

---

## Packaging

<img src="images/jar.png" style="float:right; height:15em"/>
- Separate: in own JAR
    - Portable module for re-use in multiple applications
    - Can not be deployed on its own, should be part of EAR
    - `ejb-jar.xml` deployment descriptor
- Bundled: embedded in other archive (WAR)
    - descriptor not required
        - allowed at `WEB-INF/ejb-jar.xml`

---

## EJBContext

- Allows programmatic access to the context maintained by the container
- Has subclasses per "type" of EJB:
  - `SessionContext`
  - `MessageDrivenContext`

```java
@Stateless
public class Foo {
*   @Resource private EJBContext context;
}
```
- Provides programmatic access to:
    - current user (security)
    - current transaction
    - timer service
    - JNDI lookups
    - async context (check if was cancelled)
    - ...

---

## Security

- Authentication
    - Relies on (other specs in) platform to determine principal
- Authorization
    - Annotation driven
        - `@RolesAllowed(...)`
        - `@PermitAll`
        - `@DenyAll`

---

## Exception handling

- System exception
    - EJB container error
    - (subclasses of) `java.rmi.RemoteException` and `RuntimeException`
    - Caught, wrapped (`javax.ejb.EJBException`) and rethrown to client
    - Automatically logs stacktrace (!)
    - Bean causing exception will be marked for removal
        - except for singletons
- Application exception
    - "business logic error"
    - Checked exceptions
    - Unchecked exceptions with `@ApplicationException` annotation
    - Thrown to client directly (no wrapping)

---

## Transactions

- Started as part of EJB, is now own spec (Jakarta Transactions)
- There are two modes of operation:
    - Container Managed Transactions (CMT)
        - Default
        - Uses `@TransactionAttribute`
    - Bean Managed Transactions (BMT)
        - Allows programmatic access using `UserTransaction` resource
- Choosing modes of operation using `@TransactionManagement`

| `TransactionAttributeType.`       | Meaning
|-
| `REQUIRED` (default)              | Method must be invoked within TX. Container creates one only if necessary.
| `REQUIRES_NEW`                    | Always creates new TX. Existing one is paused and not atomic with the new one.
| `MANDATORY`                       | Method requires TX, but will not be created (e.g. must participate in existing one)
| `SUPPORTS`                        | Method doesn't care about TX (works either with or without one)
| `NOT_SUPPORTED`                   | Method must not run in TX. Pause existing one and resume later.
| `NEVER`                           | Method must not run in a TX. Throw exception if one exists.

---

### Transaction Propagation Levels

- `createBook()` acts as client, `addItem()` has a _propagation level_ set

![](images/transactions-propagation-levels.png#no-border)

---

### Transaction rollback

- Always for system exceptions
    - (subclasses of) `java.rmi.RemoteException`
    - (subclasses of) `RuntimeException`
- Never for application exceptions
    - Checked exceptions
    - Unchecked exceptions with `@ApplicationException` annotation
        - Unless `@ApplicationException(rollback=true)`
- Manually triggering rollback
    - CMT
        - Call `setRollbackOnly` on `EJBContext`
    - BMT
        - Call `rollback` on `UserTransaction`

---

## Asynchronous execution

- For time-consuming "fire-and-forget" methods
- Return `void` or `Future<V>`

```java
@Stateless
public class OrderBean {

  @Asynchronous
  public void sendEmailOrderComplete(Order order, Customer customer) {
    // make and send mail
  }

  @Asynchronous
  public Future<Boolean> printOrder(Order order) {
    // make pdf from order, send to printer
    return new AsyncResult<>(printSuccesful);
  }
}
```
- Do **NOT** make your own threads!!

---

## Timers/scheduling

- Scheduled method execution:
    - according to calendar-based schedule (cron-line)
    - at a specific time
    - after certain time has elapsed
    - at specific intervals
- Persistent (survives JVM shutdown) or non-persistent
- For all EJBs except `@Stateful`
- Usage
    - Manual
        - Obtain `TimerService` to create a timer (e.g.: `@Resource TimerService`)
        - Annotate callback with `@TimeOut` (return `void`, can take `Timer` as parameter)
    - Automatic
        - `@Schedule`

???trainer
could also access timer service through JNDI lookup or through session context
persistent timers are called on startup if they expire while the server is down
???t

---

## Summary

- Standalone: `ejb-jar.xml` descriptor
- Embedded: no descriptor!
- Most used EJBs:
    - `@Stateless`: plain service
    - `@Singleton`: locking and startup behavior
- Benefits:
    - Security
    - Transactionality
    - Asynchronous
- Injection through `@EJB`