# JEE Platforms

---

## Overview

- Application servers
    - Curated collection of libraries and frameworks
        - Specific versions
        - Largely preconfigured
        - Usually allows for deep customisation
    - Full JEE (JBoss...) or web container only (Tomcat, Jetty...)
    - Must be installed and configured in advance
        - Requires totally different knowledge than JEE development
    - Originally designed to host multiple applications
- "Cloud-native"
    - Create a bootable jar (or even comile natively with GraalVM)
    - Configuration is done in code and runtime is bundled inside artifact
- Most used among our customers (q4 2021):
    - JBoss EAP 6-7
    - WebLogic

---

## Java Naming and Directory Interface (JNDI)

- Binds objects in JVM to names (String)
    - Grouped in namespaces (aka contexts)
        - Local: `java:`
        - JEE: `java:comp`, `java:module`, `java:app`, `java:global`
- Facilitates access to objects through lookup
- In JEE
    - Resources
        - Data sources (database connection pool)
        - Messaging services (JMS)
        - Mail sessions
        - URLs
    - EJBs
- JNDI objects are "managed" by the application server
    - Containers take care of creation and destruction of instance(s)

???trainer

Different for each platform, but generally speaking, all platforms offer resources
Actually Java SE technology!
some application servers add their own JNDI namespaces, like wildfly adds java:jboss

???t

---

## Obtaining a reference to a JNDI object

- Injection (!)
    - on fields or method parameters
    - only into managed objects (!)
        - Servlets
        - EJBs
        - CDI beans

```java
@Resource(lookup = "java:jboss/datasources/ExampleDS")
DataSource db;
```
- Manual lookup also possible
    - works anywhere

```java
DataSource db = (DataSource) new InitialContext().lookup("java:jboss/datasources/ExampleDS");
```

???trainer
Injection and managed objects are very important concepts!
???t
