# Jakarta Mail

---

## Rundown

- Revolves around `javax.mail.Session`
    - Typically defined in the platform but can also be bundled using `@MailSessionDefinition`
    - Injected as `@Resource`

```java
@Resource(name = "java:/mail/outlook")
Session session;

public void sendMail() {
    MimeMessage email = new MimeMessage(session);
    email.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
    email.setSubject("[JEE greeter] activity alert");
    email.setText(greeting);
    Transport.send(email);
}
```

???trainer
https://eclipse-ee4j.github.io/mail/

???t