# Jakarta Servlet

---

## Rundown

- Since JEE 1
- Quite low-level
- Request-response model
- Key features:
    - Web container
    - Servlets themselves
    - Filters
    - Events (+ listeners)
    - Security

???trainer

<properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
</properties>

<dependencies>
    <dependency>
        <groupId>jakarta.platform</groupId>
        <artifactId>jakarta.jakartaee-api</artifactId>
        <version>8.0.0</version>
        <scope>provided</scope>
    </dependency>
</dependencies>

<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <login-config>
        <auth-method>BASIC</auth-method>
    </login-config>
</web-app>

???t

---

## Web Container

- Provides network services
    - Implements HTTP (and possible other protocols)
        - Holds HTTP sessions
    - Creates new threads for concurrent requests
- Built to allow developers to plug in "servlets"
    - Maps URLs to servlets
    - Manages servlet lifecycle
- Standalone possible, but typically embedded in application server

<img src="images/web-lifecycle.png#no-border" style="height: 13em"/>

???trainer
before we start coding, first introduce the servlet/web container

Explain the servlet lifecycle:
Precondition: request is mapped to a servlet
If an instance of the servlet does not exist, the web container:
Loads the servlet class
Creates an instance of the servlet
Initializes the servlet calling `init`
Possible to specify init params
Invokes the `service` method, passing request and response objects
Container may destroy the servlet later
calls `destroy`
???t

---

## The `HttpServlet` class

- Offers Java hooks for HTTP request methods
- Direct access to request and response objects

```java
package com.inetum.realdolmen;

import javax.servlet.http.HttpServlet;

public class MyServlet extends HttpServlet { /*...*/ }
```

| HTTP Method | Java Method
|-
| `GET`       | `doGet(req, res)`
| `POST`      | `doPost(req, res)`
| `PUT`       | `doPut(req, res)`
| `DELETE`    | `doDelete(req, res)`
| `OPTIONS`   | `doOptions(req, res)`
| `HEAD`      | `doHead(req, res)`
| `TRACE`     | `doTrace(req, res)`
| `*`         | `service(req, res)`

???trainer
Only servlet subclass that is provided out of the box
Most commonly used servlet (by far)
Live coding: attention to request and response objects
start with simple example
add parameter
???t

---

## Meta-data (legacy)

- Deployment descriptor: `WEB-INF/web.xml`
    - Maven: `src/main/webapp/WEB-INF/web.xml`

```xml
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <servlet>
        <servlet-class>com.inetum.realdolmen.MyServlet</servlet-class>
        <servlet-name>myServlet</servlet-name>
    </servlet>
    <servlet-mapping>
        <servlet-name>myServlet</servlet-name>
        <url-pattern>/myServlet/*</url-pattern>
    </servlet-mapping>
</web-app>
```

- Still used (and useful)
- [XSD locator](http://www.oracle.com/webfolder/technetwork/jsc/xml/ns/javaee/index.html)

???trainer
when is this useful?
when we add a servlet from a library (e.g. faces servlet)
when we want to add stuff we can't with annotations (yet), like filter order
???t

---

## Meta-data (modern)

- Java annotation: `@WebServlet`

```java
package com.realdolmen;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet(urlPatterns="/myServlet/*")
public class MyServlet extends HttpServlet { /*...*/ }
```
- Behind the curtains, an "effective descriptor" is generated...

> Prefer this where possible!

???trainer
Mention that both are always possible, but that from
now on, the course will always use annotation over descriptor
Live coding example time
???t

---

## Filters

- For "cross-cutting concerns"
    - security, encryption, logging, ...
- Wraps "around" a servlet invocation:
    1. container receives request and matches URL to servlet + filter
    1. filter invoked
        - can modify request
    1. servlet invoked (_OR_ request terminated)
    1. back to filter
        - can modify response
    1. hand response to container (which will finish the request)
- `@WebFilter`
- Order: defined through web.xml

???trainer
Explain cross-cutting concerns
Implement logging filter
"servlet-only" implementation of AOP (can also be done more generically)

???t

---

## Events and listeners

- Web container emits (lifecycle) events
    - Register "web listener"
        - deployment descriptor
        - `@WebListener`
- Implement interface(s) to register for specific events
    - `ServletContextListener`
        - application started/stopped
    - `ServletRequestListener`
        - request started/ended
    - `HttpSessionListener`
        - HTTP session created/removed
    - More available (see spec)

???trainer
Live coding: showcase sessions
naive implementation with state in servlet
ask audience what they think will happen
introduce the http session ?
use the session listener
???t

---

## Security

- Role-based access system: which roles can access which URLs?

```xml
<security-role>
    <role-name>admin</role-name>
</security-role>
<security-constraint>
    <web-resource-collection>
        <web-resource-name>Protected Area</web-resource-name>
        <url-pattern>/protected/*</url-pattern>
   </web-resource-collection>
    <auth-constraint>
        <role-name>admin</role-name>
    </auth-constraint>
</security-constraint>
```
- Annotations

```java
@ServletSecurity(@HttpConstraint(
    value = ServletSecurity.EmptyRoleSemantic.DENY,
    rolesAllowed = "ADMIN"))
@WebServlet(urlPatterns = "/protected/*")
public class ProtectedServlet extends HttpServlet { ... }
```

???trainer
Do this live coding and show that that the endpoint is in fact protected.
???t

---

### Authentication

- Mechanism
    - Defines how provide/obtain credentials
    - Can only specify 1 (!!)
    - Legacy: in `web.xml`
        - BASIC
        - FORM
        - DIGEST
        - CLIENT-CERT
    - Since JEE8: in code ("Jakarta Security")
        - implement `HttpAuthenticationMechanism`
        - Some defaults are provided as annotations (`@BasicHttpAuthenticationMechanism`...)
- Credential provider
    - Legacy: must be configured in JEE platform (!!)
    - Since JEE8: code ("Jakarta Security")
        - `IdentityStore`
        - Some defaults are provided as annotations (`@JdbcIdentityStore`...)

---

### Basic authentication

<img src="images/security-web.png#no-border"/>
- ["Authorization" header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization)
- Sessionless

```xml
<login-config>
    <auth-method>BASIC</auth-method>
    <realm-name>ApplicationRealm</realm-name>
</login-config>
```
- Caller identity in servlet: ask the `HttpServletRequest`:
    - `getUserPrincipal()`: returns `Principal` representing current user

???trainer
This uses the "Authorization" header.
With `curl`: specify `--user username:password`.
There is no session associated, the credentials need to be included in every request.
Browsers will do this automatically by remembering the authorization header and including it in every subsequent request after logging in.

Other notable methods on the request
`isUserInRole(String)`: check if current user has specific role
needs to have roles declared on servlet using `@DeclareRoles`

Apparently, injecting `SecurityContext` requires JASPIC (see Soteria)
???t

---

### Programmatic security

- Manual authentication: through `HttpServletRequest`
    - `authenticate(Response)`: trigger authentication
        - use to indicate a protected resource
        - triggers auth mechanism defined in `web.xml` to collect username and password
    - `login(String user, String pass)`: attempt to log in
        - in case you already have the user credentials
    - `logout()`: reset user identity
- Manual authorization: through `HttpServletRequest`
    - `getUserPrincipal()`
        - access to the currently logged in user
    - `isUserInRole(String role)`
        - check if currently logged in user has role
- DIY
    - Have fun with filters (and sessions)
        - (You should not do this)

---

## Further topics

- Servlet finalization
    - override `destroy()`
- Uploading files
    - `@MultipartConfig`
    - `req.getParts()`
- Asynchronous processing
    - `@WebServlet(asyncSupported = true, ...)`
    - `AsyncContext ac = req.startAsync()`
    - `ac.start(new Runnable() {...});`

---

## Summary

- Core concepts:
    - Web container
    - HttpServlet
    - Filters
    - Events + listeners
    - Security (role-based, URL-patterns, declarative or programmatic)
- Deployment descriptor: `WEB-INF/web.xml`: `<web-app>`
- Old and low-level
    - Not commonly used directly but fundamental base for many recent frameworks!