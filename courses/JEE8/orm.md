# Object Relational Mapping

---

## @Table

- Changes the default values related to the table

| Attribute             | Meaning
|-
| `name`                | Changes the table name in the database (default is same as class)
| `schema`              | Sets the schema in the database
| `catalog`             | Sets the catalog in the database (feature of some RDBMSs)
| `uniqueConstraints`   | Specify additional unique constraints in the DB (use `@UniqueConstraint`)
| `indexes`             | Specity indexed columns in the DB (use `@Index`)

```java
@Entity
*@Table(name = "t_book")
public class Book {
  @Id
  private Long id;
  // More properties, constructors, getters, setters
}
```

---

## @SecondaryTable

- `@SecondaryTable` allows mapping a single entity to multiple tables
    - Uses _primary key join column_ to link all tables together
        - Customize with `@PrimarykeyJoinColumn`

```java
@Entity
// @Table(name = "t_address")
*@SecondaryTables({
* @SecondaryTable(name = "city"), @SecondaryTable(name = "country")
})
public class Address {
  @Id
  private Long id;
  
  private String street1;   // Uses "primary table"
  private String street2;
  
  @Column(table = "city") private String city;
  @Column(table = "city") private String state;
  @Column(table = "city") private String zipcode;
  @Column(table = "country") private String country;
  
  // Constructors, getters, setters
}
```

---

## @SecondaryTable

![](images/orm-secondary-table.png#no-border)

- Consider the issue of performance (e.g. lots of joins)

???trainer
When you use secondary tables, you must consider the issue of performance. Every time you access an entity, the persistence provider accesses several tables and has to join them. On the other hand, secondary tables can be a good thing when you have expensive attributes such as binary large objects (blObs) that you want to isolate in a different table.
???t

---

## @Id and @GeneratedValue

- `@Id` denotes the primary key
    - Use wrapper type (e.g. `Integer` instead of `int`, to allow `null` for unsaved values)
- `@GeneratedValue` indicates an automatically generated key
  - SEQUENCE (i.e. `Oracle`)
  - IDENTITY (i.e. `SQL Server`, `MySQL`)
  - TABLE (e.g. hi-lo table if no better alternative is available)
  - AUTO (chooses the _"best"_ option for the used DB)

```java
@Entity
public class Book {
* @Id
* @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String title;
  private Float price;
  private String description;
  private String isbn;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
}
```

---

## Composite Primary Keys

- _Composite primary keys_ are supported in two ways
    - `@IdClass`
    - `@EmbeddedId` + `@Embeddable`
- Both options require a primary key class
  - `equals()` and `hashCode()`
  - `public`
  - no-arg constructor
  - implement `Serializable` if crossing architectural layers

```java
public class NewsId {   // Represents PK
  private String title;
  private String language;
  
  // Constructors, getters, setters, equals, and hashCode
}
```

---

## @EmbeddedId and @Embeddable

- `@EmbeddedId` represents PK as composite object

```java
@Entity
public class News {
* @EmbeddedId
  private NewsId id;

  private String content;
  // Constructors, getters, setters
}
```

- Requires key class to be annotated with `@Embeddable`

```java
*@Embeddable
public class NewsId {
    // ...
}
```

```java
NewsId pk = new NewsId("Richard Wright has died on September 2008", "EN")
News news = em.find(News.class, pk);
```

---

## @IdClass

- `@IdClass` represents PK as separate fields
    - Names must match up with fields of key class

```java
@Entity
*@IdClass(NewsId.class)
public class News {
* @Id private String title;     // Same name as in NewsId
* @Id private String language;

  private String content;
  
  // Constructors, getters, setters
}
```

```java
*// no annotations
public class NewsId {
  private String title;     // Same name as in News
  private String language;
  
  // Constructors, getters, setters, equals, and hashCode
}
```



---

## @EmbeddedId vs. @IdClass

- On the database, nothing changes either way

```sql
create table NEWS (
  CONTENT VARCHAR(255),
  TITLE VARCHAR(255) not null,
  LANGUAGE VARCHAR(255) not null,
* PRIMARY KEY (TITLE, LANGUAGE)
);
```

- Difference is only on the OO side
  - `@IdClass` duplicates attributes
  - `@IdClass` does not require annotations on the primary key class
  - `@EmbeddedId` is referenced differently in JPQL

```sql
-- with @IdClass
select n.title from News n
-- with @EmbeddedId
select n.id.title from News n
```

---

## @Basic and @Lob

- `@Basic` overrides basic persistence
    - Specify `fetch` (see later) and `optional`
    - Mostly unneeded but useful for LOBs
- `@Lob` specifies a _CLOB_ <small>(`String`)</small> or _BLOB_ <small>(`byte[]`)</small>

```java
@Entity
public class Track {
  @Id @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String title;
  private Float duration;
* @Basic(fetch = FetchType.LAZY, optional=true)
* @Lob
  private byte[] wav;
  private String description;
  // Constructors, getters, setters
}
```

---

## @Column

- Defines the properties of a column
  - Specify `name`, `nullable`, `unique`, `updatable`, `insertable`, `table`, `length`, `precision` and `scale`
  - Influence the mapping or the queries

```java
@Entity
public class Book {
  @Id @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
* @Column(name = "book_title", nullable = false, updatable = false)
  private String title;
  private Float price;
* @Column(length = 2000)
  private String description;
  private String isbn;
* @Column(name = "nb_of_page", nullable = false)
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
}
```

---

## @Temporal

- Used on `Date` and `Calendar`
  - `DATE`
  - `TIME`
  - `TIMESTAMP`

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
  
* @Temporal(TemporalType.DATE)
  private Date dateOfBirth;

* @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;

  // Constructors, getters, setters
}
```

???trainer
Unfortunately in JEE7 we can't use the new JavaSE 8 data classes (i.e. `LocalDate`) since they are from Java 8. Extension libraries are available though to bridge this problem.
???t

---

## @Transient

- When you do not want the attribute mapped

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
  
  @Temporal(TemporalType.DATE)
  private Date dateOfBirth;
  
* @Transient
  private Integer age;

  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;

  // Constructors, getters, setters
}
```

---

## @Enumerated

- Are used to map enumerated types
  - `ORDINAL`
  - `STRING`

```java
public enum CreditCardType {
  VISA,
  MASTER_CARD,
  AMERICAN_EXPRESS
}
```

```java
@Entity
@Table(name = "credit_card")
public class CreditCard {
  @Id
  private String number;
  private String expiryDate;
  private Integer controlNumber;
* @Enumerated(EnumType.STRING)
  private CreditCardType creditCardType;
  // Constructors, getters, setters
}
```

---

## @Access

- Use annotations on attributes (fields) or properties (getters)
  - `FIELD`
  - `PROPERTY`

```java
@Entity
*@Access(AccessType.FIELD)
public class Customer {
  @Id @GeneratedValue
  private Long id;

* @Column(name = "phone_number", length = 15)
  private String phoneNumber;

  // More properties, constructors, getters, setters
  
* @Access(AccessType.PROPERTY)
* @Column(name = "phone_number", length = 555)
  public String getPhoneNumber() {
    return phoneNumber;
  }
  
  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }
}
```

???trainer
By default, the location of the `@Id` annotation determines where you will need to put other mapping annotations. If the `@Id` is located on the attributes, you will need to put the other annotations on the fields. If you put the `@Id` on the getters, you will need to put the other annotations on the getters. Annotations that are not in the right place will simply be ignored.

The location of the `@Id` will also be important when using embeddables or inheritance.

The `@Access` annotation can let you override the mapping location, so that you can choose to map differently than the location decided by `@Id`.
???t

---

## @Embeddable

- Strict ownership relationship (composition)
  - The embeddable's access type is the same as the owning entity's

```java
*@Embeddable
public class Address {
  private String street1;
  private String street2;
  private String city;
  private String state;
  private String zipcode;
  private String country;
  // More
}
```

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  
* @Embedded
  private Address address;
  // More
}
```

---

## @Embeddable

- Result

```sql
create table CUSTOMER (
  ID BIGINT not null,
  LASTNAME VARCHAR(255),
  PHONENUMBER	VARCHAR(255),
  EMAIL VARCHAR(255),
  FIRSTNAME VARCHAR(255),
* STREET2 VARCHAR(255),
* STREET1 VARCHAR(255),
* ZIPCODE VARCHAR(255),
* STATE VARCHAR(255),
* COUNTRY VARCHAR(255),
* CITY VARCHAR(255),
  primary key (ID)
);
```

---

## @ElementCollection and @CollectionTable

- To store a _collection_ of basic Java types or embeddables as a _composition_

```java
@Entity
public class Book {
  @Id @GeneratedValue
  private Long id;
  
  private String title;
  private Float price;
  private String description;
  private String isbn;
  private Integer nbOfPage;
  private Boolean illustrations;
  
* @ElementCollection
* @CollectionTable(name = "tag")    // optional, serves only to customize default names
  @Column(name = "value")
  private List<String> tags = new ArrayList<>();

  // Constructors, getters, setters
}
```

---

## @ElementCollection and @CollectionTable

- Creates a second table with a _primary key join column_
    - Customize table name and join column with `@CollectionTable`

![](images/orm-element-collection.png#no-border)

---

## @MapKeyColumn

- For maps containing basic types or embeddables
    - A `Map` represents a so-called _qualified association_
        - Allows reducing a _many_ to a _one_ relationship if a _key value_ is available

```java
@Entity
public class CD {
  @Id @GeneratedValue
  private Long id;
  
  private String title;
  private Float price;
  private String description;
  
  @Lob
  private byte[] cover;
  
  @ElementCollection
  @CollectionTable(name = "track")
* @MapKeyColumn(name = "position")  // Uses column 'position' in table 'track' as the key
*                                   // (see also @MapKeyEnumerated)
  @Column(name = "title")
  private Map<Integer, String> tracks = new HashMap<>();
  
  // Constructors, getters, setters
}
```

---

## @MapKeyColumn

![](images/orm-map-key-column.png#no-border)

---

## Exercise: JPA Mapping

- Add a new entity `Candy` with properties
    - `id` (primary key), `name` and `color` (enum)
- Add property `birthDate` to entity `Person`
    - Only the date portion is stored in the database
    - Must not be nullable
- Add property `age` to entity `Person`
    - Not stored in the database, but calculated from `birthDate`
- Make `firstName` and `lastName` not nullable and maximum 200 characters long
- Add `Address` to `Person` and make it embedded
- Add `List<CandyColor>` named `candyPreferences` as a collection table to `Person`

---

## Exercise: JPA Mapping

![](images/jee7-jpa-exercise-mapping.png#no-border)

---

## Relationship Mapping

- Unidirectional

![](images/orm-unidirectional.png#no-border)

- Bidirectional

![](images/orm-bidirectional.png#no-border)

---

## Relationship Mapping

- Multiplicity

![](images/orm-multiplicity.png#no-border)

- A relationship has an owner and an inverse (non-owner)

---

## Relationships in Relational Databases

- Using a join column

![](images/orm-join-column.png#no-border)

---

## Relationships in Relational Databases

- Using a join table

![](images/orm-join-table.png#no-border)

---

## Entity Relationships

- JPA maps associations to link entities to a relational model
- There are 7 possible combinations

Cardinality             | Direction
:---------------------- | :-------------
One-to-one              | Unidirectional
One-to-one              | Bidirectional
One-to-many             | Unidirectional
Many-to-one/One-to-many | Bidirectional
Many-to-one             | Unidirectional
Many-to-many            | Unidirectional
Many-to-many            | Bidirectional

---

## Unidirectional and Bidirectional

![](images/orm-unidirectional-customer-address.png#no-border)

- Unidirectional
  - `Customer` is the owner
  - The `CUSTOMER` table will have the foreign key

---

## Unidirectional and Bidirectional

![](images/orm-bidirectional-customer-address.png#no-border)

- Bidirectional
  - Both entities could be the owner
  - There is an owning and an inverse side
  - Explicitly specify the inverse using the `mappedBy` element
  - You cannot have a `mappedBy` on both sides
  - It would also not be correct to not have it on either side, as the provider would treat it as two independent unidirectional relationships

---

## Unidirectional and Bidirectional

![](images/orm-bidirectional-tables.png#no-border)

---

## @OneToOne Unidirectional

![](images/orm-onetoone-unidirectional.png#no-border)

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
* private Address address;
  // Constructors, getters, setters
}
```

---

## @OneToOne Unidirectional

```java
@Entity
*public class Address {
  @Id @GeneratedValue
  private Long id;
  private String street1;
  private String street2;
  private String city;
  private String state;
  private String zipcode;
  private String country;
  // Constructors, getters, setters
}
```

---

## @OneToOne and @JoinColumn

- To modify attributes of the association
  - Specify `targetEntity`, `cascade`, `fetch`, `optional`, `mappedBy`, and `orphanRemoval`
- `@JoinColumn` lets you customize the join column
  - Similar to `@Column`

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
* @OneToOne (fetch = FetchType.LAZY)
* @JoinColumn(name = "add_fk", nullable = false)
  private Address address;
  // Constructors, getters, setters
}
```

---

## @OneToMany Unidirectional

![](images/orm-onetomany-unidirectional.png#no-border)

```java
@Entity
public class Order {
  @Id @GeneratedValue
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;
* private List<OrderLine> orderLines;
  // Constructors, getters, setters
}
```

---

## @OneToMany Unidirectional

```java
@Entity
*@Table(name = "order_line")
public class OrderLine {
  @Id @GeneratedValue
  private Long id;
  private String item;
  private Double unitPrice;
  private Integer quantity;
  // Constructors, getters, setters
}
```

---

## @OneToMany

- Uses a join table by default

![](images/orm-onetomany-jointable.png#no-border)

---

## @JoinTable

- Lets you redefine the defaults for the join table
  - Specify `name`, `catalog`, `schema`, `joinColumns`, `inverseJoinColumns`, `uniqueConstraints` and `indexes`

```java
@Entity
public class Order {
  @Id @GeneratedValue
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;
  @OneToMany
* @JoinTable(name = "jnd_ord_line",
* joinColumns= @JoinColumn(name = "order_fk"),
* inverseJoinColumns= @JoinColumn(name = "order_line_fk") )
  private List<OrderLine> orderLines;
  // Constructors, getters, setters
}
```

---

## @OneToMany and @JoinColumn

- Can be combined to use foreign keys instead of join tables

```java
@Entity
public class Order {
  @Id @GeneratedValue
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;
  @OneToMany(fetch = FetchType.EAGER)
* @JoinColumn(name = "order_fk")
  private List<OrderLine> orderLines;
  // Constructors, getters, setters
}
```

![](images/orm-onetomany-joincolumn.png#no-border)

---


## @ManyToMany Bidirectional

- Uses a join table
  - You need to explicitly define the owner and the inverse using mappedBy

```java
@Entity
public class CD { // the inverse side
  @Id @GeneratedValue
  private Long id;
  private String title;
  private Float price;
  private String description;
* @ManyToMany(mappedBy = "appearsOnCDs")
  private List<Artist> createdByArtists;
  // Constructors, getters, setters
}
```

---

## @ManyToMany Bidirectional

- Customize the mapping on the owning side

```java
@Entity
public class Artist { // the owning side
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
* @ManyToMany
* @JoinTable(name = "jnd_art_cd",
* joinColumns = @JoinColumn(name = "artist_fk"),
* inverseJoinColumns = @JoinColumn(name = "cd_fk"))
  private List<CD> appearsOnCDs;
  // Constructors, getters, setters
}
```

---

## @ManyToMany Bidirectional

![](images/orm-manytomany-jointable.png#no-border)

---

## Fetching Relationships

- Use the `fetch` attribute
  - Can cause performance issues if misused!

![](images/orm-fetching-eager.png#no-border)

![](images/orm-fetching-lazy.png#no-border)

Annotation  | Default Fetching Strategy
:---------- | :------------------------
@OneToOne   | EAGER
@ManyToOne  | EAGER
@OneToMany  | LAZY
@ManyToMany | LAZY

---

## Fetching Relationships

- Change the default only when it is more efficient

```java
@Entity
public class Order {
  @Id @GeneratedValue
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;
* @OneToMany(fetch = FetchType.EAGER)
  private List<OrderLine> orderLines;
  // Constructors, getters, setters
}
```

---

## @OrderBy

- Orders the collection when you retrieve the association

```java
@Entity
public class Comment {
  @Id @GeneratedValue
  private Long id;
  private String nickname;
  private String content;
  private Integer note;
  @Column(name = "posted_date")
* @Temporal(TemporalType.TIMESTAMP)
  private Date postedDate;
  // Constructors, getters, setters
}
```

---

## @OrderBy

```java
@Entity
public class News {
  @Id @GeneratedValue
  private Long id;
  @Column(nullable = false)
  private String content;
  @OneToMany(fetch = FetchType.EAGER)
* @OrderBy("postedDate DESC, note ASC")
  private List<Comment> comments;
  // Constructors, getters, setters
}
```

---

## @OrderColumn

- Maintains a persistent ordering using a separate column where the index is stored

```java
@Entity
public class Comment {
  @Id @GeneratedValue
  private Long id;
  private String nickname;
  private String content;
  private Integer note;
  // Constructors, getters, setters
}
```

---

## @OrderColumn

```java
@Entity
public class News {
  @Id @GeneratedValue
  private Long id;
  @Column(nullable = false)
  private String content;
  @OneToMany(fetch = FetchType.EAGER)
* @OrderColumn(name = "posted_index")
  private List<Comment> comments;
  // Constructors, getters, setters
}
```

- Result

```sql
create table NEWS_COMMENT (
  NEWS_ID BIGINT not null,
  COMMENTS_ID BIGINT not null,
* POSTED_INDEX INTEGER
);
```

---

## Exercise: JPA Associations

- Create a new entity `Order` (use table name `candy_order`) with
    - An auto-generated `id` (long)
    - An embedded `Address` with name `deliveryAddress`
- Create a new entity `OrderLine` (use table name `order_line`) with
    - An auto-generated `id` (long)
    - A property `quantity` (int)
- Add entity associations between them as follows
    - Add a _bidirectional one-to-many_ association between `Person` and `Order`
    - Add a _unidirectional one-to-many_ association between `Order` and `OrderLine`
        - `OrderLine` doesn't know its `Orders`
    - Add a _unidirectional many-to-one_ association between `OrderLine` and `Candy`
        - `Candy` doesn't know its `OrderLines`
- Add method `calculateSubTotal()` to `OrderLine` <small>(`price * quantity`)</small>
- Add method `calculateTotal()` to `Order` <small>(`sum of all subtotals`)</small>

---

## Exercise: JPA Associations

![](images/jee7-jpa-exercise-associations.png#no-border)

---

## @Inheritance

- Inheritance is a completely unknown concept and not natively implemented in a relational world
- `@Inheritance` on the root entity decides the strategy
  - Single-table-per-class hierarchy strategy
  - Joined-subclass strategy
  - Table-per-concrete-class strategy (optional support in JPA 2.1)
- Select the strategy using the `InheritanceType` enum
  - Strategies can be overridden by subclasses

---

## Inheritance Hierarchy

![](images/orm-inheritance-hierarchy.png#no-border)

---

## Single-Table-per-Class Hierarchy Strategy

```java
@Entity
*// @Inheritance // default, optional
public class Item {
  @Id @GeneratedValue
  protected Long id;
  protected String title;
  protected Float price;
  protected String description;
  // Constructors, getters, setters
}
```

```java
@Entity
*public class Book extends Item {
  private String isbn;
  private String publisher;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
}
```

---

## Single-Table-per-Class Hierarchy Strategy

```java
@Entity
*public class CD extends Item {
  private String musicCompany;
  private Integer numberOfCDs;
  private Float totalDuration;
  private String genre;
  // Constructors, getters, setters
}
```

---

## Single-Table-per-Class Hierarchy Table Structure

![](images/orm-singletableperclass-table.png#no-border)

- Adding new entities involves adding new columns
- Columns of the child entities must be `nullable`

???trainer
Notice the `DTYPE` column.
???t

---

## @DiscriminatorColumn and @DiscriminatorValue

```java
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
*@DiscriminatorColumn(name = "disc", discriminatorType = DiscriminatorType.CHAR)
*@DiscriminatorValue("I")
public class Item {
  @Id @GeneratedValue
  protected Long id;
  protected String title;
  protected Float price;
  protected String description;
  // Constructors, getters, setters
}
```

```java
@Entity
*@DiscriminatorValue("B")
public class Book extends Item {
  private String isbn;
  private String publisher;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
}
```

---

## @DiscriminatorColumn and @DiscriminatorValue

```java
@Entity
*@DiscriminatorValue("C")
public class CD extends Item {
  private String musicCompany;
  private Integer numberOfCDs;
  private Float totalDuration;
  private String genre;
  // Constructors, getters, setters
}
```

---

## Joined-Subclass Strategy and Table Structure

```java
@Entity
*@Inheritance(strategy = InheritanceType.JOINED)
public class Item {
  @Id @GeneratedValue
  protected Long id;
  protected String title;
  protected Float price;
  protected String description;
  // Constructors, getters, setters
}
```

![](images/orm-joinedsubclass-table.png#no-border)

- The deeper the hierarchy, the more joins, the poorer the performance

---

## Table-per-Concrete-Class Strategy and Table Structure

```java
@Entity
*@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Item {
  @Id @GeneratedValue
  protected Long id;
  protected String title;
  protected Float price;
  protected String description;
  // Constructors, getters, setters
}
```

![](images/orm-tableperconcreteclass-table.png#no-border)

- Polymorphic queries require the `UNION` operation, which is expensive with large amounts of data

---

## @AttributeOverrides

- To override column mapping from the parent
- To override column mapping from embeddables

```java
@Entity
*@AttributeOverrides({
* @AttributeOverride(name = "id",
* column = @Column(name = "book_id")),
* @AttributeOverride(name = "title",
* column = @Column(name = "book_title")),
* @AttributeOverride(name = "description",
* column = @Column(name = "book_description"))
*})
public class Book extends Item {
  private String isbn;
  private String publisher;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
}
```

---

## Type of Classes in the Inheritance Hierarchy

- Abstract Entity
  - Provides a common data structure for its leaf entities and follows the mapping strategies
  - Cannot be instantiated
- Nonentity
  - Transient classes
  - Are not managed by the persistence provider
  - Attributes of parent nonentity classes are not persisted
- Mapped Superclass
  - Provide persistent properties to subclasses
  - Not managed by the persistence provider
  - Similar to embeddable classes, but used with inheritance

---

## @MappedSuperclass

```java
*@MappedSuperclass
public class Item {
  @Id @GeneratedValue
  protected Long id;
* @Column(length = 50, nullable = false)
  protected String title;
  protected Float price;
* @Column(length = 2000)
  protected String description;
  // Constructors, getters, setters
}
```

---
## @MappedSuperclass

```java
@Entity
*public class Book extends Item {
  private String isbn;
  private String publisher;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
}
```

- Result

```sql
create table BOOK (
* ID BIGINT not null,
* TITLE VARCHAR(50) not null,
* PRICE DOUBLE(52, 0),
* DESCRIPTION VARCHAR(2000),
  ILLUSTRATIONS SMALLINT,
  ISBN VARCHAR(255),
  NBOFPAGE INTEGER,
  PUBLISHER VARCHAR(255),
  primary key (ID)
);
```

---

## Exercise: JPA Inheritance

- Turn `Candy` into a hierarchy of classes
    - Make `Candy` abstract
    - Create a subclass `MnM` <small>(M&M's)</small> with property `nuts (boolean)`
    - Create a subclass `ChocolateBar` with property `length (int)`
    - Create a subclass `GummyBears` with property `flavour (String)`
- Make sure the hierarchy is mapped to the database
    - Try out both `single-table` and `joined`
    - Add a _discriminator column_ (`candy_type`) and values for each concrete subclass
    - `OrderLine` should be able to hold a _polymorphic_ list of `Candy`
- Add a common mapped superclass to `Person` and `Candy` with name `Tracked` with property `userCreated (String)`
    - This should not be able to support _polymorphic queries_

---

## Exercise: JPA Inheritance

![](images/jee7-jpa-exercise-inheritance.png#no-border)
