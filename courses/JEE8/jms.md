# Jakarta Messaging (JMS)

---

## Rundown

<img src="images/jms-messaging-architecture.png#no-border" style="float:right"/>
- Since JEE 1
- Handles messaging
    - Loose coupling
    - Asynchronous
    - Reliable
    - High performance
- Supports transactions
- Key concepts
    - JMS architecture
        - Provider (provided in JEE platform)
            - Provides administered objects through JNDI
        - Client (also provided in JEE platform)
            - Messages, producers, consumers
    - Messaging styles
        - Point-to-point (queues)
        - Publish/subscribe (topics)

???trainer
- Formerly Java Messaging Service (JMS) is the Java EE API that handles messaging
- JMS and MDB are not part of the Web Profile specifications

???t

---

## Point-to-Point

- The message travels from a single producer to a single consumer
  - A queue retains the messages until they are consumed
  - Sender and receiver can produce or consume the messages whenever they like

<img src="images/jms-ptp.png#no-border" style="height:15em"/>

- P2P does not guarantee any particular order in delivery

---

## Publish-Subscribe

- A single message is sent by a single producer to potentially several consumers
  - The topic retains the messages until they are distributed to all subscribers
  - Subscribers do no receive messages prior to their subscription

<img src="images/jms-ps.png#no-border" style="height:17em"/>

---

## Receiving from Topic

- Create subscription to topic, consume from subscription
- Subscription durability
    - Durable
        - Receives messages even when there are no consumers
        - Accumulated messages are delivered to first new consumer
    - Nondurable
        - Ends as soon as last consumer is closed
- Subscription sharing
    - Unshared
        - Single consumer
    - Shared
        - Multiple consumers allowed, but only one consumer will receive message
        - Subscription has a name

???trainer
More complex than queue.
A durable subscription persists its messages, comes with resource cost.
Other methods:
- `createSharedConsumer(Topic, Name)`
- `createSharedDurableConsumer(Topic, Name)`

- Default `createConsumer(Topic)` subscription is nondurable and unshared
    - use `createDurableConsumer(Topic, Name)` to create durable subscription
    - use `consumer.close()` to stop receiving
    - use `context.unsubscribe(Name)` to stop subscription

???t

---

## Programming model

<img src="images/jms-model.png#no-border" style="height:26em"/>

???trainer
Explain how JMSContext replaces Connection + Session in an attempt to make JMS 2.0 simpler than the original spec

???t

---

## Administered Objects

- Defined in JMS provider (not in application code)
- Available through JNDI
- Connection Factories
    - Used by clients to make a connection to a destination
    - `ConnectionFactory`
        - `QueueConnectionFactory`
        - `TopicConnectionFactory`
- Destinations
    - Message distribution points that receive, hold and distribute messages
    - `Destination`
        - `Queue`
        - `Topic`

```java
@Resource(lookup = "java:comp/DefaultJMSConnectionFactory")
private ConnectionFactory connectionFactory;

@Resource(lookup = "jms/MyQueue")
private Queue queue;
```

---

## JMSContext

- Entrypoint for creating producers, consumers and messages
- Obtaining a JMSContext
    - Created by a `ConnectionFactory`'s `createContext()` method
        - Specify session mode, username, password
        - `close` after use (or use try-with-resources)
    - With CDI enabled, the container can also `@Inject` the `JMSContext`
        - Container manages lifecycle (no need to create or close the `JMSContext`)
        - Use `@JMSConnectionFactory` to choose non-default connection factory
        - Use `@JMSPasswordCredential` can be used to specify a user name and password

```java
public class JMSExample {
    @Inject
    @JMSConnectionFactory("jms/InetumConnectionFactory") // optional JNDI lookup name
    @JMSPasswordCredential(userName="admin",password="mypassword") // if required
    private JMSContext context;
}
```

---

## Messages

<img src="images/jms-messages.png" style="height: 13em"/>

- Only header is required part
    - Most headers are automatically set
        - Values are influenced by context, producer...
    - Some headers need to be set manually
        - `JMSCorrelationID`
        - `JMSReplyTo`
        - `JMSType`

???trainer
Headers and properties are the same, only that header uses predefined, fixed names.

???t

---

### Types of Messages

- The body of a message is optional and contains the data to send or receive

|Interface     | Description
|-
|TextMessage   | String
|MapMessage    | Set of name-value pairs where names are strings and values are Java primitive types
|ObjectMessage | Serializable object or a collection of serializable objects
|StreamMessage | Stream of Java primitive values. It is filled and read sequentially.
|BytesMessage  | Stream of bytes
|Message       | No body
- When a message is received, its body is read-only
- Depending on the message, you have different methods to access its content

```java
textMessage.setText("This is a text message");
textMessage.getText();
bytesMessage.readByte();
objectMessage.getObject();
```

???trainer
- Since JMS 2.0, a new method `<T> T getBody(Class<T> c)` returns the message body as an object of specified types

???t

---

### Messages example

```java
TextMessage message = context.createTextMessage();
message.setText("Hello World!");
context.createProducer().send(destination, message); // destination is a Queue or Topic
```
```java
Message m = consumer.receive();
if (m instanceOf TextMessage) {
    String txt = m.getBody(String.class);
    // ...
}
```
- Shortcuts (since JMS 2.0) exist
    - For every message type except StreamMessage

```java
context.createProducer().send(destination, "Hello World!");
```
```java
consumer.receiveBody(String.class);
```

---

## JMSProducer and JMSConsumer

- `JMSProducer`
    - Sending messages of all types
    - Has properties that will be copied into message headers
        - `JMSReplyTo`, `Priority`, `TimeToLive`...
    - Lightweight, can be created for each message
    - Choose destination in `send` method
- `JMSConsumer`
    - Receiving messages (both queues and topics)
    - Choose destination in `create` method
    - Can use message selector to filter messages
        - Filter on headers and properties, not body
        - SQL92-like syntax
    - Receives synchronously or asynchronously
        - Synchronous uses `receive(timeout)` (set 0 or empty for indefinitely)
        - Asynchronous requires a `MessageListener`, `onMessage()` is called
            - Should not throw exceptions (including unchecked ones)

???trainer
- With EJB, the Message-Driven Bean (MDB) is an async consumer

???t

---

## Reliability

- JMS is usually chosen for its reliability
    - Messages are by default `PERSISTENT`
    - Message delivery must be acknowledged
    - Transaction-aware
- Reliability features can be traded for performance
    - Allow messages to expire
    - Send messages asynchronously
    - Allow unacknowledged messages
- Other interesting features
    - Message priority
        - Influence order of delivery
    - Delayed sending
    - Message filtering

---

## Writing Message-Driven Beans

- Advantages over standalone consumers
    - The container manages multithreading, security and transactions
    - MDB instances can be managed in a pool
- Class requirements
    - Implement the `javax.jms.MessageListener` interface
    - Annotate with `@javax.ejb.MessageDriven`
        - Use `@ActivationConfigProperty` to specify destination and do further customization
    - Be defined as public, and must not be final or abstract
    - Have a public no-arg constructor
    - Do not define the `finalize()` method

```java
*@MessageDriven(activationConfig = {
  @ActivationConfigProperty(
    propertyName="destinationType",
    propertyValue="javax.jms.Topic"
})
public class BillingMDB implements MessageListener {
* public void onMessage(Message message) {
    System.out.println("Message received: " + message.getBody(String.class));
  }
}
```

---

## @ActivationConfigProperty

```java
@MessageDriven(activationConfig = {
  @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
  @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "orderAmount < 3000")
})
public class BillingMDB implements MessageListener {...}
```

Property                | Description
:---------------------- | :----------------------------------------------------------------------
destinationType         | The destination type, which can be `TOPIC` or `QUEUE`
destinationLookup       | The lookup name of an administratively-defined Queue or Topic
acknowledgeMode         | The acknowledgment mode (default is `AUTO_ACKNOWLEDGE`)
messageSelector         | The message selector string used by the MDB
connectionFactoryLookup | The lookup name of an administratively defined ConnectionFactory
subscriptionDurability  | The subscription durability (default is `NON_DURABLE`)
subscriptionName        | The subscription name of the consumer
clientId                | Client identifier that will be used when connecting to the JMS provider

---

## MDB Context

- Extends `EJBContext` (but adds no extra methods)
- You can inject a `MessageDrivenContext` into your MDB
  - Roll back transaction
  - Get the user principal
  - Access the timer service

---

## Handling Exceptions

- `JMSException`
  - Checked exception
  - Does not cause automatic rollbacks
- `JMSRuntimeException`
  - Unchecked exception
  - Causes the container to rollback
- If you need a rollback in case of a `JMSException`, you need to do so explicitly

```java
public void onMessage(Message message) {
  try {
    System.out.println("Message received: " + message.getBody(String.class));
  } catch (JMSException e) {
*   messageDrivenContext.setRollBackOnly();
  }
}
```

