package world.inetum.realdolmen.jcc;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.*;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destinationType",
                propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(
                propertyName = "destinationLookup",
                propertyValue = "java:global/jms/topic/welcome"),
})
public class GreetingClient implements MessageListener {

    @Resource(lookup = "java:comp/DefaultJMSConnectionFactory")
    ConnectionFactory connectionFactory;

    @Resource
    MessageDrivenContext mdc;

    @Override
    public void onMessage(Message message) {
        try (JMSContext ctx = connectionFactory.createContext()) {
            String nameToGreet = message.getBody(String.class);
            ctx.createProducer().send(
                    message.getJMSReplyTo(),
                    "Hello, " + nameToGreet + "!"
            );
        } catch (JMSException e) {
            e.printStackTrace();
            mdc.setRollbackOnly();
        }
    }
}
