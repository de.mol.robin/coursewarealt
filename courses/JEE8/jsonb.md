# Jakarta JSON Binding (JSON-B)

---

## Rundown

- Marshalling between JSON and POJOs
- Very recent (JEE 8!)
    - Popular alternatives (from before JEE 8)
        - Jackson
        - GSON
        - (Some JAXB implementations even support JSON!)
- JSON example

```json
{
    "name": "The Matrix",
    "actors": [
        "Keanu Reeves",
        "Laurence Fishburne",
        "Carrie-Ann Moss"
    ],
    "year": 1999
}
```

???trainer
The JSON data format is similar to XML in that it largely carries the same semantic information as XML.
However, JSON has a much higher information density, because it has much less syntactic verbosity.
For this reason JSON has grown to be extremely popular in web applications, especially if the application is available on the internet.

One disadvantage of JSON compared to XML however, is that it does not benefit from the same tool-support or specifications such as XSLT or XSD, although this changing.

???t

---

## Defaults

- Basic types (primitives, wrappers, String, Number)
- `BigInteger`, `BigDecimal`, `URL`, `URI`, `Optional`
- Dates
    - old: Date, Calendar, TimeZone
    - new: `java.time.*`
- Untyped
    - object -> `Map<String, Object>`
    - array -> `List<Object>`
- Java class
    - public/protected no-arg constructor
    - deserialization ignores: transient, final, static
    - serialization ignores: transient, static

---

## Defaults

- Polymorphism: NOT supported!
- Enum: by `name()`
- Serialization in lexicographic order
- null
    - not serialized
    - deserialization sets null value (except for Optional types)
- private, protected, package-private: ignored

---

## Common annotations

- `@JsonbProperty`
    - specify name on field, getter (only deserialization) or setter (only serialization)
- `@JsonbCreator`
    - constructor with parameters
    - factory methods
    - both require `@JsonbProperty` on all parameters
- `@JsonbTransient`
    - ignore field
- `@JsonbNillable`
    - force explicit 'null' serialization
- `@JsonbAdapter`
    - force annotated field to use specific adapter
- `@JsonbDateFormat`/`@JsonbNumberFormat`
- `@JsonbSerializer`/`@JsonbDeserializer`

---

## Customizations

- Adapters
    - transform "unmappable" to "handlable" type
    - class should implements `JsonbAdapter`
- Serializer/Deserializer pair
    - low-level access to JSON-P stream
- Global settings through `JsonbConfig` object
    - register custom adapters, serializers, deserializers
    - naming strategy
        - lower case with dashes, upper camel case...
    - null handling
        - true/false
    - property order
        - lexicographic, reverse, any
    - date format/locale
    - ...

???trainer
also:
- I-JSON (stricter profile)
- visibility strategy
- binary data strategy
    - byte, base_64, base_64_url

???t

---

## Usage

- Manual
```java
JsonbConfig config = new JsonbConfig();     // specify general settings here
Jsonb jsonb = JsonbBuilder.create(config);  // config param optional, use defaults otherwise
Person fred = new Person("Fred");
String json = jsonb.toJson(fred);
System.out.println(json);                   // {"name": "Fred"}
Person joe = jsonb.fromJson("{\"name\": \"joe\"}", Person.class);
System.out.println(joe.getName());          // joe
```
- Automatic
    - JAX-RS endpoints
        - `@Produces` or `@Consumes` with `MediaType.APPLICATION_JSON`

???trainer
give example with jsonbcreator

???t
