## SOAP Web Services architecture

- SOAP architecture revolves around the following triad:
    - Web Services are discovered using UDDI
    - WSDL files are exchanged to specify the protocol bindings
    - SOAP is used for message exchange

![](images/generic-webservices-overview-soap-architecture.png#no-border)

---

## SOAP Web Services architecture

- SOAP <small>(Simple Object Access Protocol)</small>
    - XML-based communication protocol
    - Message-wrapping format
- WSDL <small>(Web Services Description Language)</small>
    - An XML format that describes the service contract <small>(name, parameters, ...)</small>
    - Also used for configuration and for generating client and server code
- UDDI <small>(Universal Description, Discovery and Integration)</small>
    - An XML-based registry for businesses to list themselves on the internet
        - E.g. "Yellow pages"
    - Each business can publish a service listing
    - Designed to be queried using SOAP messages and WSDL contracts

---

## SOAP Web Services architecture

- The SOAP architecture also benefits from a wide range of _WS-*_ specifications
    - Provide services such as:
        - _Quality of service_
        - _Security_
        - _Reliability_
        - _Non repudiation_
        - _Transactions_
        - _Policy requirements_ and _governance_
- SOAP is a good match for a number of use cases where tight integration is necessary
    - Business-to-business integration
    - Integrate applications within the same company, or over the internet when run by various organizations

???trainer
Think of tight integration as situations where data needs to be interchanged within the same company, or subsidiaries.
Also consider the banking world, where a high degree of trust is required to communicate between businesses.
???t
