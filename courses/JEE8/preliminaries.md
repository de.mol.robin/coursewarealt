# Preliminaries

???trainer

Perhaps rebrand as introduction to JEE platforms ("getting to know the application server")

???t

---

## Java Naming and Directory Interface (JNDI)

- Binds objects in JVM to names (String)
    - Grouped in namespaces (aka contexts)
        - Local: `java:`
        - JEE: `java:comp`, `java:module`, `java:app`, `java:global`
    - <small>`java:global[/<app-name>]/<module-name>/<bean-name>[!fq-interface-name]`</small>
- Facilitates access to resources and managed beans
- In JEE
    - Resources
        - Data sources (database connection pool)
        - Messaging services
        - Mail sessions
        - URLs
    - EJBs
- JNDI objects are "managed" by the application server
    - Containers take care of creation and destruction of instance(s)

???trainer
Actually Java SE technology!
some application servers add their own JNDI namespaces, like wildfly adds java:jboss
???t

---

## Obtaining a reference to a JNDI object

- Injection (!)
    - on fields or method parameters
    - only works for managed objects (!)
```java
@Resource(lookup = "java:jboss/datasources/ExampleDS")
DataSource db;
```
- Manual lookup also possible
    - works anywhere
```java
DataSource db = (DataSource) new InitialContext().lookup("java:jboss/datasources/ExampleDS");
```

???trainer
Injection and managed objects are very important concepts!
???t

---

