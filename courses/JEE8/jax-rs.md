# Jakarta RESTful Web Services (JAX-RS)

---

## Rundown

- Since JEE 6 (2009)
- Exposes POJO as RESTful web service
- _Heavily_ annotation-based
- Key features:
    - RESTfully mapping resources to URLs
    - Filters (and interceptors)
    - Providers (extensibility)
        - Custom data formats
        - Exception handling
    - Validation
    - Context information injection
    - REST client
    - Async processing

???trainer
ask audience what they think a REST client is used for (> microservices)

???t

---

## Enabling JAX-RS

- Support for RESTful web services must be enabled first
    - Needs a relative context path to base itself on

```java
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class RestApi extends Application { }
```

- Exposes all rest services under `http://localhost:8080/foo/api`
    - Here, context path is `foo` <small>(deployable is called `foo.war`)</small>
- No `xml` needed!

---

## Creating endpoints (= REST resources)

- `@Path` can be placed on class and/or method level
    - When both, eventual path is concatenation of segments

```java
@Path("movie")
public class MovieService {
    @Path("list")   // Will become "[http://localhost:8080/foo/api]/movie/list"
    public List<String> findAllMovies() { ... }
}
```

- Combine with `@GET`/`@POST`/`@PUT`/`@DELETE`/... to indicate HTTP method
    - Similar to `doGet`, `doPost`... from Servlet

```java
@GET
public Movie findById(int id) { ... }

@POST
public int createMovie(Movie movie) { ... }

@DELETE
public boolean removeMovie(int id) { ... }
```

---

## Request parameters

- Path parameters allow passing simple variables
    - `http://localhost:8080/movies/api/movie/1`

```java
@GET @Path("movie/{movie_id}")
public Movie findMovieById(@PathParam("movie_id") int id) { ... }
```
- Query parameters use the _query string_
    - `http://localhost:8080/movies/api/movie?movie_id=1`

```java
@GET @Path("movie")
public Movie findMovieById(@QueryParam("movie_id") int id) { ... }
```
- Request body (POST, PUT...)

```java
@POST @Path("movie")
public void createMovie(Movie m) { ... }
```
- Other parameter types
    - `@FormParam`, `@CookieParam`, `@HeaderParam`

---

## Data representations

- Negotiation is done using `@Produces` and `@Consumes`
    - `@Consumes` determines the supported _request_ content type
        - Negotiated using `Content-Type` (request) header
    - `@Produces` determines the supported _response_ content type(s)
        - Negotiated using `Accept` (request) and `Content-Type` (response) headers

```java
@Produces("application/json") // Response content-type will be JSON (output)
public List<Movie> findAllMovies() { ... }

@Consumes("application/xml") // Request content-type will be XML (input)
public int createMovie(Movie m) { ... }
```

- Notes:
    - Any _mediatype_ can be used as long as the server knows how to handle them
        - XML handled by _JAXB_
        - JSON handled by _JSON-B_ (since JEE8, older apps might use alternative)
        - Custom ones can be provided (`MessageBodyReader` and `MessageBodyWriter`)
    - More than one representation can be supported at the same time

???trainer
Show example: 1 endpoint, produces json AND text
- requests influences returned data with Accept header

JSON-B is very recent
Example of custom message body writer to yaml?

Should inline to JSON-B here?

???t

---

## Security

- Authentication
    - Relies on (other specs in) platform... (typically servlet)
        - `login-control` in `web.xml` (BASIC is best out-of-the-box fit)
        - Pluggable `HttpAuthenticationMechanism` also possible (Jakarta Security)
        - Token-based is a natural fit for APIs (but not supported out-of-the-box)
- Authorization
    - `@Context SecurityContext`: programmatic, NOT declarative!
    - When running on servlet, can also rely on the web container...
        - configuration of protected resources in `web.xml`

???trainer

The most annoying part is that it's actually not possible to define 2 auth methods in a single JEE component (like a WAR).
That means that if you are using FORM for JSF, you can not also use BASIC for JAX-RS...
Typically, a homegrown solution is required...

???t

---

## Providers

- public classes with `@Provider` annotation
- Responsible for cross-cutting concerns
    - Request filters and interceptors
        - ContainerRequestFilter / ContainerResponseFilter (incoming)
        - ClientRequestFilter / ClientResponseFilter (REST client)
    - Exception handling
        - `ExceptionMapper<T extends Exception>`
    - Entity providers (mediatype to Java and vice versa)
        - Mostly provided by JSON-B and JAXB
        - Can add own (MessageBodyReader/MessageBodyWriter)
    - ...

???trainer
Mention that this is somewhat similar to an EJB, but without all the extra features of the EJB container.
It just indicates that a POJO should be treated as a special class that provides some JAX-RS related functionality.
JAX-RS will detect this and use such classes automatically.

do not mention context providers:
- `ContextResolver<T>`

???t

---

### Provider example: exception mapper

- Maps exception to `Response`
    - Custom mappers:
        - Implement `ExceptionMapper`
        - Annotate with `@Provider`

```java
@Provider
public class MyExceptionMapper implements ExceptionMapper<MyException>{

    public Response toResponse(MyException exception) {
        return Response
                .status(...)
                .entity(...)
                .build();
    }

}
```
- Throwing `WebApplicationException` uses default exception mapper

---

## Contexts

- JAX-RS "resources" which can be injected
- `@Context` to inject:
    - `Application`: access to JAX-RS root class
    - `UriInfo`: access to runtime URI which triggered endpoint
        - useful for building dynamic URLs
    - `HttpHeaders`
    - `Request`
    - `SecurityContext`
    - `Providers`: interface to access providers dynamically
    - `ResourceContext`
    - `Configuration`: client and server runtime configurations
- Only in case of (http) servlet-based implementations
    - `ServletConfig`, `ServletContext`, `HttpServletRequest`, `HttpServletResponse`

???trainer
On the agenda for future JEE versions: tighter integration with CDI
- (replace @Context and @Provider with @Inject and CDI beans)

???t

---

## REST Client

- Standardized _client api_
    - Allows consuming _RESTful_ web services in a Java application
    - Can reuse existing JAX-RS _providers_
- Uses a modern _builder_ pattern with a _fluent API_
    - The fluent building process involves different classes

| Class             | Purpose
|-
| ClientBuilder     | Factory for client instances. First thing to use!
| Client            | Represents a HTTP request under construction
| WebTarget         | Represents a resource target
| Builder           | Represents a request builder for a target
| Invocation        | Represents a client request invocation
| Response          | Represents the response data (with headers) of an invocation
| ...               | ...

---

### REST Client API

- A typical JAX-RS client looks like this
    - Produces a HTTP GET request to `http://localhost:8080/api/movies/1`

```java
Movie movie = ClientBuilder.newClient()
    .target("http://localhost:8080/api/movies/")
    .path("{movieId}")
    .resolveTemplate("movieId", 1)
    .request()
    .get(Movie.class);
```

- Steps involved:
    1.  `ClientBuilder` is the entry point to the API. Creates `Client`
    2. `Client` is an expensive object to create (reuse)
    3. `target()` returns a `WebTarget`
        - Uses `path()` and `resolveTemplate()` to complete the dynamic part of the URL
    4. `request()` returns `Builder` tuned in to the target URL
        - Can control HTTP protocol items like _headers_, _method_, ...
    5. `get()` executes the request and unmarshalls the results into a domain object

---

### REST Client API

- All other HTTP methods are supported as well
    - When data should be passed to the server use `Entity.entity()`

```java
// Data object for sending
Movie movie = new Movie(1, "A New Hope", "George Lucas");

Client client = ClientBuilder.newClient();
Builder builder = client.target("...").request();

// GET
*Movie result1 = builder.get(Movie.class);
// POST
*Integer result2 = builder.post(Entity.entity(movie, MediaType.APPLICATION_JSON), Integer.class);
// PUT
*builder.put(Entity.entity(movie, MediaType.APPLICATION_JSON));
// DELETE
*builder.delete();
```

---

### REST Client API

- Make the client _asynchronous_ using `Builder.async()`
    - Results in a `Future`

```java
Future<Movie> future = builder.async().get(Movie.class);
```

- Register existing _providers_ using `Client.register()`
    - Because you can't specify such things with annotations for client calls

```java
client.register(MySpecialRepresentationMovieProvider.class);    // Stays on Client
```

- Prepare an invocation for later use using `Builder.buildGet()`
    - Analogous for `POST`, `PUT`, `DELETE`, ...

```java
Invocation i = builder.buildGet();
Movie m = i.invoke(Movie.class);
```

---

## Filters

- Do _not_ use servlet filters with JAX-RS...
- Create JAX-RS filters using these 4 "extension points"
    - For incoming requests
        - ContainerRequestFilter
        - ContainerResponseFilter
    - For outgoing requests (using client)
        - ClientRequestFilter
        - ClientResponseFilter
- Uses interceptors API
    - Default filter has no annotation: "global binding"
        - Add `@Provider` to be discovered automatically
    - Can add custom annotation using `@NameBinding` (= interceptor binding)
    - Can add `@Priority` to order filters

---

### Filter example: container request filter

```java
public class SecurityFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String user = requestContext.getHeaderString("WhoAmI");
        if (!"Batman".equals(user)) {
            // unauthorized
            requestContext.abortWith(Response.status(401).build());
        }
    }

}
```
- Do nothing to let the request pass
- Call `requestContext.abortWith(Response)` to abort
- Order is defined by `@Priority`
- Don't forget to put `@Provider`

---

### Filter example: container request filter

- `@PreMatching`
    - Applies on all requests, before JAX-RS matches request to resource

```java
@Provider @PreMatching
public class SecurityFilter implements ContainerRequestFilter { }
```
- Post matching
    - No URL pattern, matching is done through `@NameBinding` (interceptor binding)

```java
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(value = RetentionPolicy.RUNTIME)
@NameBinding
public @interface Secured { }
```
```java
@Secured
public class SecurityFilter implements ContainerRequestFilter { }
```
```java
@GET
@Secured
public String helloWorld() { }
```

???trainer

A NameBinding may also be applied to a resource (class) or to the entire JAX-RS application.

Pre matching filters always go before post matching filters (priority-wise)

???t

---

## Other features

- Asynchronous web services
    - Uses `@Asynchronous`, `@Suspended` and `AsyncResponse`
- Custom message body processing
    - Uses interceptor bindings

???trainer
Mention that the roadmap contains plans to better integrate JAXRS with CDI (make @Context things injectable through @Inject)

???t

