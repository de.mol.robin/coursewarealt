# Jakarta Batch

---

## Rundown

- _Batch Applications for the Java Platform_ is defined as JSR 352
    - JEE7
- Heavily XML based
- No generic type support
- Key features
    - Job specification language (JSL)
    - Parallel execution
    - Checkpoints and resuming jobs
    - Error handling

---

## Overview

- Batch processing is the execution of a series of _jobs_
  - Usually for non-interactive, bulk-oriented, and long-running tasks
  - Typically computationally intensive
  - Executed sequentially or in parallel
  - Scheduled or triggered on demand

![batch-architecture](images/batch-architecture.png#no-border "Batch Architecture")

???trainer

VOCAB:
- Job
  - An entity that encapsulates an entire batch process
  - Implemented with a _Job Specification Language_ in a Job XML
- Step
  - An independent, sequential phase of a job
  - Defines and controls the actual batch processing
- `JobOperator`
  - Manages all aspects of job processing (start, stop, restart)
  - Also handles job repository commands (job retrieval, step execution)
- `JobRepository`
  - Holds information about jobs (past and current)
  - The `JobOperator` gives access to this repository

???t

---

## Job elements

- Step
    - Chunk
        - Reader-Processor-Writer
            - `ItemReader` retrieves input for a step as a stream of items
            - `ItemProcessor` processes the item by transforming or applying business logic
            - `ItemWriter` writes the output for a step in aggregated _chunks_
        - Is the primary batch pattern
        - Once a _chunk_ is aggregated, the results are written out and the transaction committed
    - Task
        - Run any java file (called a batchlet)
        - Is invoked once, runs, and returns with an exit status
        - Can be cancelled
- Decision
- Flow
- Split
- (properties and parameters)

---

## A Batch Example

```java
@Named
*public class MyItemReader extends AbstractItemReader {     // or implement ItemReader
  List<String> list = ...
  @Override
  public MyInputRecord readItem() {
    return new MyInputRecord(list.remove(0));
  }
}
```
```java
@Named
*public class MyItemProcessor implements ItemProcessor {
  @Override
  public MyOutputRecord processItem(Object t) {
    MyOutputRecord o = new MyOutputRecord();
    // ...
    return o;
  }
}
```
```java
@Named
*public class MyItemWriter extends AbstractItemWriter {     // or implement ItemWriter
  @Override
  public void writeItems(List list) { /*...*/  }
}
```

???trainer
- Extends `AbstractItemReader`
- The `open` method prepares the reader to read items
  - `List<String>` is initialized in this method
  - The input parameter `c` represents the last checkpoint for this reader
      - Checkpoint data is by the `checkpointInfo` method
      - Checkpoint data contains information to resume reading items upon restart (initial value _null_)
- The `readItem` method returns the next item for chunk processing
  - For all strings read from the `List`, a new `MyInputRecord` instance is created and returned
  - Returning a `null` indicates the end of stream
- `@Named` ensures that this bean can be referenced in Job XML

- Implements the `ItemProcessor` interface
- `processItem` is part of a chunk step
  - Accepts an input from the item reader and returns an output to the output writer
  - You can apply business logic to transform a MyInputRecord to a MyOutputRecord
  - Returning `null` indicates that the item should not continue to be processed and be filtered out
- `@Named` ensures that this bean can be referenced in Job XML

- Extends `AbstractItemWriter`
- `writeItems` receives aggregated items and implements write logic
  - In this case, the method receives a List of `MyOutputRecord`
- `@Named` ensures that this bean can be referenced in Job XML

???t

---

## A Batch Example

- Define the chunk step in a Job XML
    - JAR: `META-INF/batch-jobs`
    - WAR: `WEB-INF/classes/META-INF/batch-jobs`

```xml
<job id="myJob" xmlns="http://xmlns.jcp.org/xml/ns/javaee"
                version="1.0">
  <step id="myStep">
    <chunk item-count="3">
      <reader ref="myItemReader"/>
      <processor ref="myItemProcessor"/>
      <writer ref="myItemWriter"/>
    </chunk>
  </step>
</job>
```

- Checkpoints allow step execution to periodically bookmark its current progress
  - This enables restarts from the last point of consistency
  - Interruptions can be planned or unplanned
- The end of processing for each chunk is a natural point for taking a checkpoint

???trainer
- The `job` element identifies a job
  - It has a logical name `id`
- A job may contain any number of steps identified by the step element
  - Each `step` also has a logical name `id`
- The chunk element defines a chunk type step
  - Is checkpointed according to a checkpoint policy (default policy "item")
  - `item-count` specifies the number of items to process per chunk (default 10)
      - This value also is used to define the transaction boundary
- `myItemReader` references the CDI bean name of a reader
  - Implements the `ItemReader` interface or extends `AbstractItemReader`
- `myItemProcessor` references the CDI bean name of a processor (optional)
  - The processor class implements the `ItemProcessor` interface
  - If not specified, then all items are passed from reader to writer directly
- `myItemWriter` references the CDI bean name of a writer
  - Implements the `ItemWriter` interface or extending `AbstractItemWriter`

???t

---

## Job properties

- Specify parameters for the job
    - Can also be for sub-elements
    - Can be dynamically provided to JobOperator too

```xml
<job id="loganalysis" xmlns="http://xmlns.jcp.org/xml/ns/javaee"
                      version="1.0">
  <properties>
    <property name="input_file" value="input1.txt"/>
    <property name="output_file" value="output2.txt"/>
  </properties>
  <!-- ... -->
</job>
```
- Access in Java
    - injecting `JobContext` or `StepContext`
    - injecting individual properties as `@Inject @BatchProperty(name=...)`
        - this is a CDI qualifier
        - scoped to defining element (not a parent)


---


## Starting, Restarting and Cancelling Jobs

- You can manipulate jobs using `JobOperator`

```java
JobOperator jo = BatchRuntime.getJobOperator();
long jid = jo.start("myJob", new Properties());
```

- Restart the job with the `restart` method

```java
Properties props = ...
jo.restart(jid, props);
```

- To cancel a job, use the `abandon` method

```java
jo.abandon(jid);
```

???trainer
- The start method creates a new job instance and starts execution
- The method returns the execution ID
- Restarting and cancelling require the execution ID of the job

???t

---

## Job Metadata

- You can obtain metadata of the running job

```java
JobExecution je = jo.getJobExecution(jid);
Date createTime = je.getCreateTime();
Date startTime = je.getStartTime();
int count = jo.getJobInstanceCount("myJob"); // the number of job instances with name myJob
Set<String> jobs = jo.getJobNames(); // the unique set of job names
```

---

## Custom Checkpointing

- Set `checkpoint-policy` to `custom`

```xml
<chunk item-count="3" checkpoint-policy="custom">
  <reader ref="myItemReader"/>
  <processor ref="myItemProcessor"/>
  <writer ref="myItemWriter"/>
  <checkpoint-algorithm ref="myCheckpointAlgorithm"/>
</chunk>
```
- The `checkpoint-algorithm` element references a CDI bean
  - Implements `CheckpointAlgorithm` or extends `AbstractCheckpointAlgorithm`

```java
public class MyCheckpointAlgorithm extends AbstractCheckpointAlgorithm {
  @Override
  public boolean isReadyToCheckpoint() throws Exception {
    return MyItemReader.COUNT % 3 == 0;
  }
}
```

---

## Exception Handling

- When any batch artifact throws an exception, the job ends in `FAILED`
  - This behavior can be overridden by configuring exceptions to skip or retry

```xml

*<chunk item-count="3" skip-limit="3">
  <reader .../>
  <processor .../>
  <writer .../>
* <skippable-exception-classes>
    <include class="java.lang.Exception"/>
    <exclude class="java.io.IOException"/>
  </skippable-exception-classes>
* <retryable-exception-classes>
    <include class="java.lang.Exception"/>
  </retryable-exception-classes>
</chunk>
```

- This example will skip all exceptions except `java.io.IOException`

???trainer
- `skip-limit` specifies the number of exceptions this step will skip
- `skippable-exception-classes`: which exceptions to skip
- `retryable-exception-classes`: which exceptions to retry
- Exceptions need to be defined with their fully qualified name
  - `include`: exceptions that trigger skips and retries
  - `exclude`: exceptions that do not trigger skips and retries

The `SkipReadListener`, `SkipProcessListener<T>`, and `SkipWriteListener<T>` interfaces can be implemented to receive control when a skippable exception is thrown.

The `RetryReadListener`, `RetryProcessListener<T>`, and `RetryWriteListener<T>` interfaces can be implemented to receive control when a retriable exception is thrown.

???t

---

## Batchlet Processing

- Batchlet style lets you implement your own batch pattern
  - It is a task-oriented processing style

```xml
<job id="myJob" xmlns="http://xmlns.jcp.org/xml/ns/javaee" version="1.0">
  <step id="myStep" >
    <batchlet ref="myBatchlet"/>
  </step>
</job>
```
- A batchlet implements the `Batchlet` interface or extends `AbstractBatchlet`

```java
@Named
*public class MyBatchlet extends AbstractBatchlet {
  @Override
  public String process() { // perform tasks
    // ...
    return "COMPLETED"; // job is successful
  }
}
```

- If the `process` method throws an exception, the batchlet ends in `FAILED`

???trainer
- The `batchlet` element defines the batchlet type step and references a CDI bean
  - It is mutually exclusive with the `chunk` element

magic values:
    - COMPLETED
    - FAILED
    - STOPPED

???t

---

## Listeners

- Can be used to intercept batch execution
- Can be set on multiple batch components
- Listeners can be specified in Job XML

```xml
<job id="myJob" xmlns="http://xmlns.jcp.org/xml/ns/javaee" version="1.0">
  <listeners>
    <listener ref="myJobListener"/>
  </listeners>
  <step id="myStep" >
    <listeners>
      <listener ref="myStepListener"/>
      <listener ref="myChunkListener"/>
      <listener ref="myItemReadListener"/>
      <listener ref="myItemProcessorListener"/>
      <listener ref="myItemWriteListener"/>
    </listeners>
    <chunk>
      ...
    </chunk>
  </step>
</job>
```

???trainer
Interface                                                         | Abstract Class                | Receive Control
:---------------------------------------------------------------- | :---------------------------- | :--------------------------------------------------------------------------------------------------------
`JobListener`                                                     | `AbstractJobListener`         | Before and after a job execution runs, and also if an exception is thrown during job processing
`StepListener`                                                    | `AbstractStepListener`        | Before and after a step runs, and also if an exception is thrown during step processing
`ChunkListener`                                                   | `AbstractChunkListener`       | At the beginning and end of chunk processing and before and after a chunk checkpoint is taken
`ItemReadListener`                                                | `AbstractItemReadListener`    | Before and after an item is read by an item reader, and also if the reader throws an exception
`ItemProcessListener`                                             | `AbstractItemProcessListener` | Before and after an item is processed by an item processor, and also if the processor throws an exception
`ItemWriteListener`                                               | `AbstractItemWriteListener`   | Before and after an item is written by an item writer, and also if the writer throws an exception
`SkipReadListener`, `SkipProcessListener`, `SkipWriteListener`    | None                          | When a skippable exception is thrown from an item reader, processor, or writer
`RetryReadListener`, `RetryProcessListener`, `RetryWriteListener` | None                          | When a retriable exception is thrown from an item reader, processor, or writer

???t

---

## Job Sequence

- A step is an independent, sequential phase of a job
- But a job may consist of any number of steps
  - Each step may be a chunk type or batchlet types
  - Each step is by default considered the last of the job
  - The next step has to be explicitly specified via the `next` attribute

```xml
<job id="myJob" xmlns="http://xmlns.jcp.org/xml/ns/javaee" version="1.0">
  <step id="step1" next="step2">
    <chunk item-count="3">
      <reader ref="myItemReader"></reader>
      <processor ref="myItemProcessor"></processor>
      <writer ref="myItemWriter"></writer>
    </chunk>
  </step>
  <step id="step2" >
    <batchlet ref="myBatchlet"/>
  </step>
</job>
```

???trainer
- The above job has two steps
  - `step1` is a chunk type step
  - `step2` is a batchlet type step
- `step1` is executed first, followed by `step2`
  - The order is identified by the `next` attribute
  - `step2` is the last step in the job (this is the default)

???t

---

## Job Sequence

- Other than steps, the specification outlines other elements that define a sequence
  - _Flow_: Defines a sequence of execution elements that execute together as a unit
  - _Split_: Defines a set of flows that execute concurrently
  - _Decision_: Defines a customized way of determining sequencing
- The first step, flow, or split in Job XML will be the first to execute

---

## Flow

```xml
<job id="myJob" xmlns="http://xmlns.jcp.org/xml/ns/javaee" version="1.0">
  <flow id="flow1" next="step3">
    <step id="step1" next="step2">
      ...
    </step>
    <step id="step2" >
      ...
    </step>
  </flow>
  <step id="step3" >
    ...
  </step>
</job>
```

- When the flow is finished, it is the entire flow that transitions to the next execution element

???trainer
The above job consists of a flow and a step. The flow `flow1` has two steps, `step1` followed by `step2`.

A flow may contain any execution element, but these may only transition among themselves and not outside of the flow.

The last execution element is `step3`, which is the end of the job. In the `next` attribute, you can specify the name of a step, flow, split or decision.

???t

---

## Split

```xml
<job id="myJob" xmlns="http://xmlns.jcp.org/xml/ns/javaee" version="1.0">
  <split id="split1" next="step3">
    <flow id="flow1">
      <step id="step1">
        ...
      </step>
    </flow>
    <flow id="flow2">
      <step id="step2">
        ...
      </step>
    </flow>
  </split>
  <step id="step3">
    ...
  </step>
</job>
```

- A split can only contain `flow` elements and is finished after all flows complete
- Each flow of a split runs on a separate thread, enabling multi-threaded processing of the batch job

???trainer
The above job consists of a split and a step. By default, the split is the last execution element of the job, but you can use the `next` attribute to specify the next element.

???t

---

## Decision

- Determines a customized sequence between steps, flows and splits
- Four transition elements are defined to direct or terminate execution
  - `next`: directs the execution flow to the next element
  - `fail`: causes the job to end with `FAILED` batch status
  - `end`: causes a job to end with `COMPLETED` batch status
  - `stop`: causes a job to end with `STOPPED` batch status

```xml
<job id="myJob" xmlns="http://xmlns.jcp.org/xml/ns/javaee" version="1.0">
  <step id="step1" next="decider1">
    ...
  </step>
  <decision id="decider1" ref="myDecider">
    <next on="DATA_LOADED" to="step2"/>
    <end on="NOT_LOADED"/>
  </decision>
  <step id="step2">
    ...
  </step>
</job>
```

???trainer
The job above has two steps and one decision. The decision references a `Decider` CDI bean.

???t

---

## Decision

- A decider

```java
@Named
*public class MyDecider implements Decider {
  @Override
  public String decide(StepExecution[] ses) throws Exception {
    // ...
    if (...)
      return "NOT_LOADED";
    if (...)
      return "DATA_LOADED";
  }
}
```

???trainer
The `Decider` receives an array of `StepExecution` objects as input, which represent the execution element that transitions to this decider. The `decide` method returns an exit status that updates the current job's exit status, and directs execution transition.

Fail, end and stop are _terminating elements_, because they cause the job execution to terminate.

???t

---

## Partitioning the Job

- A batch step can run as a partitioned step
  - Runs as multiple instances of the same step definition across multiple threads
  - Each thread handles one partition
  - Each partition can have unique parameters to specify on which data to operate
- `partition` is an optional element used to specify that a step is a partitioned step
  - Each partition has a plan
    - Specify the number of partitions and the number of threads (by default equal)

---

## Partitioning the Job

```xml
<step id="myStep" >
  <chunk item-count="3">
    <reader ref="myItemReader">
      <properties>
        <property name="start" value="#{partitionPlan['start']}" />
        <property name="end" value="#{partitionPlan['end']}" />
      </properties>
    </reader>
    <processor ref="myItemProcessor"></processor>
    <writer ref="myItemWriter"></writer>
  </chunk>
  <partition>
    <plan partitions="2" threads="2">
      <properties partition="0">
        <property name="start" value="1"/>
        <property name="end" value="10"/>
      </properties>
      <properties partition="1">
        <property name="start" value="11"/>
        <property name="end" value="20"/>
      </properties>
    </plan>
  </partition>
</step>
```

---

## Partitioning the Job

- Unique property values are passed to each partition and are accessible to the item reader

```java
@Inject
@BatchProperty(name = "start")
private String startProp;

@Inject
@BatchProperty(name = "end")
private String endProp;
```

- Each thread runs a separate copy of the step
  - Chunking and checkpointing occur independently on each thread

---

## Partitioning the Job

- The number of partitions and threads can be specified through a partition mapper

```xml
<partition>
  <mapper ref="myMapper"/>
</partition>
```

- Implement the `PartitionMapper` interface in the referenced CDI bean
  - You can then programmatically calculate the number of partitions and threads for each step

???trainer
If desired, you can also use more advanced features such as `PartitionCollector` and `PartitionAnalyzer`.
These come in handy when you need to share results with a control point to decide the overall outcome of the step.

???t

---

## Partitioning the Job

```java
@Named
*public class MyMapper implements PartitionMapper {
  @Override
  public PartitionPlan mapPartitions() throws Exception {
    return new PartitionPlanImpl() {
      @Override
      public int getPartitions() { return 2; }
      @Override
      public int getThreads() { return 2; }
      @Override
      public Properties[] getPartitionProperties() {
        Properties[] props = new Properties[getPartitions()];
        for (int i=0; i<getPartitions(); i++) {
          props[i] = new Properties();
          props[i].setProperty("start", String.valueOf(i*10+1));
          props[i].setProperty("end", String.valueOf((i+1)*10));
        }
        return props;
      }
    };
  }
}
```

???trainer
- `mapPartitions` returns a `PartitionPlan`
  - `PartitionPlanImpl` is a convenient basic implementation
- `getPartitionProperties` method returns an array of Properties for each partition

???t

???invisible

===

Exercise: Batch

- In this exercise, we will create a simple batch import job to fill up our database
- Open the starter project, and notice the `people.csv` file
- Open the `import.xml` file in `META-INF/batch-jobs`
  - Add a reader, processor and writer in a chunk based job (5 items at a time)
- Create `PersonReader`, `PersonProcessor` and `PersonWriter` class
  - The `PersonReader` reads the `people.csv` file line per line
  - The `PersonProcessor` converts each line to a `Person` object
      - Make sure you handle persons with special last names!
  - The `PersonWriter` uses the `EntityManager` to save `Person`s
- Start the job from the index page and check the results in the table/database

???i
