# Beyond the essentials

---

## Jakarta Messaging

- Message-driven architecture
    - Loose coupling, asynchronous, high performance, reliable
- Point-to-point or publish-subscribe
- Administered objects (lookup through JNDI, external to code)
    - Connection factories
    - Destinations (queues and topics)
- Client objects: `JMSProducer` and `JMSConsumer`
    - In JEE: consume indirectly through message-driven EJB (= MDB)
- Message = headers + properties + body
- `MessageDrivenContext` resource: rollback transaction, get principal...

???trainer
Headers and properties are the same, only that header uses predefined, fixed names.
The body of a message is optional and contains the data to send or receive

|Interface     | Description
|-
|TextMessage   | String
|MapMessage    | Set of name-value pairs where names are strings and values are Java primitive types
|ObjectMessage | Serializable object or a collection of serializable objects
|StreamMessage | Stream of Java primitive values. It is filled and read sequentially.
|BytesMessage  | Stream of bytes
|Message       | No body

???t

---

## Jakarta Interceptors

- "AOP-light"
- `@PostConstruct` and `@PreDestroy` (typically inlined)
- `@AroundInvoke`: can wrap around method invocation
    - Typically put in separate `@Interceptor` class
    - Must have 1 annotated method
        - Return `Object`, take `InvocationContext` as argument
- Applying an interceptor
    - Use `@Interceptors({...})` on target class/method
    - Use custom `@InterceptorBinding` annotation on target class/method + interceptor class

---

## Jakarta Batch

- XML-based job description language
- Job = steps, decisions, flows, splits, partitions
- Step = chunk (read, process, write) or task (batchlet)
- Jobs may be scheduled, paused, cancelled, restarted
- Checkpointing mechanism to limit impact of failures
- Listeners to hook into job execution and exception handling
- Mostly interesting for jobs with non-trivial execution flows

---

## Jakarta Concurrency

- Non-EJB way for asynchronous code execution
- Use `@Resource` to obtain managed thread pools
    - `ManagedExecutorService` (`java:comp/DefaultManagedExecutorService`)
    - `ManagedScheduledExecutorService`
- Submit `Runnable` or `Callable<V>`
- SecurityContext of origin is propagated
- Transaction of origin is NOT propagated
- CDI beans may be tasks (if `@ApplicationScoped` or `@Dependent`)
- Implement `ManagedTask` to be able to attach lifecycle callbacks

???trainer
CDI beans can be used as tasks if they implement `Callable` or `Runnable`.
This is a huge deal, because it provides integration with the rest of the JEE world.
A CDI task can access the _Persistence Context_ this way, as well as the rest of JEE's managed resources...
???t

---

## Jakarta Websockets

- Full-duplex connection between client and server
- Uses `ws://` protocol (`wss://` for secure)
- Annotation-driven
    - `@ServerEndpoint` for server-side (~ path)
    - `@OnMessage` for receiving
        - Supported param types: primitives, String, byte[], Reader, InputStream
        - Supported return types: primitives, String, byte[], void (no reply)
        - Can specify `@PathParam`s
        - Add `Session` as param: represents other side of connection
    - `@OnOpen`, `@OnClose`, `@OnError`: lifecycle callbacks
    - `@ClientEndpoint` for client-side
- Allow specification of custom encoders/decoders
    - Map `Object` to any of: Text, TextStream, Binary, BinaryStream
- Security through `web.xml`

???trainer

Testability: easy to create a WS client:
    WebSocketContainer container = ContainerProvider.getWebSocketContainer();
    container.connectToServer(...);

???t

---

## ...and more

- Jakarta Expression Language (EL)
- Jakarta Standard Tag Library (JSTL)
- (old) Jakarta Server Pages (JSP)
- (old) Jakarta Web Services (~ SOAP)
- (old) Jakarta XML Binding (JAXB)
- ...

---
