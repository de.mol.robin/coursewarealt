# Jakarta Interceptors

---

## Rundown

- Separate spec since JEE 7
    - Originally part of EJB
- Separate cross-cutting concerns from your code
- Compatible with all managed beans (EJB, CDI...)
- Key features
    - Inline interceptors
    - Interceptor classes
    - Interceptor bindings

![interceptors](images/cdi-interceptors.png#no-border "Interceptors")

---

## Interceptor Types

- `@PostConstruct`
    - after bean construction
- `@AroundInvoke`
    - around any method invocation
    - requires `InvocationContext` param
- `@AroundConstruct`
    - around constructor
- `@PreDestroy`
    - before bean destruction
- `@AroundTimeout`
    - (specifically for EJB with timers)

???trainer

![interceptor-sequence](images/cdi-interceptor-sequence.png#no-border "Interceptors Sequence")

???t

---

## Directly in the bean

```java
public class CustomerService {
    @PersistenceContext
    private EntityManager em;

    @Inject
    private Logger logger;

    public void createCustomer(Customer customer) {
        em.persist(customer);
    }

    public Customer findCustomerById(Long id) {
        return em.find(Customer.class, id);
    }

    @PostConstruct
    public void init() {
        // ...
    }
}
```
- Typical for `@PostConstruct`, `@PreDestroy`
    - `@AroundTimeout` for EJB using timers

???trainer
- Not really separating the cross cutting concern...

???t

---

## Interceptor Classes

```java
@Interceptor                            // Optional
public class LoggingInterceptor {
    @Inject private Logger logger;      // Can @Inject in an interceptor

*   @AroundConstruct
    private void init(InvocationContext ic) throws Exception {
        logger.fine("Entering constructor");
        try {
            ic.proceed();
        } finally {
            logger.fine("Exiting constructor");
        }
    }

*   @AroundInvoke
    public Object logMethod(InvocationContext ic) throws Exception {
        logger.entering(ic.getTarget().toString(), ic.getMethod().getName(), ic.getParameters());
        try {
            return ic.proceed();
        } finally {
            logger.exiting(ic.getTarget().toString(), ic.getMethod().getName());
        }
    }
}
```

???trainer
- Achieves separation of cross-cutting concerns in own class
- Using the invocation context, you can even manipulate the method parameter values!

???t

---

## Activating Interceptors

- You need to instruct the container to apply the interceptor
    - On class or method level

```java
*@Interceptors(AuditInterceptor.class)      // apply on the class
public class CustomerService {
  @Inject
  private EntityManager em;

* @Interceptors(LoggingInterceptor.class)   // apply on a method
  public void createCustomer(Customer customer) {
    em.persist(customer);
  }

* @ExcludeClassInterceptors                 // exclude class interceptors for this method
  public Customer findCustomerById(Long id) {
    return em.find(Customer.class, id);
  }
}
```

---

## Default interceptors

- Interceptors that should be activated for every bean
- Defined in deployment descriptor
    - `beans.xml` for CDI
    - `ejb-jar.xml` for EJB JARs

```xml
<beans>
    <interceptors>
        <class>com.inetum.realdolmen.LoggingInterceptor</class>
    </interceptors>
</beans>
```
- Can be disabled per-bean with `@ExcludeDefaultInterceptors`

---

## Ordering interceptors

```java
@Stateless
*@Interceptors({FirstInterceptor.class, SecondInterceptor.class})
public class CustomerService {

    public void createCustomer(Customer customer) {
        // ...
    }

*   @Interceptors({ThirdInterceptor.class, FourthInterceptor.class})
    public Customer findCustomerById(Long id) {
        // ...
    }

    public void removeCustomer(Customer customer) {
        // ...
    }

*   @ExcludeClassInterceptors
    public Customer updateCustomer(Customer customer) {
        // ...
    }
}
```

---

## Interceptor Binding

- (1) Custom annotation

```java
*@InterceptorBinding
@Target({METHOD, TYPE})
@Retention(RUNTIME)
public @interface Loggable{ ... }
```
- (2) Interceptor class

```java
*@Loggable      // Bind the annotation to an interceptor
*@Interceptor   // Mandatory this time
public class LoggingInterceptor {

    @AroundInvoke
    public Object logMethod(InvocationContext ic) throws Exception {...}
}
```
- (3) Intercepted component (application of the interceptor)

```java
*@Loggable      // Applies interceptor(s) matching the binding to this class
public class CustomerService { ... }
```

???trainer
- Interceptor class may declare multiple binding types
- Binding type may be defined by multiple interceptor classes

???t

---

## Interceptor Binding ordering

- Use `@Priority` on interceptor class
    - Lower number = executed first
    - Some default constants exist to help you

| Priority constant | value | description
|-
| `PLATFORM_BEFORE` | 0    | Reserved for the JEE platform
| `LIBRARY_BEFORE`  | 1000 | For extension libraries
| `APPLICATION`     | 2000 | For application developers
| `LIBRARY_AFTER`   | 3000 | Low priority for extension libraries
| `PLATFORM_AFTER`  | 4000 | Low priority for the JEE platform
- You should use the 2000-3000 range

```java
@Interceptor
@Loggable
*@Priority(Interceptor.Priority.APPLICATION + 10)
public class LoggingInterceptor { ... }
```
