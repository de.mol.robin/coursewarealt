# Jakarta Web Services (JAX-WS)

---

## What is a Web Service?

- According to the [W3C](https://www.w3.org/TR/ws-gloss/#defs) a Web Service is
    - _"A software system designed to support interoperable machine-to-machine interaction over a network”_
- More concretely this most typically means
    - A _business service_
        - (e.g. "payment service", "order service", "invoice service", ...)
    - Implemented on a specific software platform
        - Java, .NET, ...
    - That is remotely accessible over TCP/IP <small>(often HTTP)</small>
    - In a portable way <small>(cross platform)</small>
- Web Services have existed for a long time, but the term was popularized in the early 2000s

---

## Web Service styles

- Web Services are independent of any technology
    - They typically use a _standard_ and _human readable_ exchange format <small>(like XML and JSON)</small>     
- In modern practice there are two main styles
    - _SOAP_ Web Services <small>(Simple Object Access Protocol)</small>
        - Strong in enterprise and SOA environments
        - Uses a strong contract for communication
        - Tries to be loosely coupled from any transport medium
    - _REST_ Web Services <small>(REpresentational State Transfer)</small>
        - Can be viewed as the _architecture of the web_
        - Uses an implicit contract for communication
        - Inherently tied to HTTP as a transport medium

---

## Web Service Technologies

- SOAP Web Services are built around the following technologies
    - XML and XSD
    - The SOAP protocol
    - The WSDL description format
    - UDDI registries
    - Usually also the HTTP protocol <small>(not necessarily though)</small>    
- RESTful Web Services are built around the following technologies
    - The infrastructure of the Web <small>(HTTP, WWW, URIs, ...)</small>
    - XML and XSD or JSON
    - Any other representation supported by HTTP

---

## Java Web Service APIs

- In JEE7 there are two APIs for dealing with web services   
    - JAX-WS 2.2
        - The API for building and consuming SOAP web services
    - JAX-RS 2.0
        - The API for building and consuming REST web services
- Both APIs rely heavily on _annotations_ and _service provider interfaces_
    - Build your own custom business interface using normal and custom Java types
    - Use annotations to let JEE know how these business interfaces should be exposed as web services

---

## SOAP Web Services architecture

- SOAP architecture revolves around the following triad:
    - Web Services are discovered using UDDI
    - WSDL files are exchanged to specify the protocol bindings
    - SOAP is used for message exchange

![](images/generic-webservices-overview-soap-architecture.png#no-border)

---

## SOAP Web Services architecture

- SOAP <small>(Simple Object Access Protocol)</small>
    - XML-based communication protocol
    - Message-wrapping format
- WSDL <small>(Web Services Description Language)</small>
    - An XML format that describes the service contract <small>(name, parameters, ...)</small>
    - Also used for configuration and for generating client and server code
- UDDI <small>(Universal Description, Discovery and Integration)</small>
    - An XML-based registry for businesses to list themselves on the internet
        - E.g. "Yellow pages"
    - Each business can publish a service listing
    - Designed to be queried using SOAP messages and WSDL contracts

---

## SOAP Web Services architecture

- The SOAP architecture also benefits from a wide range of _WS-*_ specifications
    - Provide services such as:
        - _Quality of service_
        - _Security_
        - _Reliability_
        - _Non repudiation_
        - _Transactions_
        - _Policy requirements_ and _governance_
- SOAP is a good match for a number of use cases where tight integration is necessary
    - Business-to-business integration
    - Integrate applications within the same company, or over the internet when run by various organizations

???trainer
Think of tight integration as situations where data needs to be interchanged within the same company, or subsidiaries.
Also consider the banking world, where a high degree of trust is required to communicate between businesses.
???t

---

## SOAP Business Service

- To create a SOAP web service, you must start with a custom business interface

```java
public interface MovieSoapService {
    Movie findMovieById(int id);
    List<Movie> findMovies();
    int createMovie(Movie movie);
    boolean deleteMovie(int id);
    boolean updateMovie(Movie movie);
}
```

- This will become our web service contract interface

---

## Web Service Parameter Objects

- The interface uses a JAXB annotated `Movie` class

```java
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"title", "director", "actors"})
public class Movie {
    @XmlAttribute(required = false)
    private Integer id;

    @XmlElement(required = true)
    private String title;

    @XmlElement(required = true)
    private String director;

    @XmlElement(name = "actor", required = true)
    private List<String> actors = new ArrayList<>();

    // Constructors, getters and setters
}

```

---

## Web Service Implementation

- The business interface is implemented as usual
    - You can use `@Inject` to inject CDI dependencies

```java
public class MovieSoapServiceBean implements MovieSoapService {
    @Inject protected MovieRepository movieRepository; // Inject a dependency

    public Movie findMovieById(int id) {
        return movieRepository.findById(id);           // Delegate to collaborator
    }
    public List<Movie> findMovies() {
        return movieRepository.findAll();
    }
    public int createMovie(Movie movie) {
        movieRepository.create(movie);
        return movie.getId();
    }
    public boolean deleteMovie(int id) {
        return movieRepository.delete(id);
    }
    public boolean updateMovie(Movie movie) {
        return movieRepository.update(movie);
    }
}
```

---

## Web Service Annotations

- The business service can be exposed as a web service using annotations

```java
@WebService(name = "MovieService", portName="MovieServicePort")
public class MovieSoapServiceBean implements MovieSoapService {    
    @WebMethod(operationName = "GetMovie")    
    public @WebResult(name = "movie") Movie findMovieById(
            @WebParam(name = "movie_id") int id) { /* ... */ }

    @WebMethod(operationName = "FindMovies")
    public @WebResult(name = "movie") List<Movie> findMovies() { /* ... */ }

    @WebMethod(operationName = "CreateMovie")
    public @WebResult(name = "movie_id") int createMovie(
            @WebParam(name = "movie") Movie movie) { /* ... */ }

    @WebMethod(exclude = true)
    public boolean deleteMovie(int id) { /* ... */ }

    @WebMethod(operationName = "ReplaceMovie")
    @WebResult(name = "movie_replaced_status")
    public boolean updateMovie(
            @WebParam(name = "movie") Movie movie) { /* ... */ }
}
```

---

## Web Service Annotations

- There are quite a few annotations

| Annotation                | Purpose
|-
| `@WebService`             | Marks a service implementation as a web service<br/>Allows setting names for entries inside the WSDL
| `@WebMethod`              | Allows marking a service method to be exposed<br/>Allows customizing the name, and exclusions
| `@WebParam`               | Allow customizing the XML elements used for parameters
| `@WebResult`              | Allows customizing the XML element used for output
| `@OneWay`                 | Causes the server not to generate a HTTP response
| `@SOAPBinding`            | Allows customizing the WSDL's _soap binding_
| `@MTOM`                   | Allows configuration of binary attachments

---

## Accessing the web service

- The web service will be exposed by the server under the following URL
    - `http://localhost:8080/movies/MovieSoapServiceBean`
    - The WSDL is can be retrieved by adding `?wsdl`
- Can be used to create a client or import into a tool like SoapUI

![](images/jee7-jaxws-soapui.png)

---

## Top Down vs Bottom Up

- The previous examples are using a _bottom up_ or _code first_ approach
    - Many professional web service engineers prefer using a _top down_ or _contract first_ approach
- This requires manually authoring the WSDL and XSD contracts required by the service
    - Then you can use the `wsimport` tool provided by JavaSE to generate the JAXB classes and webservice interface
- Finally the developer only needs to implement the actual service implementation

---

## Exercise: JAX-WS

- (Optional) Add JAXB annotations to `Person` and `Address`
- Create a `PersonSoapService` and `PersonSoapServiceEndpoint` class
  - Use annotations to configure the Web Service in a bottom up approach
  - Retrieve all people from the database
- Try to access the `.wsdl` file from a browser
- Try to call the web service from SoapUI
- Extra:
  - Call the web service by creating a client from inside a unit test!

---

## REST Architecture

- REST is an architecture that can be implemented using any technology
    - It is a generic term, that has nothing to say about business architecture
    - HTTP is an implementation of REST principles
- RESTful web services have gained more popularity in the Web domain
    - Many key web players <small>(Amazon, Google, Yahoo!)</small> have moved to RESTful webservices
- The communication protocol is HTTP exclusively
- This makes REST simple and lightweight, with a high performance

---

## REST: Approach to Web Services

- Use POST, GET, PUT and DELETE as CRUD
    - Resources are linked and referenced using URIs
    - Can be bookmarked, listed, browsed to, etc...
- Use HTTP as uniform interface
    - Any HTTP-enabled client can connect to resources
    - If you know what the URI is, you can invoke the resource
- Resources are addressable
    - Allows to use resources in any way imaginable, even in ways you would not have expected
- Resources are connected to one another
    - Applications move from one state to the next by following a link
- REST is stateless
    - Every HTTP request is isolated from the other
    - Provides a better scalability
    
---

## REST: Resources and URIs

- In REST, every piece of information is a resource
    - A list of books from a publisher, the book "A Dance with Dragons", a job
- Resources are addressed using URIs
    - Typically links on the Web

| URI                                                       | Resource  
|-
| https://www.flickr.com/explore/2017/01/01                 | New pictures taken on new year 
| http://www.omdbapi.com/?t=Star+Wars&y=&plot=short&r=json  | Star Wars movies released
| https://weather.com/weather/today/l/USDC0001:1:US         | Weather forecase in Washington, DC
    
- Resources are acted upon using a set of simple, well defined operations
    - `GET`, `POST`, `PUT`, `DELETE`
- URIs must be as descriptive as possible and identify a unique resource
    - Note that the data for different URIs might be the same

---

## REST: Representations

- You might want the data represented as text, XML, PDF, an image or other data format
- The client always deals with a resource through its representation
    - The resource itself stays on the server
    - Representation is useful information about the state of the resource
- Different possibilities
    - Using URIs for each representation
    - Using content negotiation <small>(using HTTP headers)</small>
    - Example: https://www.flickr.com/services/api/explore/flickr.interestingness.getList

---

## REST: Uniform Contract

- Stateless protocol based on requests and responses exchanged between client and server
    - Messages exchanged are made of an envelope and a body (also called document or representation)
- HTTP uses different methods

| Method    | Purpose
|-
| `GET`     | Creates a new resource defined by the URI (side-effect-free)
| `POST`    | Creates or updates state of the resource (not _side-effect-free_)
| `PUT`     | Creates or replaces state of the resource (overrides all, not _side-effect-free)
| `DELETE`  | Deletes the resource (not side-effect-free)
| ...       | ...
    
---

## REST: Content Negotiation and Content Types

- Content negotiation allows agreeing between client and server over the most suited representation
    - Is based upon HTTP request headers
        - `Accept`, `Accept-Charset`, `Accept-Encoding`, `Accept-Language`, `User-Agent`
    - Content types correspond to Internet media types        

| Content type                              | Usage
|-
| `text/html`                                 | Representing web pages
| `text/plain`                                | Providing pure text data
| `image/gif`, `image/jpeg`, `image/png`      | Representing images
| `text/xml`, `application/xml`               | Representing XML data
| `application/json`                          | Representing JSON data
| ...                                         | ...

---

## REST: Status Codes

- Each time a response is received, an HTTP status code is associated with it

| Code Range    | Purpose
|-
| 1xx           | Informational
| 2xx           | Success
| 3xx           | Redirection
| 4xx           | Client error
| 5xx           | Server error

- Common status codes

| Code  | Purpose
|-
| 200   | OK (with data)
| 301   | Moved permanently
| 404   | Not found
| 500   | Internal server error

