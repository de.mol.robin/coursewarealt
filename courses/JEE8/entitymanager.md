# Managing Persistent Objects

---

## EntityManager

- Central to JPA to manipulate entities
- Entities manipulated by the `EntityManager` are **managed**
  - They will be synchronized with the database by the `EntityManager`
- Unmanaged or **detached** objects can be used in other layers
  - They become simple POJO's

---

## Obtaining an EntityManager

- Application managed

```java
public class Main {
  public static void main(String[] args) {

    // Creates an instance of book
    Book book = new Book("H2G2", "The Hitchhiker's Guide to the Galaxy", 12.5F,
    "1-84023-742-2", 354, false);

    // Obtains an entity manager and a transaction
*   EntityManagerFactory emf = Persistence.createEntityManagerFactory("bookPU");
*   EntityManager em = emf.createEntityManager();

    // Persists the book to the database
*   EntityTransaction tx = em.getTransaction();
    tx.begin();
    em.persist(book);
    tx.commit();

    // Closes the entity manager and the factory
    em.close();
    emf.close();
  }
}
```  

---

## Obtaining an EntityManager

- Container managed

```java
@Stateless
public class BookEJB {

* @PersistenceContext(unitName = "bookPU")
  private EntityManager em;

  public void createBook() {
    // Creates an instance of book
    Book book = new Book("H2G2", "The Hitchhiker's Guide to the Galaxy", 12.5F,
    "1-84023-742-2", 354, false);
    // Persists the book to the database
*   em.persist(book);
  }

}
```

- You can also `@Inject` an `EntityManager` if you produce it first

---

## Persistence Context

- Can be seen as a first level cache

![](images/em-firstlevelcache.png#no-border)

---

## Persistence Unit

- Bridge between the persistence context and the database

```xml
<?xml version="1.0" encoding="UTF-8"?>
<persistence xmlns="http://xmlns.jcp.org/xml/ns/persistence"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence
  http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd"
  version="2.1">
  <persistence-unit name="bookPU" transaction-type="RESOURCE_LOCAL">
    <provider>org.eclipse.persistence.jpa.PersistenceProvider</provider>
    <class>com.acme.example.Book</class>
    <class>com.acme.example.Customer</class>
    <class>com.acme.example.Address</class>
    <properties>
      <property name="javax.persistence.schema-generation.database.action" value="drop-and-create"/>
      <property name="javax.persistence.jdbc.driver" value="com.mysql.jdbc.Driver"/>
      <property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/cdbookstore"/>
      <property name="eclipselink.logging.level" value="INFO"/>
    </properties>
  </persistence-unit>
</persistence>
```

???trainer
Standard JPA Properties:

Property                                            | Description
:-------------------------------------------------- | :----------------------------------------------
javax.persistence.jdbc.driver                       | Driver class
javax.persistence.jdbc.url                          | Driver-specific URL
javax.persistence.jdbc.user                         | Username
javax.persistence.jdbc.password                     | Password
javax.persistence.database-product-name             | Database name (e.g. Derby)
javax.persistence.database-major-version            | Version number
javax.persistence.database-minor-version            | Minor version number
javax.persistence.ddl-create-script-source          | Create script
javax.persistence.ddl-drop-script-source            | Drop script
javax.persistence.sql-load-script-source            | Load script
javax.persistence.schema-generation.database.action | none, create, drop, drop-and-create
javax.persistence.schema-generation.scripts.action  | none, create, drop, drop-and-create
javax.persistence.lock.timeout                      | Pessimistic lock timeout in ms
javax.persistence.query.timeout                     | Query timeout in ms
javax.persistence.validation.group.pre-persist      | Groups targeted for validation upon pre-persist
javax.persistence.validation.group.pre-update       | Groups targeted for validation upon pre-update
javax.persistence.validation.group.pre-remove       | Groups targeted for validation upon pre-remove
???t

---

## Manipulating Entities

- The `EntityManager` can be seen as a generic DAO with CRUD

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "address_fk")
  private Address address;
  // Constructors, getters, setters
}
```

---

## Manipulating Entities

```java
@Entity
public class Address {
  @Id @GeneratedValue
  private Long id;
  private String street1;
  private String city;
  private String zipcode;
  private String country;
  // Constructors, getters, setters
}
```

![](images/em-customeraddress-tables.png#no-border)

---

## Persisting an Entity

- Inserts data into the database

```java
Customer customer = new Customer("Antony", "Balla", "tballa@mail.com");
Address address = new Address("Ritherdon Rd", "London", "8QE", "UK");
customer.setAddress(address);

tx.begin();
*em.persist(customer);
*em.persist(address);
// Data is flushed, first address is inserted, only then the customer
tx.commit();

// Check if both entities have received an identifier
assertNotNull(customer.getId());
assertNotNull(address.getId());
```

- If you need to pass entities by value, they must implement `java.io.Serializable`

---

## Finding by ID

- `find()` returns `null` if not found

```java
*Customer customer = em.find(Customer.class, 1234L);
if (customer!= null) {
  // Process the object
}
```

- `getReference()` does not retrieve data, returns an exception if not found

```java
try {
* Customer customer = em.getReference(Customer.class, 1234L);
  // Process the object
} catch(EntityNotFoundException ex) {
  // Entity not found
}
```

---

## Removing an Entity

- Deletes from the database, detaches, can no longer be synchronized
  - Is still accessible from Java code until it goes out of scope

```java
Customer customer = new Customer("Antony", "Balla", "tballa@mail.com");
Address address = new Address("Ritherdon Rd", "London", "8QE", "UK");
customer.setAddress(address);

tx.begin();
em.persist(customer);
em.persist(address);
tx.commit();

tx.begin();
// Only customer is deleted
// Depending on cascading, the address row could become an orphan
*em.remove(customer);
tx.commit();

// The data is removed from the database but the object is still accessible
assertNotNull(customer);
```

---

## Orphan Removal

- Orphans are not desirable for consistency

```java
@Entity
public class Customer {

  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;

  // Will automatically remove the Address
  // 1. When Customer is removed
  // 2. When the relationship is broken
* @OneToOne (fetch = FetchType.LAZY, orphanRemoval = true)
  private Address address;

  // Constructors, getters, setters
}
```

---

## Flushing an Entity

- Forces the persistence provider explicitly to flush the data
  - Does not commit the transaction!

```java
// This code will not work!
tx.begin();

em.persist(customer);

// Try to force the insert...
// Will fail because of the integrity constraint
*em.flush();

em.persist(address);

// The transaction will roll back
tx.commit();
```

---

## Refreshing an Entity

- Overwrites the current state of a managed entity with data as they are present in the database
  - To undo changes
  - To retrieve updates done in the database from outside the persistence context

```java
Customer customer = em.find(Customer.class, 1234L);
assertEquals(customer.getFirstName(), "Antony");

customer.setFirstName("William");

*em.refresh(customer);

assertEquals(customer.getFirstName(), "Antony");
```

---

## Contains

- Allows you to check whether or not a particular entity instance is currently managed by the entity manager

```java
Customer customer = new Customer("Antony", "Balla", "tballa@mail.com");

tx.begin();
em.persist(customer);
tx.commit();

*assertTrue(em.contains(customer));

tx.begin();
em.remove(customer);
tx.commit();

*assertFalse(em.contains(customer));
```

---

## Clear and Detach

- `clear()` empties the persistence context, causing all managed entities to become detached
- `detach()` removes the given entity from the persistence context

```java
Customer customer = new Customer("Antony", "Balla", "tballa@mail.com");

tx.begin();
em.persist(customer);
tx.commit();

assertTrue(em.contains(customer));

*em.detach(customer);

assertFalse(em.contains(customer));
```

---

## Merging an Entity

- Reattaches a detached entity to the persistence context
  - Synchronizes its state with the database
  - Only the returned entity becomes managed!

```java
Customer customer = new Customer("Antony", "Balla", "tballa@mail.com");

tx.begin();
em.persist(customer);
tx.commit();

*em.clear();

// Sets a new value to a detached entity
customer.setFirstName("William");

tx.begin();
*customer = em.merge(customer);
tx.commit();
```

---

## Updating an Entity

- When an entity is managed, changes to it will synchronize automatically

```java
Customer customer = new Customer("Antony", "Balla", "tballa@mail.com");

tx.begin();
em.persist(customer);

*customer.setFirstName("William"); // Automatically synchronized

tx.commit();
```

---

## Cascading Events

- Used to propagate an operation on the associations of an entity

```java
Customer customer = new Customer("Antony", "Balla", "tballa@mail.com");
Address address = new Address("Ritherdon Rd", "London", "8QE", "UK");
customer.setAddress(address);

tx.begin();
*em.persist(customer); // No need to persist address
tx.commit();
```

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  @OneToOne (fetch = FetchType.LAZY,
*   cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
  @JoinColumn(name = "address_fk")
  private Address address;
  // Constructors, getters, setters
}
```

---

## JPQL

- Lets you retrieve entities by criteria other than the `ID`
- Returns an entity or a collection of entities
- Object-oriented, by working on the domain model
- JPQL is mapped then transformed to SQL, called by JDBC
- `SELECT` and `FROM` are mandatory

```sql
SELECT b
FROM Book b
WHERE b.title = 'H2G2'
```
---

## Select and From

```sql
SELECT [DISTINCT] <expression> [[AS] <identification variable>]
expression ::= { NEW | TREAT | AVG | MAX | MIN | SUM | COUNT }
```

```sql
SELECT c FROM Customer c
```

```sql
SELECT c.firstName, c.lastName FROM Customer c
```

```sql
SELECT c.address FROM Customer c
```

```sql
SELECT c.firstName FROM Customer c
```

---

## Select and From

```sql
SELECT COUNT(c) FROM Customer c
```

```sql
SELECT c.address.country.code FROM Customer c
```

```sql
SELECT CASE b.editor WHEN 'Apress'
	THEN b.price * 0.5
	ELSE b.price * 0.8
END
FROM Book b
```

```sql
SELECT NEW com.acme.example.CustomerDTO(c.firstName, c.lastName, c.address.street1) FROM Customer c
```

```sql
SELECT DISTINCT c FROM Customer c
```

```sql
SELECT DISTINCT c.firstName FROM Customer c
```

---

## Where

```sql
=, >, >=, <, <=, <>, [NOT] BETWEEN, [NOT] LIKE, [NOT] IN,
IS [NOT] NULL, IS [NOT] EMPTY, [NOT] MEMBER [OF]
```

```sql
SELECT c FROM Customer c WHERE c.firstName = 'Vincent'
```

```sql
SELECT c FROM Customer c WHERE c.age > 18
```

```sql
SELECT c FROM Customer c
WHERE c.firstName = 'Vincent' AND c.address.country = 'France'
```

```sql
SELECT c FROM Customer c WHERE c.age NOT BETWEEN 40 AND 50
```

```sql
SELECT c FROM Customer c WHERE c.address.country IN ('USA', 'Portugal')
```

```sql
SELECT c FROM Customer c WHERE c.email LIKE '%mail.com'
```

---

## Binding Parameters

- Allows dynamic changes to the restriction clause of a query
- Positional parameters

```sql
SELECT c FROM Customer c WHERE c.firstName = ?1 AND c.address.country = ?2
```

- Named parameters

```sql
SELECT c FROM Customer c WHERE c.firstName = :fname AND c.address.country = :country
```

---

## Order By, Group By, Having and Subqueries

```sql
SELECT c FROM Customer c WHERE c.age > 18 ORDER BY c.age DESC
```

```sql
SELECT c FROM Customer c WHERE c.age > 18 ORDER BY c.age DESC, c.address.country ASC
```

```sql
SELECT c.address.country, count(c) FROM Customer c GROUP BY c.address.country
```

```sql
SELECT c.address.country, count(c) FROM Customer c GROUP BY c.address.country HAVING
  c.address.country <> 'UK'
```

```sql
SELECT c FROM Customer c WHERE c.age =
  (SELECT MIN(cust.age) FROM Customer cust)
```

---

## Bulk Delete and Bulk Update

- To delete large numbers of entities in a single operation

```sql
DELETE FROM <entity name> [[AS] <identification variable>] [WHERE <where clause>]
```

```sql
DELETE FROM Customer c WHERE c.age < 18
```

- To update many entities in a single operation

```sql
UPDATE <entity name> [[AS] <identification variable>] SET <update statement> {, <update statement>}*
  [WHERE <where clause>]
```

```sql
UPDATE Customer c SET c.firstName = 'TOO YOUNG TO DRIVE' WHERE c.age < 18
```

---

## Dynamic Queries

- Defined on the fly as needed by the application

```java
Query query = em.createQuery("SELECT c FROM Customer c");
List<Customer> customers = query.getResultList();
```

```java
TypedQuery<Customer> query = em.createQuery("SELECT c FROM Customer c", Customer.class);
query.setMaxResults(10);
List<Customer> customers = query.getResultList();
```

```java
String jpqlQuery = "SELECT c FROM Customer c";
if (someCriteria)
	jpqlQuery += " WHERE c.firstName = 'Betty'";
Query query = em.createQuery(jpqlQuery);
List<Customer> customers = query.getResultList();
```

```java
Query query = em.createQuery("SELECT c FROM Customer c where c.firstName = :fname");
query.setParameter("fname", "Betty");
List<Customer> customers = query.getResultList();
```

```java
Query query = em.createQuery("SELECT c FROM Customer c where c.firstName = ?1");
query.setParameter(1, "Betty");
List<Customer> customers = query.getResultList();
```

---

## Named Queries

- Static and unchangeable, but more efficient to execute

```java
@Entity
*@NamedQueries({
* @NamedQuery(name = Customer.FIND_ALL, query="select c from Customer c"),
  @NamedQuery(name = Customer.FIND_VINCENT,
  query="select c from Customer c where c.firstName = 'Vincent'"),
  @NamedQuery(name = Customer.FIND_WITH_PARAM,
  query="select c from Customer c where c.firstName = :fname")
})
public class Customer {
  public static final String FIND_ALL = "Customer.findAll";
  public static final String FIND_VINCENT = "Customer.findVincent";
  public static final String FIND_WITH_PARAM = "Customer.findWithParam";
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private Integer age;
  private String email;
  @OneToOne
  @JoinColumn(name = "address_fk")
  private Address address;
  // Constructors, getters, setters
}
```

---

## Named Queries

- Executing named queries is similar to dynamic queries

```java
TypedQuery<Customer> query = em.createNamedQuery(Customer.FIND_ALL, Customer.class);
List<Customer> customers = query.getResultList();
```

```java
Query query = em.createNamedQuery(Customer.FIND_WITH_PARAM);
query.setParameter("fname", "Vincent");
query.setMaxResults(3);
List<Customer> customers = query.getResultList();
```

- Query methods can be chained

```java
List<Customer> customers = em.createNamedQuery(Customer.FIND_WITH_PARAM, Customer.class)
.setParameter("fname", "Vincent")
.setMaxResults(3)
.getResultList();
```

---

## Criteria API or Object Oriented Queries

- Makes it possible to find developer mistakes at compile time

```sql
SELECT c FROM Customer c WHERE c.firstName = 'Vincent'
```

```java
*CriteriaBuilder builder = em.getCriteriaBuilder();
*CriteriaQuery<Customer> criteriaQuery= builder.createQuery(Customer.class);
*Root<Customer> c = criteriaQuery.from(Customer.class);
*criteriaQuery.select(c).where(builder.equal(c.get("firstName"), "Vincent"));
Query query = em.createQuery(criteriaQuery);
List<Customer> customers = query.getResultList();
```

```java
CriteriaBuilder builder = em.getCriteriaBuilder();
CriteriaQuery<Customer> criteriaQuery = builder.createQuery(Customer.class);
Root<Customer> c = criteriaQuery.from(Customer.class);
*criteriaQuery.select(c).where(builder.greaterThan(c.get("age").as(Integer.class), 40));
Query query = em.createQuery(criteriaQuery);
List<Customer> customers = query.getResultList();
```

---

## Type-Safe Criteria API

- Uses meta-model API classes to bring type-safety to entity attributes
  - Use a Maven plugin to generate static meta-model classes

```java
@Generated("EclipseLink")
@StaticMetamodel(Customer.class)
public class Customer_ {
  public static volatile SingularAttribute<Customer, Long> id;
  public static volatile SingularAttribute<Customer, String> firstName;
  public static volatile SingularAttribute<Customer, String> lastName;
  public static volatile SingularAttribute<Customer, Integer> age;
  public static volatile SingularAttribute<Customer, String> email;
  public static volatile SingularAttribute<Customer, Address> address;
}
```

```java
CriteriaBuilder builder = em.getCriteriaBuilder();
CriteriaQuery<Customer> criteriaQuery = builder.createQuery(Customer.class);
Root<Customer> c = criteriaQuery.from(Customer.class);
*criteriaQuery.select(c).where(builder.greaterThan(c.get(Customer_.age), 40));
Query query = em.createQuery(criteriaQuery);
List<Customer> customers = query.getResultList();
```

---

## Native Queries

- To use specific features of a database in native SQL

```java
*Query query = em.createNativeQuery("SELECT * FROM t_customer", Customer.class);
List<Customer> customers = query.getResultList();
```

```java
@Entity
*@NamedNativeQuery(name = "findAll", query="select * from t_customer")
@Table(name = "t_customer")
public class Customer {...}
```

---

## Stored Procedure Queries

- A stored procedure is a subroutine
  - Usually written in a proprietary language close to SQL and therefore not easily portable

```sql
CREATE PROCEDURE sp_archive_books @archiveDate DATE, @warehouseCode VARCHAR AS
	UPDATE T_Inventory
	SET Number_Of_Books_Left - 1
	WHERE Archive_Date < @archiveDate AND Warehouse_Code = @warehouseCode;

	UPDATE T_Transport
	SET Warehouse_To_Take_Books_From = @warehouseCode;
END
```

---

## Stored Procedure Queries

```java
@Entity
*@NamedStoredProcedureQuery(name = "archiveOldBooks",
procedureName = "sp_archive_books",
parameters = {
* @StoredProcedureParameter(name = "archiveDate", mode = IN, type = Date.class),
  @StoredProcedureParameter(name = "warehouseCode", mode = IN, type = String.class)
}
)
public class Book { ... }
```

```java
*StoredProcedureQuery query = em.createNamedStoredProcedureQuery("archiveOldBooks");
query.setParameter("archiveDate", new Date());
query.setParameter("warehouseCode", 1000);
query.execute();
```

---

## Cache API

![](images/em-cache.png#no-border)

---

## Cache API

- Allows code to query and remove some entities from the second-level cache in a standard manner

```java
@Entity
*@Cacheable(true)
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  // Constructors, getters, setters
}
```

- Select the caching mechanism to use with `shared-cache-mode` attribute in `persistence.xml`
  - `ALL`, `DISABLE_SELECTIVE`, `ENABLE_SELECTIVE`, `NONE`, `UNSPECIFIED`

---

## Cache API

```java
// The Customer entity is cacheable
Customer customer = new Customer("Patricia", "Jane", "plecomte@mail.com");

tx.begin();
em.persist(customer);
tx.commit();

// Uses the EntityManagerFactory to get the Cache
*Cache cache = emf.getCache();

// Customer should be in the cache
*assertTrue(cache.contains(Customer.class, customer.getId()));

// Removes the Customer entity from the cache
*cache.evict(customer);

// Customer should not be in the cache anymore
assertFalse(cache.contains(Customer.class, customer.getId()));
```

---

## Exercise: JPA Querying

- Create a class `PersonRepository` with the following methods
    - `Person findPersonById(long)`
    - `List<Person> findAllPeople()` (order by `lastName`, then `firstName`)
    - `void savePerson(Person)`
    - `void deletePersonById(long)`
    - `void updatePerson(Person)`
    - `int countAllPeople()`
- Create a class `CandyRepository` with the following methods
    - `double findAverageCandyPrice()`
    - `List<Candy> findCandyByNameLike(String)`
    - `List<Candy> findUniqueCandyForPersonOrderHistory(Person)`
- Use at least one _named query_ and one _dynamic query_

---

## Exercise: JPA Querying

![](images/jee7-jpa-exercise-queries.png#no-border)

---

## Transactional Concurrency

![](images/em-concurrency.png#no-border)

- Select a locking mechanism using the `LockModeType` enum
  - `OPTIMISTIC`, `OPTIMISTIC_FORCE_INCREMENT`
  - `PESSIMISTIC_READ`, `PESSIMISTIC_WRITE`, `PESSIMISTIC_FORCE_INCREMENT`
  - `NONE`

---

## Locking

- Read _then_ lock

```java
Book book = em.find(Book.class, 12);
// Lock to raise the price
*em.lock(book, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
book.raisePriceByTwoDollars();
```

- Read _and_ lock

```java
*Book book = em.find(Book.class, 12, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
// The book is already locked, raise the price
book.raisePriceByTwoDollars();
```

---

## Versioning

- Use `@Version`
  - `int`, `Integer`, `short`, `Short`, `long`, `Long`, or `Timestamp`

```java
@Entity
public class Book {
  @Id @GeneratedValue
  private Long id;
* @Version
* private Integer version;
  private String title;
  private Float price;
  private String description;
  private String isbn;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
}
```

---

## Versioning

- Is the core of optimistic locking

```java
Book book = new Book("H2G2", 21f, "Best IT book", "123-456", 321, false);

tx.begin();
em.persist(book);
tx.commit();

*assertEquals(1, book.getVersion());

tx.begin();
book.raisePriceByTwoDollars();
tx.commit();

*assertEquals(2, book.getVersion());
```

---

## Optimistic Locking

![](images/em-optimistic-locking.png#no-border)

- Applications are strongly encouraged to enable optimistic locking for all entities that may be concurrently accessed

---

## Pessimistic Locking

- Obtains a lock eagerly
- Results in significant performance degradation, because it requires expensive, low-level checks inside the database
- Useful in applications with great risks of contention
- Can be applied to entities that do not contain the `@Version` attribute

---

## Entity Life Cycle

![](images/em-entity-lifecycle.png#no-border)

---

## Life Cycle Callbacks

- Allow you to add your own business logic when certain life-cycle events occur

![](images/em-callbacks.png#no-border)

---

## Life Cycle Callbacks

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
  @Temporal(TemporalType.DATE)
  private Date dateOfBirth;
  @Transient
  private Integer age;
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;

* @PrePersist
* @PreUpdate
  private void validate() {
    if (firstName == null || "".equals(firstName))
    throw new IllegalArgumentException("Invalid first name");
    if (lastName == null || "".equals(lastName))
    throw new IllegalArgumentException("Invalid last name");
  }
  // ...
```

---

## Life Cycle Callbacks

```java
  // ...

* @PostLoad
* @PostPersist
* @PostUpdate
  public void calculateAge() {
    if (dateOfBirth == null) {
      age = null;
      return;
    }
    Calendar birth = new GregorianCalendar();
    birth.setTime(dateOfBirth);
    Calendar now = new GregorianCalendar();
    now.setTime(new Date());
    int adjust = 0;
    if (now.get(DAY_OF_YEAR) - birth.get(DAY_OF_YEAR) < 0) {
      adjust = -1;
    }
    age = now.get(YEAR) - birth.get(YEAR) + adjust;
  }

  // Constructors, getters, setters
}
```

---

## Life Cycle Listeners

- To extract the business logic from a callback to a separate class and share it between other entities

```java
public class AgeCalculationListener {
* @PostLoad
* @PostPersist
* @PostUpdate
* public void calculateAge(Customer customer) {
    if (customer.getDateOfBirth() == null) {
      customer.setAge(null);
      return;
    }
    Calendar birth = new GregorianCalendar();
    birth.setTime(customer.getDateOfBirth());
    Calendar now = new GregorianCalendar();
    now.setTime(new Date());
    int adjust = 0; if (now.get(DAY_OF_YEAR) - birth.get(DAY_OF_YEAR) < 0) {
      adjust = -1;
    }
    customer.setAge(now.get(YEAR) - birth.get(YEAR) + adjust);
  }
}
```

---

## Life Cycle Listeners

```java
public class DataValidationListener {
* @PrePersist
* @PreUpdate
  private void validate(Customer customer) {
    if (customer.getFirstName() == null || "".equals(customer.getFirstName()))
    throw new IllegalArgumentException("Invalid first name");
    if (customer.getLastName() == null || "".equals(customer.getLastName()))
    throw new IllegalArgumentException("Invalid last name");
  }
}
```

---

## Life Cycle Listeners

```java
*@EntityListeners({DataValidationListener.class, AgeCalculationListener.class})
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
  @Temporal(TemporalType.DATE)
  private Date dateOfBirth;
  @Transient
  private Integer age;
  // Constructors, getters, setters
}
```

---

## Default Listeners

- Have to be enabled in XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<entity-mappings xmlns="http://xmlns.jcp.org/xml/ns/persistence/orm"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence/orm
	http://xmlns.jcp.org/xml/ns/persistence/orm_2_1.xsd"
	version="2.1">
	<persistence-unit-metadata>
		<persistence-unit-defaults>
			<entity-listeners>
				<entity-listener class="com.acme.example.DebugListener"/>
			</entity-listeners>
		</persistence-unit-defaults>
	</persistence-unit-metadata>
</entity-mappings>
```

- You can exclude default listeners with `@ExcludeDefaultListeners`

---

## Exercise: JPA Concurrency and Listeners

- Implement _optimistic locking_ for `Person` and `Candy`
    - Add a _version property_
    - Make sure concurrent modifications are rejected
- Add a property `userModified` to `Tracked`
- Use an _entity listener_ to automatically fill in the following fields for `Tracked`
    - `userCreated` (filled in upon persist)
    - `userModified` (filled in upon persist, and update)
- Make sure the `age` property is automatically calculated from `birthDate` when loading a `Person` from the database
