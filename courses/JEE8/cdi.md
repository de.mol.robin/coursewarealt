# Jakarta Contexts and Dependency Injection (CDI)

---

## Rundown

- Since JEE 6 (2009)
- Annotation-driven, descriptor `beans.xml` needed
- Can be used to turn _any_ POJO into injectable, managed bean
- Destined to become the central spec of JEE on which other specs build
- Vital spec to allow decomposition of EJB into opt-in features
- Key features
    - Contexts to bind a bean's lifecycle to
    - (Type safe) dependency injection
- Other features
    - Qualifiers
    - Events (synchronous and asynchronous)
    - Decorators
    - Interceptor bindings
    - Integration with Expression Language (EL)
        - Important for JSP/JSF

???trainer

CDI has a tumultuous history and has mixed support, but was very well received by the JEE development community.
While originally, CDI was intended to bring EJB and JSF closer together, it has become the de facto model for DI.
Near future plans for JEE involve reworking existing specs so they use CDI (instead of their own, internal model; examples: JSF managed beans, JAXRS contexts, Resources, Persistence Contexts...).

???t

---

## The CDI deployment descriptor

- Deployment descriptor: `beans.xml`
    - Put it in `/META-INF/` (JAR) or `/WEB-INF/` (WAR)
        - Maven: `src/main/webapp/WEB-INF/beans.xml`

```xml
<beans bean-discovery-mode="annotated">
</beans>
```

- `bean-discovery-mode`:
    - `annotated`: only classes with a class-level scoping annotation are processed (default)
    - `all`: all classes are processed
    - `none`: CDI is effectively disabled (handy if you have another DI framework)

---

## Scopes

| Scope (* = Serializable) | Description
|-
| `@ApplicationScoped`     | Spans the entire duration of the application
| `@SessionScoped`(*)      | Spans across several HTTP requests or several method invocations for a single user's session
| `@ConversationScoped`(*) | Conversations are used across multiple pages as part of a multistep workflow
| `@RequestScoped`         | Corresponds to a single HTTP request or a method invocation
| `@Dependent`             | The life cycle is same as that of the injection point. This is the default scope for CDI

- Each scope matches a "context" object
- Scope defines the bean lifecycle
- `@Dependent` is a pseudo-scope (vs "normal" scopes):
    - There is no proxy, references are injected directly

???trainer
- Even allows injection into non-managed objects!

Don't confuse these scope annotations with the ones from JSF. All CDI scopes with the exception of `@ViewScoped`
are in the `javax.enterprise.context` package.

Explain "contextual" scope

CDI managed bean is any java class for which all of these hold:
    - Not a static inner class
    - Concrete class, or annotated with `@Decorator`
    - NOT an EJB (annotated or declared in `ejb-jar.xml`)
    - Has appropriate constructor:
        - No parameters
        - Annotated with `@Inject`

???t

---

## `@Produces`

- Make fields and method return values injectable
    - Primitives (+ wrappers) and friends
        - i.e. `int`, `long`, but also `BigDecimal`, `Date`
    - Arrays
    - POJOs that are not CDI capable by themselves
        - i.e. have special way to create/configure them like: factory, builder...
- Default scope is `@Dependent`, but can be specified
- Has matching `@Disposes` to do cleanup (rare)

```java
public class NumberProducer {
  @Produces
  private String prefix13digits = "13-";

  @Produces
  public double random() {
    return Math.abs(new Random().nextInt());
  }
}
```

???trainer
allow injecting values that are not supported by CDI as is

live coding example: make the entity manager injectable

$$ `@Produces` usage

- Typical usage is to turn typical JEE (but non-CDI resources) injectable:

```java
public class EntityManagerProducer {
    @PersistenceContext(...)
    @Produces
    private EntityManager entityManager;
}
```

???t

---

## InjectionPoint API

- Sometimes objects need to know something about the _injection point_ into which they are injected

```java
// Without CDI:
Logger log = Logger.getLogger(BookService.class.getName()); // need to know about BookService

// With CDI
*@Inject
Logger log; // <-- this is an _injection point_ for CDI
```

- Producers can receive `InjectionPoint` reference as a parameter

```java
public class LoggingProducer {
* @Produces
* public Logger createLogger(InjectionPoint injectionPoint) {
    // The logger's category class name will be automatically set
    return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
  }
}
```

???trainer

Live coding example: make JUL loggers injectable

???t

---

## Injection Points (in JEE)

- Property injection

```java
*@Inject
private NumberGenerator numberGenerator;
```

- Constructor injection

```java
*@Inject
public BookService (NumberGenerator numberGenerator) {
  this.numberGenerator = numberGenerator;
}
```

- Certain methods
    - Setters
    - Event observers
    - Producers
    - Disposers

---

## Qualifiers

- Allow narrower bean identification than type alone
- Can be put on beans and injection points
- Can have members
    - Also need to match unless those marked as `@NonBinding`
- If multiple are specified (on injection point), all (of injected bean) must match

```java
@Qualifier
@Retention(RUNTIME)
@Target({FIELD, TYPE, METHOD})
public @interface MyQualifier{ }
```

???trainer

Why qualifiers?
Because they are much more flexible than using (combinations) of interfaces and subtypes.
They also decouple the injection point from the actual bean being injected.

The "interface with multiple implementations" example is not a good one for EE developers because typically each interface will only have 1 implementation ("the highlander principle").
A much better example is to be able to disambiguate between injectable simple types, like Strings or lists of certain objects.
A project may easily have multiple producers for such simple types, requiring a means of disambiguation.

???t

---

### Qualifier stereotypes

- `@Stereotype` meta annotation
- Combines multiple qualifiers into a single qualifier
- Must imply a scope (`@Dependent` if not specified)
    - Using other scope on injection point overrides stereotype scope
- Example: `@Model`

```java
@Named
@RequestScoped
@Stereotype
@Target({TYPE, METHOD, FIELD})
@Retention(RUNTIME)
public @interface Model {}
```

---

### Special qualifiers

- `@Default`:
    - Automatically applied to beans without any other qualifiers
- `@Any`:
    - Automatically applied to all beans
- `@Alternative`:
    - To override the default injectable (useful when default is not in your own code)
- `@Named`:
    - Ensures the bean is EL discoverable
- `@Typed`:
    - Manually specify the discoverable types of a bean

---

## Instance

- Sometimes, injection can not be configured at compile time
    - The bean type or qualifiers vary dynamically at runtime
    - There may be no bean which satisfies the type and qualifiers
    - We would like to iterate over all beans of a certain type.

```java
@Inject @Any
Instance<NumberFormatter> nfs;
```
- `Instance<>` injection point is for programmatic lookup
    - Every bean has `@Any` qualifier implicitly
    - `get()` gets matching bean (if exactly 1)
    - `for(var nf: nfs)`: iterate over all eligible beans
    - `select(Annotation)` to "filter" instances (chain with `get`)

---

## Other topics

- Events
    - Observer/subscriber pattern
    - Event payloads may be qualified
    - Synchronous by default, but may be async (under heavy conditions)
- Decorators (with `@Delegate`)
- `@Specializes`
- Usage in Java SE (bootstrap Weld)

???trainer
Why are events nice?
Consider that you are writing code and it is known that when a book is added, something else must happen.
You could inject the other service, and call it, but this is a form of tight coupling.
If later, another component needs to be added and it must also be activated when a book is added, the book service needs to be changed to inject the new component and to call it.
With an even-driven architecture, this is not the case, the new component can simply observe the correct event, and it will be activated by the CDI framework.

???t

