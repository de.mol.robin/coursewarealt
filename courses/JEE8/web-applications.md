# Jakarta Server Faces (JSF)

???trainer

TODO: check the order of the slides to see if it makes sense.
Test this by trying to make a JSF project from scratch.
Are the subjects presented in more or less the order that you encounter/use them?

???t

---

## Rundown

- Since JEE 5 (2006)
- Web application development framework
- Component-based
- Extensible and customizable
- Stateful
- Steep learning curve
- Heavily focussed on form submits (postback)
- Notable features
    - Facelets
    - Managed beans + scopes (superseded by CDI since JEE 6)
    - AJAX
    - FacesContext
    - 6-phase request life cycle

???trainer

components may be rendered in different ways (uicommand > button or link)

???t

---

### History

- Originally
    - Servlets (JEE 1)
    - JSP (JEE 1)
        - Dynamic html templates (`.jsp` files)
        - JSTL: tag libraries (add `if/else`, `for`... to html templates)
        - EL: embedded java in `.jsp` files: `${...}`
- Now
    - JSF (JEE 5, 2006)
        - Stateful server-side MVC framework
        - Facelets as view technology (was originally JSP)
    - CDI (JEE 6, 2009)
        - Context-aware, scope-bound, managed "beans"
    - (u)EL (JEE 5, 2006)
    - JSTL (JEE 5, 2006)
    - Extra tag libraries

---

### MVC Achitecture

<img src="images/jsf-introduction-architecture-breakdown.png#no-border"/>

???trainer

Explain during MVC what actually happens:
- Component tree is created based on root facelet (view template)
- Request data is translated to Java and code is executed
- Resulting component tree is rendered to HTML and returned

- MVC
    - C: `FacesServlet`
        - Handles _all_ JSF requests
    - V: Facelets (`.xhtml` files)
        - Definition of the view tree (composition)
    - M: (CDI) managed beans
        - Java logic + data bindings
- Component-based view tree
    - `UIComponent` base class for state and behavior
    - View tree root: `UIView`
    - Renderers to generate HTML
    - Event and listener for users to interact with things
    - Validation of user input with message feedback
    - XHTML tag

???t

---

### Phases 1 and 6: Restore View and Render Response

<img src="images/jsf-lifecycle.png#no-border" style="float:right"/>
- Restore view:
    - Each visit:
        - Create `UIViewRoot` from facelet
        - New instance of `FacesContext` is created
    - First visit:
        - Skip immediately to "Render response"
    - Next visit(s) (postbacks):
        - Build entire component tree
        - Restore view state
        - Proceed to "Apply request values"
- Render response: render view tree (generate HTML)
    - First visit:
        - Build entire component tree
    - Each visit:
        - Recursively encode view components
        - Save view state

???trainer

About view state:

- UIComponents representing the view tree are discarded after each request
- Any changes from default values are serialized in the "View state"
    - This happens during view rendering
    - Postbacks use it to restore the view exactly as it was before (during phase 1)
        - advantage for developer (no need to do this manually)
        - security advantage (protects against replay attacks)
    - Contains mostly internal properties of UIComponents backing form inputs:
        - validity
        - disabled state
        - visibility
        - submitted (raw) value
        - ...
- Storage of state can be server-side (default) or client-side
    - When server-side, this is stored in the HTTP session
        - a hidden input with a reference is automatically included in each form
- Can opt-out entirely by marking view as "transient"

???t

---

### Phases 2 and 3: Apply Request Values and Process Validations

<img src="images/jsf-lifecycle.png#no-border" style="float:right"/>
- Apply Request Values
    - Components update their values based on request params
    - This may trigger errors
        - JSF will skip to render phase
    - Events are broadcasted to listeners
- Process Validations
    - Values are first converted from text to Java
        - Conversion errors are queued
    - Each component will validate its (converted) value
        - Validation errors are queued
    - This may include external validators
    - If there are queued errors
        - JSF will skip to render phase
    - Events are broadcasted to listeners

---

### Phases 4 and 5: Update Model Values and Invoke Application

<img src="images/jsf-lifecycle.png#no-border" style="float:right"/>
- Update Model Values
    - All the values of the backing beans or model objects associated with components will be updated
    - Possible conversion errors will be reported and queued in `FacesContext`
    - Events are broadcasted to listeners
- Invoke Application
    - Registered action listeners are called
        - Actions for the pressed commandButton / commandLink
        - Event listeners
    - Navigation rules are examined to choose the next view to display
    - Events are broadcasted to listeners

---

## JSF in a nutshell

- Create a JSF descriptor: `WEB-INF/faces-config.xml`
- Activate CDI: create `WEB-INF/beans.xml`
- Register the `FacesServlet` in `WEB-INF/web.xml`
- Facelets (`.xhtml`) to define view in `webapp/`
    - import taglibs:
        - `h:` > `http://xmlns.jcp.org/jsf/html` 
        - `f:` > `http://xmlns.jcp.org/jsf/core` 
        - ...
    - use `<h:head>` + `<h:body>`
        - enable JS + relocatable resources (CSS, ...)
- One or more backing beans (models) to supply data
    - must follow JavaBeans conventions (getter/setter pairs)
- Facelet-model interaction:
    - Expression Language to reference models from Facelets
    - `<f:metadata>`
        - `<f:viewParam>`: bind (req) param to backing bean
        - `<f:viewAction>`: run method on page load

???trainer

other tag libs

| Library | Prefix | Contents
|-
| JSF HTML | h: | JSF provided common UI components + HTML
| JSF Core | f: | JSF actions (independent of renderer)
| Pass-through elements | jsf: | Force JSF to manage plain html tags
| Pass-through attributes | p: | For attributes to be ignored by JSF
| JSF Facelets | ui: | templating
| Composite components | cc: | combine components into supercomponent
| JSTL Core (only when necessary) | c: | Original JSP/JSTL core library
| JSTL Functions (only when necessary) | fn: | Original JSP/JSTL functions

???t

---

## (Unified) Expression Language (EL)

- `#{...}` syntax
    - old `${...}` syntax from JSP supported but not recommended (works differently)
- Supports basic scripting
    - Arithmetic: `+`, `-`, `*`, `/` (div), `%` (mod)
    - Relational: `==` (eq), `!=` (ne), `<` (lt), `>` (gt), `<=` (le), `>=` (ge)
    - Logical: `&&` (and), `||` (or), `!` (not)
    - Other: `()`, `empty`, `[]`, `.`
- Working with collections:
    - Maps: `someMap['name']` or `someMap.name`
    - Lists/sets: `someList[0]`
- Accessing managed objects
    - ["Implicit objects"](https://eclipse-ee4j.github.io/jakartaee-tutorial/jsf-custom013.html#BNATJ)
        - Servlet API objects: `param`, `header`, `cookie`, `request`, `session`, `application`, `initParam`, ...
        - Scopes: `requestScope`, `viewScope`, `flash`, `sessionScope`, `applicationScope`
        - JSF objects: `facesContext`, `view`, `component`, `cc`
    - Custom objects: MVC models!

???trainer

has a lot of similarities with JS

???t

---

## Converters

- Many standard converters are included (basic types)
- Number formatter: [`<f:convertNumber>`](https://eclipse-ee4j.github.io/jakartaee-tutorial/jsf-page-core001.html#BNASX) 
    - Percentage, currency, specific format...
- DateTime formatter: [`<f:convertDateTime>`](https://eclipse-ee4j.github.io/jakartaee-tutorial/jsf-page-core001.html#BNASV)
    - use `type` attribute to match datatype in Java
        - default is (old and deprecated) `Date`!
- Make your own:
    - Implement interface `Converter<T>`
    - Annotate with `@FacesConverter`
        - `value` to define "converterId"
        - `managed="true"` to allow injections (!!)
    - Throw `ConverterException` if the conversion fails

???trainer

Without `managed="true"`, it's not possible to inject EJBs/CDI beans into your converter class!

???t

---

## Static resources

- CSS: `<h:outputStylesheet name="..."/>`
- Images: `<h:graphicImage name="..."/>`
- JS: `<h:outputScript name="..."/>`
- Resolved from `webapp/resources/...`
    - (!!) NOT `src/main/resources`
    - must follow [strict structural guidelines](https://jakarta.ee/specifications/faces/3.0/jakarta-faces-3.0.html#a758)
        - `[locale/][library/][libraryVersion/]resource[/resourceVersion]`
        - simplest form is flat folder with plain files
- By default, your browser will cache resources!!
    - clear cache reload if you don't see changes (ctrl+F5)
    - configure "cachebusting" in JSF
        - create custom `ResourceHandlerWrapper`

???trainer

cachebusting by adding last modified version query param to each resource automatically

```java
public class VersionResourceHandler extends ResourceHandlerWrapper {
    public VersionResourceHandler(ResourceHandler wrapped) {
        super(wrapped);
    }
    @Override
    public Resource createResource(String name, String library) {
        Resource resource = super.createResource(name, library);
        if (resource == null || library != null) {
            return resource;
        }
        return new ResourceWrapper(resource) {
            @Override
            public String getRequestPath() {
                String url = super.getRequestPath();
                return url
                    + (url.contains("?") ? "&" : "?")
                    + "v=" + getLastModified();
            }
            private long getLastModified() {
                try {
                    return getWrapped().getURL()
                        .openConnection().getLastModified();
                }
                catch (IOException ignore) {
                    return 0;
                }
            }
        };
    }
}
```

```xml
<application>
    <resource-handler>
        com.example.project.resourcehandler.VersionResourceHandler
    </resource-handler>
</application>
```

???t

---

## Backing beans

- `@Model` is actually short for `@Named` + `@RequestScoped`
    - `@Named`: allows EL to reference the bean
    - `@RequestScoped`: bean will be created and destroyed per request (no state)
- All existing scopes (sorted from longest to shortest):

| Scope (* = Serializable) | Description
|-
| `@ApplicationScoped`     | Spans the entire duration of the application
| `@SessionScoped`(*)      | Spans across several HTTP requests or several method invocations for a single user's session
| `@ConversationScoped`(*) | Conversations are used across multiple pages as part of a multistep workflow
| `@FlowScoped`(*)         | Flow is a sequence of pages like a wizard
| `@ViewScoped`(*)         | From package `javax.faces.view`. Spans as long as the user stays on the same page
| `@RequestScoped`      | Corresponds to a single HTTP request or a method invocation
| `@Dependent`          | The life cycle is same as that of the injection point. This is the default scope for CDI

- Best practice: use the shortest lifespan possible
- Do not use EJBs as models

???trainer
Mention these are from CDI, but that we will come back to it later.
Mention that `@Model` uses request scoped to encourage using that scope as much as possible

Design tips:

- Use shortest possible lifespan
    - RequestScoped is usually fine
    - ViewScoped common when using AJAX
- Do not use EJBs as models
    - Do not interact with the database directly from your models
    - It's OK to inject EJBs in your models though
- Facelets can interact with multiple models
- Models can be injected into each other

???t

---

## [Data tables](https://eclipse-ee4j.github.io/jakartaee-tutorial/jsf-page002.html#BNARZ)

- Showing list of objects in a table
- `<h:dataTable>`
    - Column oriented: `<h:column>`
        - Can nest facets to add things, like: `<f:facet name="header">`
    - `value`: indicates data to iterate over
        - array, `java.util.List`, `java.sql.ResultSet`...
    - `var`: indicates locally scoped variable for each data element
- You give one row definition, and JSF repeats it for you
- Is a naming container
    - guarantees auto-generates unique `id` for nested elements

???trainer

very quick and easy way to show collections, but falling out of favor ("table bad")
Use `ui:repeat` if you need loops otherwise

(Other naming containers: `<h:form>`, `<ui:repeat>`)

???t

---

## Navigation

- `<h:link outcome="..."/>` (preferred) or `<h:button/>`
    - Rule-based (legacy)
        - defined in `faces-config.xml`
        - String is mapped to facelet file
    - Implicit navigation
        - If no rule matches, outcome is treated as file name
- Can take parameters: nested `<f:param/>`

???trainer

Link is preferred to button because link generates an anchor tag (natural html link) whereas button relies on javascript to navigate.

Since outcome can take EL as value, links can be dynamic

???t

---

## Forms

- `<h:form>`
    - automatically does POST to same URL of page
- Inputs:
    - use `value` to bind to model property
        - file upload: use `Part` datatype in Java
    - select one/many options:
        - static: some `<f:selectItem>` tags
        - dynamic: `<f:selectItems>`
    - label: `<h:outputLabel/>`
- Submit (`<h:commandButton>`):
    - use `action` to bind to model method
    - return String to navigate
        - `null` to stay on page
        - append `?faces-redirect=true` to ensure "post-redirect-get"

---

### Validation

- JSF validation
    - nest validator tag inside component tag
        - reference provided [standard validator](https://eclipse-ee4j.github.io/jakartaee-tutorial/jsf-page-core003.html)
        - reference custom validator
    - use `validator` attribute on `input` component
        - reference validation method on model bean
- Jakarta Bean Validation also works!
    - but class-level (custom) constraints require special attention...
        - Model class needs to be a CDI bean
        - Activate class-level support (in `web.xml`)
            - `javax.faces.validator.BeanValidator.ENABLE_VALIDATE_WHOLE_BEAN_PARAM_NAME`
        - Put `<f:validateWholeBean/>` inside (at end of) form

---

### Showing validation error messages

- Error message has summary, detail and id
    - id links to specific input
    - id can be null to indicate "global" error message
- `<h:message for="...">`
    - only shows first message (if there are multiple)
- `<h:messages>`
    - shows all messages
        - for a specific component using `for`
    - includes global error messages
        - use `globalOnly` to show only global errors
- Programmatic:
    - through `FacesContext`'s `addMessage`

---

## Designing facelets (templating)

- Possible to define facelet "snippets" (`<ui:composition>`)
    - can be parameterized
        - simple params (through EL)
        - content placeholders
    - can be injected into other facelets
    - `http://xmlns.jcp.org/jsf/facelets` (`ui:`)
    - commonly used to make a "master layout" template
        - define look an feel once for simple reuse on all pages
- Tags
    - Create your own tags for bundling existing components
    - Consider this instead of snippets with many parameters
- Composite Components
    - Create own JSF components (tags)
    - Mostly used in case you have multiple tags that need to share a single model
    - `http://xmlns.jcp.org/jsf/composite` (`cc:`)

???trainer

A typical example for CC is a date picker which consists of different inputs to let the user select a year, month and day.
Not only is there a single model property behind these three inputs, but the values for day must be adapted if month is changed.

???t

---

### Facelets Templates

| Element               | Description
|-
| `<ui:insert>`         | Content placeholder in a template
| `<ui:include>`        | Inserts the content of a snippet
| `<ui:composition>`    | Applies a template to a page, or acts as a container for an included file
| `<ui:decorate>`       | Like `include`, but template may contain placeholders
| `<ui:define>`         | Define content for a placeholder (nest inside decorate or composition)
| `<ui:param>`          | Define simple param (will become EL object in template)
| ...                   | ...

- Remember:
    - `include` is for re-using _content_
    - `composition` is for re-using _layout_

---

### Facelets Templates: Decoration

- `ui:decorate` allows you to combine re-using content and layout
- Example of a decoratable snippet:
    - Commonly put in `/WEB-INF/decorations/`

```html
<ui:composition>
    <section class="contact">
        <header><ui:insert /></header> <!-- anonymous insert: ok if there is only one -->
        <nav>
            <ul>
                <li>✆ <a href="tel:+31612345678" title="Phone">+31 (0)6 1234 5678</a></li>
                <li>✉ <a href="mailto:info@example.com" title="Email">info@example.com</a></li>
            </ul>
        </nav>
    </section>
</ui:composition>
```
- Now include this and define content:

```html
<ui:decorate template="/WEB-INF/decorations/contact.xhtml">
    <h2>Questions? Contact us!</h2> <!-- anonymous insert is used when define is not specified -->
</ui:decorate>
```

???trainer

Note that if there is only 1 (anonymous) insert placeholder, it's not necessary to specify it explicitly with define

???t

---

### Facelets tag files

- `<ui:include>` may use `<ui:param>`, but don't go overboard...

```html
<ui:include src="/WEB-INF/includes/field.xhtml">
    <ui:param name="id" value="firstName" />
    <ui:param name="label" value="First Name" />
    <ui:param name="value" value="#{profile.user.firstName}" />
</ui:include>
```
```html
<ui:composition xmlns:="http://www.w3.org/1999/xhtml" <!-- namespace imports omitted for brevity --> >
  <div class="field" jsf:rendered="#{rendered ne false}">
    <h:outputLabel id="#{id}_l" for="#{id}" value="#{label}" />
    <c:choose>
      <c:when test="#{type eq 'password'}">
        <h:inputSecret id="#{id}" label="#{label}" value="#{value}"></h:inputSecret>
      </c:when>
      <c:when test="#{type eq 'textarea'}">
        <h:inputTextarea id="#{id}" label="#{label}" value="#{value}"> </h:inputTextarea>
      </c:when>
      <!-- More types can be added as c:when here -->
      <c:otherwise>
        <h:inputText id="#{id}" label="#{label}" value="#{value}" a:type="#{type}"> </h:inputText>
      </c:otherwise>
    </c:choose>
    <h:messages id="#{id}_m" for="#{id}" styleClass="messages" />
  </div>
</ui:composition>
```

---

### Facelets tag files

- Instead, consider creating a tag file
    - Create the file `/WEB-INF/mytags.taglib.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<facelet-taglib xmlns:="http://xmlns.jcp.org/xml/ns/javaee"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
                                    http://xmlns.jcp.org/xml/ns/javaee/web-facelettaglibrary_2_3.xsd"
                version="2.3">
    <namespace>http://example.com/tags</namespace>
    <short-name>t</short-name>
    <tag>
        <description>Renders label + input + message field.</description>
        <tag-name>field</tag-name>
        <source>tags/field.xhtml</source>
        <attribute>    <!-- attribute descriptions are optional but useful for IDE autocompletion -->
            <description>The type of the input component.</description>
            <name>type</name>
            <required>false</required>
            <type>java.lang.String</type>
        </attribute>
        <!-- other attribute definitions -->
    </tag>
</facelet-taglib>
```

---

### Facelets tag files

- Next, load the taglib in `web.xml`:
    - Normally not necessary if your filename ends in `.taglib.xml`

```xml
<context-param>
    <param-name>javax.faces.FACELETS_LIBRARIES</param-name>
    <param-value>/WEB-INF/mytags.taglib.xml</param-value>
</context-param>
```
- Then, you can use the tag file by importing your own namespace:

```html
<ui:composition template="..."
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:ui="http://xmlns.jcp.org/jsf/facelets"
*               xmlns:t="http://example.com/tags">
    <!-- ... -->
    <t:field type="email"
             id="email"
             label="Email"
             value="#{login.email}">
        <!-- if there is a ui:insert in the tag file, you can define it here -->
    </t:field>
</ui:composition>
```

---

## Other tag libs

- Passthrough attributes
    - `http://xmlns.jcp.org/jsf/passthrough` (`p:`, sometimes `a:`)
    - allow adding any attribute verbatim on rendered html
- Passthrough elements
    - `http://xmlns.jcp.org/jsf` (`jsf:`)
    - allow forcing JSF to treat a plain HTML tag as a facelet tag
        - will do a best-effort guess what `UIComponent` to make
- [JSTL](https://jakarta.ee/specifications/faces/3.0/vdldoc/jstl-core/tld-frame.html)
    - `c:`, `http://xmlns.jcp.org/jstl/core`
    - origins in JSP
    - mostly legacy, but useful parts reimplemented for JSF
    - executed during view build, not view render
        - main use: dynamically manipulate view tree
- [PrimeFaces](https://www.primefaces.org/#) (unofficial)
    - high quality, professional components
    - also available for angular, react, vue
- [OmniFaces](https://showcase.omnifaces.org/) (unofficial)
    - full of useful utilities (but no UI components)
    - written by BalusC

???trainer

When HTML5 was released, the JSF developers chose to make 2 new libs (passthrough elems + attrs).
Why didn't they just add new tags, or attributes to the existing tags?
The answer is that that solution is not future-proof: with each new HTML version, they would have to release a new version with new tags/attrs too...
This passthrough solution will keep working for all new HTML versions as-is!

???t

---

## AJAX

- _Asynchronous JavaScript And Xml_
- Client calls server to do something, without navigating
- Supported by JSF out of the box
    - No need to write any JavaScript (though you can)
- Several features of JSF are used to support AJAX
    - Partial submits
    - Partial re-render
    - The [`<f:ajax>`](https://eclipse-ee4j.github.io/jakartaee-tutorial/jsf-ajax003.html) tag
    - A supporting JavaScript library
- Auto-enabled with `<h:head>` and `<h:body>`
- Works best with `@SessionScoped` backing beans

---

### `<f:ajax>`

- can be nested inside almost all components

| Attribute     | Purpose                                               | Default
|-
| `event`       | JavaScript event trigger (click, change, blur...)     | depends on component
| `execute`     | component(s) values to send to the server (IDs)       | `@this`
| `render`      | parts to be redrawn on the page (IDs)                 | `@none`
| `listener`    | Java method to execute                                | /

- [ID resolution](https://javaee.github.io/javaee-spec/javadocs/javax/faces/component/UIComponent.html#findComponent-java.lang.String-)
    - No prefix > relative to the current naming container
    - Prefix `:` > denotes absolute ID (outside of naming container)
    - Prefix `@` > used for special search expressions
        - `@all`: Entire page
        - `@none`: Nothing
        - `@this`: Current component
        - `@form`: Parent form

???trainer

Eligible components for `<f:ajax>`:
```
<h:body>, <h:commandButton>, <h:commandLink>, <h:dataTable>, <h:form>, <h:graphicImage>, <h:inputFile>,
 <h:inputSecret>, <h:inputText>, <h:inputTextarea>, <h:button>, <h:link>, <h:outputLabel>, <h:outputLink>,
  <h:panelGrid>, <h:panelGroup>, <h:selectBooleanCheckbox>, <h:selectManyCheckbox>, <h:selectManyListbox>,
  <h:selectManyMenu>, <h:selectOneListbox>, <h:selectOneMenu>, <h:selectOneRadio> and <f:websocket>.
```

- Default `event` depends on component type:
    - `valueChange` for editable value holders
    - `action` for action sources
    - `click` for everything else

A few new search expressions since 2.3: @namingcontainer, @root, @id(id), @parent, @next, @previous...
Mention that: putting "@all" for render and execute essentially does the same as a full submit!

Explain naming containers (= component which prepends its own id automatically to all nested child components).
Show this live coding example, e.g. using a form component.

???t

---

### AJAX lifecycle

- AJAX requests are similar to "regular" JSF requests, except:
    - Phase 2 only restores "execute" inputs (defaults to `@this`)
    - Phase 6 only returns "render" outputs (defaults to `@none`)
    - Phases 1, 3, 4 and 5 are identical
- Using `@all` for `render` and `execute` is essentially a normal submit!

---

## Exception handling

- use servlet's error pages
    - define `<error-page>` in `web.xml`
        - can be `.xhtml`
    - exception details exposed as request attributes
        - `javax.servlet.error.exception.*`
        - accessible through EL
    - Recommended cases to handle:
        - `ViewStateExpired`
        - `500` (internal server error)
- Ajax: nothing happens (!!)
- Register custom `ExceptionHandlerFactory` instances

---

## Internationalization and localization

1. create one (or more) resource bundle(s)
    - `src/main/resources`
2. enumerate supported locales
    - `faces-config` > `<application>` > `<locale-config>`
3. register resource bundles as EL objects
    - single xhtml file: `<f:loadBundle>`
    - global: `faces-config` > `<application>` > `<resource-bundle>`
4. reference resource bundles in xhtml
    - `#{bndl['foo.title']}`
        - pass params through `<h:outputFormat>` + `<f:param>`
5. locale selection: set `locale` property on view root
    - automatic based on `Accept-Language` of request
    - manual override:
        - from xhtml: `<f:view locale="">` (typically in master template)
        - from java: `FacesContext` > `getViewRoot` > `setLocale`

???trainer

i18n: languages
l10n: regional preferences regarding formatting: currency, dates...

???t

---

### i18n of validation/conversion errors

- JSF-based errors:
    - Special `<message-bundle>` in `faces-config`
    - See JSF spec for [full list of i18n keys](https://jakarta.ee/specifications/faces/3.0/jakarta-faces-3.0.html#a584)

```html
javax.faces.component.UIInput.REQUIRED=Please enter the required value.
```

- respects JBV i18n bundle: `ValidationMessages` (exactly)

???trainer

More info on the BV i18n bundle can be found in the BV spec.
There is no need to register/configure that in JSF.

???t

---

### Exercise

- Add i18n to your application
- Add a language switcher to the application
    - make sure the page is translated instantly when switching language

???trainer

The idea is that they use a session scoped bean to track language, put `<f:view>` in the master template and use AJAX to reload the page directly

???t

## Security

- JSF has no security mechanism of its own...
- Authentication
    - Relies on (other specs in) platform to obtain principal
        - Typically servlet: `<auth-method>` in `web.xml`
    - Form-based approaches are a good fit
        - FORM in `web.xml` > poor, but possible
        - `CustomForm` from Jakarta Security > better, but ugly code...
    - Two "modes":
        - Container driven (user accesses protected resource, container reroutes to login)
        - User driven (user accesses login page directly, e.g. with login button)
- Authorization
    - in xhtml
        - use EL to access `HttpServletRequest` (`#{request. ...}`)
            - `principal.name`
            - `isUserInRole('foo')` (case sensitive)
    - in backing beans
        - inject `SecurityContext` (JEES)
        - or rely on EJB

???trainer
FORM authentication requires that you actually create a login and error page.
Upon noticing an unauthorized request, the container will create a session and redirect (302) the user to the login page (specified in web.xml), including the "set-cookie" header for the session id.
The client can then fill in the form and by maintaining the cookie, the user remains logged in.

???t

---

### FORM

- Activation:
    - `<auth-method>FORM</auth-method>` in `web.xml`, or...
    - `@FormAuthenticationMechanismDefinition` (JEES)
- Requires very specific form: POST to `j_security_check`
- User driven login will lead to blank page (bad UX)

```html
<form method="POST" action="j_security_check">
  <input type="text" name="j_username"/>
  <input type="password" name="j_password"/>
  <input type="submit" value="Log in"/>
</form>
```

???trainer

Though it is possible to use FORM, you're practically forced to write a plain html login page, sacrificing all that JSF power.
- no input validation
- no `<h:form>`
- no embedded error messages
- no user-driven login...
This makes FORM an unpopular choice for JSF

???t

---

### Custom FORM

- Use `@CustomFormAuthenticationMechanismDefinition`
    - Also requires an `IdentityStore`
- Allows you to bypass "j_security_check"
    - Allows you to use JSF-style login page
    - Need to handle login logic manually in custom backing bean
        - Through programmatic call to `SecurityContext#authenticate`

```java
AuthenticationStatus outcome = securityContext.authenticate(
    (HttpServletRequest) externalContext.getRequest(),
    (HttpServletResponse) externalContext.getResponse(),
    AuthenticationParameters.withParams()
        .credential(new UsernamePasswordCredential(email, password)));
```

| Outcome           | Meaning                   | Action to take
|-
| `SEND_CONTINUE`   | Auth in progress          | end request (call `facesContext.responseComplete()`)
| `SEND_FAILURE`    | Invalid credentials       | show error message (call `facesContext.addMessage(...)`)
| `SUCCESS`         | Log in success            | redirect to user dashboard
| `NOT_DONE`        | Auth skipped              | do nothing

???trainer

Though this is arguably built for JSF, it feels oddly unfinished, and you need to write the same piece of rather low-level logic in Java for each JSF application.
Perhaps this is due to the (very) recent nature of this auth mechanism and we can expect it to become better over time, but for now it's pretty awkward...

Hint: set "errorPage" on the "LogInToContinue" annotation of the auth mechanism to an empty string to ensure a failed submit remains on the login page!

In short: both ways to authenticate are somewhat awkward... and this in 2021

???t

---

### Logging out

- FORM based login is session based
- If you remove the cookie from your browser, you are "logged out"
    - This is like throwing away the key to your house, you lock yourself out
- To really log out, the server must remove the session
    - On the `HttpServletRequest` object:
        - `logout()`
        - `getSession().invalidate()`
- Typically done with mini-form and mini backing bean
