# Jakarta Enterprise Web Services (JAX-WS)

???trainer

Live code this example

Let's make a (SOAP) web service for people in our DB

Load the WSDL with SOAPUI, let it generate requests

Fire the requests to show it works

For some reason, JAX-WS IS NOT IN THE JAKARTAEE-API:8.0.0 POM
```xml
<dependency>
    <groupId>jakarta.xml.ws</groupId>
    <artifactId>jakarta.xml.ws-api</artifactId>
    <version>2.3.2</version>
    <scope>provided</scope>
</dependency>
```
https://stackoverflow.com/questions/48204141/replacements-for-deprecated-jpms-modules-with-java-ee-apis


???t

---

## Rundown

- Since JEE 4 (2003)
- SOAP web services
- Key features
    - Service API
    - Client API

???trainer
There isn't a whole lot to say about jax-ws...

???t

---

## SOAP Web Services architecture

- SOAP architecture revolves around the following triad:
    - Web Services are discovered using UDDI
    - WSDL files are exchanged to specify the protocol bindings
    - SOAP is used for message exchange

![](images/generic-webservices-overview-soap-architecture.png#no-border)

---

### SOAP Web Services architecture

- SOAP <small>(Simple Object Access Protocol)</small>
    - XML-based communication protocol
    - Message-wrapping format
- WSDL <small>(Web Services Description Language)</small>
    - An XML format that describes the service contract <small>(name, parameters, ...)</small>
    - Also used for configuration and for generating client and server code
- UDDI <small>(Universal Description, Discovery and Integration)</small>
    - An XML-based registry for businesses to list themselves on the internet
        - E.g. "Yellow pages"
    - Each business can publish a service listing
    - Designed to be queried using SOAP messages and WSDL contracts

---

### SOAP Web Services architecture

- The SOAP architecture also benefits from a wide range of _WS-*_ specifications
    - Provide services such as:
        - _Quality of service_
        - _Security_
        - _Reliability_
        - _Non repudiation_
        - _Transactions_
        - _Policy requirements_ and _governance_
- SOAP is a good match for a number of use cases where tight integration is necessary
    - Business-to-business integration
    - Integrate applications within the same company, or over the internet when run by various organizations

???trainer
Think of tight integration as situations where data needs to be interchanged within the same company, or subsidiaries.
Also consider the banking world, where a high degree of trust is required to communicate between businesses.

???t

???invisible

### SOAP Business Service

- To create a SOAP web service, you must start with a custom business interface

```java
public interface MovieSoapService {
    Movie findMovieById(int id);
    List<Movie> findMovies();
    int createMovie(Movie movie);
    boolean deleteMovie(int id);
    boolean updateMovie(Movie movie);
}
```

- This will become our web service contract interface

???i

---

## Web Service Example

- Consider `Movie` domain object and accompanying service

```java
public class Movie {
    private Integer id;
    private String title;
    private String director;
    private List<String> actors = new ArrayList<>();
    // Constructors, getters and setters
}
```
```java
public class MovieService {
    public Movie findMovieById(int id) { }
    public List<Movie> findMovies() { }
    public int createMovie(Movie movie) { }
    public boolean deleteMovie(int id) { }
    public boolean updateMovie(Movie movie) { }
}
```

---

## Web Service Annotations

- The business service can be exposed as a web service using annotations

```java
@WebService(name = "MovieService", portName="MovieServicePort")
public class MovieSoapService {
    @EJB protected MovieRepository movieRepository;

    @WebMethod(operationName = "GetMovie")
    public @WebResult(name = "movie") Movie findMovieById(@WebParam(name = "movie_id") int id) {}

    @WebMethod(operationName = "FindMovies")
    public @WebResult(name = "movie") List<Movie> findMovies() {}

    @WebMethod(operationName = "CreateMovie")
    public @WebResult(name = "movie_id") int createMovie(@WebParam(name = "movie") Movie movie) {}

    @WebMethod(exclude = true)
    public boolean deleteMovie(int id) {}

    @WebMethod(operationName = "ReplaceMovie")
    @WebResult(name = "movie_replaced_status")
    public boolean updateMovie(@WebParam(name = "movie") Movie movie) {}
}
```

---

## Web Service Annotations

- There are quite a few annotations

| Annotation                | Purpose
|-
| `@WebService`             | Marks a service implementation as a web service<br/>Allows setting names for entries inside the WSDL
| `@WebMethod`              | Allows marking a service method to be exposed<br/>Allows customizing the name, and exclusions
| `@WebParam`               | Allow customizing the XML elements used for parameters
| `@WebResult`              | Allows customizing the XML element used for output
| `@OneWay`                 | Causes the server not to generate a HTTP response
| `@SOAPBinding`            | Allows customizing the WSDL's _soap binding_
| `@MTOM`                   | Allows configuration of binary attachments

---

## Accessing the web service

- The web service will be exposed by the server under the following URL
    - `http://localhost:8080/movies/MovieSoapService`
    - The WSDL is can be retrieved by adding `?wsdl`
- Can be used to create a client or import into a tool like SoapUI

![](images/jee7-jaxws-soapui.png)

---

## Top Down vs Bottom Up

- The previous examples are using a _bottom up_ or _code first_ approach
    - You write code and let the container generate WSDL and XSD files
- Alternative is a _top down_ or _contract first_ approach
    - This requires manually authoring the WSDL and XSD contracts required by the service
    - Then you can use the `wsimport` tool provided by JavaSE to generate the JAXB classes and webservice interface
    - Finally the developer only needs to implement the actual service implementation

???trainer
top down is interesting for when you are creating a client for an existing SOAP service: you can download the contract and let your java files be generated.

???t

---

## Exercise

- Create a SOAP web service to list the people in our database
    - Support a method to list all people
    - Support a method to return the details of a person, given their id

```xml
<dependency>
    <groupId>jakarta.xml.ws</groupId>
    <artifactId>jakarta.xml.ws-api</artifactId>
    <version>2.3.2</version>
    <scope>provided</scope>
</dependency>
```
