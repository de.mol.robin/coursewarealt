# Jakarta Persistence (JPA)

???trainer

Formerly: Java Persistence API (hence the acronym)
Vlad Mihalcea is your main man.
Thorben Janssen is your backup man.

???t

---

## Rundown

- Object-Relational Mapping (ORM)
- "Configuration by Exception"
- It's a _very_ large spec
- Implementations
    - [**EclipseLink**](https://www.eclipse.org/eclipselink/)
    - [Hibernate ORM](https://hibernate.org/orm/)
    - [OpenJPA](http://openjpa.apache.org/)
    - [DataNucleus](https://www.datanucleus.org/)
- Key concepts
    - Entities and the EntityManager
    - JPQL (vendor agnostic SQL alternative)
- Other concepts
    - Metamodel API (+ criteria builder) for type-safe dynamic querying

???trainer
Persists data in a relational database
Middle-aged
_Very_ widely used (even found in Spring)
JPA is supposed to make your life easier because you can remain in the Java domain, but...
you need to learn and write JPQL, which is not really easier than SQL but more limited
you still need to know SQL
you will face all sorts of exotic bugs
JPA is not really efficient...
you still need to be aware of your DB
huge spec
probably more complex than JDBC...

<dependency>
    <groupId>jakarta.persistence</groupId>
    <artifactId>jakarta.persistence-api</artifactId>
    <version>3.0.0</version>
</dependency>

<persistence xmlns="http://xmlns.jcp.org/xml/ns/persistence"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_2.xsd"
             version="2.2">
    <persistence-unit name="meet4j" transaction-type="JTA">
        <jta-data-source>java:global/ds/meet4j</jta-data-source>
        <properties>
            <property name="javax.persistence.schema-generation.database.action" value="drop-and-create"/>
        </properties>
    </persistence-unit>
</persistence>

???t

---

## Key concept: JPA Entity

- In-memory equivalent of a piece of persistent data in the database
- Just an annotated Java class following some rules:
    - `@Entity` and `@Id` (primary key) are required
    - There must be a non-private no-arg constructor
    - It must be a top-level class (no enum nor interface)
    - It may not be `final` (or have final methods)

```java
*@Entity
public class Book {
* @Id
  private Long id;
  private String title;
  private Float price;
  private String description;
  private String isbn;
  private Integer nbOfPage;
  private Boolean illustrations;

  public Book() {}   // May also be protected or package private

  // Getters and setters
}
```

???trainer

- Explain that the class has all these requirements because JPA uses proxy classes...
- Object representation of a thing or fact described by one or more attributes

???t

---

## Key concept: the EntityManager

- Provides high-level abstractions to hide low-level JDBC concepts
    - Provides generic CRUD operations for entities
        - Offers flexible querying through JPQL and/or the criteria builder
    - Keeps entities in a specific state: new (transient), persistent (managed), detached or removed
    - While managed, changes in Java are synchronized to the database automatically
        - (but not other way around)
- Obtaining an EntityManager
  - Manual
```java
EntityManagerFactory emf = Persistence.createEntityManagerFactory("bookPU");
EntityManager em = emf.createEntityManager();
// do work with entity manager
em.close();
emf.close();
```
  - Automatic (through container)
```java
@PersistenceContext(unitName = "bookPU")
private EntityManager em;
```
- Requires a persistence unit

???trainer
You can also `@Inject` an `EntityManager` if you produce it first
???t

---

## Deployment descriptor: `persistence.xml`

- Application-wide meta configuration is inside `/META-INF/persistence.xml`
    - Maven: `src/main/resources/META-INF/persistence.xml`
- One or more "persistence units"
    - Wrapper around `javax.sql.DataSource` (see JavaSE)
    - Configuration for `EntityManager` instances

```xml
<persistence>
  <persistence-unit name="bookPU1" transaction-type="RESOURCE_LOCAL">
    <properties>
      <property name="javax.persistence.jdbc.driver" value="com.mysql.jdbc.Driver"/>
      <property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/cdbookstore"/>
      <property name="javax.persistence.jdbc.user" value="root"/>
      <property name="javax.persistence.jdbc.password" value="ilikemarvel"/>
    </properties>
  </persistence-unit>
  <persistence-unit name="bookPU2" transaction-type="JTA">
    <jta-data-source>java:/datasources/ExampleDS</jta-data-source>
  </persistence-unit>
</persistence>
```

???trainer
Standard JPA Properties:

Property                                            | Description
:-------------------------------------------------- | :----------------------------------------------
javax.persistence.jdbc.driver                       | Driver class
javax.persistence.jdbc.url                          | Driver-specific URL
javax.persistence.jdbc.user                         | Username
javax.persistence.jdbc.password                     | Password
javax.persistence.database-product-name             | Database name (e.g. Derby)
javax.persistence.database-major-version            | Version number
javax.persistence.database-minor-version            | Minor version number
javax.persistence.ddl-create-source                 | metadata, script, metadata-then-script, script-then-metadata
javax.persistence.ddl-create-script-source          | Create script (relative to src/main/resources)
javax.persistence.ddl-drop-script-source            | Drop script
javax.persistence.sql-load-script-source            | Load script
javax.persistence.schema-generation.database.action | none, create, drop, drop-and-create
javax.persistence.schema-generation.scripts.action  | none, create, drop, drop-and-create
javax.persistence.lock.timeout                      | Pessimistic lock timeout in ms
javax.persistence.query.timeout                     | Query timeout in ms
javax.persistence.validation.group.pre-persist      | Groups targeted for validation upon pre-persist
javax.persistence.validation.group.pre-update       | Groups targeted for validation upon pre-update
javax.persistence.validation.group.pre-remove       | Groups targeted for validation upon pre-remove
???t

---

## Entity Life Cycle

![](images/jpa-entity-states.png#no-border)

???trainer

Entities represent data that is stored persistently in a database.
They are managed beans (managed by the entity manager).
Changes to the java object are automatically reflected in the database.

error in image: `clear(entity)` should be `detach(entity)`
???t

---

## Jakarta Persistence Query Language (JPQL)

- Platform-independent SQL-like query language
- **Object-oriented**, by working on the domain model
- JPQL is mapped then transformed to SQL, called by JDBC
- Can return an entity (or collection thereof), or scalars
- Lets you retrieve entities by criteria other than the `ID`
- Primarily for **SELECT** with **FROM** (+WHERE, GROUP BY, HAVING and ORDER BY)
    - Supports (inner/left) JOIN and path expressions

```sql
SELECT p.vendor
FROM Employee e JOIN e.contactInfo.phones p
WHERE e.contactInfo.address.zipcode = ?1
```
- Also support for UPDATE and DELETE statements
- [JPQL Reference](https://jakarta.ee/specifications/persistence/3.0/jakarta-persistence-spec-3.0.html#a4665)

???trainer

$$ JPA Chapter: Remaining structure

- Parts:
    1. **Entity basics** + inheritance + in-memory auto-generated database
    2. **Relationships between entities** + real but auto-generated database
    3. **Locking (+versioning), entity listeners and more** + real database with migration-based scripts (flyway)
    4. **DAO/Repository design pattern** + testing
- For each part, there will be:
    - A bunch of slides with theory
    - A small example exercise, done by the instructor while going through the slides
        - You could code along, but it's more important that you pay attention and understand everything
    - An exercise time slot, where you can apply what you've learned to the project exercise
- Structurally, each part will introduce:
    - Some mapping annotations
    - Extra functionality of the entity manager (where relevant)
    - New JPQL keywords + gotchas
    - A better (more realistic) way of working with the database

???t

---

## Entity basics

- Simple entity without relations
    - a generic "item" with title, price and description
- Let JPA automatically generate database schema (!)
- Simple JPQL
    - list all items, fetch those with description, those under a certain price...
- [H2 database](https://h2database.com/html/main.html)

```xml
<dependency>
    <groupId>com.h2database</groupId>
    <artifactId>h2</artifactId>
    <version>1.4.200</version>
</dependency>
```
- [EclipseLink](https://www.eclipse.org/eclipselink/) as JPA provider

```xml
<dependency>
    <groupId>org.eclipse.persistence</groupId>
    <artifactId>eclipselink</artifactId>
    <version>2.7.8</version>
</dependency>
```

???trainer

Do a live example with "items"

Project setup:

- Dependency on a JPA implementation (EclipseLink)
    - Transitively pulls in JPA spec
- Dependency on a database driver (h2)
- JPA configuration file `persistence.xml`
    - Define a persistence unit
        - specify the h2 driver ("javax.persistence.jdbc.driver")
            - `org.h2.Driver`
        - specify the connection string ("javax.persistence.jdbc.url")
            - `jdbc:h2:mem:basic;DB_CLOSE_DELAY=-1`
        - use the "create" strategy ("javax.persistence.schema-generation.database.action")
            - The JPA implementation will automatically create tables

To get the generated scripts:
```xml
<property name="javax.persistence.schema-generation.scripts.action" value="create" />
<property name="javax.persistence.schema-generation.scripts.create-target" value="scripts"/>
```


- Create a simple entity and map it
- Start playing with the entity manager!
- Refactor the entity manager stuff into a repository
    - Remember dependency injection!

Checklist of things to show:

- for loop to generate pseudo-random objects
- select all (+ filters)
- try to insert "faulty" items (null for non-nullable fields)
- find by ID (use optional, illustrate match + no match)
    - make changes and see that they are automatically reflected
    - detach, make changes (not reflected), merge (and validate that they now are reflected)
- delete (also delete by ID using getReference)
- again find all to prove stuff was deleted

???t

---

### @Entity

| Attribute             | Meaning             | Default value
|-
| `name`                | Entity name in JPQL | Class name
- Must have (at least) 1 attribute marked as `@Id`
- Tip: **always** implement `equals` + `hashcode`

```java
*@Entity
public class Book {
    @Id
    private String isbn;
}
```

---

### @Id and @GeneratedValue

- `@Id` denotes the primary key
- `@GeneratedValue` indicates that a value is generated when not provided
    - (!) Use wrapper type to allow `null` for unsaved values
    - Different strategies possible
        - IDENTITY: database is responsible for generating a value
        - SEQUENCE: use a sql sequence
        - TABLE: use a table
        - AUTO: "best guess" based on jdbc connection string (**default**)

```java
@Entity
public class Book {
* @Id
* @GeneratedValue
  private Long id;

  // Constructors, getters, setters
}
```

???trainer

AUTO only makes sense when JPA also generates the database.
This is something you should almost always avoid, so, sadly, this is a poor default...

SEQUENCE and TABLE are "slow" because they require 2 round trips.
However, IDENTITY is not supported by all databases (but works for mysql, postgresql, oracle...).

???t

---

### @SequenceGenerator

- For `SEQUENCE` generation strategy

| Attribute             | Meaning           | Default value
|-
| `name`                | Generator name | /
| `sequenceName`                | Name of SQL sequence | DB specific
| `initialValue`                | Initial sequence value | 1
| `allocationSize`                | Amount to increment by | 50
| ... | ... | ...

- Example

```sql
CREATE SEQUENCE FOO_SEQ AS BIGINT INCREMENT BY 1 START WITH 1;
```

```java
@Id
@GeneratedValue(strategy = SEQUENCE, generator = "MyGenerator")
@SequenceGenerator(name = "MyGenerator", sequenceName = "FOO_SEQ")
Long id;
```

---

### @TableGenerator

- For `TABLE` generation strategy

| Attribute             | Meaning           | Default value
|-
| `name`                | Generator name | /
| `table`               | Name of table storing generated key values | DB specific
| `pkColumnName`        | Name of PK column in table | Provider-chosen
| `valueColumnName`     | Name of column storing last generated value | Provider-chosen
| `pkColumnValue`       | Row-value for specific generator | Provider-chosen
| ...                | ... | ...

<div style="display: flex; justify-content: space-around">
  <div>
    <pre>
      <code class="java">@Entity
public class Customer {
    @Id @GeneratedValue(
        strategy = TABLE, generator = "MyGenerator")
    @TableGenerator(
        name = "MyGenerator", table = "ID_GEN_T",
        pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
        pkColumnValue = "ADDR_ID")
    Long id;
} </code>
    </pre>
  </div>
  <div>
    <pre>
      <code class="sql">CREATE TABLE ID_GEN_T (
          GEN_KEY VARCHAR(255),
          GEN_VALUE NUMBER);
SELECT * FROM ID_GEN_T;
-- ADDR_ID, 1
 </code>
    </pre>
  </div>
</div>

???trainer
With this approach, each row can be a generator.
The PK column contains the sequence identifier, and the value column contains the sequence value.
The PK column value is used to select a row from the table to associate with the generator.
also has `initialValue` and `allocationSize`, and table-like things like `uniqueConstraints`, `schema`, `catalog`...
???t

---

### To generate, or not to generate?

- Generated keys are a contentious subject
- Pro:
    - Simple approach: a single number is lightweight and easy to embed as FK
    - Future proof: once generated, it will _never_ change
    - Ubiquitousness: consistency for all tables
    - Always available
- Con:
    - You will very likely have duplicates sooner or later (different ID but actually the same entity)
    - The ID is meta-data: it is not part of the domain model
    - It ties the data to the database: migrations will be significantly harder
    - Can be a bottleneck for performance (especially for bulk inserts)
    - (sequence-based generated values are a security weakness)
- In practice, generated keys dominate the market because
    - It is simple
    - It is everywhere (vicious circle)
    - A real, "natural" key might not be available

???trainer

You should be aware that auto-generated keys might seem great, and they are, but they are also a typical cause for bugs in production that are related to data duplication, bad performance and security breaches.

???t

---

### @Table

| Attribute             | Meaning           | Default value
|-
| `name`                | Changes the table name in the database | Entity name
| `schema`              | Sets the schema in the database        | Default for connected user
| `catalog`             | Sets the catalog in the database       | Default of DB
| `uniqueConstraints`   | Table-wide Unique constraints (`@UniqueConstraint`) | /
| `indexes`             | Specify indexed columns in the DB (use `@Index`) | /
- Typically used when the name of the table is different from the entity class name

```java
@Entity
@Table(name = "t_book")
public class Book {
  @Id @GeneratedValue
  private Long id;
  // More properties, constructors, getters, setters
}
```

---

### @Column

| Attribute             | Meaning           | Default value
|-
| `name`                | Changes column name | Property or field name
| `unique`              | Whether column is unique        | false
| `nullable`             | Whether column is nullable       | true
| `insertable`   | Whether column is included in generated INSERT statements | true
| `updatable`             | Whether column is included in generated UPDATE statements  | /
| `columnDefinition`             | SQL fragment used when generating DDL | SQL to create column
| `table`             | Name of table to which column belongs | Primary table
| `length`             | Column length (for string-types) | 255
| `precision`             | Precision for decimal | 0 (must be set)
| `scale`             | Scale for decimal | 0

```java
@Entity
public class Book {
  @Id @GeneratedValue @Column(nullable = false, updatable = false) private Long id;
  @Column(name = "book_title", nullable = false) private String title;
}
```

---

### @Enumerated

- Used to map `enum` types
  - `ORDINAL`: number
  - `STRING`: text

```java
public enum CreditCardType {
  VISA,
  MASTER_CARD,
  AMERICAN_EXPRESS
}
```

```java
@Entity
@Table(name = "credit_card")
public class CreditCard {
  @Id
  private String number;
  private String expiryDate;
  private Integer controlNumber;
* @Enumerated(EnumType.STRING)
  private CreditCardType creditCardType;
  // Constructors, getters, setters
}
```

---

### @Temporal (pre Java 8)

- Used to map `Date` and `Calendar`
  - `TemporalType.DATE`
  - `TemporalType.TIME`
  - `TemporalType.TIMESTAMP`

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;

* @Temporal(TemporalType.DATE)
  private Date dateOfBirth;

}
```
- Note: these are **not** needed for `java.time.*` types (`LocalDate` and friends)
- Avoid `ZonedDateTime`: time zone info is simply **ignored**!!

---

### @Lob (and @Basic)

- `@Lob` specifies a _CLOB_ <small>(very long `String`)</small> or _BLOB_ <small>(`byte[]`)</small>

```java
@Entity
public class Track {
  @Id @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String title;
  private Float duration;
* @Basic(fetch = FetchType.LAZY, optional=true)
* @Lob
  private byte[] wav;
  private String description;
  // Constructors, getters, setters
}
```
- By default, columns are EAGERLY fetched (see later)
    - add `@Basic` to override this

???trainer

TODO: move this to "lazy/eager fetching" bit later?

???t

---

### @Transient

- When you do not want the attribute mapped

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;

  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;

  @Temporal(TemporalType.DATE)
  private Date dateOfBirth;

* @Transient
  private Integer age;

  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;

  // Constructors, getters, setters
}
```

---

### @Embeddable

- For extracting related columns into a (non-entity) class

<div style="display: flex; justify-content: space-around; align-items: center">
  <div>
    <pre><code class="sql"> create table CUSTOMER (
  id          BIGINT NOT NULL,
  phonenumber VARCHAR(255),
  email       VARCHAR(255),
  firstname   VARCHAR(255),
  lastname    VARCHAR(255),
  street1     VARCHAR(255),
  street2     VARCHAR(255),
  city        VARCHAR(255),
  state       VARCHAR(255),
  zipcode     VARCHAR(255),
  country     VARCHAR(255),
  PRIMARY KEY (id)
);
    </code></pre>
  </div>
  <div>
    <pre><code class="java">@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String email;
  private String phoneNumber;
  private String firstName;
  private String lastName;

  @Embedded
  private Address address;
  // More
}

@Embeddable
public class Address {
  private String street1;
  private String street2;
  private String city;
  private String state;
  private String zipcode;
  private String country;
  // More
}</code></pre>
  </div>
</div>

???trainer

This is an example of a possible mismatch between the db design and the Java design.
Perhaps, in Java, you want to treat an addess as a separate class, though in the database it is all part of the same table (this is an example of a bad database design btw).
You can do that with this annotation.

???t

---

### Composite Primary Keys

- Supported in two ways
    - `@IdClass`
    - `@EmbeddedId`
- Both options require a separate class to model the primary key
  - `equals()` and `hashCode()`
  - `public`
  - no-arg constructor
  - implement `Serializable`

```java
public class NewsId implements Serializable {   // Represents PK
  private String title;
  private String language;

  // Constructors, getters, setters, equals, and hashCode
}
```

---

### @EmbeddedId

- `@EmbeddedId` represents PK as composite object

```java
@Entity
public class News {
* @EmbeddedId
  private NewsId id;

  private String content;
  // Constructors, getters, setters
}
```

- Requires key class to be annotated with `@Embeddable`

```java
*@Embeddable
public class NewsId implements Serializable {
  private String title;
  private String language;

  // Constructors, getters, setters, equals, and hashCode
}
```

???invisible
```java
NewsId pk = new NewsId("Richard Wright has died on September 2008", "EN")
News news = em.find(News.class, pk);
```
???i

---

### @IdClass

- `@IdClass` represents PK as separate fields
    - Names must match up with fields of key class

```java
@Entity
*@IdClass(NewsId.class)
public class News {
* @Id private String title;     // EXACT same as in NewsId
* @Id private String language;  // EXACT same as in NewsId

  private String content;

  // Constructors, getters, setters
}
```

```java
*// no annotations
public class NewsId implements Serializable {
  private String title;     // Same name as in News
  private String language;

  // Constructors, getters, setters, equals, and hashCode
}
```

---

### @EmbeddedId vs. @IdClass

- There is no difference in the database...

```sql
create table NEWS (
  CONTENT VARCHAR(255),
  TITLE VARCHAR(255) not null,
  LANGUAGE VARCHAR(255) not null,
* PRIMARY KEY (TITLE, LANGUAGE)
);
```

- Difference is only on the OO side
  - `@IdClass` duplicates attributes, does not require annotations on the primary key class
  - `@EmbeddedId` does not duplicate, but requires annotations
  - JPQL statements have slightly different syntax (but same functionality)

???trainer
also reflected in (JPQL) queries
```sql
select n.title from News n
select n.id.title from News n
```
???t

---


### @Inheritance

<img src="images/orm-inheritance-hierarchy.png#no-border" style="height:11em"/>
- Inheritance is not natively implemented in relational DBs
    - different tricks possible
        - single table containing all attributes of all classes in hierarchy
        - separate table for each class and superclass, joins needed
        - separate table for each class, no joins needed
    - JPA provides support for these tricks
- `@Inheritance` on the root entity decides the strategy
    - Select the strategy using the `InheritanceType` enum

---

### Single-Table-per-Class-Hierarchy Strategy

<img src="images/orm-singletableperclass-table.png#no-border" style="float:right;height:11em;padding-top:5em"/>
```java
@Entity
*// @Inheritance // default, optional
public class Item {
  @Id @GeneratedValue
  protected Long id;
  protected String title;
  protected Float price;
  protected String description;
  // Constructors, getters, setters
}
```

<div style="display: flex; justify-content: space-around">
  <div>
    <pre><code class="java">@Entity
public class Book extends Item {
  private String isbn;
  private String publisher;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
} </code></pre>
  </div>
  <div>
    <pre><code class="java">@Entity
public class CD extends Item {
  private String musicCompany;
  private Integer numberOfCDs;
  private Float totalDuration;
  private String genre;
  // Constructors, getters, setters
} </code></pre>
  </div>
</div>

- Adding new entities involves adding new columns
- Columns of the child entities must be `nullable`
- Discriminator column!

???trainer
Talk about the `DTYPE` column.

???t

---

### @DiscriminatorColumn and @DiscriminatorValue

```java
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
*@DiscriminatorColumn(name = "disc", discriminatorType = DiscriminatorType.CHAR)
*@DiscriminatorValue("I")
public class Item {
  @Id @GeneratedValue
  protected Long id;
  protected String title;
  protected Float price;
  protected String description;
  // Constructors, getters, setters
}
```

<div style="display: flex; justify-content: space-around">
  <div>
    <pre><code class="java">@Entity
@DiscriminatorValue("B")
public class Book extends Item {
  private String isbn;
  private String publisher;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
} </code></pre>
  </div>
  <div>
    <pre><code class="java">@Entity
@DiscriminatorValue("C")
public class CD extends Item {
  private String musicCompany;
  private Integer numberOfCDs;
  private Float totalDuration;
  private String genre;
  // Constructors, getters, setters
} </code></pre>
  </div>
</div>

---

### Joined-Subclass Strategy

```java
@Entity
*@Inheritance(strategy = InheritanceType.JOINED)
public class Item {
  @Id @GeneratedValue
  protected Long id;
  protected String title;
  protected Float price;
  protected String description;
  // Constructors, getters, setters
}
```

![](images/orm-joinedsubclass-table.png#no-border)

- The deeper the hierarchy, the more joins, the poorer the performance

---

### Table-per-Concrete-Class Strategy

```java
@Entity
*@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Item {
  @Id @GeneratedValue
  protected Long id;
  protected String title;
  protected Float price;
  protected String description;
  // Constructors, getters, setters
}
```

![](images/orm-tableperconcreteclass-table.png#no-border)

- Polymorphic queries require the `UNION` operation, which is expensive with large amounts of data

---

### @AttributeOverrides

- To override column mapping from
    - parent class
    - embeddables

```java
@Entity
@AttributeOverrides({
  @AttributeOverride(
    name = "id",
    column = @Column(name = "book_id")),
  @AttributeOverride(
    name = "title",
    column = @Column(name = "book_title")),
  @AttributeOverride(
    name = "description",
    column = @Column(name = "book_description"))
})
public class Book extends Item {
  private String isbn;
  private String publisher;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
}
```

---

### Type of Classes in the Inheritance Hierarchy

- Entity
  - Provides a common data structure for its leaf entities and follows the mapping strategies
- Abstract Entity
  - Same as entity, except that it can not be instantiated
- Nonentity
  - Transient classes
  - Are not managed by the persistence provider
  - Attributes of parent nonentity classes are not persisted
- Mapped Superclass
  - Provide persistent properties to subclasses
  - Not managed by the persistence provider
  - Similar to embeddable classes, but used with inheritance

---

### @MappedSuperclass

<div style="display: flex; justify-content: space-around">
  <div>
    <pre><code class="java">@MappedSuperclass
public class Item {
  @Id @GeneratedValue
  protected Long id;
  @Column(length = 50, nullable = false)
  protected String title;
  protected Float price;
  @Column(length = 2000)
  protected String description;
  // Constructors, getters, setters
}</code></pre>
  </div>
  <div>
    <pre><code class="java">@Entity
public class Book extends Item {
  private String isbn;
  private String publisher;
  private Integer nbOfPage;
  private Boolean illustrations;
  // Constructors, getters, setters
}</code></pre>
  </div>
</div>

```sql
create table BOOK (
* ID BIGINT not null,
* TITLE VARCHAR(50) not null,
* PRICE DOUBLE(52, 0),
* DESCRIPTION VARCHAR(2000),
  ILLUSTRATIONS SMALLINT,
  ISBN VARCHAR(255),
  NBOFPAGE INTEGER,
  PUBLISHER VARCHAR(255),
  primary key (ID)
);
```

---

### EntityManager usage

- One-stop-shop for all interaction with entities
- The `EntityManager` can be seen as a generic DAO supporting CRUD
    - Create: `persist`
    - Read: `find` (by id)
    - Update: `merge`
    - Delete: `remove`
- Modifications of the data require a transaction
- Example of inserting data:

```java
public void doWithEM(EntityManager entityManager) {
    Item i = new Item();
    EntityTransaction transaction = entityManager.getTransaction();
    transaction.begin();
    entityManager.persist(i);
    transaction.commit();
}
```

---

### Retrieving entities from the database

- By ID: `find(class, id)`
    - returns `null` if not found

```java
Customer customer = em.find(Customer.class, 1234L);
// check if customer is null, or wrap in Optional.ofNullable(...)
```

- All other cases: use JPQL
    - `createQuery(sqlString, class)` creates an _anonymous_ (=dynamic) query...
    - ... `getResultList` executes the query and returns a (possibly empty) list of results

```java
List<Customer> customers = em.createQuery("select c from Customer c", customer.class);
                             .getResultList();
// list should never be null, but can be empty
```

```java
// get all customers whose first name starts with J
var statement = "select c from Customer c where firstName like :fn";
List<Customer> customers = em.createQuery(statement, customer.class);
                             .setParameter("fn", "J%")
                             .getResultList();
```

???trainer

Move this to a later part

- `getReference(class, id)` does not retrieve data
    - Ideal for obtaining references to rows without running SELECT statement
        - For removing row
        - For adding relation to existing row
    - Throws exception when not found **and first used**

```java
Customer customer = em.getReference(Customer.class, 1234L);
Order order = em.find(Order.class, 879L);
// if there is no customer for id 1234L, the committing the transaction will fail
order.setCustomer(customer);
```


???t

---

### JPQL and polymorphism

- The EntityManager supports polymorphic querying:

```java
Item item = em.find(Item.class, 1L);
System.out.println(item.getClass.getSimpleName()); // Book or CD
```

```java
// assuming that item with id 1L is a book:
Book b = em.find(Book.class, 1L);
System.out.println(b); // book...
CD c = em.find(CD.class, 1L);
System.out.println(c); // null
```

```java
List<Item> items = em.createQuery("select i from Item i", Item.class).getResultList(); // books + cds
```
- Use `TREAT` to downcast in JPQL:

```java
var q = "select i from Item i where " +
            " treat(i as CD).numberOfCDs > ?1" +
            " or treat(i as Book).completed = ?2";
em.createQuery(q, Item.class)
  .setParameter(1, 2)
  .setParameter(2, false)
  .getResultList()
  .forEach(System.out::println);
```

---

### Named Queries

- Static and unchangeable, but more efficient to execute (pre-compiled)

```java
@Entity
@NamedQuery(name = Customer.ALL, query="select c from Customer c"),
@NamedQuery(name = Customer.BY_FNAME, query="select c from Customer c where c.firstName = :fname")
public class Customer {
  public static final String ALL = "Customer.findAll";
  public static final String BY_FNAME = "Customer.findByFirstName";
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  // ...
}
```
- Executing named queries is similar to dynamic queries

```java
TypedQuery<Customer> query = em.createNamedQuery(Customer.ALL, Customer.class);
List<Customer> customers = query.getResultList();
```

```java
List<Customer> customers = em.createNamedQuery(Customer.BY_FNAME, Customer.class)
                             .setParameter("fname", "Vincent")
                             .setMaxResults(3)
                             .getResultList();
```

???trainer

makes use of prepared statement
since jee8, it's no longer needed to wrap repeated annotations

???t

---

### Updates to entities

- While an entity is managed, changes to it will synchronize automatically
    - Don't forget that a transaction is still required to make changes

```java
em.getTransaction().begin();
Customer customer = em.find(Customer.class, 1L);
customer.setFirstName("William");
em.getTransaction().commit(); // will run "update customer set firstname = 'William' where id = 1;"
```

- Changes to detached entities are NOT synchronized!
    - Entities are automatically detached when the entity manager is closed
    - Entities can also be manually detached (but this is _rarely_ done)

```java
Customer customer = em.find(Customer.class, 1L);
*em.close();
customer.setFirstName("William"); // not synchronized, there also isn't any transaction...
```
```java
em.getTransaction().begin();
Customer customer = em.find(Customer.class, 1L);
*em.detach(customer);
customer.setFirstName("William");
em.getTransaction().commit(); // NOT synchronized
```

---

### Re-synchronize detached entities with merge

- Imagine an entity that becomes detached and is modified...

```java
em = emf.createEntityManager();
Customer customer = em.find(Customer.class, 1L);
em.close(); // the customer becomes detached at this point
```
- Now imagine that the customer is changed (could be in an exernal component)

```java
customer.setFirstName('William');
```
- To synchronize the changes to the database, we could:
    - use `find()` to get the customer, then call all relevant setters...
    - ...but we can also use `merge`

```java
em = emf.createEntityManager(); // This would typically be a different entity manager instance
em.getTransaction().begin();
em.merge(customer);
em.getTransaction().commit(); // JPA will do a diff and update the customer in the database
```
- Also possible through JPQL

```sql
UPDATE Customer c SET ... WHERE ...;
```

???trainer

Merge should be more efficient because it should enable JPA to run just a single update statement, rather than a select + update

???t

---

### Removing an Entity

- Deletes from the database, detaches, can no longer be synchronized
  - Is still accessible from Java code until it goes out of scope

```java
tx.begin();
Customer c = em.find(Customer.class, 1L);
*em.remove(c);
tx.commit();

// The data is deleted from the database but the object is still accessible
assertNotNull(c);
```
- Wasteful to do a select just to run a delete...
    - Use `getReference(class, id)` to prevent the select from running

```java
tx.begin();
*Customer c = em.getReference(Customer.class, 1L);
em.remove(c);
tx.commit();
```
- Also possible through JPQL

```sql
DELETE FROM Customer c WHERE ...;
```

---

## Multiple entities and relationships

- Switch to a [postgresql](https://www.postgresql.org/) database (or [mysql](https://www.mysql.com/))
    - Install natively, or through docker...

```sh
docker run --name pg -p 5432:5432 \
           -e POSTGRES_PASSWORD=acaddemicts -e POSTGRES_USER=acaddemicts \
           -e POSTGRES_DB=mydatabase postgres:alpine
```
- `pom.xml`

```xml
<dependency>
    <groupId>org.postgresql</groupId>
    <artifactId>postgresql</artifactId>
    <version>42.2.20</version>
</dependency>
```
- Update `persistence.xml`
    - Change the driver: `org.postgresql.Driver`
    - Change the connection url: `jdbc:postgresql:[//localhost:5432/]<dbname>`
    - Change the strategy to "drop-and-create" to ensure that the database is reset each run
    - Add `javax.persistence.jdbc.user` and `javax.persistence.jdbc.password`

???trainer

- Database generation based on scripts
    - Retain strategy "drop-and-create" to ensure that the database is reset each run
    - Copy/paste the generated `create.sql` to `src/main/resources/META-INF/create.sql`
    - Add the following properties:
        - `javax.persistence.schema-generation.create-source` = `script`
        - `javax.persistence.schema-generation.create-script-source` = `META-INF/create.sql`

???t

---

### @ElementCollection and java.util.Collection

- To store a _collection_ of basic Java types or embeddables as a _composition_
    - `Set`, `List` (or `Collection`)

```java
@Entity
public class Book {
  // ...
* @ElementCollection
* @CollectionTable(name = "TAG", joinColumns = @JoinColumn(name = "BOOK_ID"))    // optional
  @Column(name = "VALUE")
  private Set<String> tags = new HashSet<>();
  // ...
}
```
- Creates a second table with a _primary key join column_

![](images/orm-element-collection.png#no-border)

---

### @ElementCollection and java.util.Collection

- `@CollectionTable` to customize the table name and join column (nested)

| Attribute             | Meaning           | Default value
|-
| `name`                | Changes table name | "entity_collection-field"
| `joinColumns`   | Foreign key column(s) of collection table | `@JoinColumn` defaults
| `foreignKey`             | Foreign key constraint specification (only for generation) | DB specific
| ...  | same as `@Table` | ...
- `@JoinColumn` specifies foreign key column
    - Defaulted when not specified, single join column assumed
    - Composite PK: must specify `@JoinColumn` for each foreign key column
        - MUST fill in `name` and `referencedColumnName` in that case

| Attribute             | Meaning           | Default value
|-
| `name`                | Changes column name | "(entity/field)_referenced-id-field"
| `referencedColumnName`   | Name of referenced column | Name of PK column of referenced table
| `foreignKey`             | Foreign key constraint specification (generation) | DB specific
| ...  | same as `@Column` | ...

---

### @ElementCollection and java.util.Map

- For `Map`s with values that are basic types or embeddables

```java
@Entity
public class CD {
  // ...
  @ElementCollection
  @CollectionTable(name = "track")
* @MapKeyColumn(name = "position")  // Maps map keys to column 'position' in table 'track'
  @Column(name = "title")           // Maps map values to column 'title' in table 'track'
  private Map<Integer, String> tracks = new HashMap<>();
  // ...
}
```

![](images/orm-map-key-column.png#no-border)

---

### @ElementCollection and java.util.Map

- `@MapKeyColumn`
    - for map keys that are basic types
        - same properties as `@Column`
        - `name` defaults to: "field_KEY"
- `@MapKeyEnumerated`
    - for map keys that are enums
        - same properties as `@Enumerated`
- `@MapKeyTemporal`
    - for map keys that are `Date` or `Calendar`
        - same properties as `@Temporal`

???trainer
TODO: Map keys that are embeddable??

???t

---

### Relationships = entity-to-entity associations

- In relational database:
    - Foreign key column
    - Multiplicity
        - Always "singular" (refers to 1 instance)
    - No "direction"
        - In _either_ A or B, **NOT** both
        - `... from A join B on A.id = B.fk`
        - `... from B join A on A.id = B.fk`
- In OO
    - Composition
    - Multiplicity
        - Can be singular, can be collection
    - Has direction
        - A _has a_ B
        - B _has a_ A
        - can be both!

???trainer
M-N not directly possible
split into two 1-N's (join table)
???t

---

### Relationship types

- Relationship is defined by 2 attributes
    - Participation: "at least one?"
        - Handled by nullability
        - "does A always need to have a B?"
    - Multiplicity (also: cardinality): "at most one?"
        - "can A have more than one B?"
- Relationship types (possible combinations)
    - 1-1 (one to one): Person + PersonDetails
    - 1-N / N-1 (one to many/many to one): Order + OrderLines
    - M-N (many to many): Student + Course

| Relationship type | In Java                                              | In DB                                  
|-                                                                         
| 1-1               | A _has a_ B, B _has a_ A                             | FK on either table (but not both)     
| 1-N/N-1           | A _has a_ `Collection<B>`, B _has a_ A               | FK always on N side                   
| M-N               | A _has a_ `Collection<B>`, B _has a_ `Collection<A>` | Not supported (join table workaround) 

???trainer
Determine multiplicity by asking
"from A to B, is there..."
in opposite direction, too
???t

---

### Join tables

- Relational DBs do not natively support "Collection"-FKs
    - Can not really express M-N
    - Solved by splitting M-N into two 1-N relations
        - Extra table called "join table" or "junction table"

![](images/orm-join-table.png#no-border)

???trainer
You should think of the join table as a (hidden) entity!
In fact, you can explicitly do such a mapping, if you create a class to represent the join
(in example on slide: a "registration" of a person living at an address)
can add extra attributes to the join table that way
???t

---

### Entity Relationships in OO

- JPA maps entity associations to a relational model (FKs)
- Due to directionality, there are 7 possible combinations (using 4 annotations)
    - Unidirectional
        - `@OneToOne`   (either side of 1-1)
        - `@OneToMany`  (1 side of 1-N)
        - `@ManyToOne`  (N side of 1-N)
        - `@ManyToMany` (either side of M-N)
    - Bidirectional
        - `@OneToOne` + `@OneToOne(mappedBy)`
        - `@ManyToOne` + `@OneToMany(mappedBy)`
        - `@ManyToMany` + `@ManyToMany(mappedBy)`
- Bidirectional has **owning side** and **inverse side** (with `mappedBy`)
    - Necessary so JPA understands the two annotations describe the same relationship
    - For 1-1 and N-1, owning side corresponds to table with FK
    - For M-N, the owning side exists only in Java

???trainer
In total: 4 annotations
???t

---

### 1-1: @OneToOne

- For "A _has a_ B", not for collections

| Attribute             | Meaning           | Default value
|-
| `targetEntity`                | Associated entity class | Type of field or property
| `cascade`                | Entity manager operations that must cascade | /
| `fetch`                | EAGER or LAZY fetching? | EAGER
| `optional`                | Whether or not association is optional | true
| `mappedBy`                | To indicate owner of relation | /
| `orphanRemoval`                | Whether disassociation in Java triggers remove in DB | false

- Explicitly add `@JoinColumn` to override the default join column values
- Only specify `mappedBy` on inverse side of bidirectional association!

---

### 1-1 unidirectional

- Customer is owner
    - Customer table contains FK column to address table

<div style="display: flex; justify-content: space-around">
  <div>
    <pre>
      <code class="java">@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
  @OneToOne
  @JoinColumn(name="addr_fk") // optional
  private Address address;
  // Constructors, getters, setters
} </code>
    </pre>
  </div>
  <div>
    <pre>
      <code class="java">@Entity
public class Address {
  @Id @GeneratedValue
  private Long id;
  private String street1;
  private String street2;
  private String city;
  private String state;
  private String zipcode;
  private String country;
  // Constructors, getters, setters
} </code>
    </pre>
  </div>
</div>

???trainer
Explain that FK in DB must sit in customer table.
run through annotation properties:
fetch type
optional
orphan removal
cascade
???t

---

### 1-1 bidirectional

- Customer is owner
    - Customer table contains FK column to address table

<div style="display: flex; justify-content: space-around">
  <div>
    <pre>
      <code class="java">@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
  @OneToOne
  @JoinColumn(name="addr_fk") // optional
  private Address address;
  // Constructors, getters, setters
} </code>
    </pre>
  </div>
  <div>
    <pre>
      <code class="java">@Entity
public class Address {
  @Id @GeneratedValue
  private Long id;
  private String street1;
  private String street2;
  private String city;
  private String state;
  private String zipcode;
  private String country;

  @OneToOne(mappedBy="address")
  private Customer customer;

  // Constructors, getters, setters
} </code>
    </pre>
  </div>
</div>

???trainer
mention mappedBy
only decorate owning side with `@JoinColumn` (because it is the owner)
???t

---

### 1-N: @ManyToOne and @OneToMany

- `@ManyToOne`

| Attribute             | Meaning           | Default value
|-
| `targetEntity`                | Associated entity class | Type of field or property
| `cascade`                | Entity manager operations that must cascade | /
| `fetch`                | EAGER or LAZY fetching? | EAGER
| `optional`                | Whether or not association is optional | true
- `@OneToMany`

| Attribute             | Meaning           | Default value
|-
| `targetEntity`                | Associated entity class | Type of field or property
| `cascade`                | Entity manager operations that must cascade | /
| `fetch`                | EAGER or LAZY fetching? | LAZY
| `mappedBy`                | To indicate owner of relation | /
| `orphanRemoval`                | Whether disassociation in Java triggers remove in DB | false

???trainer
many to one
For N side of 1-N relationship (corresponds to FK column in DB)
The natural owner of the 1-N relationship
Defaults to `@JoinColumn`
one to many
For 1 side of 1-N relationship
Used on collections of entity types
In case of `Map`, map value type should be an entity
???t

---

### 1-N bidirectional

<div style="display: flex; justify-content: space-around">
  <div>
    <pre>
      <code class="java">@Entity
public class Order {
  @Id @GeneratedValue
  Long id;
  @Temporal(TemporalType.TIMESTAMP)
  Timestamp creationDate;
  @OneToMany(mappedBy = "order")
  private List&lt;OrderLine&gt; orderLines;
  // other fields, Constructors, getters, setters
} </code>
    </pre>
  </div>
  <div>
    <pre>
      <code class="java">@Entity
@Table(name = "order_line")
public class OrderLine {
  @Id @GeneratedValue
  private Long id;
  private String item;
  private Double unitPrice;
  private Integer quantity;
  @ManyToOne
  @JoinColumn(name = "order_fk")
  private Order order;
  // Constructors, getters, setters
} </code>
    </pre>
  </div>
</div>

![](images/orm-onetomany-joincolumn.png#no-border)

---

### 1-N unidirectional, natural owner (@ManyToOne)

<div style="display: flex; justify-content: space-around">
  <div>
    <pre>
      <code class="java">@Entity
public class Order {
  @Id @GeneratedValue
  Long id;
  @Temporal(TemporalType.TIMESTAMP)
  Timestamp creationDate;
  // other fields, Constructors, getters, setters
} </code>
    </pre>
  </div>
  <div>
    <pre>
      <code class="java">@Entity
@Table(name = "order_line")
public class OrderLine {
  @Id @GeneratedValue
  private Long id;
  private String item;
  private Double unitPrice;
  private Integer quantity;
  @ManyToOne
  @JoinColumn(name = "order_fk")
  private Order order;
  // Constructors, getters, setters
} </code>
    </pre>
  </div>
</div>

![](images/orm-onetomany-joincolumn.png#no-border)

---

### 1-N unidirectional, inverted owner (@OneToMany)

<div style="display: flex; justify-content: space-around">
  <div>
    <pre>
      <code class="java">@Entity
public class Order {
  @Id @GeneratedValue
  Long id;
  @Temporal(TemporalType.TIMESTAMP)
  Timestamp creationDate;
  @OneToMany
  private List&lt;OrderLine&gt; orderLines;
  // other fields, Constructors, getters, setters
} </code></pre>
  </div>
  <div>
    <pre>
      <code class="java">@Entity
@Table(name = "order_line")
public class OrderLine {
  @Id @GeneratedValue
  private Long id;
  private String item;
  private Double unitPrice;
  private Integer quantity;
  // Constructors, getters, setters
} </code></pre>
  </div>
</div>

- Defaults to `@JoinTable`!!
    - Explicitly add `@JoinColumn` to use join column instead

| Attribute             | Meaning           | Default value
|-
| `joinColumns`                | FK column(s) referencing this entity (owning side) | Defaults of `@JoinColumn`
| `inverseJoinColumns`         | FK column(s) referencing other entity (inverse side) | Defaults of `@JoinColumn`
| . | same as `@Table` | .

---

### 1-N unidirectional, inverted, with @JoinTable

<div style="display: flex; justify-content: space-around">
  <div>
    <pre>
      <code class="java">@Entity
public class Order {
  @OneToMany
  @JoinTable(
    name = "order_order_line",
    joinColumns= @JoinColumn(name = "order_id"),
    inverseJoinColumns= @JoinColumn(name = "orderlines_id")
  private List&lt;OrderLine&gt; orderLines;
  // other fields, Constructors, getters, setters
} </code>
    </pre>
  </div>
  <div>
    <pre>
      <code class="java">@Entity
@Table(name = "order_line")
public class OrderLine {
  @Id @GeneratedValue
  private Long id;
  private String item;
  private Double unitPrice;
  private Integer quantity;
  // Constructors, getters, setters
} </code>
    </pre>
  </div>
</div>

![](images/orm-onetomany-jointable.png#no-border)

???trainer
Specify `name`, `catalog`, `schema`, `joinColumns`, `inverseJoinColumns`, `uniqueConstraints` and `indexes`
???t

---

### 1-N unidirectional, inverted, with @JoinColumn

<div style="display: flex; justify-content: space-around">
  <div>
    <pre>
      <code class="java">@Entity
public class Order {
  @OneToMany
  @JoinColumn(name = "order_fk")
  private List&lt;OrderLine&gt; orderLines;
  // other fields, Constructors, getters, setters
} </code>
    </pre>
  </div>
  <div>
    <pre>
      <code class="java">@Entity
@Table(name = "order_line")
public class OrderLine {
  @Id @GeneratedValue
  private Long id;
  private String item;
  private Double unitPrice;
  private Integer quantity;
  // Constructors, getters, setters
} </code>
    </pre>
  </div>
</div>

![](images/orm-onetomany-joincolumn.png#no-border)

---

### M-N: @ManyToMany

| Attribute             | Meaning           | Default value
|-
| `targetEntity`                | Associated entity class | Type of field or property
| `cascade`                | Entity manager operations that must cascade | /
| `fetch`                | EAGER or LAZY fetching? | LAZY
| `mappedBy`                | To indicate owner of relation | /
- Requires a join table
    - You need to explicitly define the owner and the inverse using mappedBy
    - Explicit `@JoinTable` should go on owning side

---

### M-N unidirectional, owner A

<div style="display: flex; justify-content: space-around">
  <div>
    <pre><code class="java">@Entity
public class Artist { // the owning side
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  @ManyToMany
  @JoinTable(name = "jnd_art_cd",
    joinColumns = @JoinColumn(name = "artist_fk"),
    inverseJoinColumns = @JoinColumn(name = "cd_fk"))
  private List&lt;CD&gt; appearsOnCDs;
  // Constructors, getters, setters
} </code></pre>
  </div>
  <div>
    <pre><code class="java">@Entity
public class CD { // the inverse side
  @Id @GeneratedValue
  private Long id;
  private String title;
  private Float price;
  private String description;
  // Constructors, getters, setters
} </code></pre>
  </div>
</div>

![](images/orm-manytomany-jointable.png#no-border)

---

### M-N bidirectional, owner A

<div style="display: flex; justify-content: space-around">
  <div>
    <pre><code class="java">@Entity
public class Artist { // the owning side
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  @ManyToMany
  @JoinTable(name = "jnd_art_cd",
    joinColumns = @JoinColumn(name = "artist_fk"),
    inverseJoinColumns = @JoinColumn(name = "cd_fk"))
  private List&lt;CD&gt; appearsOnCDs;
  // Constructors, getters, setters
} </code></pre>
  </div>
  <div>
    <pre><code class="java">@Entity
public class CD { // the inverse side
  @Id @GeneratedValue
  private Long id;
  private String title;
  private Float price;
  private String description;
  @ManyToMany(mappedBy = "appearsOnCDs")
  private List&lt;Artist&gt; createdByArtists;
  // Constructors, getters, setters
} </code></pre>
  </div>
</div>

![](images/orm-manytomany-jointable.png#no-border)

---

### M-N unidirectional, owner B

<div style="display: flex; justify-content: space-around">
  <div>
    <pre><code class="java">@Entity
public class Artist {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  // Constructors, getters, setters
} </code></pre>
  </div>
  <div>
    <pre><code class="java">@Entity
public class CD { // the owning side
  @Id @GeneratedValue
  private Long id;
  private String title;
  private Float price;
  private String description;
  @ManyToMany
  @JoinTable(name = "jnd_art_cd",
    joinColumns = @JoinColumn(name = "cd_fk"),
    inverseJoinColumns = @JoinColumn(name = "artist_fk"))
  private List&lt;Artist&gt; createdByArtists;
  // Constructors, getters, setters
} </code></pre>
  </div>
</div>

![](images/orm-manytomany-jointable.png#no-border)

---

### M-N bidirectional, owner B

<div style="display: flex; justify-content: space-around">
  <div>
    <pre><code class="java">@Entity
public class Artist { // the inverse side
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  @ManyToMany(mappedBy = "createdByArtists")
  private List&lt;CD&gt; appearsOnCDs;
  // Constructors, getters, setters
} </code></pre>
  </div>
  <div>
    <pre><code class="java">@Entity
public class CD { // the owning side
  @Id @GeneratedValue
  private Long id;
  private String title;
  private Float price;
  private String description;
  @ManyToMany
  @JoinTable(name = "jnd_art_cd",
    joinColumns = @JoinColumn(name = "cd_fk"),
    inverseJoinColumns = @JoinColumn(name = "artist_fk"))
  private List&lt;Artist&gt; createdByArtists;
  // Constructors, getters, setters
} </code></pre>
  </div>
</div>

![](images/orm-manytomany-jointable.png#no-border)

---

### Derived identities

- An entity may share (part of) its key with its "parent" entity
- put `@Id` on relationship
  - compatible with `IdClass` for composite keys
- put `@MapsId` on relationship
  - compatible with `EmbeddedId` for composite keys
- When parent has composite key, add `@JoinColumn` per id

???trainer
Examples in the spec: https://jakarta.ee/specifications/persistence/3.0/jakarta-persistence-spec-3.0.html#a149

???t

---

### @OrderBy

- Orders the collection when you retrieve the association
    - When specified without explicit `value`, sorts by PK ASC

<div style="display: flex; justify-content: space-around">
  <div>
    <pre><code class="java">@Entity
public class News {
  @Id @GeneratedValue
  private Long id;
  @Column(nullable = false)
  private String content;
  @OneToMany(fetch = FetchType.EAGER)
  @OrderBy("postedDate DESC, note ASC")
  private List&lt;Comment&gt; comments;
  // Constructors, getters, setters
} </code></pre>
  </div>
  <div>
    <pre><code class="java">@Entity
public class Comment {
  @Id @GeneratedValue
  private Long id;
  private String content;
  private Integer note;
  @Column(name = "posted_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date postedDate;
  // Constructors, getters, setters
} </code></pre>
  </div>
</div>

---

### Relationship navigation: JOIN

- In JPQL `FROM`, you can specify "navigational declarations"
    - Note also `DISTINCT` in `SELECT` to avoid duplicates

```sql
SELECT DISTINCT o
FROM Order o JOIN o.orderLines l JOIN l.product p
WHERE p.productType = 'office supplies'
```
- Join types
    - [inner] join
        - default
        - rows that can't be joined, are lost
    - left [outer] join
        - always keep all rows on left side, even if there are none on right side
    - "," (cartesian product)
        - don't use this unless you need to join 2 entities without foreign key
    - "implicit joins" = path expressions

```sql
SELECT p.vendor
FROM Employee e JOIN e.contactInfo.phones p
WHERE e.contactInfo.address.zipcode = '95054'
```

???trainer

Note that there is no right join.
This is why directionality in Java is important.

???t

---

### Fetch strategies

- Two options:
    - EAGER fetching: association is loaded automatically
    - Lazy fetching: association is not loaded until explicitly used
        - Will throw an exception if the entity manager is closed by then
- **(almost) always use LAZY**
    - **NEVER** fetch more than you need...
    - EAGER fetching (default for xToOne) is prone to ["the N+1 query problem"](https://vladmihalcea.com/n-plus-1-query-problem/)
        - This is not the same as 1+N query problem (but that also can be avoided by using LAZY)

Annotation  | Default Fetching Strategy | Advice
:---------- | :------------------------ | :----
@OneToOne   | EAGER                     | <- explicitly override this!
@ManyToOne  | EAGER                     | <- explicitly override this!
@OneToMany  | LAZY                      | <- keep this
@ManyToMany | LAZY                      | <- keep this
- Sadly, LAZY is only a recommendation... the provider may ignore it

---

### Fetching lazy associations

- Use `JOIN FETCH` in JPQL
    - Without `FETCH`, the collection is not fetched

```java
String query = "SELECT o FROM Order o JOIN FETCH o.orderLines";
List<Order> orders = em.createQuery(query, Order.class).getResultList();
```
- Using entity graphs
    - Named or ad-hoc (like queries)
    - Pass as hint during find or query

```java
@NamedEntityGraph(name = "with-post", attributeNodes = @NamedAttributeNode("post"))
// if attribute node is an entity, you may specify a subgraph
public class Comment {...}
```
```java
var entityGraph = em.getEntityGraph("post-entity-graph");
Map<String, Object> properties = Map.of("javax.persistence.loadgraph", entityGraph);
Comment c = em.find(Comment.class, 1L, properties);
```

???trainer

    - `EntityGraph`, `AttributeNode`, `Subgraph` interfaces
    - pass as either property when doing a find or query:
        - `javax.persistence.fetchgraph`
        - `javax.persistence.loadgraph`

???t

---


### Cascading Events

```java
public enum CascadeType {ALL, PERSIST, MERGE, REMOVE, REFRESH, DETACH};
```
- Influences what happens to related entities

```java
@Entity
public class Customer {
  @OneToOne (fetch = FetchType.LAZY,
*   cascade = {CascadeType.PERSIST, ...})
  @JoinColumn(name = "address_fk")
  private Address address;
  // ...
}
```

```java
Customer customer = new Customer("Antony", "Balla", "tballa@mail.com");
Address address = new Address("Ritherdon Rd", "London", "8QE", "UK");
customer.setAddress(address);

tx.begin();
*em.persist(customer); // No need to persist address, this is done automatically through cascading
tx.commit();
```
- Use with **caution**: cascading REMOVE could drop your entire database!
    - Tip: use cascade sparingly, only when you can explain clearly why you need it

---

### Orphan Removal

- Orphan = _dangling_ data (row in database that is no longer linked)
- Example:

```java
@Entity
public class Customer {

  // Address will become an orphan when:
  // 1. this Customer is removed without removing the address
  // 2. the relationship is broken: customer.setAddress(null);
  // in both bases, address would be garbage collected (but not removed from the db)
* @OneToOne (fetch = FetchType.LAZY)
  private Address address;

}
```
- Can be prevented by either:
    - manually deleting `address` too
    - including `CascadeType.REMOVE` in the cascade list of the relationship
    - specifying `orphanRemoval = true` in the relationship

---

### Updates and bidirectional relationships

- There is a significant disadvantage to working with bidirectional relationships:
    - **both** sides must be updated if the relationship changes

```java
@Entity class Post{
    @Id Long id;
    @OneToMany(mappedBy="post", cascade=ALL, orphanRemoval=true) List<Comment> comments;
}

@Entity class Comment{
    @Id Long id;
    @ManyToOne Post post;
}
```
- Example: adding a new comment `c` to post `p`

```java
Post p = em.find(Post.class, 1L);
em.getTransaction().begin();
Comment c = new Comment(99L);
*c.setPost(p);                      // update owning side
*p.getComments().add(c);            // update inverse side
em.getTransaction().commit();
```

---

## JPA 102

- Conversion to migration-based database scripts (e.g.: [Flyway](https://flywaydb.org/) or [Liquibase](https://www.liquibase.org/))
    - These are **NOT** a part of JEE!

```xml
<dependency>
    <groupId>org.flywaydb</groupId>
    <artifactId>flyway-core</artifactId>
    <version>7.9.0</version>
</dependency>
```
- Transactional concurrency: locking mechanisms
- Life cycle callbacks and auditing
- Criteria builder
- Native queries
- Query projections
- Caching
- Testing tips

???trainer

Should we cover attribute converters?
I think it's an anti pattern (I have yet to find a good use case), so I've decided to leave it out for now...

???t

---

### Transactional Concurrency

- Two parallel transactions modifying the same row = trouble!

<img src="images/em-concurrency.png#no-border" style="height: 200px"/>

- Might seem exotic, but happens all too often (double click submits...)
- Solution: locking
    - Optimistic: check if data is changed just before flushing
    - Pessimistic: only 1 tx can run (others must wait)

---

### Optimistic Locking

- `@Version` enables optimistic locking automatically!
    - `int`, `Integer`, `short`, `Short`, `long`, `Long`, or `Timestamp`

```java
@Entity
public class Book {
  @Id @GeneratedValue
  private Long id;
* @Version
* private Integer version;
  // ...
}
```

<img src="images/em-optimistic-locking.png#no-border" style="height: 200px"/>

---

### Pessimistic Locking

- Obtains a lock eagerly (see also `javax.persistence.lock.timeout`)
- Performance degradation, because only 1 transaction may run at any time

<img src="images/locking-strategies.png"/>
- Useful in applications with great risks of contention
- Can also be applied to entities that do not contain the `@Version` attribute

???trainer

<img src="images/pessimistic-locking.png"/>

???t

---

### [Lock modes](https://jakarta.ee/specifications/persistence/3.0/jakarta-persistence-spec-3.0.html#a2084)

- Select a locking mechanism using the `LockModeType` enum
    - `OPTIMISTIC` (= `READ`), `OPTIMISTIC_FORCE_INCREMENT` (= `WRITE`)
    - `PESSIMISTIC_READ`, `PESSIMISTIC_WRITE`, `PESSIMISTIC_FORCE_INCREMENT`
    - `NONE`
- Read, _then_ lock

```java
Book book = em.find(Book.class, 12);
// Lock to raise the price
*em.lock(book, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
book.raisePriceByTwoDollars();
```

- Read _and_ lock

```java
*Book book = em.find(Book.class, 12, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
// The book is already locked, raise the price
book.raisePriceByTwoDollars();
```

---

### Life Cycle Callbacks

- Allow you to add your own business logic when certain life-cycle events occur

![](images/em-callbacks.png#no-border)

---

### Life Cycle Callbacks

```java
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String email;
  private LocalDate dateOfBirth;
  @Transient
  private Integer age;

* @PrePersist
* @PreUpdate
  private void validate() {
    if (email == null || "".equals(email)) throw new IllegalArgumentException("Invalid email");
  }

* @PostLoad
* @PostPersist
* @PostUpdate
  public void calculateAge() {
    if (dateOfBirth == null) {
      age = null;
      return;
    }
    // ...
  }
}
```

---

### Life Cycle Listeners

- Extract logic to a separate class for reusability
    - Note how customer is passed as parameter

```java
public class EmailValidationListener {
  @PrePersist
  @PreUpdate
  private void validate(Customer customer) {
    if (customer.getEmail() == null || "".equals(customer.getEmail())) {
      throw new IllegalArgumentException("Invalid email");
    }
  }
}
```

```java
*@EntityListeners({EmailValidationListener.class})
@Entity
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String email;
  private LocalDate dateOfBirth;
  @Transient
  private Integer age;
  //...
}
```

---

### Common usecase: auditing

- Audit fields:
    - Who created this, when was it created, when was it last modified, by whom...

```java
@MappedSuperclass
@EntityListeners(AuditListener.class)
public class AuditableEntityBase {
    private String createdBy;
    private String lastModifiedBy;
    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;
    // getters, setters
}
```
```java
public class AuditListener {
    @PrePersist void setCreationFields(AuditableEntityBase e) {
        e.setCreatedBy(...);
        e.setCreatedAt(LocalDateTime.now());
        setModificationFields(e);
    }

    @PreUpdate void setModificationFields(AuditableEntityBase e) {
        e.setLastModifiedBy(...);
        e.setLastModifiedAt(LocalDateTime.now());
    }
}
```

---

### Default Listeners

- Listeners that should be applied to each entity
- Have to be enabled in _second_ descriptor XML: `orm.xml`
    - Maven: `src/main/resources/META-INF/orm.xml`

```xml
<entity-mappings>
    <persistence-unit-metadata>
        <persistence-unit-defaults>
            <entity-listeners>
                <entity-listener class="com.inetum.realdolmen.DebugListener"/>
            </entity-listeners>
        </persistence-unit-defaults>
    </persistence-unit-metadata>
</entity-mappings>
```

- Selectively exclude default listeners with `@ExcludeDefaultListeners`

---

### Criteria Builder API

- Allows construction of dynamic queries through Java objects
    - Provides more compile-time safety than working String based

```sql
SELECT c FROM Customer c WHERE c.age = 40
```
- Compare this to:

```java
CriteriaBuilder builder = em.getCriteriaBuilder();
CriteriaQuery<Customer> criteriaQuery = builder.createQuery(Customer.class);
Root<Customer> c = criteriaQuery.from(Customer.class);
criteriaQuery.select(c).where(builder.greaterThan(c.get("age").as(Integer.class), 40));
```
- Can be further "improved" using meta-model API
    - Generated by JPA implementation based on detected entities
        - Requires maven plugin and marking generated files as extra source root
    - Typically named `Entity_` (with the underscore)

```java
CriteriaBuilder builder = em.getCriteriaBuilder();
CriteriaQuery<Customer> criteriaQuery = builder.createQuery(Customer.class);
Root<Customer> c = criteriaQuery.from(Customer.class);
*criteriaQuery.select(c).where(builder.greaterThan(c.get(Customer_.age), 40));
```

---

### Native Queries

- To use specific features of a database in native SQL

```java
*Query query = em.createNativeQuery("SELECT * FROM t_customer", Tuple.class);
List<Tuple> customers = query.getResultList();
```

```java
@Entity
*@NamedNativeQuery(name = "findAll", query="select * from t_customer")
@Table(name = "t_customer")
public class Customer {...}
```
- You should avoid this as much as possible
    - Avoid binding your code to a specific version of a specific DB

---

### Query projections

- Tuple (~ Map)
    - Generic interface that can be used when selecting anything

```java
em.createNativeQuery("SELECT * FROM t_customer", Tuple.class)
  .getResultStream()
  .map(t -> new Customer(((Tuple)t).get("firstName", String.class), ...))
  .collect(Collectors.toList());
```
- Constructor Expressions
    - Create instances of any non-entity straight from JPQL!

```java
class CustomerDTO {
    private String firstName;
    private String lastName;

    public CustomerDTO(String firstName, String lastName) { ... }
}

em.createQuery(
  "select new com.inetum.realdolmen.CustomerDTO(c.firstName, c.lastName) from Customer c",
  CustomerDTO.class
).getResultList();
```

---

### Caching

![](images/em-cache.png#no-border)

---

### L2 Caching

- Effort to standardize L2 caching through `@Cacheable(boolean)`
    - L2 cache is **not** provided by container!

```java
@Entity
*@Cacheable(true)
public class Customer {
  @Id @GeneratedValue
  private Long id;
  private String firstName;
  private String lastName;
  private String email;
  // Constructors, getters, setters
}
```

- Specify `shared-cache-mode` attribute in `persistence.xml`
  - `ALL`, `DISABLE_SELECTIVE`, `ENABLE_SELECTIVE`, `NONE`, `UNSPECIFIED`

---

### Testing guidelines

- Do not (unit) test your entities themselves, this is trivial
- Do not test your repositories directly, this is also trivial
- Write (integration) tests for your service layer
    - This will test your entities and repositories indirectly
    - Do not mock the database!
    - Truncate all tables before each test
    - Verify the correctness of inserts/updates/deletes, not selects
        - Since reading data does not change the database, testing select has less value
- Write (unit) tests for the business logic

