# Jakarta Transactions (JTA)

---

## Rundown

- Own spec since JEE 7 (2013)
    - Originally part of EJB
- Uses package `javax.transaction`
- JEE transaction can span multiple resources and even servers
- Key features
    - Declarative transaction management
        - `@Transactional`
    - Programmatic transaction management
        - `UserTransaction` (JNDI resource)


???trainer
 Understanding Transactions

- Business logic often consists of _logical groups of operations_
    - These groups must be executed as a single _unit of work_ or _transaction_
        - E.g. they must succeed or fail together

- A _transaction_ is described by four properties called _ACID_

| Property          | Meaning
|-
| _Atomicity_       | All logical operations are executed together as a single atomic unit <small>(e.g. "undividable")</small>
| _Consistency_     | No corruption must occur on the data after the transaction is complete
| _Isolation_       | The intermediary state of operations should not be visible to other transactions
| _Durability_      | When complete the changes are permanent and become available to others

???t

---

## Global vs Local Transactions

<img src="images/jta-ex1.png#no-border" style="float:right; height: 22em"/>
- _Local_ transactions span only across a single datasource
    - Requires no special support by the driver
- _Global_ or _distributed_ transactions span across multiple datasources
    - Requires driver to be _XA <small>(eXtended Architecture)_</small> capable
        - Protocol that supports _multi-phase commits_
    - Not all datasources support this
- Configuration is usually done on the _application server_

---

## Transaction Propagation Levels

- Can be specified on methods
    - Specified on class = for all methods

```java
public void createBook(Book b) {     // If this method is transactional...
    em.persist(b);
    inventoryEjb.addItem(b);         // ... how does that transaction propagate into this method?
}
```

| `TxType.`                         | Meaning
|-
| `REQUIRED` (default)              | Method must be invoked within TX. Container creates one only if necessary.
| `REQUIRES_NEW`                    | Always creates new TX. Existing one is paused and not atomic with the new one.
| `MANDATORY`                       | Method requires TX, but will not be created (e.g. must participate in existing one)
| `SUPPORTS`                        | Method doesn't care about TX (works either with or without one)
| `NOT_SUPPORTED`                   | Method must not run in TX. Pause existing one and resume later.
| `NEVER`                           | Method must not run in a TX. Throw exception if one exists.

???trainer
This is a repeat of the EJB slides but with a different enum

???t

---

### Transaction Propagation Levels

- `createBook()` acts as client, `addItem()` has a _propagation level_ set

![](images/transactions-propagation-levels.png#no-border)

???trainer
This is a repeat of the EJB slides

???t

---

## @Transactional

- Non-EJB managed beans are not transactional by default
    - Produces `TransactionRequiredException` on DB access
- `@Transactional` lets you demarcate transaction boundaries
    - Can be used both on class and method level

| Property              | Meaning
|-
| `value`               | Define the _propagation level_ <small>(defaults to `REQUIRED`)</small>
| `rollbackOn`          | Cause _rollback_ on any of the specified exception classes
| `dontRollbackOn`      | Cause _commit_ on any of the specified exception classes <small>(takes precedence over `rollbackOn`)</small>

- Exception behavior
    - Checked exceptions: commit
    - Unchecked exceptions: rollback
- `@TransactionScoped`: CDI scope as long as the current transaction

???invisible

 JPA: Transaction Isolation level

- The level of _isolation_ can be varied
    - Less isolation is usually beneficial for performance, but "side effects" may occur

| Isolation level                       | Effects
|-
| Read uncommitted (least strict)     | Current transaction can see uncommitted data from others<br/><small>Side effects: _dirty reads_, _non-repeatable reads_, _phantom reads_</small>
| Read committed                      | Current transaction can see only committed data from others<br/><small>Side effects: _non-repeatable reads_, _phantom reads_
| Repeatable read                     | Current transaction can not observe changes by others<br/><small>Side effects: _phantom reads_</small>
| Serializable (most strict)          | Current transaction locks out all others while executing

- Can be configured on JDBC driver or JPA persistence unit level

???i