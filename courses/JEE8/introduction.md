# JEE

???trainer
Who knows JEE (welcome answers from audience)
Course is an introduction to JEE, not a deep dive (80/20 rule)
Goals:
understand what is JEE, strengths and weaknesses
knowledge of the specs
hands-on experience with the most important specs
???t

---

## Specifications

| Name | Abbr | Purpose | Implementation(s)|
| -
| Persistence | JPA | SQL database manipulation | Eclipselink, Hibernate, OpenJPA... |
| Servlet | / | General purpose request handlers | Undertow, Grizzly...|
| Context and Dependency Injection | CDI | Context and dependency injection | Weld... |
| Server Faces | JSF | Web MVC framework | Mojarra, MyFaces... |
| RESTful web services | JAX-RS | RESTful web services | Jersey, RESTeasy, CXF... |
| Messaging | JMS | Messaging API | ActiveMQ, OpenMQ...|
| ... | . | ... | ... |
- Full list of specifications: https://jakarta.ee/specifications/
- Each spec has own release cycle
- [JEE release](https://www.oracle.com/java/technologies/javaee/javaeetechnologies.html#javaee8) (=version): snapshot of different versions of different specs
- JEE application typically combines multiple (but rarely all) specs

???trainer
These are only some of the specs.
There are, at the time of writing (2020-12-22), 41 specs on the Jakarta EE site.
Mention that looking up a spec for info is a good habit.
Mention also the jakarta ee tutorial?
???t

---

## Profiles

![](images/jee-profiles-custom.png)

???trainer

- [Full platform](https://jakarta.ee/specifications/platform/9/jakarta-platform-spec-9.html#a3252)
- [Web profile](https://jakarta.ee/specifications/webprofile/9/jakarta-webprofile-spec-9.html#web-profile-definition)
- [MicroProfile](https://microprofile.io/) (unofficial for now, might be part of JEE 10 as "core profile")

Pretty good reply here by Basil Bourque, endorsed by Arjan Tijms:
https://stackoverflow.com/questions/24239978/java-ee-web-profile-vs-java-ee-full-platform

???t

---

## History

- 7 releases by Oracle (once Sun microsystems)
    - J2EE 1.2 (12 December 1999)
    - J2EE 1.3 (24 September 2001)
    - J2EE 1.4 (11 November 2003)
    - Java EE 5 (11 May 2006)
    - Java EE 6 (10 December 2009)
    - Java EE 7 (28 May 2013)
    - Java EE 8 (31 August 2017)
- Handed over to Eclipse Foundation in September 2017
    - Name change for legal reasons -> **Jakarta EE** (codename EE4J)
    - Jakarta EE 8 (10 September 2019)
        - identical to Java EE 8
    - Jakarta EE 9 (22 November 2020)
        - functionally identical to JEE 8 but package naming change: javax -> jakarta
- JSR/JCP is now called JESP (Jakarta EE Specification Process)

???trainer
What is a JEE release?
JEE release = combination of specific versions of all the specs
JEE has been around for more than 20 years! (probably older than students)
When JEE was first conceived, servers were the workhorse, monolith was king.
NoSQL databases practically didn't exist and there were no smartphones yet, no mobile users.
Because of the confusion caused by the name changes, the common abbreviation is JEE.
???t

---

## A typical JEE application: a distributed application model

<img src="images/jee-tiers-full.png#no-border" style="float: right; padding: 10px; height: 27em">

- Client components:
    - (Desktop) application
    - Web clients
- Web components:
    - Servlets
    - Webpages
- Business components:
    - Enterprise JavaBeans (EJB)
- Packaging
    - JAR
    - WAR
    - EAR (wrapper containing JAR, WAR and more)

???trainer
A JEE application is divided into components according to function, and components are installed on various machines depending on the tier to which they belong.

How is an enterprise application different from a regular (java SE) application?

It is in fact just a way for multiple users, possibly with different profiles, to concurrently operate on a shared data set.
There is practically always a layer to access the data.
This can be a UI, to allow users direct access (typically something like JSF), or an API, to allow programmatic access from other applications (which could also be UIs, separated from the Java side, like an angular application).
In case of an API, almost always HTTP is chosen as protocol, and almost always in a RESTful way, though SOAP and JMS are also still used.
It is often a requirement that data is protected, which requires that users can identify themselves.
Especially in an enterprise setting, this is rarely the responsibility of the application itself, user management is typically delegated to a central security system (e.g.: Active Directory or LDAP/Kerberos).

???t

---

## WAR file: JEE Web Application
<img src="images/war.png#no-border" style="height: 22em; float: right"/>
- Most common form of JEE
- Contains:
    - java classes
    - static (non-java) files
    - deployment descriptor
        - `WEB-INF/web.xml` ([XSD](http://www.oracle.com/webfolder/technetwork/jsc/xml/ns/javaee/web-app_4_0.xsd))

```xml
<web-app>
    <!-- servlet definitions  -->
    <!-- context params       -->
    <!-- and much more...     -->
</web-app>
```
- Maven:
    - `maven-war-plugin`
    - `src/main/webapp/`
        - put non-java files here
    - `<packaging>war</packaging>`

???trainer
JEE application = one or more units
Unit is packaged for deployment in specific container
Packaged unit contains
Functional components (compiled java files)
Deployment descriptor (sometimes optional)
???t

???invisible
Enterprise Archive (EAR)
EAR structure
images/ear.png#no-border
???i

---

## JEE Platforms (AKA JEE Runtimes)

- Application servers (traditional)
    - Curated collection of libraries and frameworks
        - Specific versions
        - Largely preconfigured
        - Usually allows for deep customisation
    - Full JEE (JBoss...) or web container only (Tomcat, Jetty...)
    - Must be installed and configured in advance
        - Requires totally different knowledge than JEE development
    - Originally designed to host multiple applications
- "Cloud-native" (future?)
    - Create a bootable jar (or even compile natively with GraalVM)
    - Configuration is done in code and runtime is bundled inside artifact
- Curse and blessing...
    - Created to make running JEE applications easy
    - Usually complicated and knowledge is required for development, too

???trainer

Most used among our customers (q4 2021):
  - JBoss EAP 6-7
  - WebLogic

This is perhaps one of the most hated parts of JEE but at the same time one of its largest strenghts: if you master JEE, you can write applications that can run in ANY JEE compliant platform.
If tomorrow a new platform is released that runs on mars rovers, you can write applications for mars rovers without having to learn anything new.
This is VERY interesting for you as an application developer, because new platforms ARE in fact being created right now, in the ongoing move to the cloud.

- Look for compliance certification (typically for a specific JEE version)
    - e.g. [on the Jakarta website](https://jakarta.ee/compatibility/)
    - even [on wikipedia](https://en.wikipedia.org/wiki/Jakarta_EE#Certified_referencing_runtimes)
    - vendor's own site...

???t

---

### Java Naming and Directory Interface (JNDI)

- Binds objects in JVM to names (String)
    - Grouped in namespaces (aka contexts)
        - Local: `java:`
        - JEE: `java:comp`, `java:module`, `java:app`, `java:global`
- Facilitates access to objects through lookup
- In JEE
    - Resources
        - Data sources (database connection pool)
        - Messaging services (JMS)
        - Mail sessions
        - URLs
    - EJBs
- JNDI objects are "managed" by the application server
    - Containers take care of creation and destruction of instance(s)

???trainer

Different for each platform, but generally speaking, all platforms offer resources
Actually Java SE technology!
some application servers add their own JNDI namespaces, like wildfly adds java:jboss

???t

---

### Obtaining a reference to a JNDI object

- Injection (!)
    - on fields or method parameters
    - only into managed objects (!)
        - Servlets
        - EJBs
        - CDI beans

```java
@Resource(lookup = "java:app/datasources/ExampleDS")
DataSource db;
```
- Manual lookup also possible
    - works anywhere

```java
DataSource db = (DataSource) new InitialContext().lookup("java:app/datasources/ExampleDS");
```

???trainer
Injection and managed objects are very important concepts!
???t

---

## Learning JEE

- Challenges
    - JEE is old
        - Some specs are no longer really relevant
        - Some specs contain old and new approaches
        - Some (modern) specs (partially) replace other (older) specs
    - Need to know/understand at least one JEE platform
- Benefits
    - Stability
    - Longevity
    - Simplicity (relative)
    - Flexibility (= freedom)
    - Mature (= time tested)
    - Large community
    - Strong market position
        - You may not be convinced that JEE is the future, but there are a lot of jobs in JEE now

---

## Development cycle

- Not just "build and run" like JavaSE...
1. Write code
2. Add deployment descriptor(s) (if necessary)
3. Compile code and package compiled output + descriptor(s) (if revelant) into deployable archive
    - `mvn package`
4. Deploy to JEE platform
    - Wildfly: place archive in `/standalone/deployments/`
    - Payara: place archive in `/domains/domain1/autodeploy/`
    - Other: consult the documentation of the platform
5. Interact by accessing through relevant protocol
    - typically an HTTP client

???trainer

Some http clients: browser, curl, postman, but also an e2e test!

<!-- https://mvnrepository.com/artifact/jakarta.platform/jakarta.jakartaee-api -->
<dependency>
    <groupId>jakarta.platform</groupId>
    <artifactId>jakarta.jakartaee-api</artifactId>
    <version>8.0.0</version>
    <scope>provided</scope>
</dependency>
???t

---

## Course model

- Exercise + theory
    - Some long-running exercises: mini-projects
    - Some break-out exercises: focus on a particular thing
- Live coding examples by the instructor
- Technical setup checklist
    - Java **JDK 11** ([**AdoptOpenJDK**](https://adoptopenjdk.net/), [SDKMAN](https://sdkman.io/) for \*nix systems...)
        - set this as your `$JAVA_HOME`
        - make sure the `bin` folder of the JDK is on your path
    - [Maven](https://maven.apache.org/)
        - verify that the correct JDK is used with `mvn -version`
    - [Docker](https://www.docker.com/)
    - Browser (Chromium, Firefox, Safari, Opera, Brave, Edge...)
    - Code editor (IntelliJ, VSCode, Eclipse, Netbeans, VIM, Emacs...)
    - HTTP client (curl, [postman](https://www.postman.com/), [SOAPUI](https://www.soapui.org/), insomnia, paw, httpie...)
    - SQL database (postgres, mysql...)
        - Can be run through docker
    - JEE Platform ([**WildFly**](https://www.wildfly.org/), [Payara Server (CE)](https://www.payara.fish/), [Open Liberty](https://openliberty.io/)...)

???trainer
Docker setup

Make network
`docker network create rd`
Postgresql
`docker run --detach --name pg --network rd -p 5432:5432 -v pgdata:/var/lib/postgresql/data -e POSTGRES_PASSWORD=acaddemicts -e POSTGRES_USER=acaddemicts postgres:alpine`
Wildfly (only for the brave and familiar with docker)
`docker run --detach --name wf --network rd -p 8080:8080 -p 9990:9990 -v C:/deployments:/opt/jboss/wildfly/standalone/deployments -v wfconf:/opt/jboss/wildfly/standalone/config jboss/wildfly /opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0`
`docker exec wf /opt/jboss/wildfly/bin/add-user.sh admin P@ssw0rd --silent`
Network stack alternative:
`docker network create rd`
`docker network connect rd wf`
`docker network connect rd pg`

???t

---

## Starting a new JEE project

- No starter/generator, but it's actually quite simple (with maven)...
    - Generate a new, empty project
        - (you can throw out any generated tests and code)
    - Add the following dependency in the pom

```xml
<dependency>
    <groupId>jakarta.platform</groupId>
    <artifactId>jakarta.jakartaee-api</artifactId>
    <version>8.0.0</version>
    <scope>provided</scope>
</dependency>
```
- Other sane standards in maven:
    - `<maven.compiler.source/target>`: set to correct JDK
        - (1.8 for JEE 8, 11 for JEE 9.1)
    - `<project.build.sourceEncoding>`: set to `UTF-8`
    - In plugins:
        - Specify (recent) version for `maven-war-plugin` to override (outdated) default

???trainer

With maven, it's actually rather easy to start a new JEE project, despite the lack of a starter/generator.
All you need to do is specify the jakarta api dependency, which is an aggregator for all individual specs.
Normally you don't want to overspecify too many dependencies, but since they are in scope provided, this is not a concern here!

Of course, this implies you will later deploy to a platform which also provides the full api... but this is typically true.

???t