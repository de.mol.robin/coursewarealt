# Jakarta Persistence (JPA)

???trainer
Formerly: Java Persistence API (hence the acronym)
???t

---

## Rundown

- Persists data in a relational database
- Object-Relational Mapping (ORM)
  - Using meta-data: annotations or XML
  - Configuration by Exception

---

## Understanding Entities

- Entities live shortly in memory and persistently in a database
- Once mapped, they can be managed by JPA
  - Data is synchronized
- ORM lets you manipulate entities
  - The database is accessed under the covers
- Entities follow a defined life cycle

---

## Understanding Entities

- Entities are typically just JavaBeans with annotations

```java
*@Entity
public class Book {
* @Id
  private Long id;

  private String title;
  private Float price;
  private String description;
  private String isbn;
  private Integer nbOfPage;
  private Boolean illustrations;
  
  public Book() {   // May also be protected or package private 
  }
  
  // Getters, setters
}
```

---

## Understanding Entities

- Rules
    - `@Entity` and `@Id` are required
        - `@Id` maps to the _primary key_
    - There must be a non-private no-arg constructor
        - Package private would do 
    - It must be a top-level class
    - It may not be `final`
    - When passed by value (i.e. RMI) must be `Serializable`

---

## Object Relational Mapping

- Entities are represented both as a class and as a table

![](images/jpa-orm.png#no-border)

---

## Object Relational Mapping

- The corresponding table may look like this

```sql
*CREATE TABLE BOOK (
  ID BIGINT NOT NULL,
  TITLE VARCHAR(255),
  PRICE FLOAT,
  DESCRIPTION VARCHAR(255),
  ISBN VARCHAR(255),
  NBOFPAGE INTEGER,
  ILLUSTRATIONS SMALLINT DEFAULT 0,
* PRIMARY KEY (ID)
)
```

- Can be generated from the class representation

---

## Querying Entities

- JPA lets you query entities in an object-oriented way
    - The gateway to JPA is `EntityManager` (see later)

```java
EntityManagerFactory emf = Persistence.createEntityManagerFactory("booksPU");
EntityManager em = emf.createEntityManager();

em.persist(book);   // Actually executes an INSERT INTO ...
```

![](images/jpa-querying-entities.png#no-border)

---

## Querying Entities

- JPA queries over entities using JPQL
    - Looks a lot like SQL but not exactly (see later)

```sql
SELECT b FROM Book b WHERE b.title = 'H2G2'
```

- Named queries are precompiled (e.g. _prepared_)

```java
@Entity
*@NamedQuery(name = "findAwesomeBook", query = "SELECT b FROM Book b WHERE b.title ='H2G2'")
public class Book {
  @Id
  @GeneratedValue
  private Long id;

  // More properties, constructors, getters, setters
}
```

---

## Querying Entities

```java
*// Notice the absence of SQL and JDBC!
public class Main {
  public static void main(String[] args) {
*   // 1-Creates an instance of book
    Book book = new Book("H2G2", "The Hitchhiker's Guide to the Galaxy", 12.5F, "1-84023-742-2",
      354, false);

*   // 2-Obtains an entity manager and a transaction
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("booksPU");
    EntityManager em = emf.createEntityManager();

*   // 3-Persists the book to the database
    EntityTransaction tx = em.getTransaction();
    tx.begin();
    em.persist(book);
    tx.commit();

*   // 4-Executes the named query
    book = em.createNamedQuery("findBookH2G2", Book.class).getSingleResult();

*   // 5-Closes the entity manager and the factory (should use finally)
    em.close();
    emf.close();
  }
}
```

---

## Persistence Unit

- Application-wide meta configuration is inside `/META-INF/persistence.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<persistence xmlns="http://xmlns.jcp.org/xml/ns/persistence"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence
  http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd"
  version="2.1">
  <persistence-unit name="booksPU" transaction-type="RESOURCE_LOCAL">
    <provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>
*   <class>com.acme.example.Book</class>
    <properties>
      <property name="javax.persistence.schema-generation.database.action" value="drop-and-create"/>
      <property name="javax.persistence.jdbc.driver" value="com.mysql.jdbc.Driver"/>
      <property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/cdbookstore"/>
      <property name="javax.persistence.jdbc.user" value="root"/>
      <property name="javax.persistence.jdbc.password" value=""/>
    </properties>
  </persistence-unit>
</persistence>
```

---

## Entity Life Cycle

![](images/jpa-entity-lifecycle.png#no-border)

---

## Integration with Bean Validation

- JPA automatically delegates validation to Bean Validation (covered later)
    - Executes validation rules right before persisting

```java
@Entity
public class Book {
  @Id
  @GeneratedValue
  private Long id;
  
* @NotNull
  private String title;

  private Float price;
  
* @Size(min = 10, max = 2000)
  private String description;
  
  private String isbn;
  private Integer nbOfPage;
  private Boolean illustrations;
  
  // Constructors, getters, setters
  
}
```

---

## What's New in JPA 2.1?

- Standardized database schema generation
- Converters
  - Convert between database and attributes representations
- CDI support
  - Injection is now possible into event listeners
- Support for stored procedures
  - Bulk update and delete criteria queries
- `TREAT` operator
  - Allows access to subclass-specific state in queries (e.g. "cast")
- Reference implementation: _EclipseLink 2.5_

---

## Automatic Schema Generation

- Automatic schema (re)creation using standardized properties

```xml
<?xml version="1.0" encoding="UTF-8"?>
<persistence xmlns="http://xmlns.jcp.org/xml/ns/persistence" version="2.1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence
  http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd">
  <persistence-unit name="MyPersistenceUnit" transaction-type="RESOURCE_LOCAL">
    <properties>
      <!-- When starting the server, drop and create all tables + execute a post-create.sql file -->
      <property name="javax.persistence.schema-generation.database.action" value="drop-and-create"/>
      <property name="javax.persistence.schema-generation.create-source" value="metadata-then-script"/>
      <property name="javax.persistence.schema-generation.create-script-source"
        value="META-INF/post-create.sql"/>

      <!-- Script file containing bulk insertion of eden data -->
      <property name="javax.persistence.sql-load-script-source" value="META-INF/data.sql"/>

      <!-- We want to create a schema.sql file from JPA's annotation metadata -->
      <property name="javax.persistence.schema-generation.scripts.action" value="create"/>
      <property name="javax.persistence.schema-generation.scripts.create-target"
        value="file:///tmp/schema.sql"/>
    </properties>
  </persistence-unit>
</persistence>
```

???trainer
Now you can replace the old vendor-specific properties by these new and standardized ones:

| Property key                                                                                                                      | Values                                                        | Meaning
| -
| javax.persistence.schema-generation.database.action,<br/>javax.persistence.schema-generation.scripts.action                       | none, create, drop-and-create, drop                           | Specifies how to generate the database structure
| javax.persistence.schema-generation.create-source,<br/>javax.persistence.schema-generation.drop-source                            | metadata, script, metadata-then-script, script-then-metadata  | Use annotation data or custom script to execute schema creation
| javax.persistence.schema-generation.create-database-schemas                                                                       | true, false                                                   | Should the schema itself be created (e.g. CREATE SCHEMA)
| javax.persistence.schema-generation.scripts.create-target, javax.persistence.schema-generation.scripts.drop-target                | File URL                                                      | Locations (file URL) where to write generated scripts
| javax.persistence.database-product-name, javax.persistence.database-major-version, javax.persistence.database-minor-version       | (names)                                                       | Database dialect to use for generating scripts
| javax.persistence.schema-generation.scripts.create-script-source, javax.persistence.schema-generation.scripts.drop-script-source  | File URL                                                      | Location (file URL) to the script files to execute
| javax.persistence.schema-generation.connection                                                                                    | (names)                                                       | JDBC connection to use
| javax.persistence.sql-load-script-source                                                                                          | File URL                                                      | Location of SQL data load script

Be careful when using this in a production environment. A wrong setting here may drop your entire production database. During development when entities are still rapidly changing this feature offers a productivity boost, since
you don't always have to manually change the database schema whenever a property changes.

This feature was already supported for years by both _Hibernate_ and _EclipseLink_, but was never officially standardized.

An example of _Hibernate_'s (old) way:

```xml
<property name="hibernate.hbm2ddl.auto" value="create" />
```

An example of _EclipseLink_'s (old) way:

```xml
<property name="eclipselink.ddl-generation" value="create-tables" />
```
???t

---

## Converters

- Converters can be used to map between property and column types

```java
@Converter
public class RuntimeConverter implements AttributeConverter<Integer, String> {
  @Override
  public String convertToDatabaseColumn(Integer i) {
    return i / 60 + "h" + i % 60 + "m";
  }
  @Override
  public Integer convertToEntityAttribute(String s) {
    Matcher matcher = Pattern.compile("^(\\d+)h(\\d+)m$").matcher(s);   // example: 2h35m
    if(matcher.matches()) {
      return Integer.parseInt(matcher.group(1)) * 60 + Integer.parseInt(matcher.group(2));
    } else {
      return null;    // Maybe throw exception
    }
  }
}
```

- Use them on the Entity fields

```java
@Convert(converter = RuntimeConverter.class)
private Integer runtime;
```

???trainer
In JPA 2.1, converters are a part of the specification. Previously this feature was left to the JPA provider in vendor-specific ways.
???t

---

## Entity Graphs

- Named entity graphs allow defining groups of properties to eagerly load

```java
Map<String, Object> properties = new HashMap<>();
*properties.put("javax.persistence.fetchgraph", em.getEntityGraph("withActors"));
Movie m = em.find(Movie.class, 1, properties);  // Eagerly loads actors
```

```java
@Entity
*@NamedEntityGraph(name = "withActors", attributeNodes = { @NamedAttributeNode("actors") })
public class Movie {
  @Id @GeneratedValue private Integer id;
* @OneToMany(fetch = FetchType.LAZY) List<Actor> actors = new ArrayList<>();  // Lazy!
}

@Entity
public class Actor {
  @Id @GeneratedValue private Integer id;
  private String name;
}
```

| Property                      | Purpose
|-
| javax.persistence.loadgraph   | Only load this graph
| javax.persistence.fetchgraph  | Load this graph together with defaults

???trainer
The granularity of loading entities using the eager-lazy fetching mechanism is quite coarse grained. This caused many applications to generally fetch more then their usescase actually needed.
In JPA 2.1 there is now the feature of _Entity Graphs_ in which you can create a named set of properties that will be fetched eagerly.

This will solve the problem of ugly "touching logic" where a business method would previously `get` all required properties to cause them to lazy-load before returning it to the call site:

```java
// No longer necessary!
Movie loadMovie(Long id) {
  Movie movie = entityManager.find(Movie.class, id);
  movie.getDirector();    // Director is lazy, but in this particular case we want it before crossing tx boundary
  for(Actor actor : movie.getActors()) {
    actor.getName();    // Trigger actors to load lazily before crossing tx boundary
  }
}
```

It is also possible to create `EntityGraph` instances programmatically rather than using annotations:

```java
EntityGraph withActors = em.createEntityGraph(Movie.class);
withActors.addAttributeNodes("actors");
```
???t

---

## Other Improvements

- `@ConstructorResult` allows mapping native queries to DTO using a constructor

```java
@NamedNativeQuery(name="myQuery", query="select id, title from Movie", resultSetMapping="movie-short")
@SqlResultSetMapping(name="movie-short", classes = {
  @ConstructorResult(targetClass=MovieShortDto.class, columns = {
    @ColumnResult(name="id", type=Integer.class),
    @ColumnResult(name="title", type=String.class)
  })
}
)
@Entity
public class Movie {...}
```

- Programmatically creating named queries

```java
Query query = em.createQuery("SELECT m FROM Movie m");
emf.addNamedQuery("runtimeNamedQuery", query);
```

---

## Other Improvements

- New JPQL keywords `ON`, `TREAT` and `FUNCTION`

```sql
SELECT p FROM Person p WHERE TYPE(p) = Actor AND TREAT(p as Actor).yearsActive > 1  -- Downcasting
SELECT m FROM Movie m WHERE FUNCTION('month', m.releaseDate) = 12                   -- Functions
SELECT m FROM Movie m JOIN Director d ON m.releaseYear = d.birthDate                -- Explicit Join
```

- Support for _stored procedures_

```java
@NamedStoredProcedureQuery(name = "MovieById", resultClasses = Movie.class, procedureName = "MOV_BY_ID",
parameters = {
  @StoredProcedureParameter(mode = IN, name="M_ID", type = Integer.class)
}
)
@Entity
public class Movie {...}
```

```java
StoredProcedureQuery q = em.createStoredProcedureQuery("MovieById", Movie.class);
```

---

## Other Improvements

- Bulk _update_ and _delete_ in criteria queries

```java
CriteriaBuilder cb = em.getCriteriaBuilder();
CriteriaUpdate update = cb.createCriteriaUpdate(Movie.class);
Root movie = update.from(Movie.class);
update.set(movie.get("price"), 10); // All movies cost 10 EUR
Query query = em.createQuery(update);
query.executeUpdate();
```

- `EntityListeners` support `@Inject`

```java
public class MovieListener {
* @Inject Logger log;

  @PrePersist public void prePresist(Movie m) {
    // ...
  }
}
```

```java
@EntityListeners(MovieListener.class)
@Entity
public class Movie {...}
```

---

## Exercise: JPA Getting Started

- Create a new JPA _entity_ `Person` with properties
    - `id` as `Long` must be an auto-generated primary key
    - `firstName` and `lastName` as `String`
- Configure a `persistence.xml` with
    - JDBC connection properties
    - Schema should be dropped and recreated automatically
    - Should load a `data.sql` file with test dummy data
- Create a class `AbstractPersistenceTest` which acts as a base class for writing unit tests
- Create test `PersonPersistenceTest` with two unit tests
    - `personCanBeSaved()`
    - `personCanBeLoaded()`
    
---

## Exercise: JPA Getting Started

![](images/jee7-jpa-exercise-getting-started.png#no-border)
