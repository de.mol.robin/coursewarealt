# Concurrency Utilities

---

## Situating Concurrency Utilities

- A new specification in JEE7 is _Concurrency Utilities for Java EE_ <small>(JSR-236)</small>
    - A simple API for concurrency in a container <small>(e.g. _Application Server_)</small>
- Extends on concurrency classes introduced in _Java SE 5_
    - `java.util.concurrent` <small>(JSR-166)</small>
    - You probably know (some of) them
        - `ReadWriteLock`, `ConcurrentHashMap`, `AtomicInteger`, `Future`, `ExecutorService`, `CountDownLatch`, `CyclicBarrier`, `Condition`, ...

---

## Rationale

- In Java EE you are **not** allowed to spawn your own threads
    - Don't use `Thread`, `ExecutorService`, ... directly

```java
@Stateless  // This is an EJB SLSB
public class MyEjb {
    public String retrieveData() {
*       Future<String> promise = Executors.newFixedThreadPool(5)    // Epic failure:
*               .submit(new SomeHeavyTask());                       // Never ever ever do this!
        try {
            return promise.get();
        } catch (InterruptedException | ExecutionException e) {
            return null;
        }
    }
}
```

- There are two main reasons for this
    - Container runs every task on a managed thread
        - Does not expect managed resources to be accessed from a different thread
    - Spawning your own thread potentially thwarts certain enterprise features
        - Performance, scalability, ...

???trainer
Traditionally, it is not allowed in JEE to spawn your own thread. This is because the _Application Server_ manages the
entire environment, including thread management.

The container decides which task is performed by which thread. Generally, each task is processed by a thread from an internal thread pool.
The server associates resources to these threads such as by using a `ThreadLocal`. It does not expect any managed access (to say the `EntityManager`) originating
from one thread to suddenly be referenced from outside of that thread (e.g. your own custom thread).

Also, since dealing with threads is such a risky endeavour in Java, chances are that you don't use them the right way or (more likely) your own threads
are somehow stirring up the managed threading environment. This may undermine the enterprise features an application server actually tries to provide, such as
performance, reliability, scalability, etc...

Up until now, this meant that you could not do multi-core computing in ways that the application server didn't already provide. This had to be fixed: multi-core computing is a rising star
in recent years, so enterprise developers _should_ be able to use the machine's execution pipelines to their full potential.
???t

---

## Managed Executor Service

- You can use a managed _thread pool_ using `ManagedExecutorService`
    - It's a managed version of `ExecutorService`
- You can inject it into your CDI or EJB beans
    - Use `@Resource` for this

```java
@Named  // Or @Stateless, @Stateful, ...
public class ImageProcessorBean {
*    @Resource
*    ManagedExecutorService executorService;
}
```

- Using the `ManagedExecutorService` is the same as using a normal `ExecutorService`
    - Construct a _task_
    - Submit the _task_ to the `ExecutorService`
    - Optionally wait for it to complete

???trainer
The `ManagedExecutorService` is a managed resource, so traditionally you have to inject it into your beans using `@Resource`.
Like all managed resources, a JNDI lookup is performed behind the scenes.
The default JNDI string for this resource is `java:comp/DefaultManagedExecutorService`.
You can also perform a lookup programmatically, or using `ejb-jar.xml`:

```java
InitialContext ctx = new InitialContext();  // Only from inside the managed environment.
ManagedExecutorService executor = (ManagedExecutorService)ctx.lookup("java:comp/DefaultManagedExecutorService");
```

It is possible to configure additional thread pools if needed, but this is vendor specific.
You'll have to check the documentation of your particular application server.

If you are already familiar with `ExecutorService`s, then you probably know what to do from here!
The general practice is to create a task abstraction and offer it to the executor service.
This executor service will then take one of its available threads to process that task.
If you want you can wait for the task to complete (synchronously).
Otherwise you just _fire and forget_ (asynchronously).
???t

---

## Review of Runnable, Callable and Future

- There are two types of tasks
    - Runnable
    - Callable&lt;T&gt;


```java
Runnable task = new Runnable() {
    @Override
    public void run() {                             // Simple
        // Do some computation here
    }
};
Callable<Integer> task2 = new Callable<Integer>() { // More flexible because it
    @Override                                       // allows throwing and returns a value
    public Integer call() throws Exception {
        return 42; // Do some computation here
    }
};
```

- Task typically are
    - Very computationally expensive (slow)
    - Composed together to form a larger algorithm (parallelization)

???trainer
Creating a _task_ is a matter of creating an instance of type `Callable` or `Runnable`.
Both have a _single abstract method_ (SAM) that can be implemented to perform the task at hand.
Since you are intending to offload this task to a different thread, these tasks are usually

- Very time consuming (image processing for example)
- Split into a number of smaller tasks for parallelization

If you are fortunate enough to run in a Java SE 8 environment, you can use _lambdas_ instead!
This will make the code much more concise.

```java
// Lambda
Runnable task = () -> {
    // Do some computation here
};
Callable<Integer> task2 = () -> {
    // Do some computation here
    return 42;
};

// Or even with method references
Callable<Integer> task3 = this::answer;
```
???t

---

## Review of Runnable, Callable and Future

- A _Future_ is an object representing a _future_ result
    - Similar to _Promises_ or _Deferred_ in other languages
- Often returned by _asynchronous_ APIs

```java
Future<Integer> result = asyncMethod();     // Returns immediately (without waiting for completion).
int a = doSomeThingsWeCanDoInTheMeantime(); // Some things may be done parallel to waiting.
                                            // Allows more time efficiency.
int b = result.get();                       // Can't postpone any longer. Wait until b is ready.
return a * b;                               // Need both results here.
```

- When calling `get()` the thread will block until the answer is available
    - If it was already available, no need to wait
- Futures can be cancelled using `cancel()`
- Futures give the option of continuing _synchronously_ or _asynchronously_


???trainer
Using a future, an asynchronous API gives options to the user: do you want to wait for the results to become available, continue without waiting at all or a little of both.

One option would be to dispatch a slow running task to a background process, then continuing with any work that is independent of the result of that task.
At some point, you can no longer do any more work without the results of the dispached task.
At that point you may decide to start waiting (and doing nothing useful yourself while doing so).
This happens when you call `get()`.

It is not at all necessary to ever call `get()`. If you don't care about the result of the task you sent off to a different thread, that's perfectly fine. In this case it is a _fire and forget_ action.
???t

---

## Submitting Tasks

- The `ManagedExecutorService` has four interesting methods

| Method                                       | Purpose
| -
| void execute(Runnable)                       | Submit a Runnable to the executor. Fire-and-forget only!
| Future&lt;T&gt; submit(...)                  | Submits a single task, returning a Future.
| List&lt;Future&lt;T&gt;&gt; invokeAll(...)   | Submits a number of tasks to be all completed. Returns a List of Futures.
| T invokeAny(...)                             | Submits a number of tasks of which the first completed wins. All others are cancelled.

- Some of these methods have an optional _timeout_ parameter

???trainer
None of these methods are specific to JEE. It is all based on the `ExecutorService` top level interface which is part of Java SE 5+.

**Note:** A number of methods is inherited from `ExecutorService` such as `shutdown()`. You must never call these life-cycle methods on a `ManagedExecutorService`.
The Application Server will take care of all housekeeping tasks.
???t

---

## Propagating Managed Resources

- When dispatching a task to a `ManagedExecutorService` the _security context_ is propagated
    - This means the task runs as the same _Principal_ and can do the same security checks as the origin
- The _transaction_ is **not** automatically propagated
    - A task can lookup the `UserTransaction` though

```java
public class MyTransactedTask implements Runnable {
    @Override
    public void run() {
        InitialContext context = new InitialContext();  // JNDI lookup inside task
        UserTransaction tx = (UserTransaction)context.lookup("java:comp/UserTransaction");
        tx.start();     // Work with transaction depending on context
        // ...
        tx.commit();
    }
}
```

???trainer
BMT is a common shorthand for `Bean Managed Transactions`, and can be enabled using `@TransactionManagement`.
???t

---

## Integration with CDI

- You can implement a task as a CDI bean
    - This offers seamless integration with the rest of JEE7
- One limitation
    - Only works for `@ApplicationScoped` and `@Dependent` beans <small>(but that's ok)</small>

```java
@Named
@Dependent
public class PersonRetrievalTask implements Callable<List<Person>> {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Person> call() throws Exception {
        return entityManager.createQuery("select p from Person p").getResultList();
    }
}
```

---

## Integration with CDI

- An example of a task being submitted as a CDI bean

```java
@Named
@RequestScoped
public class PersonRetrievalBean {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    PersonRetrievalTask task;

    @Resource
    ManagedExecutorService executorService;

    public String loadPeople() {
        Future<List<Person>> future = executorService.submit(task); // Submit task for execution
        logger.info("Task submitted to retrieve persons");          // Do some stuff while waiting
        try {
            List<Person> people = future.get().toString();          // Wait for result and return
            return people.toString();
        } catch (InterruptedException | ExecutionException  e) {
            return "Failed";
        }
    }
}
```

???trainer
CDI beans can be used as tasks if they implement `Callable` or `Runnable`.
This is a huge deal, because it provides integration with the rest of the JEE7 world.
As shown in this example, you a task can access the _Persistence Context_ this way, as well as the rest of JEE's managed resources.
???t

---

## Managed Task

- A task can optionally implement `ManagedTask`
    - Allows registering a `ManagedTaskListener`
    - Allows specifying execution hints to the underlying `ExecutorService`

```java
public class Work implements Runnable, ManagedTask {
    @Override
    public void run() {
        // Do some work
    }

    @Override
    public Map<String, String> getExecutionProperties() {
        HashMap<String, String> properties = new HashMap<>();
        properties.put("myCustomProperty", "Awesome!");
        return properties;
    }

    @Override
    public ManagedTaskListener getManagedTaskListener() {
        return new MyTaskListener();    // See later
    }
}
```

???trainer
The ManagedTask interface can be used to specify custom (or vendor specific) properties to the underlying implementation of the used `ManagedExecutorService`.

More interestingly you can register a `ManagedTaskListener` this way.
???t

---

## Managed Task Listener

- A listener can intecept interesting _life-cycle_ events

```java
class MyTaskListener implements ManagedTaskListener {
    @Override
    public void taskAborted(Future<?> f, ManagedExecutorService e, Object t, Throwable ex) {
        // Executed when the task was cancelled (example in case of invokeAny() or Future.cancel())
    }

    @Override
    public void taskDone(Future<?> f, ManagedExecutorService e, Object t, Throwable ex) {
        // Executed when the task is completed (success or failure)
    }

    @Override
    public void taskStarting(Future<?> f, ManagedExecutorService e, Object t) {
        // Executed when the task is about to be started (not same as offered to the service)
    }

    @Override
    public void taskSubmitted(Future<?> f, ManagedExecutorService e, Object t) {
        // Executed when the task is submitted (may be queued for later execution)
    }
}
```

???trainer
The `ManagedTaskListener` allows life-cycle events to be detected without coupling this to the executing task itself. You can see that each of these methods
recevies an parameter `Object t` which is the task object that is passed to the listener.
???t

---

## Managed Scheduled Executor Service

- For scheduled (timed) tasks use `ManagedScheduledExecutorService`
    - It's a managed version of `ScheduledExecutorService`
- You can inject it into your CDI or EJB beans
    - Use `@Resource` for this

```java
@Named  // Or @Stateless, @Stateful, ...
public class ImageProcessorBean {
*    @Resource
*    ManagedScheduledExecutorService executorService;
}
```

- Usage is the same as a normal `ScheduledExecutorService`

???trainer
Using the `ManagedScheduledExecutorService` is very similar to `ManagedExecutorService`. It also works with task, but this time
they are not offered on-demand, but rather scheduled to be invoked at a later time or repeatedly using an interval.

The JNDI name is `java:comp/DefaultManagedScheduledExecutorService` so as an alternative to `@Resource` you can also do a programmatic JDNI lookup.

```java
InitialContext ctx = new InitialContext();  // Only from inside the managed environment.
ManagedScheduledExecutorService executor = (ManagedScheduledExecutorService)ctx.lookup("java:comp/DefaultManagedScheduledExecutorService");
```
???t

---

## Scheduling Tasks

- A scheduled executor service has three interesting methods

| Method                                            | Purpose
|-
| ScheduledFuture<V> schedule(...)                  | Schedule a task once at a time in the future
| ScheduledFuture<?> scheduleAtFixedRate(...)       | Schedule a task repeatedly at a fixed rate
| ScheduledFuture<?> scheduleWithFixedDelay(...)    | Schedule a task repeatedly with a fixed delay between invocations

- They all return a `ScheduledFuture`
    - Has a single extra `getDelay()` method
        - Returns how much time left until it is triggered

???trainer
The difference between a _fixed rate_ and a _fixed delay_ is that a fixed delay starts to wait for a said amount of time after it has finished, whereas
the fixed rate would take the execution time of the task itself into account and triggers more reliably at said intervals.

A `ScheduledFuture` is identical to a normal `Future` but with one additional method: `getDelay()`, which allows you to estimate how much time left until the
task is even attempted. This is necessary because unlike a regular `ExecutorService`, the `ScheduledExecutorService` does not invoke the task
at the earliest possible time.
???t

---

## Scheduling Tasks

- An example usage that schedules a task with a delay

```java
@Named
@ApplicationScoped
public class InvoiceBean {
    @Resource
    ManagedScheduledExecutorService ses;

    @PersistenceContext
    EntityManager entityManager;

    public String addInvoice(Invoice invoice) {
        entityManager.persist(invoice);
        ses.schedule(new Runnable() { // In 7 days send a gentle reminder to the user
            @Override
            public void run() {
                sendEmailReminder(invoice); // Dummy. Implement this yourself.
            }
        }, 7, TimeUnit.DAYS);
        return "Scheduled";
    }
}
```

???trainer
With this example we try to show how the `ManagedScheduledExecutorService` could be used in the real world.
Of course, writing all the code to actually send an email reminder hardly fits on one page, let alone one slide and also
would't be very relevant to the topic at hand. Focus on understanding how to use the scheduling mechanism using the
`schedule()` family of methods, and how to apply this to real world cases.
???t

---

## Managed Threads

- The `ManagedThreadFactory` allows creating individual managed threads
    - Still safe to use in an JEE environment

```java
@Named
public class SingleAction {
    @Resource(lookup="")
    ManagedThreadFactory factory;

    public void doSomeWork() {
        factory.newThread(new MyTask());
    }
}
```

???trainer
Just like before it is possible to obtain the `MangedThreadFactory` using a manual JNDI lookup. The lookup string is `java:comp/DefaultManagedThreadFactory`.

???t

???invisible

===

Exercise Concurrency

- In this exercise we will try out the new concurrency utilities
- Open and run the starter project for this exercise
- Update the `PersonServer` WebSocket server endpoint
  - Create a `Producer` for the `ManagedScheduledExecutorService`
  - Inject the `ManagedScheduledExecutorService`
  - Schedule the save operation to happen after 30 seconds
  - Insert several `Person`s using the form
  - Notice the arrival of the messages through the WebSocket after the scheduled time
  - Update the `PersonServerTest` to reflect the changes made

???i
