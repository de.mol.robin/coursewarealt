# Timer Service

---

## Timer Service Rationale

- Many application require a task to be executed repetitively
    - A batch job needs to be run every night
    - Send birthday e-mails to users
    - Invalidate caches every 30 seconds
    - Generate a monthly report
- EJB provides the `TimerService` to handle just this sort of situation
    - Supports CRON syntax using `@Schedule`
    - Allows a callback to be invoked using `@Timeout`
    - Management of timers
    - Persistent timers that "survive" server reboot
    - No need to manually handle complex threading logic

---

## Scheduled Timers

- _Scheduled timers_ can be created _declaratively_ using `@Schedule`

```java
@Singleton  // Must be singleton or stateless
public class UserNotificationService {
*   @Schedule(second = "0", minute = "0", hour = "1", dayOfMonth = "*", dayOfWeek = "*", year = "*")
    public void notifyUsers() {
        // ...
    }

*   @Schedules({   // Multiple schedules on the same method
*       @Schedule(minute = "0/30", hour = "2"),
*       @Schedule(hour = "14", dayOfWeek = "WED")
*   })
    public void generateReport(Timer t) {
        // ...
    }
}
```

---

## Scheduled Timers

- The expressions of `@Schedule` follow _CRON_-like syntax

| Attribute     | Description                               | Values                                                                                                    | Default
|-
| `second`      | One or more seconds within the minute     | `0` to `59`                                                                                               | `0`
| `minute`      | One or more minutes within the hour       | `0` to `59`                                                                                               | `0`
| `hour`        | One or more hours within the day          | `0` to `23`                                                                                               | `0`
| `dayOfMonth`  | One or more days within the month         | `1` to `31`<br/>`-7` to `-1` <small>(days before last of month)</small><br/>`last`<br/>`1st Sun`, ...     | `*`
| `month`       | One or more months within the year        | `1` to `12`<br/>`Jan` to `Dec`                                                                            | `*`
| `dayOfWeek`   | One or more weekdays within the week      | `0` to `7` <small>(`0` and `7` are `Sun`)</small><br/>`Sun` to `Sat`                                       | `*`
| `year`        | A particular calendar year                | 4-digit year                                                                                              | `*`
| `timezone`    | A specific timezone                       | `0` to `59`                                                                                               | &nbsp;

---

## Scheduled Timers

- You can also use special notations

| Notation      | Example                           | Meaning
|-
| Wildcard      | `*`                               | All allowable values for a given attribute
| List          | `1, 5, 7` or `Mon, Fri, Sat`      | Multiple values <small>(`1` and `5` and `7`)</small>
| Range         | `1-10` or `Mon-Fri`               | Range of all values including bounds
| Increments    | `*/5` or `15/30`                  | `x/y`  every `y` starting from `x` <small>(`15/10` = `15, 25, 35, 45, 55`)</small>

- Some examples

```html
dayOfWeek="Wed"                             // Every wednesday at midnight
minute="55", hour="6", dayOfWeek="Mon-Fri"  // Every weekday at 6:55
minute="*", hour="*"                        // Every minute of every hour of every day
minute="*/5", hour="*"                      // Every 5 minutes within the hour
hour="13", dayOfMonth="-3"                  // Four days before the last day of each month at 1pm
hour="12/2", dayOfMonth="2nd Tue"           // Every other hour within the day starting at noon...
                                            // ...on the second tuesday of every month
```

---

## Programmatic Timers

- Timers can be created _programmatically_ using `TimerService`

```java
@Stateless  // Must be stateless or singleton
public class CustomerService {
*   @Resource TimerService timerService;
    @PersistenceContext private EntityManager em;
    @EJB EmailService emailService;

    public void createCustomer(Customer customer) {
        em.persist(customer);

        // Create a CRON expression (uses fluent API)
*       ScheduleExpression birthDay = new ScheduleExpression().
*               dayOfMonth(customer.getBirthDay()).
*               month(customer.getBirthMonth());
*       TimerConfig config = new TimerConfig(customer, true);   // Set the payload and persistent flag
*       timerService.createCalendarTimer(birthDay, config);     // Create the timer
    }

*   @Timeout
*   public void sendBirthdayEmail(Timer timer) {
*       Customer customer = (Customer) timer.getInfo();
*       emailService.send(customer.getEmail(), "Happy Birthday!");
*   }
}
```

---

## Programmatic Timers

- `TimerService` has a couple of methods for creating timers

| Method    | Purpose
|-
| `createCalendarTimer()`       | Create timer that triggers based on a `ScheduleExpression` <small>(CRON expression)</small>
| `createIntervalTimer()`       | Create timer that triggers after a given interval
| `createSingleActionTimer()`   | Create timer that triggers at a given time
| `createTimer()`               | Create timer that triggers after a given duration
| `getAllTimers()`              | Retrieves all timers (including other's)
| `getTimers()`                 | Create retrieves the timers of this bean

- When a timer expires <small>(e.g. triggers)</small> the bean's `@Timeout` method is called

```java
@Timeout
public void myTimeoutFunction(Timer t) {
    // ...
}
```

---

## Timer Class

- The `Timer` class has a number of useful methods

| Method                | Purpose
|-
| `isPersistent()`      | Checks if the timer is _persistent_
| `getInfo()`           | Retrieves the _payload_ of the timer
| `getNextTimeout()`    | Retrieves how long until the next timeout
| `cancel()`            | Cancels the timer (will not trigger again)
| `isCalendarTimer()`   | Checks if this is a CRON based timer

- _Persistent_ timers will be remembered across server restarts <small>(true by default)</small>

```java
@Schedule(seconds="*", persistent = false)
```

- Timers can carry a _info_ <small>(payload)</small> field

```java
timerService.createTimer(1000, new Movie("Rogue One")); // Info field is movie (useable when expires)
```

---

## Exercise: TimerService

- In this exercise we will send a notification 5 seconds after a person has registered in our application
- Create a Singleton `NotificationService`
- Inject the `NotificationService` in the `RegistrationService`
- Inject the `TimerService` in the `NotificationService`, as well as a `Logger`
- When a person registers himself, call the `NotificationService` to log a message
  - Configure this timer programmatically to trigger after 5 seconds...
      - **Hint**: Save the persons's id instead of the `Person` entity in the timer!
  - When the timer triggers, lookup the `Person` entity, and print its details to the console
- Call the `RegistrationServiceRemoteIntegrationTest` and notice the message being printed on the console after 5 seconds
