# WebSockets

---

## What are WebSockets?

- A W3C HTML5 specification
    - Allows _full-duplex_ communication over TCP
        - Both client and server can send data at the same time
    - After performing initial _HTTP Upgrade_ handshake
        - Allows punching through firewalls
- Advantages
    - No need for HTTP header overhead
    - Two-way communication
- Disadvantages
    - Internet infrastructure (e.g. proxies, ...) must support this
    - Once switched to TCP, there is no application-level protocol

???trainer
The full-duplex nature of WebSockets are especially interesting because it provides an elegant alternative for server-side-events, ajax-polling or comet.
All of which are _hacky_ have their problems.

Once an initial connection is established, it is no longer necessary to send all the HTTP headers for every frame of data that needs to be sent. This is a considerable speedup. Both
client and server can communicate symmetrically: each can take initiative to send some data independently of the other.

It will take some time before all infrastructure on the internet allows WebSocket Upgrade connection requests. Proxies need to explicitly support this, so the technology is not entirely transparent.
Firewalls can be punched through however, because the connection handshake happens over the HTTP port (80), which most firewalls allow.

Once the connection is established, communication is done through pure TCP sockets. This would be as if you manually connect a client and server socket. This means that all communication
needs to happen using a protocol that you define yourself, which can be challenging to get right.
???t

---

## WebSocket Protocol

- This is how WebSockets work on the HTTP level
- Client initiates a connection to a server listening for WebSocket requests

```http
GET /chat HTTP/1.1
Host: server.example.com
Upgrade: websocket
*Connection: Upgrade
*Sec-WebSocket-Key: x3JJHMbDL1EzLkh9GBhXDw==
*Sec-WebSocket-Protocol: chat, superchat
*Sec-WebSocket-Version: 13
Origin: http://example.com
```

- Server accepts and replies switching to pure TCP communication

```http
HTTP/1.1 101 Switching Protocols
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: HSmrc0sMlYUkAGmm5OPpG2HaGWk=      // Hash includes Sc-WebSocket-Key from request
Sec-WebSocket-Protocol: chat
```

- Once handshake is complete, data is sent in _frames_ of text or octets

---

## WebSocket JavaScript API

- On clients (browsers) WebSockets are controlled via JavaScript
- A minimal HTML page for WebSockets may look like this

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WebSocket</title>
        <script>
            var ws = new WebSocket('ws://www.realdolmen.com/messenger');
            // ...
        </script>
    </head>
    <body>
        <h1>WebSocket</h1>
        <div id="messages"></div>
    </body>
</html>
```

???trainer
You can embed WebSockets in any page. The simplest possible case of which is shown in this example.
The `<script>` block must be completed with some more JavaScript to communicate with WebSockets.
???t

---

## WebSocket JavaScript API

- Creating a WebSocket from JavaScript is easy:
    - It automatically tries to open a connection to the server

```javascript
var ws = new WebSocket('ws://localhost:8080/jee7-starter/messenger');    // Use wss:// for secure
ws.onopen = function() {
    ws.send('Hello World');
    ws.onmessage = function(message) {
        document.getElementById('messages').innerHTML = '<p>Message reveived: ' + message.data + '</p>';
    }
};
```

- The `ws` object only has some interesting properties <small>(many more are omitted)</small>

| Property  | Meaning
| -
| onopen        | Callback function executed when a connection is established
| onclose       | Callback function executed when the connection was closed
| onerror       | Callback function executed when a connection could't be established
| onmessage     | Callback function when a message is received from the server
| send(data)    | Send a message to the server

???trainer
The WebSocket object in JavaScript is very simple. It only has a few event handlers for monitoring connection status.
The most interesting properties however are `send(data)` and `onmessage`. A method to send data to the server, and a callback
event handler to receive a message from the server.

When you want to send binary data from a JavaScript client, you can use `new ArrayBuffer()`

```javascript
ws.binaryType = "arraybuffer";
var buffer = new ArrayBuffer(16);   // 16 bytes of storage
var bytes = new Uint8Array(buffer); // Interpret as bytes
for (var i=0; i < bytes.length; i++) {
    bytes[i] = i;                   // Fill with numbers 0..15
}
ws.send(buffer);
```
???t

---

## Java Support for WebSockets

- Provided in JEE7 as _Java API for WebSocket 1.0_
    - Brand new API <small>JSR-356</small>
- Most notable features
    - Annotation driven
    - Supports _text_, _binary_ and _control_ messages
    - Has programmable _life cycle events_
    - Allows configuration of _timeouts_, _retries_, _cookies_, _pooling_
    - Integrates with JEE security model
- Package is `javax.websocket.*`

---

## Simple Example

- Server side <small>(Java)</small>

```java
@ServerEndpoint("/squadron")
public class SquadronServer {
    @OnMessage
    public String receive(String message) {
        return message;
    }

    @OnOpen
    public void onOpen(Session session) throws InterruptedException {
        List<String> messages = Arrays.asList("Red five", "Red four", "Red three", "Red two");
        for (String message : messages) {
            Thread.sleep(1000);
            session.getAsyncRemote().sendText(message + " standing by.");
        }
    }
}
```

???trainer
This shows the Java or Server side of WebSockets. As you can see, it's all annotation driven. The `@OnMessage` annotation
marks a receiving endpoint for incoming messages, while the `@OnOpen` allows you to program the lifecycle of a WebSocket.

In this case, the socket sends out some messages upon first connect, and then starts to listen for incoming connections.
???t

---

## Simple Example

- Client side <small>(HTML and JavaScript)</small>

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Red Squadron</title>
        <script>
            var ws = new WebSocket('ws://localhost:8080/jee7-starter/squadron');
            ws.onopen = function() {
                ws.send('Red leader standing by. May the force be with you!');
            };
            ws.onmessage = function(message) {
                var p = document.createElement('p');
                p.textContent = message.data;
                document.getElementById("messages").appendChild(p);
            }
        </script>
    </head>
    <body>
        <h1>Red Squadron</h1>
        <div id="messages"></div>
    </body>
</html>
```

???trainer
This side shows the JavaScript side. This course will not focus on the aspects of JavaScript. If you would like to know
more on the details of WebSockets with JavaScript, check our or HTML5 course: https://education.realdolmen.com/en/Course/HTM050

The output of the example is as follows:

```html
Red five standing by.
Red four standing by.
Red three standing by.
Red two standing by.
Red leader standing by. May the force be with you!
```
???t

---

## Server Endpoints

- A WebSocket is managed by a _Server Endpoint_

```java
@ServerEndpoint("/messenger")
public class MessengerServer {
    // ...
}
```

- The annotation has the following properties:

| Property              | Meaning
|-
| value                 | The URL that will be used to connect to
| subprotocols          | A list of known WebSocket protocols that are supported by this endpoint
| encoders, decoders    | Allows plugging in used defined marshalling strategy to support any type of object
| configurator          | To Specify custom endpoint configurations

---

## @OnMessage

- Receiving messages is done using the `@OnMessage` annotation

```java
@ServerEndpoint("/chat")
public class MessengerServer {
*   @OnMessage
    public String handleMessage(String message) {   // Method name can be chosen freely
        return message.toUpperCase();   // Echo in upper case
    }
}
```

- The method supports various different parameter and return types

| Parameter                 | Purpose                                       | Type
|-
| String                    | For plain or parsable (XML, JSON, ...) text   | parameter & return
| int, long, double, ...    | All primitives and their wrappers             | parameters & return
| ByteBuffer, byte[ ]        | For binary messages                           | parameter & return
| Reader                    | To receive a large text message               | parameter only
| InputStream               | To receive a large binary message             | parameter only
| PongMessage               | An interface for control messages (w/ payload)| parameter only

---

## @OnMessage

- Some examples of valid `@OnMessage` methods

```java
@OnMessage
public void accept(String s) {
    // ...
}

@OnMessage
public byte[] produceImage(int i) {
    // ...
}

@OnMessage
public int handleReader(Reader reader) {
    // ...
}

@OnMessage
public String receiveData(ByteBuffer b) {
    // ...
}
```

- The return value represents the reply message

---

## Path Parameters

- You can capture (multiple) parts of the URL as a `@PathParam`
    - Accepts String and primitives

```java
*@ServerEndpoint('/squadron/{color}')
public class SquadronServer {
    @OnMessage
*    public String receive(String message, @PathParam("color") String color) {
        System.out.println(color);  // Would produce "red" when socket url
                                    // was ws://localhost:8080/jee7-starter/squadron/red
        return message;
    }
}
```

???trainer
Do not confuse this annotation with the equally named one from JAX-RS. This one's FQN is `javax.websocket.server.PathParam`
???t

---

## Partial Messages

- To process very large messages in parts, you can add a boolean parameter

```java
private String fullMessage = "";

@OnMessage
public void processHugeMessage(String part, boolean isLastPart) {
    fullMessage += part;
    if(isLastPart) {
        handleFullMessage(fullMessage);
    }
}
```
- Also works for binary messages <small>(`byte[]`, `ByteBuffer`)</small>

???trainer
This allows for more memory efficient processing in case there are some very large messages.
An alternative for this would be to use a `Reader` or `InputStream`, but they will block execution, thus preventing others
to communicate through the same server in the meantime.
???t

---

## Message Size Restrictions

- To force a message size limit, you can use `maxMessageSize` of `@OnMessage`

```java
@OnMessage(maxMessageSize = 10)     // No longer than 10 characters (or bytes)
public String receive(String message) {
    return message;
}
```

- Will produce an Exception
    - `java.lang.IOException`: Message exceeded max message size of 2
    - Can be handled with `@OnError`

???trainer
In a sense, this is the opposite of _Partial Messages_, where you do not want to process messages larger than _n_ bytes or characters.
It is the responsibility of the client to respect this limit. If this is not the case, the socket will be closed.

Errors can be handled using a special life-cycle callback. This will be covered later in the course.
???t

---

## Session

- An instance of `Session` can be bound for programmatic access
    - Session represents the other side of the connection

```java
@OnMessage
public void receive(String message, Session session) {
    session.getAsyncRemote().sendText('Goodbye');
}
```

- A lot of useful methods are available on this class


| Method                            | Method                        | Method                     Method
| -
| addMessageHandler()               | getMaxIdleTimeout()           | getProtocolVersion()      | isSecure()
| close()                           | getMaxTextMessageBufferSize() | getQueryString()          | removeMessageHandler()
| getAsyncRemote()                 | getMessageHandlers()          | getRequestParameterMap()  | setMaxBinaryMessageBufferSize()
| getBasicRemote()                  | getNegotiatedExtensions()     | getRequestURI()           | setMaxIdleTimeout()
| getContainer()                    | getNegotiatedSubprotocol()    | getUserPrincipal()        | setMaxTextMessageBufferSize()
| getId()                           | getOpenSessions()             | getUserProperties()       | &nbsp;
| getMaxBinaryMessageBufferSize()   | getPathParameters()           | isOpen()                  | &nbsp;

???trainer
The `Session` object can be bound (at any parameter index) to provide more powerful control over what the method should do.
It gives the developer a programmatic API to work with.
???t

---

## @OnOpen

- Use `@OnOpen` to react to a new WebSocket connection

```java
@OnOpen
public void open(Session s) {
    s.getAsyncRemote().sendText('Welcome to Red Squadron. Be careful out there!');
}
```

- Optional parameters
    - `EndpointConfig`, `Session`, `@PathParam`s

---

## @OnClose

- Use `@OnClose` to react to a WebSocket being closed

```java
@OnClose
public void close(CloseReason r) {
    if(r.getCloseCode() == CloseReason.CloseCodes.CLOSED_ABNORMALLY) {
        System.out.println("They came from... behind!");
    }
}
```

- Optional parameters
    - `CloseReason`, `Session`, `@PathParam`s
- Close Reason has a `CloseCode`

| Code | Code | Code | Code
| -
| NORMAL_CLOSURE    | RESERVED             | VIOLATED_POLICY        | SERVICE_RESTART
| GOING_AWAY        | NO_STATUS_CODE       | TOO_BIG                | TRY_AGAIN_LATER
| PROTOCOL_ERROR    | CLOSED_ABNORMALLY    | NO_EXTENSION           | &nbsp;
| CANNOT_ACCEPT     | NOT_CONSISTENT       | UNEXPECTED_CONDITION   | &nbsp;

---

## @OnError

- Use `@OnError` to react to WebSockets causing errors

```java
@OnError
public void error(Throwable t) {
    t.printStackTrace();
}
```

- Mandatory parameter
    - `Throwable` <small>(or subclass)</small>
- Optional parameters
    - `Session`, `@PathParam`

---

## Integration with CDI

- Seamless integration with CDI is provided

```java
@ServerEndpoint("/squadron/{color}")
public class SquadronServer {
*   @Inject
    private SquadronRepository repository;

*   @Inject
*   Event<String> event;

    @OnMessage
    public String receive(String message) {
        repository.save(message);
        event.fire(message);
        return "Roger " + message;
    }
}
```

- You can use all CDI features, including
    - Injection using `@Inject` on _fields_, _methods_ and _constructors_
    - Interceptors and Decorators
    - Events

???trainer
Seamless integration is provided for CDI. Any JavaBean currently capable of being used with CDI can be used as-is in a
WebSocket endpoint.
???t

---

## Configurators

- Advanced configuration is done using a _Configurator_

```java
*@ServerEndpoint(value = "/squadron", configurator = MyConfigurator.class)
public class SquadronServer {
    // ...
}
```

```java
class MyConfigurator extends ServerEndpointConfig.Configurator {
    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest req, HandshakeResponse res) {
        super.modifyHandshake(sec, req, res);
        res.getHeaders().put("DeathStarPlans", "44 65 61 74 68 20 53 74 61 72");    // Add header
    }
}
```

- _Configurator_ is an abstract class with the following methods

| Method                        | Method
|-
| getNegotiatedSubprotocol()    | modifyHandshake()
| getNegotiatedExtensions()     | getEndpointInstance()
| checkOrigin()                 | &nbsp;

???trainer
Remember that once a WebSocket is established, you have a pure TCP socket. That means that you can use this socket to
transfer _any_ data, including application level protocols. These can be configured using _sub protocols_. For example it is
possible to use a WebSocket for XMPP, SOAP, FTP, SSH, SMB, etc...

You can also influence the handshake mechanism. For example it might be interesting to add some extra HTTP headers to
the response, or check against a security store.
???t

---

## Programmatic Endpoints

- A programmatic endpoint extends `Endpoint`

```java
public class SquadronServer extends Endpoint {
    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        // ...
    }

    @Override
    public void onClose(Session session, CloseReason closeReason) {
        // ...
    }

    @Override
    public void onError(Session session, Throwable thr) {
        // ...
    }
}
```

- The annotations have been replaced by method overrides

???trainer
Do not make the mistake of importing the wrong class. Always stick to `javax.websocket.*`!

Deciding whether to use _annotations_ or a _programmatic_ API is always a matter of balancing convenience and flexibility.
Generally, a programmatic approach is more _flexible_ because you can do everything that is possible in Java this way (such as `if`, `while`, etc...).
On the other hand, using _annotations_ (also called _declarative_) is generally much more readable and easier to use and reconfigure.
???t

---

## Programmatic Endpoints

- Register one or more `MethodHandler`s using `Session`
    - Replaces the `@OnMessage` annotation

```java
@Override
public void onOpen(Session session, EndpointConfig endpointConfig) {
*   session.addMessageHandler(new MessageHandler.Whole<String>() {
        @Override
        public void onMessage(String message) {
            // Message received!
        }
    });
}
```

- Partial messages are still supported

```java
*session.addMessageHandler(new MessageHandler.Partial<String>() {
    @Override
    public void onMessage(String partialMessage, boolean last) {
        // Multipart received. If last is true it's the end!
    }
});
```

???trainer
Using programmatic endpoint, we no longer use annotations. They have been replaced by method overrides.
However, since a _Server Endpoint_ can have **many** `@OnMessage` annotations, this can not be replaced by a method override.
Adding message handlers is thus done using `Session`, wich has several options to register a new `MessageHandler`

- _Whole_ message handlers are for full messages
- _Partial_ message handlers are for multipart messages, with an extra boolean parameter to indicate this is the last part

Note that this example uses `String` messages, but these can be _any_ type that are also supported by the annotation based endpoints.
???t

---

## Programmatic Endpoints

- Communication is done using `Session`

```java
@Override
public void onOpen(final Session session, EndpointConfig endpointConfig) {
    session.addMessageHandler(new MessageHandler.Whole<String>() {
        @Override
        public void onMessage(String message) {
*           Future<Void> f = session.getAsyncRemote().sendText("Echo " + message); // It's a Future!
        }
    });
}
```

- There is also a _synchronous_ variant:

```java
try {
    session.getBasicRemote().sendText("Echo " + message);
} catch(IOException e) {
    e.printStackTrace();
}
```

- Both variants offer several methods for sending:
    - sendText(), sendBinary(), sendObject(), ...

???trainer
Note that when using Java 7, you still have to mark the `Session` parameter from the outer method scope `final`.
This is because Java only supports _closure_ for immutable variables.

There are two types of remotes:

- _Basic_ (`getBasicRemote()`) Blocks the call until the message is sent. Also throws a checked `IOException` (which is rather inconvenient).
- _Async_ (`getAsyncRemote()`) Returns immediately. One variant of this method returns a `Future` that can be used to check the status of the _async_ message.

A `Future` is a JSE class that is comparable to a `Promise` or `Deferred` in other languages. It provides a way to
immediately get a reference to a task that will be completed in the future (or may have already). You can query the `Future` using `get()` at which
point it will _block_ until the task is complete.

Both types of remotes have several methods to be used to send different types of data (text, binary, objects and control messages).
???t

---

## Programmatic Endpoints Configuration

- You can add a _gate_ class for registering endpoints programmatically

```java
public class SquadronApplicationConfig implements ServerApplicationConfig {
    @Override
    public Set<ServerEndpointConfig> getEndpointConfigs(Set<Class<? extends Endpoint>> set) {
        // ...
    }

    @Override
    public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> set) {
        return null; // Or return a set of @ServerEndpoint classes
    }
}
```

- Any class implementing `ServerApplicationConfig` will be picked up

???trainer
This class will be picked up by the Application Server by just being there. The contents of the methods can be considerable.
???t

---

## Programmatic Endpoints Configuration

- Adding programmatic endpoints is done using a builder pattern

```java
@Override
public Set<ServerEndpointConfig> getEndpointConfigs(Set<Class<? extends Endpoint>> set) {
    Set<ServerEndpointConfig> configs = new HashSet<>();
    configs.add(
        ServerEndpointConfig.Builder.create(SquadronServer.class, "/squadron")
            .configurator(new MyConfigurator())     // Optional
            .build()
    );
    return configs;
}
```

- You can not programmatically add annotated _Server Endpoints_ this way
    - Use `getAnnotatedEndpointClasses()` and `@ServerEndpoint` for this.

---

## WebSocket Client

- WebSocket clients do not _have_ to be in a Web Browser
    - You can also create a client from within a Java application

```java
public static void main(String[] args) {
    WebSocketContainer container = ContainerProvider.getWebSocketContainer();
    String uri = "ws://localhost:8080/jee7-starter/squadron";
    container.connectToServer(SquadronClient.class, URI.create(uri));
}
```

- A client class is annotated with `@ClientEndpoint`

```java
@ClientEndpoint
public class SquadronClient {
    @OnOpen public void onOpen(Session session) { ... }
    @OnMessage public String message(String message) { ... }
}
```

- Very similar to a `@ServerEndpoint`
    - Can also have a `@OnError` and `@OnClose`
    - Can have a `ClientEndpointConfig.Configurator`

???trainer
The _Client Endpoint_ is almost identical to a _Server Endpoint_. It has the same annotations, it also has a configurer (`ClientEndpointConfig` instead of `ServerEndpointConfig`)
and it can work with `Server` instances to access `getAsyncRemote()` or `getBasicRemote()` to send data with `sendText()` or `sendBinary()`.

When running from a `public static void main(String[])` you do have to have the client dependencies in your project.
For this you can use the following _Maven_ configuration. Tyrus is the reference implementation of JSR-356.

```xml
<dependency>
    <groupId>io.undertow</groupId>
    <artifactId>undertow-websockets-jsr</artifactId>
    <version>1.3.15.Final</version>
</dependency>
```
???t

---

## WebSocket Client

- Configuration of a _Client Endpoint_ is done using `ClientEndpointConfig`
    - Has two different methods compared to the `ServerEndpointConfig`

```java
public class MyConfigurator extends ClientEndpointConfig.Configurator {
    @Override
    public void beforeRequest(Map<String, List<String>> headers) {
        // ...
    }

    @Override
    public void afterResponse(HandshakeResponse response) {
        // ...
    }
}
```

```java
@ClientEndpoint(configurator = MyConfigurer.class)
public class SquadronClient {
    // ...
}
```

???trainer
This configuration is a little different, because a client does not have so much control over the creation of the socket.

Do not worry too much about this part, as it is used _only_ in case of special configuration requirements. A _normal_ WebSocket
connection can simply get away without specifying such extra configuration classes.
???t

---

## WebSocket Client

- Programmatic _Client Endpoints_ are also possible

```java
public class SquadronClient extends Endpoint {
    // Implement onError, onClose, onOpen as usual...
}
```

- Configuration can then be done using a builder

```java
WebSocketContainer container = ContainerProvider.getWebSocketContainer();
String uri = "ws://localhost:8080/jee7-starter/squadron";
container.connectToServer(SquadronClient.class,
    ClientEndpointConfig.Builder
        .create()
        .configurator(new MyConfigurer())
        .build(), URI.create(uri));
```

???trainer
We do not focus much on this particular case, because it is almost entirely the same as with a _Server Endpoint_ and is
necessary only in very specialized cases where:

- you decide to use a _programmatic_ approach over _annotations_ **and**
- you have the need for special configuration

This type of configuration is provided in the philosophy of "make simple things easy and _difficult things possible_".
???t

---

## Encoders and Decoders

- So far we have only sent and received messages of primitive types
    - String, int, byte[], Reader, InputStream, ...
- It is of course possible to send and receive arbitrary _Objects_
    - In this case we need to provide our own conversion strategy
- We use _encoders_ and _decoders_ for this
    - There are four types of each (eight total)

| Type | Use case
|-
| Decoder.TextStream&lt;T&gt;, Encoder.TextStream&lt;T&gt;      | Converting an object from/to a Reader
| Decoder.Text&lt;T&gt;, Encoder.Text&lt;T&gt;                  | Converting an object from/to a String
| Decoder.Binary&lt;T&gt;, Encoder.Binary&lt;T&gt;              | Converting an object form/to a ByteBuffer
| Decoder.BinaryStream&lt;T&gt;, Encoder.BinaryStream&lt;T&gt;  | Converting an object from/to a InputStream

---

## Encoders and Decoders

- Consider some arbitrary classes we want to use in our WebSocket

```java
public class PilotReport {          // Incoming message
    private String callsign;
    private String status;
    private String message;

    public PilotReport(String callsign, String status, String message) {
        this.callsign = callsign;
        this.status = status;
        this.message = message;
    }
    // Getters and setters
}

public class PilotInstruction {     // Outgoing message
    private String callsign;
    private String instruction;

    public PilotInstruction(String callsign, String instruction) {
        this.callsign = callsign;
        this.instruction = instruction;
    }
    // Getters and setters
}
```

???trainer
There is nothing special about these classes. They are simple POJOs. In some cases this might even be a JPA entity although
that may not be a good idea with security in mind. A more likely case is these classes are JAXB classes for use with
a marshaller.
???t

---

## Encoders and Decoders

- The _Server Endpoint_ looks like this

```java
@ServerEndpoint(value = "/squadron",
*   encoders = PilotInstructionEncoder.class,
*   decoders = PilotReportDecoder.class)
public class SquadronServer {
    @OnMessage
    public PilotInstruction message(PilotReport r) {
        System.out.println("Pilot " + r.getCallsign() + " is " + r.getStatus());
        if(report.getStatus().equals("zealous")) {
            return new PilotInstruction(r.getCallsign(), "Calm down " + r.getCallsign() + "!");
        } else {
            return new PilotInstruction(r.getCallsign(), "Roger " + r.getCallsign() + ". Carry on.");
        }
    }
}
```

- Input and output messages are expected to be CSV

```html
Input:  Starbuck,zealous,I know the path to Earth!
Output: Starbuck,Calm down Starbuck!
```

???trainer
This example shows that you can have an `@OnMessage` method receive and return arbitrary Java objects (`PilotReport` and ``PilotInstruction`).
We do need an agreed upon data representation format for this to work. In many cases this would be XML, JSON, binary or anything else.
We demonstrate with a very simple example though, to better illustrate the point.
???t

---

## Encoders and Decoders

- The _encoder_ looks like this:

```java
public class PilotInstructionEncoder implements Encoder.Text<PilotInstruction> {
    @Override
    public String encode(PilotInstruction instruction) throws EncodeException {
        return instruction.getCallsign() + "," + instruction.getInstruction();
    }

    @Override
    public void init(EndpointConfig config) {
        // Add init logic if necessary
    }

    @Override
    public void destroy() {
        // Add cleanup code if necessary
    }
}
```

???trainer
The encoder demonstrated here simply converts a `PilotInstruction` to a CSV format `Racetrack,Roger Racetrack. Carry on.`
???t

---

## Encoders and Decoders

- The _decoder_ looks like this

```java
public class PilotReportDecoder implements Decoder.Text<PilotReport> {
    @Override
    public PilotReport decode(String s) throws DecodeException {
        String[] parts = s.split(",");
        return new PilotReport(parts[0], parts[1], parts[2]);
    }

    @Override
    public boolean willDecode(String s) {
        return true;        // Pre-parse decision (useful if there are multiple decoders)
    }

    @Override
    public void init(EndpointConfig config) {
        // Add init logic if necessary
    }

    @Override
    public void destroy() {
        // Add cleanup code if necessary
    }
}
```

???trainer
The decoder demonstrated here simply converts a CSV string `Boomer,excited,Actual: I found water!` into an instance of
`PilotReport`.
???t

---

## Encoders and Decoders

- In the real world messages are likely to be formatted in XML or JSON

```json
{
    "callsign":"Starbuck",
    "message":"I found water",
    "status":"zealous"}
}
```

- We can create a decoder with the new JSON API.

```java
@Override
public PilotReport decode(String s) throws DecodeException {
    JsonObject object = Json.createReader(new StringReader(s)).readObject();
    return new PilotReport(
        object.getString("callsign"),
        object.getString("status"),
        object.getString("message")
    );
}
```

- Notice that we did not have to change the _Server Endpoint_
    - Encoders and Decoders allow to make abstraction of the used data format

???trainer
Writing custom encoders and decoders can be quite tedious. Fortunately we can use our familiar tools: JAXB, Jackson, XStream, etc...
In the case of simple JSON graphs, we can use the new JEE7 JSON specification as demonstrated in this example.
???t

---

## Integration with JEE Security

- Access control can be declared in `web.xml`
    - WebSockets using `ws://` actually still perform initial handshake over `http://`

```xml
<web-app ...>
    <security-constraint>
        <web-resource-collection>
            <web-resource-name>Squadron Server</web-resource-name>
            <url-pattern>/squadron</url-pattern>
            <http-method>GET</http-method>
        </web-resource-collection>
        <auth-constraint>
            <role-name>ROLE_PILOT</role-name>
        </auth-constraint>
    </security-constraint>
    <login-config>
        <auth-method>BASIC</auth-method>
        <realm-name>galacticEmpireRealm</realm-name>
    </login-config>
    <security-role>
        <role-name>ROLE_PILOT</role-name>
    </security-role>
</web-app>
```

???trainer
Integration with JEE Security is done in the `web.xml`, where you can configure all the usual http:// url based access control
You can use this with several authentication schemes, such as _Basic_, _Digest_ or _Form_ based security.

In this example, it is assumed that the Application Server has a security realm in which there are users associated with role `ROLE_PILOT`.
The WebSocket connection can only be established by already authenticated users with `ROLE_PILOT`. This authentication should have
been previously established, because the WebSocket API can not present the user with a login-page as the response of a WebSocket connection request.

???t

???invisible

===

Exercise WebSockets

- Let's try out the new WebSockets API
- Create a class `PersonServer` as a `@ServerEndpoint`
  - The server receives the full name of a `Person`
  - The server parses out the first and last name, and saves the `Person` in the database
  - Send "success" or "failure" as response
- In `people.xhtml` create a form to add new `Person`s
  - Add a field for both first and last name
  - Send the data to the `PersonServer` over a web socket
  - Show the result on the `people.xhtml` page
- From a `PersonServerTest` class, test the websocket client API
  - You may need to wait until the answer is received before asserting the result...

???i
