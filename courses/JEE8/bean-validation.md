# Jakarta Bean Validation (JBV)

---

## Rundown

- Since JEE 6 (2009)
- Separates constraint definition from validation
- Annotation-driven (deployment descriptor also possible)
- Common constraints provided, possible to add your own

| Constraint                | Accepted Types
|-
| @Null, @NotNull           | Object
| @Max, @Min                | BigDecimal, BigInteger, byte, short, int, long, and their wrappers
| @Size                     | Object[], CharSequence, Collection, Map
| @Future(OrPresent), @Past(OrPresent)            | Calendar, Date, any from `java.time.*`
| @Pattern                  | CharSequence
| @NotEmpty                  | CharSequence, Collection, Map, Object[]
| @NotBlank                  | CharSequence
| @Email                  | CharSequence
| @Valid                  | Object
| ... | ...

???trainer
for full list, refer to spec
???t

---

## Constraint = Annotation + Validator

- `@Constraint(validatedBy = "...")`

````java
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
@Documented
*@Constraint(validatedBy = NotNullValidator.class)
public @interface NotNull {
  String message() default "{javax.validation.constraints.NotNull.message}";
  Class<?>[] groups() default {}; // to control the order of validation
  Class<? extends Payload>[] payload() default {}; // to associate meta-data
}
````
- implement `ConstraintValidator<Annotation, T>`

```java
public class NotNullValidator implements ConstraintValidator<NotNull, Object>{
  public void initialize(NotNull parameters) {
  }
  public boolean isValid(Object object, ConstraintValidatorContext context) {
    return object != null;
  }
}
```

???trainer
ConstraintValidator: Annotation, Target
???t

---

## Constraints

- Can be specified on:
    - fields
    - method parameters
    - methods (= return value)...
    - generics (e.g.: `List<@Email String> emails`)
    - classes (for constraints on relations between multiple fields)
- Can be composed (e.g.: `@NotNull @Positive Integer foo`)
- May be grouped into "meta-constraint"
- Can have parameters (access them in validator's `initialize` method)
- Are inherited

???trainer
meta-constraint
- define new annotation with @Constraint
- put multiple other constraints on it
- do NOT fill in "validatedBy"

???t

---

## Error message

- `message` property of annotation definition
- Default can be overridden (specify `message` property on annotation)
- When value is written as `"{...}"`, it is automatically translated
    - Requires `ValidationMessages.properties` in classpath
        - Under `resources` for maven WAR project

```java
String message() default "{com.inetum.realdolmen.Email.message}";
```

```html
com.inetum.realdolmen.Email.message=Email address doesn't look good
```
- May contain Expression Language (EL) snippets
    - Variables are filled in based on the annotation's properties

```java
javax.validation.constraints.Size.message =
    Value must be less than {max} ${min > 0 ? 'and larger than ' + min}
```

---

## Performing validation

- Manual
    - Get validator + call specific methods
```java
ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
Validator validator = factory.getValidator();
// perform validation
factory.close();
```
    - Flexible
        - Groups (sets of rules)
        - Partial validation (only specific properties/methods)
- Automatically done by container for:
    - JSON-B
    - JPA entities
    - JSF (except class level annotations)
    - Any EJB (or CDI) method parameter

???trainer
Prefer automatic, unless more control is needed

Closing thoughts

pro: separation of concern

pro: reusability

pro: declarative validation

con: obtain can exist in inconsistent state, validity is only tested when triggered

con: limited expressiveness

- JBV has received criticism...
    - It ties domain classes to a framework
    - It allows creation of objects in an inconsistent state
    - It is actually harder to test
- Conclusions
    - Use JBV for DTOs (datastructures), not for domain classes (objects)

links:
- https://idontbyte.jaun.org/blog/2019/10/beanvalidation
- https://in.relation.to/2014/06/19/blah-vs-bean-validation-you-missed-the-point-like-mars-climate-orbiter/

???t
