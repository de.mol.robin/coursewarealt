## REST Architecture

- REST is an architecture that can be implemented using any technology
    - It is a generic term, that has nothing to say about business architecture
    - HTTP is an implementation of REST principles
- RESTful web services have gained more popularity in the Web domain
    - Many key web players <small>(Amazon, Google, Yahoo!)</small> have moved to RESTful webservices
- The communication protocol is HTTP exclusively
- This makes REST simple and lightweight, with a high performance

---

## REST: Approach to Web Services

- Use POST, GET, PUT and DELETE as CRUD
    - Resources are linked and referenced using URIs
    - Can be bookmarked, listed, browsed to, etc...
- Use HTTP as uniform interface
    - Any HTTP-enabled client can connect to resources
    - If you know what the URI is, you can invoke the resource
- Resources are addressable
    - Allows to use resources in any way imaginable, even in ways you would not have expected
- Resources are connected to one another
    - Applications move from one state to the next by following a link
- REST is stateless
    - Every HTTP request is isolated from the other
    - Provides a better scalability
    
---

## REST: Resources and URIs

- In REST, every piece of information is a resource
    - A list of books from a publisher, the book "A Dance with Dragons", a job
- Resources are addressed using URIs
    - Typically links on the Web

| URI                                                       | Resource  
|-
| https://www.flickr.com/explore/2017/01/01                 | New pictures taken on new year 
| http://www.omdbapi.com/?t=Star+Wars&y=&plot=short&r=json  | Star Wars movies released
| https://weather.com/weather/today/l/USDC0001:1:US         | Weather forecase in Washington, DC
    
- Resources are acted upon using a set of simple, well defined operations
    - `GET`, `POST`, `PUT`, `DELETE`
- URIs must be as descriptive as possible and identify a unique resource
    - Note that the data for different URIs might be the same

---

## REST: Representations

- You might want the data represented as text, XML, PDF, an image or other data format
- The client always deals with a resource through its representation
    - The resource itself stays on the server
    - Representation is useful information about the state of the resource
- Different possibilities
    - Using URIs for each representation
    - Using content negotiation <small>(using HTTP headers)</small>
    - Example: https://www.flickr.com/services/api/explore/flickr.interestingness.getList

---

## REST: Uniform Contract

- Stateless protocol based on requests and responses exchanged between client and server
    - Messages exchanged are made of an envelope and a body (also called document or representation)
- HTTP uses different methods

| Method    | Purpose
|-
| `GET`     | Creates a new resource defined by the URI (side-effect-free)
| `POST`    | Creates or updates state of the resource (not _side-effect-free_)
| `PUT`     | Creates or replaces state of the resource (overrides all, not _side-effect-free)
| `DELETE`  | Deletes the resource (not side-effect-free)
| ...       | ...
    
---

## REST: Content Negotiation and Content Types

- Content negotiation allows agreeing between client and server over the most suited representation
    - Is based upon HTTP request headers
        - `Accept`, `Accept-Charset`, `Accept-Encoding`, `Accept-Language`, `User-Agent`
    - Content types correspond to Internet media types        

| Content type                              | Usage
|-
| `text/html`                                 | Representing web pages
| `text/plain`                                | Providing pure text data
| `image/gif`, `image/jpeg`, `image/png`      | Representing images
| `text/xml`, `application/xml`               | Representing XML data
| `application/json`                          | Representing JSON data
| ...                                         | ...

---

## REST: Status Codes

- Each time a response is received, an HTTP status code is associated with it

| Code Range    | Purpose
|-
| 1xx           | Informational
| 2xx           | Success
| 3xx           | Redirection
| 4xx           | Client error
| 5xx           | Server error

- Common status codes

| Code  | Purpose
|-
| 200   | OK (with data)
| 301   | Moved permanently
| 404   | Not found
| 500   | Internal server error
