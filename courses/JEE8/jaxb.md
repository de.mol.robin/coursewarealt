# Jakarta XML Binding (JAXB)

---

## Rundown

- For marshalling between XML and POJOs
- Annotation-driven

```java
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class Book {
    @XmlElement(name = "book_title") private String title;    
    @XmlElement private String author;    
    @XmlElement private String publisher;    
    @XmlAttribute(name = "isbn_number") private String isbn;    
    // ... constructors, getters, setters as needed
}
```

```xml
<book isbn_number="0-553-10354-7">
    <book_title>A Game of Thrones</book_title>
    <author>George R. R. Martin</author>
    <publisher>Bantam Spectra</publisher>
</book>
```

???trainer
JAXB is actually part of Java SE.

???t

---

## Using JAXB

- There are many annotations available in JAXB, here are some:

| Annotation        | Purpose
|-
| `@XmlRootElement` | Marks a class as a top-level XML root element
| `@XmlElement`     | Marks a property or field as an `<element>`
| `@XmlAttribute`   | Marks a property or field as an attribute
| `@XmlTransient`   | Ignores this property or field
| `@XmlType`        | Allows setting the order of an XML _sequence_ and namespaces
| `@XmlList`        | Allows a `List` of elements in XML

- For an in depth view at JAXB and other Java XML APIs, check our course:
    - https://education.realdolmen.com/en/Course/JXP010

