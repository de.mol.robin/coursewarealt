# Web Services

---

## Web Services

- According to the [W3C](https://www.w3.org/TR/ws-gloss/#defs) a Web Service is
    - _"A software system designed to support interoperable machine-to-machine interaction over a network"_
- More concretely this most typically means
    - A _business service_
        - (e.g. "payment service", "order service", "invoice service", ...)
    - Implemented on a specific software platform
        - Java, .NET, ...
    - That is remotely accessible over TCP/IP <small>(often HTTP)</small>
    - In a portable way <small>(cross platform)</small>
- Web Services have existed for a long time, but the term was popularized in the early 2000s

---

## Web Service styles

- Web Services are independent of any technology
    - They typically use a _standard_ and _human readable_ exchange format <small>(like XML and JSON)</small>
- In modern practice there are two main styles
    - _SOAP_ Web Services <small>(Simple Object Access Protocol)</small>
        - Strong in enterprise and SOA environments
        - Uses a strong contract for communication
        - Tries to be loosely coupled from any transport medium
    - _REST_ Web Services <small>(REpresentational State Transfer)</small>
        - Can be viewed as the _architecture of the web_
        - Uses an implicit contract for communication
        - Inherently tied to HTTP as a transport medium

---

## Web Service Technologies

- SOAP Web Services are built around the following technologies
    - XML and XSD
    - The SOAP protocol
    - The WSDL description format
    - UDDI registries
    - Usually also the HTTP protocol <small>(not necessarily though)</small>
- RESTful Web Services are built around the following technologies
    - The infrastructure of the Web <small>(HTTP, WWW, URIs, ...)</small>
    - XML and XSD or JSON
    - Any other representation supported by HTTP

---

## RESTful web services

- REST stands for REpresentational State Transfer
- REST is an architecture that can be implemented using any technology
  - It is a generic term, that has nothing to say about business architecture
  - HTTP is an implementation of REST principles
- REST can be viewed as the _architecture of the web_
  - It describes the principles underlying the HTTP architecture a posteriori


[](img/rest.png)

source: https://martinfowler.com/articles/richardsonMaturityModel.html

---

### HTTP as transport means

> "The web is an existence proof of a massively distributed system that works really well"

- HTTP is a rich protocol for exchanging information between two processes
    - URLs as "addresses"
    - Headers for metadata
    - Status codes (success, error, ...)
    - Syntax for parameters
    - Syntax for body data transfer
    - Negotiation protocols (content-type, language, ...)

---

### Resources

- URL correponds to particular resource, like:
    - Collection of things
    - Single thing
    - An action
- Static (file at url) or dynamic (code execution generates response)
- Canonical format:
    - `http://[host][:port]/[path]?[query-string]`
- Examples:
    - `http://localhost:8080/reminders`: collection of reminders
    - `http://localhost:8080/reminders/1`: reminder with id 1
    - `http://localhost:8080/reminders/1/export`: action to export a reminder

---

### Verbs

- GET
    - Read data
- POST
    - Create data
- PUT
    - Update (or insert) data
- DELETE
    - Delete data
