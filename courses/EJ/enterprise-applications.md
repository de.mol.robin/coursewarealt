# Enterprise Applications

---

## What is an enterprise application

- Software to satisfy the needs of an organisation
- It's all about the data
- Distributed architecture (different components that interact with each other)
    - Database: store data
    - APIs: expose/manipulate data
    - Client applications: bridge to user

> Enterprise applications are about the display, manipulation, and storage of large amounts of often complex data and the support or automation of business processes with that data. (Martin Fowler, Patterns of Enterprise Application Architecture)

---

## Typical challenges

- Multiple concurrent users with different access levels
    - Transactionality
    - Security
- Interacting with one or more persistent databases
    - Va
- Distribution across multiple machines
    - client(s), web server, database(s)...
- Scalability
- Internationalization
- ...
- Typical challenges
    - Security (access control)
    - Transactionality
    - Validation
    - Performance, robustness...
