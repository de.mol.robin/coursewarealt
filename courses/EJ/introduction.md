# Building Enterprise Applications with Java

---

## Overview

- What is an enterprise application
- Which Java frameworks exist
- Enterprise design patterns
    - Inversion of control (containers, managed objects and dependency injection)
    - Aspect-Oriented Programming
- Monolith, SOA, microservices...
- Web Services 
- Web Applications
- ORMs
- Testing

---

## Which Java frameworks exist

- JEE
    - not really a framework
    - Hibernate
- Spring (the popular one)
- Vert.X
- Dropwizard
- GWT
- Javalin
- Grails (Groovy)
- Ktor (kotlin)
- Vaadin
- Struts
- Apache Wicket
- Play (Scala)
- Helidon
- Micronaut
- Quarkus
- ...

---

## EDP: Inversion of control

- Found everywhere, also outside Java
