package world.inetum.realdolmen.jcc.spring.helloworldclient;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
public class WelcomeListener {

    private final JmsTemplate jmsTemplate;

    public WelcomeListener(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = "jms/topics/welcome")
    public void greet(Message broadcast) throws JMSException {
        String name = ((TextMessage) broadcast).getText();
        jmsTemplate.convertAndSend(
                broadcast.getJMSReplyTo(),
                "Hello, " + name + "!"
        );
    }
}
