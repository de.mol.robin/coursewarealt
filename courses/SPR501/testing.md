# Testing Improvements

---

## JUnit 5

- Spring 5.0 fully supports JUnit 5 Jupiter to write tests and extensions
  - http://junit.org/junit5/docs/current/user-guide/
  - The Jupiter project provides a test engine to run Jupiter-based tests
- Spring 5.0 provides support for parallel test execution
- For reactive programming, `spring-test` includes a `WebTestClient`
  - This client does not need a running server
  - Using mock requests and responses, it can bind directly to the Webflux server infrastructure
- Of course, JUnit 4 is still fully supported, as JUnit 5 is only just about to go GA
