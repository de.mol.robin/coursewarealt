# Core Updates

---

## JDK 8 is the Baseline

- The entire Spring 5.0 codebase runs on Java 8
  - JDK 8 is the minimum requirement to work with Spring 5.0
  - This is a significant change!
      - The framework was carrying a lot of baggage to support deprecated Java releases
- Originally, Spring was expected to release with Java 9
  - Java 9 was unfortunately lagging behind...
  - The Spring team then decided to decouple the release
  - Spring will be ready when Java 9 releases in late 2017

---

## Core Framework Revisions

- Core Spring has been revised to use the new features of Java 8
  - Access to method parameter metadata using Java 8 reflection enhancements
  - Java 8 default methods in interfaces provide selective declarations
  - Use `@NotNull` and `@Nullable` to mark nullable arguments and return values
      - This deals with null values at compile time instead of NullPointerExceptions at runtime
- Spring now comes with a Commons Logging bridge module `spring-jcl`
  - This version will auto detect Log4j 2.x, SLF4J and JUL without extra bridges
- Resource abstraction provides the `isFile()` indicator for the `getFile()` method

---

## Core Container Updates

- Spring 5.0 supports a candidate component index
  - Add a `META-INF/spring.components` file with an application build task
  - At compile time, the source is introspected and JPA entities and Spring Components are registered
  - Not significant for small projects with less than 200 classes...
  - ... but very significant for large projects as loading the index is cheap!
  - It effectively makes the Spring startup time constant!
      - This will be significantly faster than classpath scanning every time
- `@Nullable` can now be used for optional injection points
  - It also imposes any consumers to prepare for the value to be null

---

## Other Enhancements

- Functional programming style in `GenericApplicationContext` and `AnnotationConfigApplicationContext`
- Consistent detection of annotations on interface methods
  - Such as transactions, caching and async
- XML namespaces are streamlined to unversioned schemas

---

## Libraries and Updates Supported

- Jackson 2.6+
- EhCache 2.10+ / 3.0 GA
- Hibernate 5.0+
- JDBC 4.0+
- XmlUnit 2.x+
- OkHttp 3.x+
- Netty 4.1+

---

## Discontinued Support

- The following packaged are no longer supported
  - `beans.factory.access`
  - `jdbc.support.nativejdbc`
  - `mock.staticmock`
  - `web.view.tiles2M` (Tiles 3 is now the minimum)
  - `orm.hibernate3` and `orm.hibernate4` (Hibernate 5 is now the minimum)
- The following libraries are no longer supported
  - Portlet
  - Velocity
  - JasperReports
  - XMLBeans
  - JDO
  - Guava
