# What's New in Spring 5.0?

---

## Goal

> Learn about the new exciting features of Spring 5.0

---

## Agenda

- Core Updates
- Kotlin Support
- Reactive Streams
- Webflux Module
- Testing Improvements

---

## Spring 5.0 in a nutshell

- Spring is the most popular framework in the world for creating Java enterprise applications
- Spring 5 is the first major release of the framework in 4 years!
  - It comes packed with lots of new and exciting features
- In a nutshell:
  - Spring has been rewritten for **Java 8**
  - Spring Core received several updates
  - Functional programming supported with **Kotlin**
  - New standard for asynchronous handling using **Reactive Streams**
  - New non-blocking web application paradigm with **Spring Web Flux**
  - Testing improvements
  - New libraries supported, some support discontinued
