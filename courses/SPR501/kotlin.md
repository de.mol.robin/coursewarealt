# Kotlin

---

## Functional Programming with Kotlin

- Spring 5.0 adds support for JetBrains Kotlin language
- Kotlin is an object oriented programming language that supports functional programming
  - It runs on the JVM
- This allows functional web endpoints and bean registrations

```kotlin
{
  ("/book" and accept(TEXT_HTML)).nest {
    GET("/", bookController::findAll)
    GET("/{cover}", bookController::findOne)
  }
  ("/api/book" and accept(APPLICATION_JSON)).nest {
    GET("/", bookApiController::findAll)
    GET("/{id}", bookApiController::findOne)
  }
}
```

---

## Functional Programming with Kotlin

- Kotlin can also be used to register beans
  - This is an alternative to `@Configuration` and `@Bean`

```kotlin
val context = GenericApplicationContext {
  registerBean()
    registerBean { BookService(it.getBean()) }
}
```
