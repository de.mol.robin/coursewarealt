# Reactive Streams Support

---

## Reactive Stack Web Framework

- Spring 5.0 introduces the new exciting reactive stack framework
  - It is fully reactive and non-blocking
  - It allows an event-loop style processing that scales with a small number of threads
- Reactive Streams is an API
  - http://www.reactive-streams.org/
  - It is developed by engineers from Netflix, Pivotal, Typesafe, Red Hat, Oracle, Twitter and Spray.io
  - This common API can be implemented by reactive programming implementations
- Reactive Streams is officially part of Java 9
  - For Java 8 you will need an extra dependency
- Spring 5.0 uses the Project Reactor implementation for the Reactive Streams support
  - https://projectreactor.io/

---

## Reactive Programming Expectations

- Reactive programming is a significant paradigm shift
  - Spring 5.0 will be a cornerstone release for reactive programs
  - Upcoming releases of Spring sub-projects will see reactive programming features being added
  - Spring Data has already implemented reactive support for MongoDB and Redis
- While reactive programming is the new thing in Spring, it will not be supported everywhere yet
  - You can expect the Spring Framework to evolve as more reactive programming options become available
