# Spring Webflux

---

## Webflux

- The Spring 5.0 Webflux module spring-webflux supports reactive HTTP and WebSocket clients
- It also provides support server-side for REST, HTML and WebSocket-style interactions
- There are two programming models on the server-side
  - Annotation-based with `@Controller`
  - Functional style routing and handling with Java 8 lambdas
- While Spring Webflux adds exciting new capabilities, traditional Spring MVC is still fully supported

---

## WebClient

- The new reactive `WebClient` can be used as a non-blocking alternative to the `RestTemplate`

```java
WebClient webClient = WebClient.create();
Mono person = webClient.get()
  .uri("http://localhost:8080/book/42")
  .accept(MediaType.APPLICATION_JSON)
  .exchange()
  .then(response -> response.bodyToMono(Book.class));
```
