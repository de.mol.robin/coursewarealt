# Spring Data REST

---

## Spring Data REST

- REST web services have become the number one means for application integration on the web
- REST defines resources that clients interact with
  - Resources are implemented in a hypermedia driven way
- Spring MVC offers a solid foundation to build such services
  - … but implementing them is tedious and filled with boilerplate code
- Spring Data REST builds on top of Spring Data repositories and automatically exports those as REST resources

---

## Configuring Spring Data REST

- Adding Spring Data REST to Spring Boot with Maven

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-rest</artifactId>
</dependency>
```

- Adding Spring Data REST to Maven

```xml
<dependency>
  <groupId>org.springframework.data</groupId>
  <artifactId>spring-data-rest-webmvc</artifactId>
  <version>2.4.0.RELEASE</version>
</dependency>
```

---

## Configuring Spring Data REST

- Spring Data REST is configured in a class called `RepositoryRestMvcConfiguration`
  - Import this class in an existing Spring MVC configuration using `@Import`
  - Or subclass it and override any of the `configXXX()` methods to add your own configuration
- These steps are not required with Spring Boot
  - Spring Boot will automatically enable and configure Spring Data REST
- Start Spring Data REST as a Spring Boot app or as a classic Spring MVC app
- Spring Data REST supports
  - Spring Data JPA, MongoDB, Neo4j, GemFire and Cassandra

---

## Spring Data REST settings

- Changing the base URI
  - By default, Spring Data REST serves resources at the root URL "/"
- With Spring Boot, use a property in `application.properties`

```sh
spring.data.rest.basePath=/api
```

- If you are not using Spring Boot, add this configuration

```java
@Configuration
public class CustomizedRestMvcConfiguration extends RepositoryRestMvcConfiguration {

  @Override
  public RepositoryRestConfiguration config() {
    RepositoryRestConfiguration config = super.config();
    config.setBasePath("/api");
    return config;
  }
}
```

---

## Spring Data REST settings

- There are many other properties you can alter


Property           | Description
:----------------- | :-----------------------------------------------------------------------
`basePath`           | Root URI for Spring Data REST
`defaultPageSize`    | Change default number of items served in a single page
`maxPageSize`        | Change maximum number of items in a single page
`pageParamName`      | Change name of the query parameter for selecting pages
`limitParamName`     | Change name of the query parameter for number of items to show in a page
`sortParamName`      | Change name of the query parameter for sorting
`defaultMediaType`   | Change default media type to use when none is specified
`returnBodyOnCreate` | Change if a body should be returned on creating a new entity
`returnBodyOnupdate` | Change if a body should be returned on updating an entity

---

## Creating a domain object

- A domain object with JPA annotations

![](images/data-rest-child.jpg#no-border)

```java
@Entity
public class Child {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  private String firstName;
  private String lastName;
  // getters and setters
}
```

---

## Creating a repository

- A simple repository is sufficient
  - The `PagingAndSortingRepository` interface provides multiple operations on children

```java
public interface ChildRepository extends PagingAndSortingRepository<Child, Long> {
  List<Child> findByLastName(String name);
}
```

- Spring Data REST will use this repository to create REST endpoints
- You can change some details of the exported endpoints with annotations
  - These are optional, but exports to "/children" instead of the default "/childs"

```java
*@RepositoryRestResource(collectionResourceRel = "children", path = "children")
public interface ChildRepository extends PagingAndSortingRepository<Child, Long> {
* List<Child> findByLastName(@Param("name") String name);
}
```

---

## Starting the REST application

- You can deploy the application as a traditional WAR file in a servlet container
  - With Spring Boot you can make the application executable

```java
@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
```

- Spring Data REST is a Spring MVC application
  - The `RepositoryRestMvcConfiguration` class imports the necessary beans and converters to create a RESTful front end

---

## Testing the application

- Once running, you can use any REST client to test the application
- Spring Data REST uses the Hypertext Application Language (HAL) format for JSON output
  - This is a flexible format and offers a convenient way to supply links next to the data that is served
  - See http://stateless.co/hal_specification.html for more information
- http://localhost:8080 is the top level service, and contains an overview


```json
{
  "_links" : {
    "children" : {
      "href" : "http://localhost:8080/children{?page,size,sort}",
      "templated" : true
    }
  }
}
```

---

## Testing the application

- http://localhost:8080/children

```json
{
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/children{?page,size,sort}",
      "templated" : true
    },
    "search" : {
      "href" : "http://localhost:8080/children/search"
    }
  },
  "page" : {
    "size" : 20,
    "totalElements" : 0,
    "totalPages" : 0,
    "number" : 0
  }
}
```

---

## Testing the application

- Notice there was no content yet in the previous example
- You can perform a `POST` with JSON payload to create a new entity

```json
{  "firstName" : "Kevin",  "lastName" : "McCallister" }
```

- The POST method will create a new entity when you send it to http://localhost:8080/children


![](images/data-rest-kevinmc.jpg)

---

## Testing the application

- When you query again, you can now see your entity in the contents

```json
{
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/children{?page,size,sort}",
      "templated" : true
    },
    "search" : {
      "href" : "http://localhost:8080/children/search"
    }
  },
  "_embedded" : {
    "children" : [ {
      "firstName" : "Kevin",
      "lastName" : "McCallister",
      "_links" : {
        "self" : {
          "href" : "http://localhost:8080/children/1"
        }
        }} ] ... }}
```

---

## Testing the application

- Spring Data REST uses Evo Inflector to pluralize the name for groupings
  - book -> books
  - entry -> entries
  - person -> persons (instead of people)
- You can query the individual record directly
  - http://localhost:8080/children/1

```json
{
  "firstName" : "Kevin",
  "lastName" : "McCallister",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/children/1"
    }
  }
}
```

---

## Testing the application

- When domain objects are related to each other, Spring Data REST will render additional links to navigate to connected records
- To find custom queries
  - http://localhost:8080/children/search
  - The query parameter matches the `@Param("name")` embedded in the interface

```json
{
  "_links" : {
    "findByLastName" : {
      "href" : "http://localhost:8080/children/search/findByLastName{?name}",
      "templated" : true
    }
  }
}
```

---

## Testing the application

- You can also issue `PUT`, `PATCH`, and `DELETE` REST calls to either replace, update, or delete existing records
  - `PUT` replaces an entire record, fields not supplied will be replaced with null
  - `PATCH` can be used to update a subset of items
  - `DELETE` will delete the record
- The examples we saw before all use the hypermedia-driven interface
  - You can discover all the RESTful endpoints
  - There is no need to exchange a formal contract or interface document with your customers

---

## The HAL browser

- The HAL browser is a web app with HAL-powered JavaScript
  - You can point it to any Spring Data REST API and use it to navigate the app and create resources
- All you need to add this tool, is add a single dependency

```xml
<dependencies>
  <dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-rest-hal-browser</artifactId>
  </dependency>
</dependencies>
```

- This dependency will autoconfigure the HAL browser to be served up when you visit your application's root URI in a browser
  - http://localhost:8080/browser

---

## The HAL browser

- The HAL browser shows links from the API and lists them on the left
  - You can click on `GET` and navigate to the collections
  - You can click on the non-`GET` option to make changes
- For `POST`, `PUT` and `PATCH` a pop-up dialog appears
  - The headers are filled out properly for submitting a JSON document
- The HAL browser understands URI templates
  - You can edit the template for testing projections, excerpts, paging and sorting

---

## The HAL browser

![](images/data-rest-hal-browser.png)

---

## Projections and excerpts

- Sometimes you may need to alter the default view
  - Projections and excerpts let you define simplified and reduces views

```java
@Entity
public class Child {

  @Id @GeneratedValue
  private Long id;
  private String firstName, lastName;

  @OneToOne
* private Address address;
  …
}
```

- Notice the `address` property in the above example

---

## Projections and excerpts

- If address has a repository, the attribute will be rendered as a URI to the corresponding `Address` resource

```json
{
  "firstName" : "Kevin",
  "lastName" : "McCallister",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/children/1"
    },
    "address" : {
      "href" : "http://localhost:8080/children/1/address"
    }
  }
}
```

---

## Projections and excerpts

- If the Address domain object does not have it's own repository, Spring Data REST will inline the data fields directly

```json
{
  "firstName" : "Kevin",
  "lastName" : "McCallister",
  "address" : {
    "street": "671 Lincoln Ave, Winnetka",
    "state": "Illinois",
    "country": "US"
  },
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/children/1"
    }
  }
}
```

![](images/data-rest-address.jpg)

- But what if you don't want any address details at all?

---

## Projections and excerpts

- You can offer the consumer of your REST service an alternative by defining one or more projections

```java
*@Projection(name = "noAddresses", types = { Child.class })
interface NoAddresses {
  String getFirstName();
  String getLastName();
}
```

- You now have an extra lookup option
  - http://localhost:8080/children/1?projection=noAddresses
- To find other profiles, you can consult the Application-Level Profile Semantics
   - Spring Data REST exposes ALPS documents from the `/profile` link from the root
   - This allows you to discover details about the resources, e.g. `/profile/children`

---

## Projections and excerpts

- A common situation in REST services is when you compose domain objects
  - Since consumers almost always fetch related data, an excerpt projection can inline that data to save you an extra `GET`

```java
@Projection(name = "inlineAddress", types = { Child.class })
interface InlineAddress {
  String getFirstName();
  String getLastName();
* Address getAddress();
}
```

- Plug it into the repository as follows

```java
*@RepositoryRestResource(excerptProjection = InlineAddress.class)
interface ChildRepository extends CrudRepository<Child, Long> {}
```

---

## Projections and excerpts

- The resulting HAL document is as follows

```json
{
  "firstName" : "Kevin",
  "lastName" : "McCallister",
	"address" : {
    "street": "671 Lincoln Ave, Winnetka",
    "state": "Illinois",
    "country": "US"
  },  
	"_links" : {
    "self" : {
      "href" : "http://localhost:8080/children/1"
    },
    "address" : {
      "href" : "http://localhost:8080/children/1/address"
    }
  }
}
```

---

## Exercise: Spring Data REST

- Exercise one: Creating repositories
  - Your starter project contains the domain model with `Animal`, `Food` and `DietEntry`
  - You will have to create Spring Data repositories
      1. Create an `AnimalRepository`
      2. Create a `FoodRepository`
      3. Create a `DietEntryRepository`
      4. In the `DietEntryRepository`, add a method `findDietEntryByAnimalType`
      5. Execute the unit tests from the `com.realdolmen.spring.repository` package

---

## Exercise: Spring Data REST

- Exercise two: Configuring Spring Data REST
  - Once the repositories are created, we can now activate Spring Data REST
  - You will need to perform the following steps:
      1. Add the Spring Data REST dependency to the `pom.xml` file
      2. Add an `application.properties` file to the resources folder
      3. Configure Spring Data REST on the `/api` URI in the `application.properties` file
      4. Configure the `FoodRepository` to export the REST interface on `/food` instead of `/foods`
      5. Run the `main` method from the `Main` class, and use a browser to retrieve JSON files


---

## Exercise: Spring Data REST

- Exercise three: Adding the HAL browser
  - The HAL browser is a convenient tool to test your REST interfaces
  - Let's configure the HAL browser!
      1. Add the Spring Data REST HAL browser dependency to the `pom.xml`
      2. Run the `main` method from the `Main` class
      3. Point your browser to the HAL browser URL
  - We can now test some functionality of our REST API using the HAL browser
      1. Create a new `Animal`
      2. Retrieve all `Animal`s
      3. Search for an `Animal`
      4. Update an existing `Animal`
      5. Delete an `Animal`
