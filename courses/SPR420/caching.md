# Caching data

---

## Caching

- Stateless components discard their results at the end of a request
  - This makes them scalable, but also prone to repetition
  - This means time and resources are spent over and over again to retrieve the same answer
- Caching frees your application from repeated instructions
  - If an answer is not likely to change frequently, it makes sense to remember it
  - This is usually a cheaper operation than looking it up otherwise
- Caching can have a positive impact on application performance

---

## Caching in Spring

- Spring does not implement a cache solution, but integrates with several popular caching implementations
- Caching in Spring is an aspect-oriented activity
  - You can code your business first, and apply caching around your methods later
- Caching can be configured using annotations or XML in a declarative way

---

## Enabling cache support

- You can enable caching by adding `@EnableCaching` on your Java configuration

```java
@Configuration
*@EnableCaching
public class CachingConfig {
  @Bean
  public CacheManager cacheManager() {
    return new ConcurrentMapCacheManager();
  }
}
```

- This creates an aspect with pointcuts that trigger off of Spring's caching annotations
  - These annotations will fetch, add or remove a value from the cache

---

## Enabling cache support

- In XML, use the `<cache:annotation-driven>` element from the Spring cache namespace

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:cache="http://www.springframework.org/schema/cache"
  xsi:schemaLocation="http://www.springframework.org/schema/beans
  http://www.springframework.org/schema/beans/spring-beans.xsd
  http://www.springframework.org/schema/cache
  http://www.springframework.org/schema/cache/spring-cache.xsd">
* <cache:annotation-driven />
* <bean id="cacheManager"
    class="org.springframework.cache.concurrent.ConcurrentMapCacheManager" />
</beans>
```

---

## Configuring a cache manager

- In the previous examples a `ConcurrentMapCacheManager` is declared
  - This simple cache manager uses a `ConcurrentHashMap` as its cache store
  - Ideal for development, testing and basic applications
  - Less ideal for larger production applications (memory based, tied to application lifecycle)
- Spring has several cache managers for different implementations
  - `SimpleCacheManager`
  - `NoOpCacheManager`
  - `ConcurrentMapCacheManager`
  - `CompositeCacheManager`
  - `EhCacheCacheManager`
- From Spring Data
  - `RedisCacheManager`
  - `GemfireCacheManager`

???trainer
- SimpleCacheManager: Simple cache manager working against a given collection of caches. Useful for testing or simple caching declarations.
- NoOpCacheManager: A basic, no operation CacheManager implementation suitable for disabling caching, typically used for backing cache declarations without an actual backing store.
- ConcurrentMapCacheManager: CacheManager implementation that lazily builds ConcurrentMapCache instances for each getCache(java.lang.String) request.
- CompositeCacheManager: Composite CacheManager implementation that iterates over a given collection of delegate CacheManager instances.
- EhCacheCacheManager: CacheManager backed by an EhCache CacheManager.
- JCacheCacheManager: CacheManager implementation backed by a JCache CacheManager.
- RedisCacheManager: CacheManager implementation for Redis. By default saves the keys directly, without appending a prefix (which acts as a namespace). To avoid clashes, it is recommended to change this (by setting 'usePrefix' to 'true').
- GemfireCacheManager: Spring Framework CacheManager backed by a Gemfire Cache. Automatically discovers the created caches (or Regions in Gemfire terminology).
???t

---

## Configuring a cache manager

- Selecting the cache manager depends on the underlying cache provider
  - Each provides a different caching flavor
  - Some are more production ready than others
  - The choice you make has no impact on the caching rules declared in Spring
- Cache managers are configured as a bean in the Spring application context

---

## Caching with Ehcache

- Ehcache is one of the most popular cache providers

> JAVA’S MOST WIDELY-USED CACHE

```java
@Configuration
@EnableCaching
public class CachingConfig {
  @Bean
  public EhCacheCacheManager cacheManager(CacheManager cm) {
*   return new EhCacheCacheManager(cm);
  }
  @Bean
  public EhCacheManagerFactoryBean ehcache() {
*   EhCacheManagerFactoryBean ehCacheFactoryBean = new EhCacheManagerFactoryBean();
    ehCacheFactoryBean.setConfigLocation(new ClassPathResource("ehcache.xml"));
    return ehCacheFactoryBean;
  }
}
```

---

## Caching with Ehcache

- Ehcache defines its own configuration schema
- The contents of the cache will vary from application to application
  - You need to declare at least a minimal cache (`ehcache.xml`)

```xml
<ehcache>
	<cache name="spittleCache" maxBytesLocalHeap="50m" timeToLiveSeconds="100"></cache>
</ehcache>
```

- You will probably want to take advantage of the rich set of configuration options provided by Ehcache

???trainer
See http://ehcache.org/documentation/configuration
???t

---

## Caching with Redis

- A caching entry is usually nothing more than a key-value pair
  - Redis, which is a key-value store, is perfectly suited to be a cache store!
  - `RedisCacheManager` works with a Redis server via a `RedisTemplate` to store cache entries

```java
@Configuration
@EnableCaching
public class CachingConfig {
  @Bean
  public CacheManager cacheManager(RedisTemplate redisTemplate) {
*   return new RedisCacheManager(redisTemplate);
  }
  @Bean
  public JedisConnectionFactory redisConnectionFactory() { // default localhost, port 6379
*   JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
    jedisConnectionFactory.afterPropertiesSet();
    return jedisConnectionFactory;
  }
  // continues...
}
```

---

## Caching with Redis

```java
@Configuration
@EnableCaching
public class CachingConfig {
  // ...continued
  @Bean
  public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory redisCF) {
*   RedisTemplate<String, String> redisTemplate = new RedisTemplate<String, String>();
    redisTemplate.setConnectionFactory(redisCF);
    redisTemplate.afterPropertiesSet();
    return redisTemplate;
  }
}
```

- Setting up the Redis server is out of scope for this module

---

## Working with multiple cache managers

- Use Spring's `CompositeCacheManager`
  - Can be configured with one or more cache managers
  - It iterates over them as it tries to find a previously cached value

```java
@Bean
public CacheManager cacheManager(net.sf.ehcache.CacheManager cm,
javax.cache.CacheManager jcm) {
* CompositeCacheManager cacheManager = new CompositeCacheManager();
  List<CacheManager> managers = new ArrayList<CacheManager>();
* managers.add(new JCacheCacheManager(jcm));
* managers.add(new EhCacheCacheManager(cm));
* managers.add(new RedisCacheManager(redisTemplate()));
  cacheManager.setCacheManagers(managers);
  return cacheManager;
}
```

---

## Annotating methods for caching

- Spring provides four annotations for declaring caching rules
  - These annotations can be put on a method or on a class

Annotation    | Description
:------------ | :---------------------------------------------------------------------------------------------------------------------------
`@Cacheable`  | Look in the cache for the return value first. If found, return it. If not, invoke the method and put the value in the cache.
`@CachePut`   | Put the return value in the cache. The cache is not checked, the method is always invoked.
`@CacheEvict` | Evict one or more entries from the cache.
`@Caching`    | Grouping annotation to apply multiples of the previous annotations at once.

---

## Populating the cache

- Use `@Cacheable` and `@CachePut`
  - `@Cacheable` looks in the cache first, when not found, invokes the method
  - `@CachePut` never checks the cache, always allows the method to be invoked
  - After the method is invoked, both put the return value in the cache

```java
*@Cacheable("spittleCache")
public Spittle findOne(long id) {
  try {
    return jdbcTemplate.queryForObject(SELECT_SPITTLE_BY_ID, new SpittleRowMapper(), id);
  } catch (EmptyResultDataAccessException e) {
    return null;
  }
}
```

- The value attribute specifies the name of the cache to use
  - The cache key is the `id` parameter passed to the `findOne()` method

---

## Populating the cache

- Consider placing the annotation on the interface instead of the implementation
  - This allows the caching annotation to be inherited by all implementations of the interface

```java
*@Cacheable("spittleCache")
Spittle findOne(long id);
```

- Use `@CachePut` to preload the cache before the value is needed
  - Ideal when saving entities, as there is a high likelihood the value will soon be asked for
  - Make sure to specify a cache key, or else the parameter will be used as key!

```java
*@CachePut(value="spittleCache", key="#result.id")
Spittle save(Spittle spittle);
```

---

## Customizing the cache key

- Use the key attribute of `@Cacheable` and `@CachePut`
  - A SpEL expression can return a relevant key to the object to store

Annotation          | Description
:------------------ | :----------------------------------------------------------------------------------------------
`#root.args`        | The arguments passed in to the cached method, as an array
`#root.caches `     | The caches this method is executed against, as an array
`#root.target `     | The target object
`#root.targetClass` | The target object’s class; a shortcut for `#root.target.class`
`#root.method `     | The cached method
`#root.methodName`  | The cached method’s name; a shortcut for `#root.method.name`
`#result `          | The return value from the method call (not available with `@Cacheable`)
`#Argument  `       | The name of any method argument (such as `#argName`) or argument index (such as `#a0` or `#p0`)

---

## Conditional caching

- There may be cases you want to turn off the caching on a method
- Use the `unless` and `condition` attributes
  - If the `unless` SpEL expression returns true, the returned data is not saved in the cache
  - If the `condition` SpEL expression returns false, the caching is disabled on the method
  - With `unless`, the cache can still be searched for a value to be returned
  - With `condition` evaluating to false, there is no interaction with the cache

```java
*@Cacheable(value="spittleCache", unless="#result.message.contains('NoCache')")
Spittle findOne(long id);
```

```java
@Cacheable(value="spittleCache", unless="#result.message.contains('NoCache')",
* condition="#id >= 10") // disable cache for IDs less than 10
Spittle findOne(long id);
```

- Also note `condition` is evaluated before going into the method…

---

## Removing cache entries

- Use `@CacheEvict` to remove one or more entries from the cache
- This should be done any time a cached value is no longer valid

```java
*@CacheEvict("spittleCache") // removes the entry corresponding to spittleId
void remove(long spittleId);
```

- Use the extra attributes on `@CacheEvict` to influence its behavior
  - `allEntries`: if true, all entries from the cache should be removed
  - `beforeInvocation`: if true, the entries are removed from the cache before the method is invoked; if false (default), after a successful method invocation

---

## Declaring caching in XML

- Spring also offers an XML namespace for caching
  - When you do not feel comfortable putting Spring-specific annotations in your code
  - When you need to apply caching to beans for which you do not own the source code
- This allows you to keep the caching configuration separate from the code
- The Spring `cache` namespace lets you declare caching rules in XML
  - This namespace is paired with the `aop` namespace for declaring pointcuts where caching should be applied

---

## Declaring caching in XML

- Your XML file must include the `cache` and `aop` namespaces

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
* xmlns:cache="http://www.springframework.org/schema/cache"
* xmlns:aop="http://www.springframework.org/schema/aop"
  xsi:schemaLocation="http://www.springframework.org/schema/aop
  http://www.springframework.org/schema/aop/spring-aop.xsd
  http://www.springframework.org/schema/beans
  http://www.springframework.org/schema/beans/spring-beans.xsd
  http://www.springframework.org/schema/cache
  http://www.springframework.org/schema/cache/spring-cache.xsd">
  <!-- Caching configuration will go here -->
</beans>
```

---

## Declaring caching in XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" ...>
  <aop:config>
    <aop:advisor advice-ref="cacheAdvice"
*     pointcut="execution(* com.acme.spittr.db.SpittleRepository.*(..))"/>
  </aop:config>
  <cache:advice id="cacheAdvice">
    <cache:caching>
*     <cache:cacheable cache="spittleCache" method="findRecent" />
      <cache:cacheable cache="spittleCache" method="findOne" />
      <cache:cacheable cache="spittleCache" method="findBySpitterId" />
*     <cache:cache-put cache="spittleCache" method="save" key="#result.id" />
*     <cache:cache-evict cache="spittleCache" method="remove" />
    </cache:caching>
  </cache:advice>
  <bean id="cacheManager"
      class="org.springframework.cache.concurrent.ConcurrentMapCacheManager"/>
</beans>
```

---

## Exercise: Caching data

- Exercise one: Exploring the starter application
  - Your starter project contains a simple web application in which you can register authors
  - Explore the contents of the application
      1. Run the `ApplicationConfiguration` class' `main` method
      2. Open a browser and go to http://localhost:8080
      3. Register a new author, then remove one
      4. Edit the `application.properties` file, add the `spring.jpa.show-sql` property and set it to `true`
      5. Restart the application and perform the same actions as before
      6. Notice several SQL queries generated by Hibernate on the console log

---

## Exercise: Caching data

- Exercise two: Configuring caching
  - In this exercise, we will configure caching using Ehcache
  - These are the steps necessary
      1. Add the `spring-boot-starter-cache` dependency to the `pom.xml`
      2. Add the `net.sf.ehcache:ehcache:2.10.1` dependency to the `pom.xml`
      3. Add a file `ehcache.xml` file to the root of the classpath, in folder `resources`
      4. Add the minimal ehcache configuration to the `ehcache.xml` file to create an `authorCache`
      5. Enable caching in the `MvcConfig` class
      6. Restart the application, and notice the creation of the `ehCacheCacheManager`
      7. Go to http://localhost:8080/metrics and locate the `authorCache` size

---

## Exercise: Caching data

- Exercise three: Adding cache rules
  - We are now able to add cache rules to the repository methods to avoid hitting the database for frequently requested data
  - Add cache rules to the following methods of `AuthorRepository`
      - `findAll()`
      - `delete(id)`
      - `save(Author)`
  - You will have to select the right annotation to use on each method
      - Check the console log when performing finds, deletes and saves and notice how the cache is used by Spring
