# Spring Security

---

## The importance of security

![](images/security-hacker.png)

- Information is probably the most valuable item we have
  - Hackers are continuously trying to steal our data and identities
- As software developers, we must take steps to protect the information that resides in our applications
  - E.g. protect email accounts with username/password, PIN, etc…
- Security is a crucial aspect of most applications
  - Security transcends an application's functionality
  - Security is a cross-cutting concern

---

## Spring Security

- Spring Security is a security framework that provides declarative security for your Spring-based applications
  - This is implemented with Spring AOP and servlet filters
  - Handles both authentication and authorization, at web request and method invocation level
- Spring Security comes from Acegi Security
  - Big turn-off: it required a LOT of XML configuration
- Since release 3.2, Spring Security simplifies the configuration using Java annotations

---

## Spring Security modules

Module        | Description
:------------ | :-------------------------------------------------------------------------------------
ACL           | Provides support for domain object security through access control lists (ACLs)
Aspects       | Support for AspectJ-based aspects instead of standard Spring AOP
CAS Client    | Support for single sign-on authentication using Jasig’s Central Authentication Service
Configuration | Contains support for configuring Spring Security with XML and Java
Core          | Provides the essential Spring Security library
Cryptography  | Provides support for encryption and password encoding
LDAP          | Provides support for LDAP-based authentication
OpenID        | Contains support for centralized authentication with OpenID
Remoting      | Provides integration with Spring Remoting
Tag Library   | Spring Security’s JSP tag library
Web           | Provides Spring Security’s filter-based web security support

---

## Filtering web requests

- Spring Security relies on servlet filters to provide several aspects of security
  - You will need to configure a `DelegatingFilterProxy`

![](images/security-proxy.png#no-border)

- You can add this filter in the web.xml

```xml
<filter>
	<filter-name>springSecurityFilterChain</filter-name>
	<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
</filter>
```

- The filter-name must be named `springSecurityFilterChain`

---

## Filtering web requests

- With a `WebApplicationInitializer` you'll need the following code
  - This will be discovered by Spring and will be used to register the `DelegatingFilterProxy`

```java
public class SecurityWebInitializer extends AbstractSecurityWebApplicationInitializer {}
```

- The configured filters will intercept incoming requests and delegate them to a bean whose ID is `springSecurityFilterChain`
- You will almost never have to explicitly declare any filters or beans, as they will be created when you enable Spring Security!

---

## Adding Spring Security to Maven

- Adding Spring Security to Maven with Spring Boot

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

- With a standard Maven project, add dependencies for all the modules you need

```xml
<dependency>
	<groupId>org.springframework.security</groupId>
	<artifactId>spring-security-web</artifactId>
	<version>4.0.2.RELEASE</version>
</dependency>
<dependency>
	<groupId>org.springframework.security</groupId>
	<artifactId>spring-security-config</artifactId>
	<version>4.0.2.RELEASE</version>
</dependency>
```

---

## The simplest configuration

- Enable Spring Security from a Java-based configuration
    - You will need both the annotation and the superclass

```java
@Configuration
*@EnableWebSecurity
*public class SecurityConfig extends WebSecurityConfigurerAdapter {
}
```

- This configuration will lock down an application so tightly that nobody can get in!

![](images/security-locked-down.jpg)

---

## The simplest configuration

- You can configure the finer points of web security by overriding `configure()` methods

Method                                  | Description
:-------------------------------------- | :-------------------------------------------------------------
`configure(WebSecurity) `                 | Override to configure Spring Security’s filter chain
`configure(HttpSecurity)`                 | Override to configure how requests are secured by interceptors
`configure(AuthenticationManagerBuilder)` | Override to configure user-details services

- When you do not override `configure()` methods, the default is

```java
protected void configure(HttpSecurity http) throws Exception {
	http
		.authorizeRequests()
		.anyRequest().authenticated().and()
		.formLogin().and()
		.httpBasic();
}
```

---

## The simplest configuration

- The default says:
  - HTTP requests are secured
  - All requests must be authenticated
  - Authentication is supported via form-based login (with predefined login page)
  - Authentication used HTTP Basic
- There is no user store to back the authentication process
  - This requires an override of the `configure(AuthenticationManagerBuilder)` method
- Next steps:
  - Configure a user store
  - Specify which requests should and should not require authentication and handle authorities
  - Provide a custom login screen to replace the plain default login
  - Selectively render content on the web views based on security constraints

---

## User detail services

- A user store is a service where usernames, passwords and other data can be kept and retrieved for making authentication decisions
- Spring Security is so flexible you can authenticate users against virtually any data store
  - In-memory, relational database and LDAP are provided out of the box
  - You can also create and plug in custom user store implementations

---

## In-memory user store

- Override the `configure(AuthenticationManagerBuilder)` method
  - With the `inMemoryAuthentication()` method we can enable and populate an in-memory user store

```java
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth
*   .inMemoryAuthentication()
    .withUser("user").password("password").roles("USER").and()
    .withUser("admin").password("password").roles("USER", "ADMIN");
  }
}
```

---

## In-memory user store

- The builder style configuration supports multiple methods

Method                          | Description
:------------------------------ | :-----------------------------------------------------
`accountExpired(boolean)`       | Defines if the account is expired or not
`accountLocked(boolean)`        | Defines if the account is locked or not
`and()`                         | Used for chaining multiple user configurations
`authorities(GrantedAuthority)` | Specifies one or more authorities to grant to the user
`authorities(List…)`            | Specifies one or more authorities to grant to the user
`authorities(String…)`          | Specifies one or more authorities to grant to the user
`credentialsExpired(boolean)`   | Defines if the credentials are expired or not
`disabled(boolean)`             | Defines if the account is disabled or not
`password(String)`              | Specifies the user's password
`roles(String)`                 | Specifies one or more roles to assign to the user

---

## In-memory user store

- Note that the `roles()` method is a shortcut for the `authorities()` methods
  - Any values given to `roles()` are prefixed with `"ROLE_"` and assigned as authorities to the user
- An in-memory user store is very useful for debugging and testing
- For production-ready purposes, it is usually better to maintain user data in a database

---

## JDBC-backed user store

- Override the configure(AuthenticationManagerBuilder) method
  - The jdbcAuthentication() method configures Spring Security to authenticate against a database

```java
@Autowired
DataSource dataSource;

@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  auth
* .jdbcAuthentication()
  .dataSource(dataSource);
}
```

- This minimal configuration makes some assumptions about the database schema

```sql
select username,password,enabled from users where username = ?
select username,authority from authorities where username = ?
```

---

## JDBC-backed user store

- Your database will probably have a different structure… so you need to configure your own queries

```java
@Override
protected void configure(AuthenticationManagerBuilder auth)  throws Exception {
  auth
  .jdbcAuthentication()
  .dataSource(dataSource)
* .usersByUsernameQuery("select username, password, true from Spitter where username=?")
* .authoritiesByUsernameQuery("select username, 'ROLE_USER' from Spitter where username=?");
}
```

- Rule
  - Adhere to the basic contract of the queries, by respecting the selected fields and parameters!

---

## Working with encoded passwords

- You can specify a `passwordEncoder()`

```java
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  auth
  .jdbcAuthentication()
  .dataSource(dataSource)
  .usersByUsernameQuery("select username, password, true from Spitter where username=?")
  .authoritiesByUsernameQuery("select username, 'ROLE_USER' from Spitter where username=?")
* .passwordEncoder(new StandardPasswordEncoder("53cr3t"));
}
```

- This method accepts implementations of the `PasswordEncoder` interface
  - `BCryptPasswordEncoder`,  `NoOpPasswordEncoder`, and `StandardPasswordEncoder`
  - Passwords are never decoded, but what the user enters is encoded and then compared
  - You can easily create your own `PasswordEncoders`…

---

## LDAP-backed authentication

- Override the `configure(AuthenticationManagerBuilder)` method
  - Use the `ldapAuthentication()` method

```java
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  auth
* .ldapAuthentication()
  .userSearchBase("ou=people")
  .userSearchFilter("(uid={0})")
  .groupSearchBase("ou=groups")
  .groupSearchFilter("member={0}");
}
```

- The `userSearchBase()` method provides a base query for finding users
- The `groupSearchBase()` specifies the base query for finding groups

---

## LDAP-backed authentication

- To authenticate the user
  - Perform a bind operation to authenticate directly to the LDAP server
  - Another option is to perform a comparison operation on the password

```java
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  auth
  .ldapAuthentication()
  .userSearchBase("ou=people")
  .userSearchFilter("(uid={0})")
  .groupSearchBase("ou=groups")
  .groupSearchFilter("member={0}")
* .passwordCompare()
* .passwordEncoder(new Md5PasswordEncoder())
* .passwordAttribute("passcode");
}
```

---

## LDAP-backed authentication

- We still need to configure an LDAP server
  - The default configuration assumes the LDAP server is on localhost:33389
  - Use `contextSource()` to configure another location

```java
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  auth
  .ldapAuthentication()
  .userSearchBase("ou=people")
  .userSearchFilter("(uid={0})")
  .groupSearchBase("ou=groups")
  .groupSearchFilter("member={0}")
* .contextSource()
* .url("ldap://acme.com:389/dc=acme,dc=com");
}
```

---

## LDAP-backed authentication

- Spring Security can provide an embedded LDAP server for you
  - Instead of `url()`, use the `root()` method to configure the root suffix for the server
  - When the LDAP server starts, it attempts to load the LDIF files it can find on the classpath
  - The `ldif()` method explicitly defines which file to load

```java
.contextSource().root("dc=acme,dc=com").ldif("classpath:users.ldif");
```

- LDIF (LDAP Data Interchange Format) files have a very specific syntax
  - See https://tools.ietf.org/html/rfc2849

---

## Configuring a custom user service

- If you need to authenticate against users in another way, you can always implement you own implementation of the `UserDetailsService` interface


```java
*public class SpitterUserService implements UserDetailsService {
  private final SpitterRepository spitterRepository;
  public SpitterUserService(SpitterRepository spitterRepository) {
    this.spitterRepository = spitterRepository;
  }
  @Override
* public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Spitter spitter = spitterRepository.findByUsername(username);
    if (spitter != null) {
      List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
      authorities.add(new SimpleGrantedAuthority("ROLE_SPITTER"));
*     return new User(spitter.getUsername(), spitter.getPassword(), authorities);
    }
    throw new UsernameNotFoundException("User '" + username + "' not found.");
  }
}
```

---

## Configuring a custom user service

- Next, you use the created user detail service in your Spring Security configuration

```java
@Autowired
SpitterRepository spitterRepository;
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
* auth.userDetailsService(new SpitterUserService(spitterRepository));
}
```

- Now that we have a user store, we can start intercepting web requests

---

## Intercepting requests

- Not all requests should be secured equally
  - Some may require authentication; some may not
  - Some requests may only be available to users with certain authorities
- Carefully consider your pages and domain
  - Do you have any public pages, such as an index or home page?
  - Do you have any objects which data is essentially public?
  - Is object creation only allowed to specific users?
  - Are there any pages that require the user to authenticate himself, such as profile pages?

---

## Intercepting requests

- Override the `configure(HttpSecurity)` method
  - This allows you to selectively apply security to different  URL paths

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
  http
* .authorizeRequests()
* .antMatchers("/spitters/me").authenticated()
* .antMatchers(HttpMethod.POST, "/spittles").authenticated()
* .anyRequest().permitAll();
}
```

- The antMatchers() methods support Ant-style wildcarding

```java
.antMatchers("/spitters/**").authenticated();
```

- There’s also a  regexMatchers() method that accepts regular expressions

```java
.regexMatchers("/spitters/.*").authenticated();
```

---

## Path configuration methods

Method                   | Description
:----------------------- | :----------------------------------------------------------------
`access(String)`           | Allows access if the given SpEL expression evaluates to true
`anonymous() `             | Allows access to anonymous users
`authenticated()`          | Allows access to authenticated users
`denyAll()`                | Denies access unconditionally
`fullyAuthenticated()`     | Allows access if the user is fully authenticated (not remembered)
`hasAnyAuthority(String…)` | Allows access if the user has any of the given authorities
`hasAnyRole(String…)`      | Allows access if the user has any of the given roles
`hasAuthority(String)`     | Allows access if the user has the given authority
`hasIpAddress(String)`     | Allows access if the request comes from the given IP address
`hasRole(String)`          | Allows access if the user has the given role

---

## Path configuration methods

Method         | Description
:------------- | :------------------------------------------------------------
`not()`        | Negates the effect of any of the other access methods
`permitAll()`  | Allows access unconditionally
`rememberMe()` | Allows access for users who are authenticated via remember-me

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
  http
  .authorizeRequests()
* .antMatchers("/spitter/me").hasRole("SPITTER")
* .antMatchers(HttpMethod.POST, "/spittles").hasRole("SPITTER")
  .anyRequest().permitAll();
}
```

- You can chain as many calls as necessary, but remember that the rules are applied in order!
  - Configure the most specific paths first, so that least specific paths do not trump more specific ones

---

## Securing with Spring expressions

- If you need more specific security rules, you will have to use SpEL
  - E.g. restricting access to specific roles on Tuesday
- Using the `access()` method, you can use SpEL for declaring access requirements

```java
.antMatchers("/spitter/me").access("hasRole('ROLE_SPITTER')")
```

- There are many SpEL expressions available in Spring Security
  - `authentication`, `denyAll`, `hasAnyRole()`, `hasRole()`, `hasIpAddress()`, `isAnonymous()`, `isAuthenticated()`, `isFullyAuthenticated()`, `isRememberMe()`, `permitAll()`, `principal`
  - The possibilities become virtually endless!

```java
.antMatchers("/spitter/me").access("hasRole('ROLE_SPITTER') and hasIpAddress('192.168.1.2')")
```

---

## Enforcing channel security

- The `HttpSecurity` object passed also has a `requiresChannel()` method
This lets you declare channel requirements for various URL patterns, such as HTTPS

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
  http
  .authorizeRequests()
  .antMatchers("/spitter/me").hasRole("SPITTER")
  .antMatchers(HttpMethod.POST, "/spittles").hasRole("SPITTER")
  .anyRequest().permitAll();
  .and()
* .requiresChannel()
* .antMatchers("/spitter/form").requiresSecure()
* .antMatchers("/").requiresInsecure();
}
```
- The `requiresSecure()` method automatically redirects requests to go over HTTPS

---

## Preventing cross-site request forgery
- Cross-site request forgery (CSRF) happens when one site tricks a user into submitting a request to another server, with possibly negative outcome
  - Such as performing some undesired operation on you bank account!

![](images/security-headache.jpg)

---

## Preventing cross-site request forgery

- Since Spring Security 3.2 CSRF protection is enabled by default
- Spring Security uses a synchronized token
  - State-changing requests will be intercepted and checked for the token
  - If the token is not available, or does not match, the request will fail with an `CsrfException`
  - Any forms must submit a token in a `_csrf` field, and that token must be the same as the one calculated and stored by the server

---

## Preventing cross-site request forgery

- Adding the token is handled automatically in certain conditions
- Use the `action` attribute from the `form`-tag prefixed with a Thymeleaf namespace

```html
<form method="POST" th:action="@{/spittles}">...</form>
```

- When you have JSP pages, use a hidden field

```html
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
```

- When using Spring's form-binding tag library, the `<sf:form>` tag automatically adds the hidden CSRF tag
- You can also disable the Spring CSRF protection by calling `csrf().disable()`

---

## Authenticating users

- Once you override the `configure(HttpSecurity)`, you lose the default login page
  - To get that page back, use `formLogin()`
  - The page is available on `/login`, but it is extremely plain!

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
  http
* .formLogin()
  .and()
  .authorizeRequests()
  .antMatchers("/spitter/me").hasRole("SPITTER")
  .antMatchers(HttpMethod.POST, "/spittles").hasRole("SPITTER")
  .anyRequest().permitAll();
  .and()
  .requiresChannel()
  .antMatchers("/spitter/form").requiresSecure();
}
```

---

## Adding a custom login page

- Check the code of the default login to see what is necessary, and convert to Thymeleaf

```html
*<form name="f" th:action="@{/login}" method="POST">
  <table>
    <tr>
      <td>User:</td>
*     <td><input type="text" name="username" value="" /></td>
    </tr>
    <tr>
      <td>Password:</td>
*     <td><input type="password" name="password"/></td>
    </tr>
    <tr>
      <td colspan="2">
        <input name="submit" type="submit" value="Login"/>
      </td>
    </tr>
  </table>
</form>
```

---

## Enabling HTTP Basic authentication

- Is one way to authenticate a user to an application directly in the HTTP request itself
  - This is useful when the user of the application is also an application (e.g. when using a RESTful API)
  - You will get a HTTP 401 response, suitable for REST clients to authenticate against the services they're consuming

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
  http
  .formLogin()
  .loginPage("/login")
  .and()
* .httpBasic()
  .realmName("Spittr")
  .and()
  ...
}
```

---

## Enabling remember-me functionality

- Remember-me functionality lets a user login once, and is then remembered
  - This prevents the application from prompting for a login every time the user comes back to it later
- To turn on remember-me support, call the `rememberMe()` method

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
  http
  .formLogin()
  .loginPage("/login")
  .and()
* .rememberMe()
* .tokenValiditySeconds(2419200) // 4 months, default 2 weeks
* .key("spittrKey") // default is SpringSecured
  ...
}
```

---

## Enabling remember-me functionality

- The remember-me token is stored in a cookie
  - The token is made up of the username, password, expiration date and a private key
  - The data is encoded with an MD5 hash
- The login request will need to include a `remember-me` parameter

```html
<input id="remember_me" name="remember-me" type="checkbox"/>
<label for="remember_me" style="display: inline">Remember me</label>
```

---

## Logging out

- All you need to log out is a link that uses the `logout` capability

```html
<a th:href="@{/logout}">Logout</a>
```

- The request for logout will be handled by a `LogoutFilter`
  - The user will be logged out and any remember-me tokens will be cleared
  - The user's browser will be redirected to `/login?logout` to give the user the opportunity to login again
- Because of CSRF, the logout is an intentional `POST`
  - To support a `GET` link as above, add the following:

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
  http
  .logout()
* .logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
}
```

---

## Logging out

- You can redirect the logout to another page

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
  http
  .formLogin()
  .loginPage("/login")
  .and()
  .logout()
* .logoutSuccessUrl("/")
  ...
}
```

---

## Securing the view

- You may want to show security related information on your site
  - Such as the authenticated user's principal
- Or you may want to conditionally render certain view elements, depending on the user's authority
  - It is a good idea to never show links that a user won't be able to follow…
- There are two ways to work with security in the views
  - Using the Spring Security JSP tag library
  - Using a Thymeleaf dialect

---

## Using Spring Security’s JSP tag library

- This library is very small, and only includes three tags
  - `<security:accesscontrollist>`
  - `<security:authentication>`
  - `<security:authorize>`
- To use them, declare the JSP tag library in a JSP file

```html
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
```

---

## Accessing authentication details

- The JSP tag library gives you access to the logged-in user information

```html
Hello <security:authentication property="principal.username" />!
```

- You have access to the following variables
  - `authorities`, `credentials`, `details` and `principal`
- The `<security:authentication>` tag can also put the values in a variable, in any JSP scope

---

## Conditional rendering

- Example: there is no point in rendering a login form to a user who is already logged in…
- Use the `<security:authorize>` tag to conditionally render a portion of the view

```html
<sec:authorize access="hasRole('ROLE_SPITTER')"> <!-- only for SPITTER role -->
  <s:url value="/spittles" var="spittle_url" />
  <sf:form modelAttribute="spittle" action="${spittle_url}">
    <sf:label path="text"><s:message code="label.spittle" text="Enter spittle:"/></sf:label>
    <sf:textarea path="text" rows="2" cols="40" />
    <sf:errors path="text" />
    <br/>
    <div class="spitItSubmitIt">
      <input type="submit" value="Spit it!" class="status-btn round-btn disabled" />
    </div>
  </sf:form>
</sec:authorize>
```

---

## Conditional rendering

- Using SpEL you can come up with several interesting security constraints
  - Example: make some functionality only available to a user with specific username

```html
<security:authorize access="isAuthenticated() and principal.username=='holden'">
  <a href="/admin">Administration</a>
</security:authorize>
```

- But when hiding links, do not forget to also filter them!
  - Nothing stops a user from trying the `/admin` URL in the browser's address line…
  - Add a new call to `antMatchers()` to tighten the security
  - Avoid the same SpEL expression with the `url` attribute of the tag

```java
.antMatchers("/admin").access("isAuthenticated() and principal.username=='holden'");
```

```html
<spring:url value="/admin" var="admin_url" />
<security:authorize url="${admin_url}">
  <a href="${admin_url}">Admin</a>
</security:authorize>
```

---

## Working with the Thymeleaf Spring Security dialect

- The Thymeleaf Spring Security dialect works in the same way as Spring Security's JSP tag library
  - `sec:authentication`
  - `sec:authorize`
  - `sec:authorize-acl`
  - `sec:authorize-expr`
  - `sec:authorize-url`
- Add the Thymeleaf Extra's dependency


```xml
<dependency>
  <groupId>org.thymeleaf.extras</groupId>
  <artifactId>thymeleaf-extras-springsecurity4</artifactId>
  <version>3.0.0.RELEASE</version>
</dependency>
```

---

## Working with the Thymeleaf Spring Security dialect

- Declare the security namespace in the templates

```html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
	xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4">
	...
</html>
```

- Now you can use the Thymeleaf attributes wherever you want

```html
<div sec:authorize="isAuthenticated()">
	Hello <span sec:authentication="name">someone</span>
</div>
```

```html
<span sec:authorize-url="/admin">
	<br/><a th:href="@{/admin}">Admin</a>
</span>
```

---

## Securing methods

- What if there is a hole in the application's web layer security?
  - This can happen by mistake, or from clever hacking…
  - A controller that handles a valid request could call a method that fetches data that the user is not allowed to see…

![](images/security-hole.jpg)

- By securing both the web layer and the methods behind the scenes, you can make sure that no logic will be executed unless the user is authorized
- This can be achieved using annotations and expressions

---

## Securing methods with annotations

- Using annotations has its benefits
  - Security rules are clearly visible when looking at the method
  - You can apply the security rules on existing business methods easily
- Spring Security provides three kinds of security annotations
  - `@Secured` (Spring Security)
  - `@RolesAllowed` (JSR-250)
  - `@PreAuthorize`, `@PostAuthorize`, `@PreFilter`, `@PostFilter` (using expressions)

---

## Securing methods with annotations

- You will need a configuration class to activate method-level security

```java
@Configuration
*@EnableGlobalMethodSecurity(securedEnabled=true)
*public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
}
```

- The `securedEnabled=true` will create a pointcut, so that methods annotated with `@Secured` are wrapped with a Spring Security aspect

```java
*@Secured({"ROLE_SPITTER", "ROLE_ADMIN"})
public void addSpittle(Spittle spittle) {
  // ...
}
```

- If the user is not authenticated or does not have the privileges…
  - … you will get Exceptions: `AuthenticationException` or `AccessDeniedException`

---

## Securing methods with annotations

- JSR-250’s `@RolesAllowed` is one of Java's standard annotations
  - You need to turn it on in your configuration
  - This annotation style is not mutually exclusive with `securityEnabled`
  - Use the annotation in the same way as `@Secured`

```java
@Configuration
*@EnableGlobalMethodSecurity(jsr250Enabled=true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
}
```

```java
*@RolesAllowed("ROLE_SPITTER")
public void addSpittle(Spittle spittle) {
  // ...
}
```

---

## Using expressions for method-level security

- The following annotations accept SpEL expressions
  - The expression can be any valid SpEL expression and may include Spring Security expressions
  - If the expression evaluates to true, the security rule passes; otherwise, it fails

Annotation       | Description
:--------------- | :--------------------------------------------------------------------------------------------------
`@PreAuthorize`  | Restricts access to a method before invocation based on the result of evaluating an expression
`@PostAuthorize` | Allows a method to be invoked, but throws a security exception if the expression evaluates to false
`@PostFilter`    | Allows a method to be invoked, but filters the results of that method based on an expression
`@PreFilter`     | Allows a method to be invoked, but filters input prior to entering the method

- This gives you a tremendous amount of flexibility in defining security constraints!

---

## Using expressions for method-level security

- You will first need to enable these annotations

```java
@Configuration
*@EnableGlobalMethodSecurity(prePostEnabled=true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
}
```

- Preauthorizing method access

```java
*@PreAuthorize("hasRole('ROLE_SPITTER')")
public void addSpittle(Spittle spittle) {
  // ...
}
```

- This is similar to the previous examples…
  - The biggest difference is you can add SpEL expressions for more advanced constraints!

---

## Using expressions for method-level security

- Note the limit on the length of the method parameter
  - The `#spittle` in the expression refers to the method parameter of the same name

```java
*@PreAuthorize("(hasRole('ROLE_SPITTER') and #spittle.text.length() <= 140) or hasRole('ROLE_PREMIUM')")
public void addSpittle(Spittle spittle) {
  // ...
}
```

---

## Using expressions for method-level security

- Postauthorizing method access
  - This typically involves making decisions based on the object returned from the secured method
  - This means the method must be invoked and given a chance to return a value
  - The `returnObject` variable gives you access to the returned object
  - If the object does not belong to the authenticated user, an Exception will be thrown

```java
*@PostAuthorize("returnObject.spitter.username == principal.username")
public Spittle getSpittleById(long id) {
  // ...
}
```

- Be careful!
  - Since the method is executed first, and intercepted afterwards, make sure the method does not have any undesirable side effects if authorization fails!

---

## Filtering methods on input and output

- Sometimes, you want to filter the data that is being passed into or returned from the method
  - `@PostFilter` allows to narrow down return values
  - If the user gets through the `@PreAuthorize`, the method will execute and the `List` will be returned
  - The `@PostFilter` will then filter that list, ensuring the user only sees those objects he is allowed to see!
  - The variable `filterObject` refers to an individual element

```java
@PreAuthorize("hasAnyRole({'ROLE_SPITTER', 'ROLE_ADMIN'})")
*@PostFilter("hasRole('ROLE_ADMIN') || filterObject.username == principal.name")
public List<Spittle> getOffensiveSpittles() {
  // ...
}
```

---

## Filtering methods on input and output

- In occasion, you may want to filter the values passed into a method
  - This is possible using @PreFilter
  - @PreFilter filters those members of a collection going into the method
  - The list being passed into deleteSpittles() will contain only Spittle s that the current user has permission to delete
  - The targetObject variable provides the current list item to evaluate against

```java
@PreAuthorize("hasAnyRole({'ROLE_SPITTER', 'ROLE_ADMIN'})")
*@PreFilter("hasRole('ROLE_ADMIN') || targetObject.username == principal.name")
public void deleteSpittles(List<Spittle> spittles) { ... }
```

- Gotcha!
  - Avoid writing complex security expressions or embedding too much non-security business logic into the expressions
  - Those expressions can be difficult to test and to debug!

---

## Defining a permission evaluator

- When your expressions become too unwieldy and complex, you should replace it with a permission evaluator
  - The `hasPermission()` function is a Spring Security-provided extension to SpEL
  - This allows you to plugin whatever logic you want to perform
  - You will need to write and register a custom permission evaluator

```java
@PreAuthorize("hasAnyRole({'ROLE_SPITTER', 'ROLE_ADMIN'})")
*@PreFilter("hasPermission(targetObject, 'delete')")
public void deleteSpittles(List<Spittle> spittles) { ... }
```

---

## Defining a permission evaluator

```java
public class SpittlePermissionEvaluator implements PermissionEvaluator {
  private static final GrantedAuthority ADMIN_AUTHORITY = new GrantedAuthorityImpl("ROLE_ADMIN");
* public boolean hasPermission(Authentication authentication, Object target, Object 		
  permission) {
    if (target instanceof Spittle) {
      Spittle spittle = (Spittle) target;
      String username = spittle.getSpitter().getUsername();
      if ("delete".equals(permission)) {
        return isAdmin(authentication) || username.equals(authentication.getName());
      }
    }
    throw new UnsupportedOperationException("hasPermission not supported for object <" + target +
      "> and permission <" + permission + ">");
  }
  public boolean hasPermission(Authentication authentication, Serializable targetId, String
  targetType, Object permission) {
    throw new UnsupportedOperationException();
  }
  private boolean isAdmin(Authentication authentication) {
    return authentication.getAuthorities().contains(ADMIN_AUTHORITY);
  }
}
```

---

## Defining a permission evaluator

- To register the new method, overriding the `createExpressionHandler()` method from `GlobalMethodSecurityConfiguration`
  - Anytime you use `hasPermission()`, the `SpittlePermissionEvaluator` will be invoked

```java
@Override
protected MethodSecurityExpressionHandler createExpressionHandler() {
  DefaultMethodSecurityExpressionHandler expressionHandler =
  new DefaultMethodSecurityExpressionHandler();
* expressionHandler.setPermissionEvaluator(new SpittlePermissionEvaluator());
  return expressionHandler;
}
```

---

## Exercise: Spring Security

- Exercise one: The unsecured web application
  - Your starter project contains a web application with some basic unsecured functionality
  - Your first tasks:
      1. Import the project and explore its contents
      2. Run the main method from `ApplicationConfiguration`
      3. Open the application in a browser
      4. Register five `Author`s and their `Blog`s
      5. Tweak the `application.properties` to keep the `Author`s and `Blog`s in the database

---

## Exercise: Spring Security

- Exercise two: Configuring Spring Security
  - Let's add Spring Security to our application
  - Steps:
      1. Add the Spring Boot Security starter dependency to the `pom.xml` file
      2. Create the Spring Security Java configuration class
      3. Override the `configure(AuthenticationManagerBuilder)` method and  configure a user and administrator with an in-memory user store
      4. Restart the application… and notice the default "ugly" login form
      5. Log in as user or as administrator…

---

## Exercise: Spring Security

- Exercise three: Adding a custom login form
  - In this exercise, we will add our own login form to let users log in to the application
  - You will need to:
      1. Override the `configure(HttpSecurity)` method
      2. Protect the `/authors`, `/blogs` and `/blog` pages to logged in users
      3. Redirect to your login page
      4. Add your own login form to the `login.html` Thymeleaf template
      5. Check if you are able to log into your application
      6. Add remember-me functionality
      7. Configure logout and add a logout link to your pages

---

## Exercise: Spring Security

- Exercise four: Protecting the delete operation and hiding links
  - We will now proceed to protect some functionality of the site depending on user role
  - These are your TODOs:
      1. Protect the remove URLs to users with `ADMIN` role
      2. Add the Thymeleaf Extra's Dependency for Spring Security
      3. Add the Thymeleaf Security namespace to `fragments/fragments`, `/authors` and `/blogs`
      4. Hide the Remove operations for non-`ADMIN` users
      5. Hide the Authors, Blogs, and Logout links for non-authenticated users
      6. Hide the Login and Register link for authenticated users

---

## Exercise: Spring Security

- Exercise five: Database users
  - We will now allow our registered users to log in, and hide the administrator from the list
  - You will need to do the following:
      1. Add a user with `ADMIN` role to your database
      2. Replace the in-memory user store with a JDBC-backed user store, so  your registered users can now log in
      3. Add the name of the logged in user to the right of the top menu
      4. Enable expressions for method level security
      5. Add a filter to the `AuthorController` and `BlogController`, so you will not return the `ADMIN` user or `ADMIN`-blog in the list
- Extra: Encrypt and salt the passwords of your users for extra security!
