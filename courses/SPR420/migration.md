# Migrating from earlier versions of Spring

---

## Migrating guides

![](images/migration-nightmare.jpg "Nightmare")

- This can be as easy as a drop-in replacement to a developer's nightmare!
- Guides can be found in the Spring documentation
  - From older Spring to Spring 3.1
  - From Spring 3.1 to Spring 3.2
  - From Spring 3.2 to Spring 4.1
  - From Spring 4.0 to Spring 4.1
  - From Spring 4.1 to Spring 4.3

???trainer
Migration guides are actively maintained by the community. See
https://github.com/spring-projects/spring-framework/wiki/Migrating-from-earlier-versions-of-the-spring-framework
???t

---

## Reasons for migrating

- Keeping and acquiring talent in new technology
- Avoiding stagnation and technical debt
- No longer required to support older code
- Support for new versions, such as Java 8
  - Who is still on Java 6?
- You can take advantage of new features
  - Such as web sockets and Java configuration
- The upgrade and migration process is not that bad
  - You don't need to do a huge re-platform of everything at once
  - Spring has excellent backwards compatibility

---

## Migration tips

- Before you start updating, make sure you have sufficient unit tests!
  - This is required to avoid any regressions
- Make the code compile with the new version of Spring
  - Follow the migration guides to update configuration and library versions
  - This will set the right dependencies
  - Refactoring will be needed for changed packages/classes/configuration and deprecated code
- Code in multiple passes, include new features gradually
- Replace XML configuration with Java and annotation-based configuration
  - Since they can be mixed, you can keep the original configuration as long as required
