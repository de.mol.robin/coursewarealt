package com.realdolmen.spring.blog.service;

import com.realdolmen.spring.blog.domain.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * Created by cda5732 on 4/08/2016.
 */
@Service
public class EmailSenderServiceImpl implements EmailSenderService {
    @Autowired
    JavaMailSender mailSender;

    @Autowired
    SpringTemplateEngine thymeleaf;

    @Override
    @Async
    public void sendConfirmationEmail(Author author) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom("noreply@realdolmenblog.com");
        helper.setTo(author.getEmail());
        helper.setSubject("Confirming registration");

        Context ctx = new Context();
        ctx.setVariable("authorName", author.getFirstName() + " " + author.getLastName());
        ctx.setVariable("firstName", author.getFirstName());
        ctx.setVariable("lastName", author.getLastName());
        ctx.setVariable("email", author.getEmail());
        ctx.setVariable("userName", author.getUserName());
        String emailText = thymeleaf.process("email", ctx);

        helper.setText(emailText, true);
        mailSender.send(message);
    }
}
