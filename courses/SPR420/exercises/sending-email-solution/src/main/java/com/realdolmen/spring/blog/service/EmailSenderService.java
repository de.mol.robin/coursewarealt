package com.realdolmen.spring.blog.service;

import com.realdolmen.spring.blog.domain.Author;

import javax.mail.MessagingException;

/**
 * Created by cda5732 on 4/08/2016.
 */
public interface EmailSenderService {
    void sendConfirmationEmail(Author author) throws MessagingException;
}
