# TODO
- Create three pages in a `hello` folder by copying the `index.html` page: `welcome.html`, `confirm.html` and `thanks.html`
- Add a simple flow between the three pages using links or buttons
- Make sure you can transition forwards and backwards between the pages

- Create a `registration-flow.xml` file under `src/main/resources/flows/registration`
- Let the user flow from a registration form, to a blog creation and present a confirm page!
- Make sure you save the created Author and Blog when the user confirms at the end