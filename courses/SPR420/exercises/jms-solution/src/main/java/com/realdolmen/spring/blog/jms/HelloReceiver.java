package com.realdolmen.spring.blog.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class HelloReceiver {

    @JmsListener(destination = "helloBox")
    public void receiveHelloMessage(HelloMessage helloMessage) {
        System.out.println("Received <" + helloMessage + ">");
    }

}