package com.realdolmen.spring.blog.jms;

import com.realdolmen.spring.blog.dao.AuthorRepository;
import com.realdolmen.spring.blog.dao.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class RegistrationReceiver {
    private final BlogRepository blogRepository;

    @Autowired
    public RegistrationReceiver(BlogRepository blogRepository) {
        this.blogRepository = blogRepository;
    }

    @JmsListener(destination = "registrationQueue")
    public void receiveRegistration(RegistrationMessage registration) {
        System.out.println("Received <" + registration + ">");
        blogRepository.save(registration.getBlog());
    }
}
