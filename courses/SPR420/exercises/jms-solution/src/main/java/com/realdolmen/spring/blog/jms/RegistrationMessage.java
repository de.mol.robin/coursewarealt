package com.realdolmen.spring.blog.jms;

import com.realdolmen.spring.blog.domain.Author;
import com.realdolmen.spring.blog.domain.Blog;

public class RegistrationMessage {
    private Blog blog;

    public RegistrationMessage() {
    }

    public RegistrationMessage(Blog blog) {
        this.blog = blog;
    }

   public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    @Override
    public String toString() {
        return "RegistrationMessage{" +
                ", blog=" + blog +
                '}';
    }
}
