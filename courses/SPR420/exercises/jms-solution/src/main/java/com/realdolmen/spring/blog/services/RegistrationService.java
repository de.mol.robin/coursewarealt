package com.realdolmen.spring.blog.services;

import com.realdolmen.spring.blog.dao.AuthorRepository;
import com.realdolmen.spring.blog.domain.Author;
import com.realdolmen.spring.blog.domain.Blog;
import com.realdolmen.spring.blog.error.AuthorExistsException;
import com.realdolmen.spring.blog.jms.RegistrationMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

// TODO Adapt the `RegistrationService` to send a `Blog` through a `JmsTemplate`

@Service
public class RegistrationService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    public String save(Author author, Blog blog) throws AuthorExistsException {
        if (authorRepository.findByUserName(author.getUserName()).isPresent()) {
            throw new AuthorExistsException("User name is already in use!");
        }
        jmsTemplate.convertAndSend("registrationQueue",new RegistrationMessage(blog));
        return "success";
    }
}
