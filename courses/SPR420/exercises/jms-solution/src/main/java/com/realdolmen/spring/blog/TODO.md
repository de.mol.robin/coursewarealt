# TODO

- Create a `HelloMessage` POJO
- Add a `HelloReceiver` class with annotations to receive the `HelloMessage`
- Make sure you receive the message by printing it out

- Create a `RegistrationMessage` that contains a `Blog`
- Create a `RegistrationReceiver` that receives the message
- Let the `RegistrationReceiver` save `Blog` in the database
- Log a message to the console when saving