package com.realdolmen.education.service;

import java.math.BigInteger;

public interface HolidayService {
    String requestHoliday(String firstName, String lastName, String employeeNumber, String start, String end);
}
