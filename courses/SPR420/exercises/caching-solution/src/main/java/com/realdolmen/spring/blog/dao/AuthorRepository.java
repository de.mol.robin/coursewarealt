package com.realdolmen.spring.blog.dao;

import com.realdolmen.spring.blog.domain.Author;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// TODO Add cache rules to the following methods of AuthorRepository findAll()
// TODO Add cache rules to the following methods of AuthorRepository delete(id)
// TODO Add cache rules to the following methods of AuthorRepository save(Author)
public interface AuthorRepository extends JpaRepository<Author, Long> {

    @Cacheable(value="authorCache", key="'authorList'")
    @Override
    List<Author> findAll();

    @Caching(evict = {@CacheEvict(value="authorCache", key="'authorList'")}, put = {@CachePut(value="authorCache", key="#result.id")})
    @Override
    Author save(Author author);

    @Caching(evict = {@CacheEvict(value="authorCache", key="'authorList'"), @CacheEvict(value="authorCache", key="#id")})
    @Override
    void delete(Long id);

    @Cacheable(value="authorCache", key="#id")
    @Override
    Author findOne(Long id);

}
