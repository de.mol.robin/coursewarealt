package com.realdolmen.spring.blog.services;

import com.realdolmen.spring.blog.dao.AuthorRepository;
import com.realdolmen.spring.blog.dao.BlogRepository;
import com.realdolmen.spring.blog.domain.Author;
import com.realdolmen.spring.blog.domain.Blog;
import com.realdolmen.spring.blog.error.AuthorExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;

@Service
public class RegistrationService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BlogRepository blogRepository;

    @Transactional
    public String save(Author author, Blog blog) throws AuthorExistsException {
        if(authorRepository.findByUserName(author.getUserName()).isPresent()){
            throw new AuthorExistsException("User name is already in use!");
        }
        blogRepository.save(blog);
        return "success";
    }
}
