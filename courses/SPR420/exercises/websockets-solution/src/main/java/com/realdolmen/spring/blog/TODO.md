# TODO

- Add the `WebSocketConfig` class
- Add a `websockets` package
    - Add a `RequestMessage` and `ResponseMessage`
- Add a `MessageController` that takes the `RequestMessage` as argument and returns a `ResponseMessage`