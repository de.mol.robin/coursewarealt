package com.realdolmen.spring.blog.controllers;

import com.realdolmen.spring.blog.websockets.RequestMessage;
import com.realdolmen.spring.blog.websockets.ResponseMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MessageController {
    @MessageMapping("/request")
    @SendTo("/topic/responses")
    public ResponseMessage answer(RequestMessage message) throws Exception {
        return new ResponseMessage("Hello, " + message.getName() + "!");
    }
}
