package com.realdolmen.spring.repository;

import com.realdolmen.spring.domain.Food;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodRepository extends JpaRepository<Food, Integer> {
}
