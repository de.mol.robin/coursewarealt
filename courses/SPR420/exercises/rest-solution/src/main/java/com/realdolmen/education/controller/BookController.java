package com.realdolmen.education.controller;

import com.realdolmen.education.domain.Book;
import com.realdolmen.education.domain.BookList;
import com.realdolmen.education.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @RequestMapping(method = RequestMethod.GET, value = "book/all")
    public BookList getBooks() {
        return new BookList(bookService.findAllBooks());
    }

    @RequestMapping(method = RequestMethod.GET, value = "book/{isbn}")
    public Book getBookById(@PathVariable String isbn) {
        return bookService.findBookByIsbn(isbn);
    }

    @RequestMapping(method = RequestMethod.POST, value = "book/create")
    public void createBook(@RequestBody Book book) {
        bookService.createBook(book);
    }
}
