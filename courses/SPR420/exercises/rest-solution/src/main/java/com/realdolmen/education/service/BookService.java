package com.realdolmen.education.service;

import com.realdolmen.education.domain.Book;

import java.util.List;
import java.util.Set;

public interface BookService {
    String findBookTitleByIsbn(String isbn);
    Book findBookByIsbn(String isbn);
    List<Book> findAllBooks();
    void createBook(Book book);
}