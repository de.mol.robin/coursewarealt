package com.realdolmen.education.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

// Uncomment to receive XML
// @XmlRootElement
public class BookList {
    public List<Book> books;

    public BookList() {
    }

    public BookList(List<Book> books) {
        this.books = books;
    }

    public int size() {
        return books.size();
    }


}
