package com.realdolmen.spring.blog.error;

public class AuthorExistsException extends Exception {
    public AuthorExistsException() {
    }

    public AuthorExistsException(String message) {
        super(message);
    }

    public AuthorExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorExistsException(Throwable cause) {
        super(cause);
    }

    public AuthorExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
