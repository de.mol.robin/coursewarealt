package com.realdolmen.education.config;

import com.realdolmen.education.domain.Book;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

// TODO This is a configuration class
public class AppConfig {

    // TODO This list can be injected
    public List<Book> books() {
        List<Book> books = new ArrayList<>();
        books.add(new Book(1, "A Game Of Thrones", "George R. R. Martin", "0000000000001"));
        books.add(new Book(2, "Nineteen Eighty Four", "George Orwell", "0000000000002"));
        books.add(new Book(3, "Uncle Tom's Cabin", "Harriet Bleecher Stowe", "0000000000003"));
        return books;
    }

}
