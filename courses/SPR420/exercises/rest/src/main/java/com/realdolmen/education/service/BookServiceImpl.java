package com.realdolmen.education.service;

import com.realdolmen.education.domain.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

// TODO This is a Service
public class BookServiceImpl implements BookService {

    private List<Book> books; // TODO Inject the list of books

    @Override
    public String findBookTitleByIsbn(String isbn) {
        Book book = findBookByIsbn(isbn);
        if(book == null) {
            return null;
        }
        return book.getTitle();
    }

    @Override
    public Book findBookByIsbn(String isbn) {
        for (Book book : books) {
            if(book.getIsbn().equals(isbn)) {
                return book;
            }
        }
        return null;
    }

    @Override
    public List<Book> findAllBooks() {
        return Collections.unmodifiableList(books);
    }

    @Override
    public void createBook(Book book) {
        books.add(book);
    }

}
