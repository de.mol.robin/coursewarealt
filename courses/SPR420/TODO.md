# TODO
- Chapters
  - Caching data (DONE!)
  - Spring Security (DONE!)
  - Spring Web Flow (DONE!)
  - Spring SOAP Web Services (DONE!)
  - Spring RESTful Web Services (DONE!)
  - Spring Data REST (DONE!)
  - WebSockets (DONE!)
  - Task Scheduling (DONE!)
  - Spring JMS Integration (DONE!)
  - Integration with NoSQL databases
  - Sending Email (DONE!)

Add schema.sql with:

```sql
drop schema blog;
create schema blog;
```

this to avoid issues with the create-drop from Hibernate

Add introduction and tail
