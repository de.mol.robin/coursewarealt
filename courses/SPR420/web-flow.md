# Spring Web Flow

---

## Conversational web applications

- There are times when a web application must take control of a user's surfing, leading him step by step through an application
  - E.g. the checkout process on an e-commerce site
      - Starting with the shopping cart, enter shipping details, enter billing information, displaying the order confirmation...
- Spring Web Flow enables the development of such flows
  - While you can develop a web flow in any web framework, the flow definition is usually scattered across various elements
  - With Spring Web Flow the definition of the flow is separated from the classes and views that implement its behavior
  - It enables stateful web applications and solves the "back button problem"
- Spring Web Flow is based on a foundation of Spring MVC
  - Special beans must be configured to handle the flow request and execute the flow

???trainer
Creating stateful web applications with flows can be hard. There are several issues you need to solve:

- Visualizing the flow is very difficult
- The application has a lot of code accessing the HTTP session
- Enforcing controlled navigation is important but not possible
- Proper browser back button support seems unattainable
- Browser and server get out of sync with "Back" button use
- Multiple browser tabs causes concurrency issues with HTTP session data

Spring Web Flow provides a solution to the above issues.

With Spring Web Flow, you can implement scenarios with one or more of the following properties:

- You have a clear start and end point
- You will lead you users through a set of screens in a specific order
- Changes are not finalized until the last step
- Once completed, you should not be able to repeat the transaction accidentally
???t

---

## Adding Spring Web Flow dependencies

- To add Spring Web Flow to your project, add the following dependency to your Maven `pom.xml` file

```xml
<dependency>
  <groupId>org.springframework.webflow</groupId>
  <artifactId>spring-webflow</artifactId>
  <version>2.4.5.RELEASE</version>
</dependency>
```

---

## Configuring Spring Web Flow

- Web Flow provides configuration support for both Java and XML-based configuration
- To get started with Java configuration you need to
  - Extend `AbstractFlowConfiguration`
  - Add the `@Configuration` annotation

```java
@Configuration
public class WebFlowConfig extends AbstractFlowConfiguration {
}
```

---

## Configuring Spring Web Flow

- Add the Spring Web Flow namespace to the context definition file for XML configuration

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
* xmlns:flow="http://www.springframework.org/schema/webflow-config"
* xsi:schemaLocation="http://www.springframework.org/schema/webflow-config
* http://www.springframework.org/schema/webflow-config/spring-webflow-config-2.4.xsd
  http://www.springframework.org/schema/beans
  http://www.springframework.org/schema/beans/spring-beans.xsd">
  <!-- configuration comes here -->
</beans>
```

---

## Wiring a flow executor

- The flow executor drives the execution of a flow
  - It creates and launches an instance of the flow when a user enters it
  - When a flow pauses, the flow executor resumes the flow once the user takes some action

```java
@Bean
public FlowExecutor flowExecutor() {
  return getFlowExecutorBuilder(flowRegistry()).build();
}
```

```xml
<flow:flow-executor id="flowExecutor" />
```

---

## Configuring a flow registry

- The flow registry loads flow definitions and makes them available to the flow executor

```java
@Bean
public FlowDefinitionRegistry flowRegistry() {
  return getFlowDefinitionRegistryBuilder()
  .addFlowLocationPattern("/WEB-INF/flows/**/*-flow.xml")
  .build();
}
```

---

## Configuring a flow registry

```xml
<flow:flow-registry id="flowRegistry">
  <flow:flow-location-pattern value="/WEB-INF/flows/**/*-flow.xml" />
</flow:flow-registry>
```

- The flow registry will look in the `/WEB-INF/flows` subdirectories and any XML file whose name ends with `-flow.xml` will be considered a flow definition
- Flows are referred to by their IDs
    - By default, flows will be assigned registry identifiers equal to their filenames minus the file extension
        - Unless a registry base path is defined

---

## Configuring a flow registry

- You can explicitly identify the flow definition location

```java
@Bean
public FlowDefinitionRegistry flowRegistry() {
  return getFlowDefinitionRegistryBuilder()
  .addFlowLocation("/WEB-INF/flows/registration.xml")
  .build();
}
```

```xml
<flow:flow-registry id="flowRegistry">
  <flow:flow-location path="/WEB-INF/flows/registration.xml" />
</flow:flow-registry>
```

---

## Configuring a flow registry

- If you want to be more explicit about the flow ID, set the `id` attribute directly

```java
public FlowDefinitionRegistry flowRegistry() {
  return getFlowDefinitionRegistryBuilder()
  .addFlowLocation("/WEB-INF/flows/registration.xml", "registerUser")
  .build();
}
```

```xml
<flow:flow-registry id="flowRegistry">
  <flow:flow-location id="registerUser" path="/WEB-INF/flows/registration.xml" />
</flow:flow-registry>
```

---

## Configuring a flow registry

- When you start seeing the same base path multiple times, it can be useful to set it in the configuration:

```java
public FlowDefinitionRegistry flowRegistry() {
  return getFlowDefinitionRegistryBuilder()
  .setBasePath("/WEB-INF/flows")
  .addFlowLocationPattern("/registration/registration-flow.xml")
  .build();
}
```

```xml
<webflow:flow-registry id="flowRegistry" base-path="/WEB-INF/flows">
  <webflow:flow-location path="/registration/registration-flow.xml" />
</webflow:flow-registry>
```

- The flow ID is now the directory relative to the `flows` directory

![](images/web-flow-ids.png#no-border)

---

## Handling flow requests

- A Spring MVC `DispatcherServlet` will dispatch to controllers
- For flows we need a `FlowHandlerMapping` to help the `DispatcherServlet` to send requests to Spring Web Flow
  - This will be wired to the flow registry to find flow definitions
  - When a URL ends with `/registration`, it will find the flow with ID `registration`

```xml
<bean class="org.springframework.webflow.mvc.servlet.FlowHandlerMapping">
  <property name="flowRegistry" ref="flowRegistry" />
</bean>
```

- The "controller" that will handle and process the flow request, is the `FlowHandlerAdapter`
  - It references the flow executor to start the flow

```xml
<bean class="org.springframework.webflow.mvc.servlet.FlowHandlerAdapter">
  <property name="flowExecutor" ref="flowExecutor" />
</bean>
```

???trainer
Note that you can simply replace these XML bean declarations with Java `@Bean` configurations. This gets a bit more difficult with Spring Boot as it will add its own automatic configuration, which needs to be customized.
???t

---

## Resolving views inside flow definitions

- When using view IDs in flow definitions, you need a view resolver
  - You can configure Spring Web Flow to use your existing one as follows

```java
@Bean
public FlowBuilderServices flowBuilderServices() {
  return getFlowBuilderServicesBuilder()
  .setViewFactoryCreator(viewFactoryCreator())
  .build();
}

@Bean
public ViewFactoryCreator viewFactoryCreator() {
  MvcViewFactoryCreator viewFactoryCreator = new MvcViewFactoryCreator();
  viewFactoryCreator.setFlowViewResolver(internalResourceViewResolver());
  return viewFactoryCreator;
}
```

---

## Resolving views inside flow definitions

- The same for the XML configuration

```xml
<flow:flow-builder-services id="flowBuilderServices" view-factory-creator="viewFactoryCreator"/>
<bean id="viewFactoryCreator" class="org.springframework.webflow.mvc.builder.MvcViewFactoryCreator">
  <property name="viewResolvers">
    <list>
      <ref bean="internalResourceViewResolver"/>
    </list>
  </property>
</bean>
```

---

## Importing the Spring Web Flow configuration

- To include the Spring Web Flow XML configuration into a Java based configuration, use the `@ImportResource` annotation

```java
@Configuration
@ImportResource("classpath:/webflow-context.xml")
public class WebMvcConfig extends WebMvcConfigurerAdapter {
  // method overrides here
}
```

---

## Adding Spring Web Flow to Spring Boot

- For Spring Boot projects, you will need all the previously configured beans
- Let's put them all together with the existing auto-configuration from Spring Boot

```java
@Configuration
public class WebFlowConfig extends AbstractFlowConfiguration {

  @Autowired
  private List<ViewResolver> viewResolvers;

  @Bean
  public FlowExecutor flowExecutor() { // the flow executor
    return getFlowExecutorBuilder(flowRegistry())
    .addFlowExecutionListener(new SecurityFlowExecutionListener(), "*")
    .build();
  }
  // continues
}
```

---

## Adding Spring Web Flow to Spring Boot

```java
  // continued
  @Bean
  public FlowDefinitionRegistry flowRegistry() { // the flow registry
    return getFlowDefinitionRegistryBuilder(flowBuilderServices())
    .setBasePath("classpath*:/flows")
    .addFlowLocationPattern("/**/*-flow.xml")
    .build();
  }

  @Bean
  public FlowBuilderServices flowBuilderServices() { // flow builder services
    return getFlowBuilderServicesBuilder()
    .setViewFactoryCreator(mvcViewFactoryCreator())
    .build();
  }

  @Bean
  public MvcViewFactoryCreator mvcViewFactoryCreator() { // integration with existing resolvers
    MvcViewFactoryCreator factoryCreator = new MvcViewFactoryCreator();
    factoryCreator.setViewResolvers(viewResolvers);
    factoryCreator.setUseSpringBeanBinding(true);
    return factoryCreator;
  }
  // continues
```

---

## Adding Spring Web Flow to Spring Boot

```java
  // continued
  @Bean
  public FlowHandlerMapping flowHandlerMapping() { // set flows at a higher priority
    FlowHandlerMapping handlerMapping = new FlowHandlerMapping();
    handlerMapping.setOrder(-1);
    handlerMapping.setFlowRegistry(flowRegistry());
    return handlerMapping;
  }

  @Bean
  public FlowHandlerAdapter flowHandlerAdapter() { // integrate web flow with Spring MVC
    FlowHandlerAdapter handlerAdapter = new FlowHandlerAdapter();
    handlerAdapter.setFlowExecutor(flowExecutor());
    handlerAdapter.setSaveOutputToFlashScopeOnRedirect(true);
    return handlerAdapter;
  }
```

???trainer
For more details about this configuration and why there is no Spring Boot Web Flow starter, see the following link:

https://github.com/spring-projects/spring-boot/issues/502
???t

---

## Components of a flow

- A flow is primarily defined by three elements
  - states: points in the flow where something happens
  - transitions: to get from one state to another
  - flow data: data collected as the flow progresses
- A state is where logic is performed, some decision is made or some page is presented to the user

---

## States

- Spring Web Flow defines five different kinds of state

State    | Description
:------- | :----------------------------------------------------------------------------------------------------------------
Action   | Action states are where the logic of a flow takes place.
Decision | Decision states branch the flow in two directions, routing the flow based on the outcome of evaluating flow data.
End      | The end state is the last stop for a flow. Once a flow has reached its end state, the flow is terminated.
Subflow  | A subflow state starts a new flow in the context of a flow that is already underway.
View     | A view state pauses the flow and invites the user to participate in the flow.

---

## View states

- Are used to display information and to interact with the user
  - This could be any of the views supported by Spring MVC, such as JSP or Thymeleaf

```xml
<view-state id="welcome" />
```

- The `id` attribute serves a dual purpose
  - It identifies the state in the flow
  - It specifies welcome as the logical name of the view to be rendered
- You can explicitly identify another view name with the `view` attribute

```xml
<view-state id="welcome" view="greeting" />
```

---

## View states

- If a flow presents a form to the user, you may want to specify the object to which the form will be bound
  - Set the `model` attribute

```xml
<view-state id="takePayment" model="flowScope.paymentDetails"/>
```

- The form in the `takePayment` view will be bound to the `paymentDetails` object
  - This object is kept during the whole flow with `flowScope`
  - Add to the form using the `commandName` attribute

---

## Action states

- Are where the application itself goes to work
- Action states typically invoke methods on Spring-managed beans and transition to another state depending on the outcome

```xml
<action-state id="saveOrder">
  <evaluate expression="registerFlowActions.saveOrder(order)" />
  <transition to="thankYou" />
</action-state>
```

- The `evaluate` element declares the expression to perform when the state is entered
  - The `expression` attribute takes a SpEL expression
  - `evaluate` elements can be used on flow start, state entry, view render, transition execution, state exit and flow end

---

## Decision states

- These states enable a binary branch in a flow execution
- A decision state evaluates a `boolean` expression

```xml
<decision-state id="check">
  <if test="registerFlowActions.check(customer.zipCode)"
    then="addCustomer"
    else="warning" />
</decision-state>
```

---

## Subflow states

- Subflow states allow you to break up your flow into discrete parts
  - With a subflow, you can call another flow from within an executing flow

```xml
<subflow-state id="order" subflow="pizza/order">
  <input name="order" value="order"/>
  <transition on="orderCreated" to="payment" />
</subflow-state>
```

- The `input` element passes an object as an input to the subflow
- When the subflow ends on an end-state with ID `orderCreated`, the flow transitions to the state with ID `payment`

---

## End states

- All flows must come to an end, which happens when they transition to an end state

```xml
<end-state id="customerReady" />
```

- What happens next depends on a few factors
  - If the ending flow is a subflow, the calling flow will proceed from the `subflow-state` and the end state ID will be used to trigger the transition away
  - If the `end-state` element has a `view` attribute set, the specified view will be rendered
      - Prefix with `externalRedirect:` to redirect to an external page
      - Prefix with `flowRedirect:` to redirect to another flow
  - If the ending flow is not a subflow, and there is no `view` set, the flow ends, the browser lands on the flow base URL and a new flow begins
- Remember that flows can have multiple end states!

---

## Transitions

- Transitions connect states within a flow
  - Every state in the flow (except end states) should have at least one transition so the flow will know where to go to when the state has completed
  - There can be multiple transitions, each one representing a different path
- The simplest form is identifying the next state in the flow

```xml
<transition to="customerReady" />
```

- Usually, transitions are triggered through events
  - Set the `on` attribute

```xml
<transition on="phoneEntered" to="lookupCustomer"/>
```

---

## Transitions

- Flows can also transition to another state in response to an `Exception` being thrown

```xml
<transition on-exception="com.acme.service.CustomerNotFoundException"
  to="registrationForm" />
```

---

## Global transitions

- You may find that several states share common transitions
  - Rather than repeating these in multiple states, you can define them as global transitions

```xml
<global-transitions>
  <transition on="cancel" to="endState" />
</global-transitions>
```

- Now all states will have an implicit `cancel` transition

---

## Flow data

- As the flow progresses, you can accumulate data
  - This data may be carried through the entire flow to be used as the flow completes
- Create a variable using the `<var>` element
  - This variable is available to all states of the flow

```xml
<var name="customer" class="com.acme.domain.Customer"/>
```

---

## Flow data

- You can also create variables as part of an action state or on entry to a view state

```xml
<evaluate result="viewScope.categories"
  expression="T(com.acme.domain.Categories).asList()" />
```

- The `<set>` element can set a variable's value

```xml
<set name="flowScope.customer"
  value="new com.acme.domain.Customer()" />
```

---

## Scoping flow data

- There are five scopes defined in Spring Web Flow

| Scope | Lifespan and visibility     |
| :------------- | :------------- |
| Conversational       | Created when a top-level flow starts, and destroyed when the top-level flow ends. Shared by a top-level flow and all of its subflows.        |
| Flow       | Created when a flow starts, and destroyed when the flow ends. Only visible in the flow it was created by.        |
| Request       | Created when a request is made into a flow, and destroyed when the flow returns.        |
| Flash       | Created when a flow starts, and destroyed when the flow ends. It’s also cleared out after a view state renders.        |
| View       | Created when a view state is entered, and destroyed when the state exits. Visible only in the view state.        |

- When using `<var>` the variable is always flow-scoped
- When using `<set>` or `<evaluate>` the scope is prefixed to the `name` or `result` attribute

---

## Flow samples

- Using `<view-state>`, `<transition>` and `<end-state>` allows to define page navigation

```xml
<flow xmlns="http://www.springframework.org/schema/webflow"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.springframework.org/schema/webflow
  http://www.springframework.org/schema/webflow/spring-webflow-2.4.xsd">
  <view-state id="enterAddress">
    <transition on="submit" to="review" />
  </view-state>
  <view-state id="review">
    <transition on="confirm" to="confirmed" />
    <transition on="revise" to="enterAddress" />
    <transition on="cancel" to="cancelled" />
  </view-state>
  <end-state id="confirmed" />
  <end-state id="cancelled" />
</flow>
```

---

## Flow samples

- By adding `<evaluate>` you can prepare data at the start of a flow

```xml
<flow xmlns="http://www.springframework.org/schema/webflow"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.springframework.org/schema/webflow
  http://www.springframework.org/schema/webflow/spring-webflow-2.4.xsd">
  <input name="customerId" />
  <var name="address" class="com.acme.domain.Address"/>
  <on-start>
    <evaluate expression="customerService.createOrder(customerId)"
      result="flowScope.order" />
  </on-start>
  <view-state id="enterAddress" model="address">
    <transition on="submit" to="review" />
  </view-state>
  <view-state id="review">
    <transition on="confirm" to="confirmed" />
    <transition on="revise" to="enterAddress" />
    <transition on="cancel" to="cancelled" />
  </view-state>
  <end-state id="confirmed">
    <output name="orderId" value="order.id"/>
  </end-state>
  <end-state id="cancelled" />
</flow>
```

???trainer
Notice the `<input>` and `<output>` elements. These can provide input and output when calling this flow as a subflow.
???t

---

## Model validation

- Web Flow supports JSR-303 validation
  - Configure the flow-builder-services with the `LocalValidatorFactoryBean`

```xml
<flow:flow-registry flow-builder-services="flowBuilderServices" />
<flow:flow-builder-services id="flowBuilderServices" validator="validator" />
<bean id="validator" class="org.springframework.validation.beanvalidation.LocalValidatorFactoryBean" />
```

- The validator will be applied to all model attributes after data binding

---

## Model validation

- In the `WebFlowConfig`, this looks like this:

```java
@Autowired
private Validator validator;

@Bean
public FlowBuilderServices flowBuilderServices() {
  return getFlowBuilderServicesBuilder()
  .setViewFactoryCreator(mvcViewFactoryCreator())
* .setValidator(validator)
  .build();
}
```

- in `WebMvcConfig`, add the validator:

```java
@Bean
public LocalValidatorFactoryBean validator() {
  return new LocalValidatorFactoryBean();
}
```

---

## Partial validation

- In a flow definition, you can specify `validation-hints` on a view state or transition
  - Put the validation groups you want to trigger

```java
@NotNull
@Size(min = 2, max = 30, groups = Group1.class)
private String name;
```

```xml
<view-state id="page1" model="customer" validation-hints="'group1,group2'">
```

???trainer
Define `Group1` as follows:

```java
public interface Group1 {}
```
???t

---

## Supressing validation

- In some scenarios, you want to disable validation for particular events
  - Set the `validate` attribute to false

```xml
<view-state id="chooseOptions" model="order">
  <transition on="proceed" to="reviewOrder"/>
  <transition on="back" to="enterAddress" validate="false" />
</view-state>
```

---

## Securing web flows

- States, transitions, and entire flows can be secured in Spring Web Flow by using the `<secured>` element
  - For example, to secure access to a view state:

```xml
<view-state id="restricted">
  <secured attributes="ROLE_ADMIN" match="all"/>
</view-state>
```  

- Access to the view state will be restricted to only users who are granted `ROLE_ADMIN` access

---

## Signaling events

- Events are signaled by activating buttons, links or other user interface commands
- When a button is pressed, Web Flow looks for a request parameter starting with `_eventId_`

```html
<input type="submit" name="_eventId_proceed" value="Proceed" />
<input type="submit" name="_eventId_cancel" value="Cancel" />
```

- In a form, you can also set the `_eventId` using a hidden field if there is only one button

```html
<input type="submit" value="Proceed" />
<input type="hidden" name="_eventId" value="proceed" />
```

---

## Signaling events

- Events can also be triggered by links

```html
<a href="${flowExecutionUrl}&_eventId=cancel">Cancel</a>
```

- When using Thymeleaf, your links may look a bit different through Thymeleaf attibutes

```html
<a th:href="@{${flowExecutionUrl}(_eventId=next)}">Go to Next</a>
```

---

## Advanced Web Flow features

- When learning more about Web Flow, you may need the following advanced features
  - Using flow inheritance
  - Integrating Web Flow with JSF
  - Rendering fragments with Ajax calls
  - Embed flows on a page
  - Handle file uploads
  - Recording messages during flow executions

---

## Exercise: Spring Web Flow

- Exercise one: importing the "web-flow" project
  - The goal of this exercise is to import the existing starter project
- Perform the following steps:
  1. Import the "web-flow" project into your IDE as a Maven project
  2. Check out the code
  3. Run the `ApplicationConfiguration` class
  4. Open a browser at http://localhost:8080
  5. Register an `Author` and log in
  6. Create a `Blog` using the provided form

---

## Exercise: Spring Web Flow

- Exercise two: configuring Spring Web Flow
  - In this exercise, you will add and configure Spring Web Flow in the project
- You will need to:
  1. Add the Spring Web Flow Maven dependency in the `pom.xml` file
  2. Add the `WebFlowConfig` for the Spring Boot integration
  3. Add the validator configuration
  4. Make sure the project still works!

---

## Exercise: Spring Web Flow

- Exercise three: Spring Web Flow "Hello World!"
  - In this exercise, we will create a first flow to test out the configuration
- For this exercise, follow these steps:
  1. Create a `hello-flow.xml` file under `src/main/resources/flows/hello`
  2. Create three pages in a `hello` folder by copying the `index.html` page: `welcome.html`, `confirm.html` and `thanks.html`
  3. Add a simple flow between the three pages using links or buttons
  4. Run the application and test it from the browser
  5. Make sure you can transition forwards and backwards between the pages

---

## Exercise: Spring Web Flow

- Exercise four: creating a "registration" flow
  - Now that we have a working Web Flow configuration, we can create a "registration" flow
- These are the steps for you to follow:
  1. Create a `registration-flow.xml` file under `src/main/resources/flows/registration`
  2. Adapt the "Register" link to start the flow (on 2 locations!)
  3. Let the user flow from a registration form, to a blog creation and present a confirm page!
  4. Make sure you save the created Author and Blog when the user confirms at the end, with a `RegistrationService`
  5. Also handle the fact that there cannot be two users with the same user name
  6. Launch and test your `registration` flow!

???trainer
Hint: You can copy the `register` and `blog` pages and adapt them for web flow use.

Note the `CascadeType.PERSIST` on the `Blog.author` field. This allows you to save both `Blog` and `Author` in one operation of the `RegistrationService`.
???t
