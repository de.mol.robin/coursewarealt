# Restful Web Services

---

## Concepts of REST

- REST stands for REpresentational State Transfer
- REST is an architecture that can be implemented using any technology
  - It is a generic term, that has nothing to say about business architecture
  - HTTP is an implementation of REST principles
- REST can be viewed as the _architecture of the web_
  - It describes the principles underlying the HTTP architecture a posteriori

---

## Key principles

- Identifiable resources
  - REST is all about resources
- Uniform interface
  - No need to have an interface for each and every application scenario
- Stateless communication
  - Preferable to stateful because of scaling
- Resource representations
  - Interact with resource representations instead of the resources themselves
  - E.g. a book can have many representations
      - The image showing its cover
      - The HTML showing its description
- Hypermedia
  - Linking the current resource to related resources

---

## Identifiable resources

- A resource represents a real or virtual entity
  - Not all application objects have to be exposed as resources
  - A customer, a vehicle, a fleet of vehicles, a shopping cart, …
- On the Web, resources are identified using URIs
  - Each URI adds value to the Net as a whole
- The Net would not be useful without URIs
  - E.g. if all our books in our bookshop are hidden behind one URI, what would the value be?
  - If those books are hidden, we are not able to link to those resources externally (from an email, browsers, ...)
  - Being able to access resources using URIs is a valuable concept!

---

## Uniform interface

- Once we know the ID of a resource, we can interact with it using a single standard interface
  - We need a set of defined operations that we can rely on being supported by any URI
- HTTP has such a set of operations:
  - `GET`, `POST`, `PUT`, `DELETE`, ...
  - E.g. the GET operation allows you to have a link, click on it, and retrieve the representation of that resource
- Since these operations are well defined, this allows for optimization
  - E.g. GET supports “caching”, so we might get some indication how long we may cache the resource representation

---

## Uniform interface

- HTTP is a complete protocol, with status and error codes associated to responses
  - 1xx: informational
  - 2xx: success
  - 3xx: redirection
  - 4xx: client error
  - 5xx: server error
- Common status codes
  - 200: OK
  - 301: Moved Permanently
  - 404: Not Found
  - 500: Internal Server Error

---

## Stateless communication

- HTTP is by default stateless
  - The server will not maintain state for each client
- This gives advantages in scalability
- It also enforces loose coupling
  - The application has no need to know about session or shared content
- The best example of the scalability of HTTP is the World Wide Web

---

## Resource representations

- Resources are always accessed through a representation
- There can be more than one
  - One per URI, or more than one per URI depending on your application design
  - E.g. HTML, PDF, XML, …
- HTTP provides content types and content negotiation
  - The requester negotiates what it wants and what it can accept with which priority
  - The server decides what it will deliver
- Resources should be represented using well-known (ideally standardized) content types
  - Clients will then already have the logic needed to process the content

---

## Resource representations

- Content negotiation
  - The process of selecting the best representation for a given response when there are multiple representations available
  - This is based on the HTTP request headers
      - `Accept`, `Accept-Charset`, `Accept-Encoding`, `Accept-Language`, `User-Agent`
- Content types correspond to Internet media types
  - text, image, audio, video and application
  - Further divided in subtypes: `text/plain`, `text/xml`, `text/html`, ...
- Common content types
  - `text/plain`: the default
  - `text/html`: the WWW
  - `image/gif`, `image/jpg`, `image/png`: images requiring a display device
  - `text/xml`, `application/xml`: for XML exchange
  - `application/json`: lightweight data-interchange text format

---

## Hypermedia

- Possible (client) state transitions are made explicit through links in the resource representation
  - E.g. the client gets a list of 500 addresses
  - With a link, the client can get the next 500 addresses
  - If you want more details for an address, follow the link...
- This enables the existence of relations or connections between resources
  - Such as part-whole, detail, belongs-to, …
- Links are always provided by the server, not created by the client
- This enables seamless evolution and distribution
  - Adding more links to resources or representations increases possible state transitions
  - The location of the resources is hidden behind the URI, so distribution, load-balancing, clusters,... can be optimized

---

## Implementing REST

- We need identifiable resources and a uniform interface on a standard protocol
  - URIs with `GET`, `POST`, `PUT` and `DELETE` on HTTP

![](images/rest-sample.png#no-border)

---

## Implementing REST

- Only use the uniform interface
  - This is the main difference from a services approach
  - Each operation will have a different business meaning and will be mapped to a resource or content type
- Using URLs we can expose resources
  - This does not have to be the Internet, it can also be internally
- One URL for each resource – possibly millions
  - One for every Customer, Order, ...
- Use 4-7 supported operations per resource
  - `GET`, `PUT`, `POST`, `DELETE` (`HEAD`, `TRACE`, `OPTIONS`)
- The resources become cacheable, addressable, linkable, ...
- A lot is possible on the web because of the shared assumptions between every well-behaved HTTP application

---

## Implementing REST

- These are the steps required:
  - Identify resources and design URIs
      - URI design is necessary, links are created by the server
  - Select formats (or create new ones)
      - Atom, RSS, ...
  - Identify method semantics
      - `GET` always makes sense for a resource
  - Select response codes
      - HTTP codes are application response codes that can be used for many scenarios

---

## Advantages of RESTful HTTP

- Universal support
  - Programming languages, operating systems, servers (Apache), ...
  - HTTP is more supported than anything else
- Proven scalability
- Support for redirecting, caching, different representations, resource identification, ...
- _Real_ web integration for machine to machine communication
- Support for XML and many other formats

---

## Comparing REST to Web Services

- REST is the architecture of the Web
  - The world's most successful distributed system
- Web Services do not use the Web, they abuse it
  - HTTP is not just a transport protocol, it does much more!
- Very often, just using HTTP is the best advice
  - If you need to remember one thing: HTTP is good enough
- Understanding REST principles helps you build better Web-based systems

---

## RESTful services in Spring

- Spring MVC has fantastic support for building RESTful services
  - Controllers can handle HTTP requests for all methods
  - `@PathVariable` enables controllers to handle parameterized URLs
  - Resources can be viewed in a variety of ways with several view resolvers
  - Views can be bypassed using `@ResponseBody` and various `HTTPMessageConverter`s
  - Inbound data can be converted to Java objects automatically
  - Spring applications can consume REST resources using `RestTemplate`

---

## Rendering responses

- Responses can be rendered in two ways
  - Using `View` and `ViewResolver`
  - Using `@ResponseBody` with `HttpMessageConverter`
- The two choices are triggered in different ways
  - When returning a `String` from a `Controller` referring to a `View`
  - When returning an `Object` with a `@ResponseBody` annotation or using `ResponseEntity`
- Which one to use?
  - Use `View`s to generate display documents
      - HTML, PDF, Excel, ...
  - Use `@ResponseBody` to exchange data with Web Service Clients
      - JSON, XML, ...

---

## Available HTTPMessageConverters

- `StringHttpMessageConverter`
  - `text/*` and `text/plain`
- `FormHttpMessageConverter`
  - `application/x-www-form-urlencoded`
  - Uses `MultiValueMap<String, String>`
- `ByteArrayMessageConverter`
  - `application/octet-stream`
  - Supports all media types (`*/*`)
- `MarshallingHttpMessageConverter`
  - `text/xml` and `application/xml`

---

## Available HTTPMessageConverters

- `MappingJacksonHttpMessageConverter`
  - `application/json`
- `SourceHttpMessageConverter`
  - `text/xml` and `application/xml`
  - Uses a `javax.xml.transform.Source`
- `BufferedImageHttpMessageConverter`
  - Any media type supported by the Java I/O API
  - Uses `java.awt.image.BufferedImage`

---

## Adding HTTPMessageConverters

- You can add additional `HTTPMessageConverters` in XML and Java configuration
- You can easily create your own custom converters by implementing this simple interface

```java
public interface HttpMessageConverter<T> {
  boolean canRead(Class<?> clazz, MediaType mediaType);
  boolean canWrite(Class<?> clazz, MediaType mediaType);
  List<MediaType> getSupportedMediaTypes();
  T read(Class<? extends T> clazz, HttpInputMessage in) throws IOException,
    HttpMessageNotReadableException;
  void write(T t, MediaType contentType, HttpOutputMessage out) throws IOException,
    HttpMessageNotWritableException;
}
```

---

## Creating a REST service

- Start from domain objects
- These will be converted to JSON or XML using JAXB
  - Support for JAXB is enabled out of the box
  - Use annotations such as `@XmlRootElement`, `@XmlElement`, `@XmlAttribute`, `@XmlTransient`, ...

```java
public class Book {
  private String isbn;
  private String title;
  private String publisher;
  private Author author;
  // ...
}
```

---

## Creating a REST service

- Some more domain objects
  - JAXB annotations are optional but necessary to tweak the result

```java
public class Author {
  private String firstName;
  private String lastName;
  // ...
}
```

```java
// a wrapper class
public class BookList {
  private List<Book> books;
  // ...
}
```

---

## Creating a REST service

- Next, create a backing service
  - This service can be implemented in any way you like
      - Returning mock data, calling database code using JDBC, JPA, ...
  - The service will be called through its interface by the `Controller`

```java
public interface BookService {
  List<Book> findByIsbn(String isbn);
  List<Book> findAllBooks();
  void create(Book book);
}
```

---

## Creating a REST service

- We can now create a REST `Controller`
  - This is a traditional Spring MVC `Controller`
  - You will need to add methods that will use `HTTPMessageConverters` in combination with `@RequestBody` and `@ResponseBody`

```java
*@Controller
public class BookServiceController {
  @Autowired
  private BookService bookService;
  // service methods here
}
```

---

## Creating a REST service

- Let's add a RESTful method
  - `@ResponseBody` treats the return value as data instead of a logical view id
  - The other HTTP methods are available as well
      - `GET`, `POST`, `PUT`, `DELETE`, `HEAD`, `OPTIONS`, `TRACE`

```java
@RequestMapping(method = RequestMethod.GET, value = "/book/all")
*@ResponseBody
public BookList getAll() {
	return new BookList(bookService.findAll());
}
```

- When you start having multiple methods with `@ResponseBody`, use `@RestController` on the class
  - This is a new annotation that makes `@ResponseBody` the default for all methods

```java
*@RestController
public class BookServiceController { ... }
```

---

## Creating a REST service

- Path variables can be accessed from the URL as parameters
  - Use the `@PathVariable` annotation
  - In the following example, the `{isbn}` pattern is bound to the method parameter

```java
@RequestMapping(method = RequestMethod.GET, value="/book/{isbn}")
@ResponseBody // optional with @RestController annotation
public Book getById(@PathVariable("isbn") String isbn) {
  return bookService.findByIsbn(isbn);
}
```

- Other ways to access parameters are
  - `@RequestParam` for request parameters
  - `@RequestHeader` for custom headers

---

## Creating a REST service

- You can still use object binding when creating REST controllers
  - Add `@RequestBody` to the method parameter
  - This will cause the request body data to be interpreted as an object

```java
@RequestMapping(method = RequestMethod.POST, value = "/book")
*public @ResponseBody void create(@RequestBody Book book) {
  bookService.create(book);
}
```

- You will need to add the data to the request in a format that can be converted

---

## Creating a REST service

- To make use of content negotiation, add `produces` and `consumes` attributes
  - `produces` is for the response content type
  - `consumes` is for the request content type
  - JSON is the default

```java
@RequestMapping(method = RequestMethod.GET, value="/book/{isbn}",
* produces = "application/json")
@ResponseBody
public Book getById(@PathVariable("isbn") Integer isbn) {
  return bookService.findByIsbn(isbn);
}
```

```java
@RequestMapping(method = RequestMethod.POST, value = "/book",
* consumes = "application/json")
public @ResponseBody void create(@RequestBody Book book) {
  bookService.create(book);
}
```

---

## Running and testing

- The application will run on the server
  - Point your browser to the mapped URL and you will see the converted output
- The service can return any representation and can be consumed by any client
  - Using AJAX from a web application
  - Using a mobile or native client
  - With tools such as SOAPUI or Firefox RESTClient

---

## Adding JSON support

- JavaScript Object Notation can be consumed directly by JavaScript in web pages

```json
{
  "books":[
    {
      "id":1,
      "title":"A Game Of Thrones",
      "author":"George R. R. Martin",
      "isbn":"0000000000001"
    }, ...
]}
```

---

## Adding JSON support

- Adding support for JSON can be done by simply adding a Maven dependency
  - Spring automatically detects it and enables JSON as representation
  - Alternative XML marshalling frameworks can be enabled as well
  - From a Spring Boot web project, you don't have to do anything!

```xml
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-databind</artifactId>
  <version>2.8.1</version>
</dependency>
```

---

## Creating a REST client

- `RestTemplate` is the central Spring class for client-side HTTP access
- It works similarly to other `-Template` classes in Spring
  - Thread-safe
  - Uses callbacks to customize its default operations
- Its methods enforce REST best practices
  - The name of the methods indicate the HTTP method used as well as what is returned

HTTP Method | RestTemplate Method
:---------- | :------------------
`DELETE`      | `delete()`
`GET`         | `getForObject()`
`HEAD`        | `headForHeaders()`
`OPTIONS`     | `optionsForAllow()`
`POST`        | `postForLocation()`
`PUT`         | `put()`

---

## Creating a REST client

- To create a `RestTemplate`, use its default constructor
  - Each method takes a URI as first argument
  - This can be a URI template
  - Variables can be used to expand the template to a normal URI
- Template variables can be passed in two forms
  - As a `String` variable arguments or as a `Map`

```java
String isbn = ... ;
String result = restTemplate.getForObject("http://acme.com/books/{isbn}", String.class, isbn);
```

```java
String isbn = ...;
Map<String, String> vars = new HashMap<>();
vars.put("isbn", isbn);
String result = restTemplate.getForObject("http://acme.com/books/{isbn}", String.class, vars);
```

---

## Creating a REST client

- A `RequestCallback` can be added to the `RestTemplate` to customize its behavior
  - The callback will be called when the `execute()` method is invoked

```java
public <T> T execute(String url,
  HttpMethod method,
  RequestCallback requestCallback,
  ResponseExtractor<T> responseExtractor,
  Object... urlVariables)
  throws RestClientException
```

- A callback can be created by implementing the `RequestCallback` interface

```java
public interface RequestCallback {
  void doWithRequest(ClientHttpRequest request) throws IOException;
}
```

---

## Exercise: RESTful Web Services
- Exercise one: Explore the starter application
  - Import the starter application into your IDE
  - Explore its contents:
      1. Locate the `BookService` and `BookServiceImpl`
      2. Notice the `AppConfig` class
      3. Notice the empty `BookController`
      4. Explore the `BookServiceRestClientTest` for examples on how to use `RestTemplate`
      5. Run the `BookServiceTest` and `BookServiceRestClientTest`
  - It is your job to fix the unit tests and to finish up the TODOs!

---

## Exercise: RESTful Web Services
- Exercise two: Implement the REST service
  - You will need to:
      1. Add annotations to the `AppConfig` class
      2. Add annotations to the `BookServiceImpl`
      3. Inject the `BookService` in the `BookController`
      4. Implement 3 methods in the `BookController` and add annotations
      5. Run in the browser and check both JSON and XML output by tweaking the annotations
      6. Make sure the unit tests return all green!
