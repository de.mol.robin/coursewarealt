# Sending email

---

## Sending email

- Email is a very common form of communication
  - Offers asynchronous benefits between sender and receiver
- Email messages are often send by applications
  - e.g. confirming an order, notifications of bank activity, ...
- Spring offers support for sending email
  - Email sender abstraction
  - Email templates
  - Boot support

![](images/email-laptop.jpg)

---

## Configuring Spring to send email

- Spring's email abstraction is based on the `MailSender` interface
  - `MailSender` implementations send email by connecting with an email server

![](images/email-mailsender.png)

- Spring comes with one implementation `JavaMailSenderImpl`
  - This implementation has to be configured as a bean

---

## Configuring a mail sender

- Use the following configuration code to create a `JavaMailSenderImpl` bean

```java
@Bean
public MailSender mailSender(Environment env) {
* JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
  mailSender.setHost(env.getProperty("mailserver.host"));
  mailSender.setPort(env.getProperty("mailserver.port")); // default 25
  mailSender.setUsername(env.getProperty("mailserver.username"));
  mailSender.setPassword(env.getProperty("mailserver.password"));
  return mailSender;
}
```

- Note that most properties are optional
- As shown in the code, you can fetch values from `Environment` or `.properties` files if you prefer

---

## Configuring a mail session

- By default, `JavaMailSenderImpl` creates its own mail session
  - You may already have a `javax.mail.MailSession` configured on your server
  - If so, you can configure it to use the `MailSession` you already have

```java
@Bean
public JndiObjectFactoryBean mailSession() {
  JndiObjectFactoryBean jndi = new JndiObjectFactoryBean();
  jndi.setJndiName("mail/Session");
  jndi.setProxyInterface(MailSession.class);
  jndi.setResourceRef(true);
  return jndi;
}
```

- You can then wire it to the mail sender

```java
@Bean
public MailSender mailSender(MailSession mailSession) {
  JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
* mailSender.setSession(mailSession);
  return mailSender;
}
```

---

## Configuring a mail sender with Spring Boot

- Using Spring Boot, you get a free, default `JavaMailSender`

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-mail</artifactId>
</dependency>
```

- You can then configure properties with the `spring.mail` prefix

```sh
spring.mail.host = smtp.acme.com
spring.mail.port = 25
spring.mail.username = bugsbunny@acme.com
spring.mail.password = whatsupdoc
```

![](images/email-bugs-computer.jpg)

???trainer
Note that you will need properties depending on the mail server of your choice. As an example, the following properties can be used when connecting to the Gmail SMTP server

```sh
spring.mail.host = smtp.gmail.com
spring.mail.username = xxxxxxxxx@gmail.com
spring.mail.password = xxxxxxxxx

spring.mail.properties.mail.smtp.auth = true
spring.mail.properties.mail.smtp.socketFactory.port = 465
spring.mail.properties.mail.smtp.socketFactory.class = javax.net.ssl.SSLSocketFactory
spring.mail.properties.mail.smtp.socketFactory.fallback = false
```
???t

---

## Wiring and using the mail sender

- You can now wire the mail sender into the bean that will use it
  - Ideally, create a separate `EmailServiceImpl` class

```java
@Autowired
JavaMailSender mailSender;
```

- You are now ready to construct and send email messages

```java
public void sendSimpleEmail(String to, User user) {
  SimpleMailMessage message = new SimpleMailMessage();
  String name = user.getFullName();
  message.setFrom("noreply@acme.com");
  message.setTo(to);
  message.setSubject("New message from " + name);
  message.setText(name + " says: What's Up Doc?");
* mailSender.send(message);
}
```

- Note that email sending is implemented synchronously
  - You might want to combine this method with the `@Async` annotation or scheduling

???trainer
See module "Task scheduling" in our Spring 4 Advanced Topics course.
???t

---

## Testing the mail sender

- You could inject the mail sender into a `Controller` in a web application or start it from a `main` method...
- For unit tests, prefer a fake SMTP server
  - Like FakeSMTP, SubEtha SMTP (Wiser), DevNull SMTP server, or Dumbster
- These can usually be started from JUnit, so you can perform assertions on the email messages sent
  - This is better than "eye-ball assertions", as the tests can now be automated

---

## Adding attachments

- The trick to send email with attachments it to create multipart messages
  - One part being the body, the other parts being the attachments
- Therefore, `SimpleMailMessage` is too ... simple
- Create a Multipurpose Internet Mail Extensions (MIME) message

```java
MimeMessage message = mailSender.createMimeMessage();
```
- Unfortunately, the `MimeMessage` class is too cumbersome to use on its own
  - That's why Spring includes a `MimeMessageHelper` class

```java
MimeMessageHelper helper = new MimeMessageHelper(message, true);
```

- The second parameter indicates this is a multipart message

---

## Adding attachments

- From the `MimeMessageHelper` you can now assemble the email using helper methods

```java
String name = user.getFullName();
helper.setFrom("noreply@acme.com");
helper.setTo(to);
helper.setSubject("New message from " + name);
helper.setText(name + " says: What's Up Doc?");
```

- You can then load a file (such as an image) and pass it as attachment...

```java
FileSystemResource image = new FileSystemResource("/images/lola-bunny.png");
helper.addAttachment("lola-bunny.png", image);
```

- ... and then send it

```java
mailSender.send(message);
```

---

## Sending email with rich content

- Just set the text of the message as HTML, using true as second parameter
  - This indicates the text will be HTML, so that the content type is set accordingly

```java
helper.setText("<html><body><img src='cid:acme-logo'>" + "<h4>" + user.getFullName() + " says...</h4>"
+ "<i>What's Up Doc?</i>" + "</body></html>", true);
```

- Note the `<img>` tag and its `src` attribute
  - You could set a standard `http:` URL
  - Here, you embed the image in the mail message, identified as `acme-logo`

---

## Sending email with rich content

- Adding the embedded image is like adding an attachment, but you use `addInline()` instead

```java
ClassPathResource image = new ClassPathResource("acme-logo.png");
helper.addInline("acme-logo", image);
```

- Too bad the message is written using `String` concatenation...

---

## Generating email with templates

- The idea is to create the email as an HTML template, then transform it to a `String` to be passed to the `setText()` method
- Spring has several options to choose from
  - Velocity
  - Thymeleaf
- Using templates has several advantages
  - It allows you to see what the resulting message will be like
  - The email layout can be produced by a graphic designer
  - You avoid mixing HTML in Java code

---

## Constructing email messages with Velocity

- Velocity is a general-purpose templating engine from Apache
  - It can be used for code generation and even replace JSP
  - We will use it to format rich email messages
- First, we need to declare a `VelocityEngine` to wire into our `EmailServiceImpl`

```java
@Bean
public VelocityEngineFactoryBean velocityEngine() {
  VelocityEngineFactoryBean velocityEngine = new VelocityEngineFactoryBean();
  Properties props = new Properties();
  props.setProperty("resource.loader", "class");
  props.setProperty("class.resource.loader.class", ClasspathResourceLoader.class.getName());
  velocityEngine.setVelocityProperties(props); // must be set!
  return velocityEngine;
}
```

- The above code configures the engine to load templates from the classpath

---

## Constructing email messages with Velocity

- The `VelocityEngine` can be wired into the `EmailServiceImpl`

```java
@Autowired
VelocityEngine velocityEngine;
```

- Transform a template into a `String` with `VelocityEngineUtils`

```java
Map<String, String> model = new HashMap<String, String>();
model.put("name", name);
model.put("text", "What's Up Doc?");
String emailText = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
  "emailTemplate.vm", model);
```

- Use the merged email text with the `setText()` method

```java
helper.setText(emailText, true);
```

---

## Constructing email messages with Velocity

- The template itself looks as follows
  - It sits at the root of the classpath in a file called `emailTemplate.vm`

```html
<html>
  <body>
    <img src='cid:acme-logo'>
    <h4>${name} says...</h4>
    <i>${text}</i>
  </body>
</html>
```

- This file is easier to read, maintain and edit

---

## Constructing email messages with Thymeleaf

- Thymeleaf templates don't have any special tag libraries or unusual markup
  - This makes them compatible with any HTML tools

```html
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
  <body>
    <img src="images/acme-logo.png" th:src='cid:acme-logo'>
    <h4><span th:text="${name}">Daffy Duck</span> says...</h4>
    <i><span th:text="${text}">Ha ha! It's me, again!</span></i>
  </body>
</html>
```

- Generating and sending the email is similar to Velocity

```java
Context ctx = new Context();
ctx.setVariable("name", name);
ctx.setVariable("text", "What's Up Doc?");
String emailText = thymeleaf.process("emailTemplate", ctx);
...
helper.setText(emailText, true);
mailSender.send(message);
```

---

## Constructing email messages with Thymeleaf

- The Thymeleaf engine is the same `SpringTemplateEngine` that you configure for Spring MVC ...

```java
@Autowired
private SpringTemplateEngine thymeleaf;
```

- ... but by default tries to resolve templates from the servlet context
  - In addition to the `ServletContextTemplateResolver`, you also need a `ClassLoaderTemplateResolver`

```java
@Bean
public ClassLoaderTemplateResolver emailTemplateResolver() {
  ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
  resolver.setPrefix("mail/"); // find templates in mail directory
  resolver.setSuffix(".html");
  resolver.setTemplateMode("HTML5");
  resolver.setCharacterEncoding("UTF-8");
  resolver.setOrder(1);
  return resolver;
}
```

---

## Constructing email messages with Thymeleaf

- Other resolvers need to be ordered, using the `order` property

```java
@Bean
public ServletContextTemplateResolver webTemplateResolver() {
  ServletContextTemplateResolver resolver =  new ServletContextTemplateResolver();
  resolver.setPrefix("/WEB-INF/templates/");
  resolver.setTemplateMode("HTML5");
  resolver.setCharacterEncoding("UTF-8");
* resolver.setOrder(2);
  return resolver;
}
```

- Add the resolvers to the `SpringTemplateEngine`

```java
@Bean
*public SpringTemplateEngine templateEngine(Set<ITemplateResolver> resolvers) {
  SpringTemplateEngine engine = new SpringTemplateEngine();
  engine.setTemplateResolvers(resolvers);
  return engine;
}
```

- Note that none of this is required when working with Spring Boot :-)

---

## Exercise: Sending email
- Exercise one: Exploring the starter application
  - Your starter project contains a simple web application in which you can register authors
  - Explore the contents of the application
      1. Run the `ApplicationConfiguration` class' `main` method
      2. Open a browser and go to http://localhost:8080
      3. Register a new author, then remove one
      4. Start the FakeSMTP server from the included jar file
      5. Notice the TODO's for the next exercises

---

## Exercise: Sending email
- Exercise two: Configuring email
  - In this exercise, we want to send email confirmations to the authors that register themselves on our site
  - As sending emails can take some time, we will use an asynchronous method
  - You will need to:
      1. Update `application.properties` with the mailserver host
      2. Edit `MvcConfig` by enabling asynchronous methods
      3. Create an interface `EmailSenderService` and implementation class `EmailSenderServiceImpl`
      4. Inject the JavaMailSender and SpringTemplateEngine

---

## Exercise: Sending email
- Exercise three: Sending the mail
  - We will now create an email template and send it
  - Your tasks:
      1. Create an `email.html` template file in the templates directory
      2. Implement a method that takes an author as argument and sends an HTML email with Thymeleaf
      3. Replace placeholders in the template with some author specific values
      4. Inject the `EmailSenderService` in the `RegistrationController`
      5. Call your email sending method from the controller once your author has been saved
      6. Check FakeSMTP to view the email sent
