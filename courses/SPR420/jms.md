# Messaging in Spring

---

## Asynchronous messaging

> Asynchronous messaging is a way of indirectly sending messages from one application to another without waiting for a response

- Spring supports
  - Java Messaging Service (JMS)
  - Message-driven POJO's
  - and more!

---

## Asynchronous messaging

- Point-to-point messaging

![](images/jms-p2p.png#no-border)

- Publish-subscribe messaging

![](images/jms-publish-subscribe.png#no-border)

---

## Advantages of asynchronous messaging

- Advantages
  - The client does not have to wait for the service to process the message or be delivered
  - Clients do not have to be aware of any service specifics, like method signatures
  - Services that handle messages can be located on any machine
  - With a publish/subscribe model, multiple services can receive, handle and process the message
  - It allows applications to scale
  - Messaging can be configured with guaranteed delivery of messages

???trainer
The second advantage, is a form of loose coupling: there is no direct link between applications. You send a message to a service, that will deliver the message to the receiving application.
???t

---

## Spring JMS dependencies

- Add the following dependencies to a Spring Boot configuration

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-activemq</artifactId>
</dependency>
<dependency>
  <groupId>org.apache.activemq</groupId>
  <artifactId>activemq-broker</artifactId>
</dependency>
```

---

## Creating a message

- Let's create a message first
 - This is just a simple POJO


```java
public class HelloMessage implements Serializable {
  private String message;

  public HelloMessage() {}

  public HelloMessage(String message) {
    this.message = message;
  }

  // Getters, setters and toString()
}
```

---

## Creating a message receiver

- This is a message-driven POJO

```java
@Component
public class HelloReceiver {

  @JmsListener(destination = "helloBox", containerFactory = "container")
  public void receiveHelloMessage(HelloMessage helloMessage) {
    System.out.println("Received <" + helloMessage + ">");
  }

}
```

- Note that there are no JMS imports required!

???trainer
The `@JmsListener` annotation defines the name of the `destination` to use. The reference to the `containerFactory` is not really required, as Spring Boot registers a default listener factory if necessary.

There is no specific interface to implement.

The annotated method can have a flexible signature:

- The raw `javax.jms.Message` or any of its subclasses
- The `javax.jms.Session` to access native JMS
- The `org.springframework.messaging.Message` representing the incoming JMS message
- `@Header`method arguments to extract a specific header value
- `@Headers` to a `java.util.Map` for getting access to all headers
- A non-annotated element as the payload (optional `@Payload` and `@Valid`)
???t

---

## Configuring Spring JMS

- The `ApplicationConfiguration` class has the JMS configuration

```java
@SpringBootApplication
*@EnableJms // Enables discovery of @JmsListener annotations
public class ApplicationConfiguration {
  // This is similar to the default container from Spring Boot
  @Bean
  public JmsListenerContainerFactory<?> container(ConnectionFactory connectionFactory,
    DefaultJmsListenerContainerFactoryConfigurer configurer) {
    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
    // Adds Spring Boot defaults to the factory
    configurer.configure(factory, connectionFactory);
    // Override defaults here if necessary
    return factory;
  }

  public static void main(String[] args) {
    ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
    JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class); // Can be injected
    // Send a message
    jmsTemplate.convertAndSend("helloBox", new HelloMessage("Hello, World!"));
  }
}
```

???trainer
Notice that two beans are not defined explicitly: `JmsTemplate` and `ConnectionFactory`. These are created automatically by Spring Boot. This means the ActiveMQ broker will run embedded to the Spring container.

Destinations are also
???t

---

## Adding message converters

- The default `MessageConverter` can convert basic types
  - `String`, `Map`, `Serializable`
  - You can configure other `MessageConverter`s

```xml
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-databind</artifactId>
</dependency>
```

```java
@Bean // Serialize message content to json using TextMessage
public MessageConverter jacksonJmsMessageConverter() {
  MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
  converter.setTargetType(MessageType.TEXT);
  converter.setTypeIdPropertyName("_type");
  return converter;
}
```

- This converts the message to JSON as a `TextMessage`
  - It will be assigned to `JmsTemplate` and `JmsListenerContainerFactory`

---

## Configuring to publish-subscribe

- By default, the `pubSubDomain` setting is put to false
  - This means the `JmsTemplate` and `JmsListenerContainerFactory` are configured with queues only!
- Configure the `application.properties` to override the setting

```sh
spring.jms.isPubSubDomain=true
```

---

## Sending a response to another queue

- Using the `@SendTo` annotation, you can send a response to another queue automatically

```java
@JmsListener(destination = "helloBox")
*@SendTo("answerBox")
public String receiveMessage(HelloMessage helloMessage) {
  System.out.println("Received <" + helloMessage + ">"); // Handle message
  return helloMessage.getMessage(); // Send the contents back
}
```

```java
@JmsListener(destination = "answerBox")
public void receiveMessage(String message) {
  System.out.println("Received <" + message + ">"); // Handle the anwser
}
```

---

## Exercise: Messaging in Spring

- Exercise one: configuring Spring JMS
  - Let's configure Spring JMS first
- These are the steps you need:
  1. Import the `jms-starter` project into you IDE as a Maven project
  2. Add the Spring Boot dependencies to the `pom.xml`
  3. Include the `jackson-databind` dependency for JSON conversion
  4. Enable JMS in a `JMSConfig` class
  5. Add the `MessageConverter` for JSON

---

## Exercise: Messaging in Spring

- Exercise two: adding a `HelloMessage`
  - Next, we'll try if JMS if functional using something basic
- Follow the steps below:
  1. Create a `HelloMessage` POJO in a `jms` package
  2. Add a `HelloReceiver` class with annotations to receive the `HelloMessage`
  3. Send a message using a `JMSTemplate`
  4. Make sure you receive the message by printing it out

---

## Exercise: Messaging in Spring

- Exercise three: doing something meaningful
  - Did you succeed the previous exercise?
  - We will now use JMS to save a user registration
- Do the following:
  1. Create a `RegistrationMessage` that contains a `Blog`
  2. Create a `RegistrationReceiver` that receives the message
  3. Adapt the `RegistrationService` to send the `Blog` through a `JmsTemplate`
  4. Let the `RegistrationReceiver` save the `Blog` in the database
  5. Log a message to the console when saving
  6. Test the application by going through the registration flow

???trainer
Note the `CascadeType.PERSIST` on the `Blog.author` field. This allows you to save both `Blog` and `Author` in one operation.
???t
