# WebSockets

---

## WebSockets support in Spring

- WebSockets is a protocol providing full-duplex communication between a browser and a server
  - It enables asynchronous messaging
  - The server can send messages to the browser as well as the browser sending messages to the server
- Spring offers support for WebSocket messaging
  - A low-level API for sending and receiving messages
  - SockJS support to cope with the lack of WebSockets support in browsers, servers, and proxies
  - A messaging template for sending messages
  - A higher-level API for handling messages in Spring MVC controllers (STOMP)


???trainer
Using the low-level API or even SockJS support will feel too low-level for practical use. STOMP (Simple Text Oriented Messaging Protocol) adds a layer on top of WebSocket to add proper messaging semantics to browser-server communication.
???t

---

## Configuring WebSockets

- Add the following dependency to a Spring Boot project

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-websocket</artifactId>
</dependency>
```

- Next, you can add WebJars

```xml
<dependency>
  <groupId>org.webjars</groupId>
  <artifactId>webjars-locator</artifactId>
</dependency>
<dependency>
  <groupId>org.webjars</groupId>
  <artifactId>sockjs-client</artifactId>
  <version>1.0.2</version>
</dependency>
```

---

## Configuring WebSockets

- Some more WebJars

```xml
<dependency>
  <groupId>org.webjars</groupId>
  <artifactId>stomp-websocket</artifactId>
  <version>2.3.3</version>
</dependency>
<dependency>
  <groupId>org.webjars</groupId>
  <artifactId>bootstrap</artifactId>
  <version>3.3.7</version>
</dependency>
<dependency>
  <groupId>org.webjars</groupId>
  <artifactId>jquery</artifactId>
  <version>3.1.0</version>
</dependency>
```

???trainer
WebJars allow you to resolve JavaScript libraries as part of the project’s Maven or Gradle build, just like any other dependency.

To support WebJars, you might need to setup a resource handler in the Spring MVC configuration to resolve requests starting with `/webjars/**`.

```java
@Override
public void addResourceHandlers(ResourceHandlerRegistry registry) {
  registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
}
```

With the resource handler in effect, you can now load JavaScript libraries as in the following example:

```html
<script th:src="@{/webjars/sockjs-client/1.0.2/sockjs.min.js}"></script>
```

This takes advantage of the Thymeleaf `@{}` expression to callculate the full context-relative URL for the JavaScript file.

With the latest Spring Boot and the `webjars-locator` dependency shown above, this handler is automatically configured.
???t

---

## Configuring Spring for STOMP

```java
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
  @Override
  public void configureMessageBroker(MessageBrokerRegistry config) {
    config.enableSimpleBroker("/topic");
    config.setApplicationDestinationPrefixes("/app");
  }
  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry.addEndpoint("/websocket").withSockJS();
  }
}
```

???trainer
Notice:
- `@EnableWebSocketMessageBroker` enables WebSocket messaging, backed by a message broker
- Override `configureMessageBroker()`
  - Call `enableSimpleBroker()` to enable a simple memory-based message broker to carry the responses back to the client on destinations prefixed with `/topic`
  - The `/app` prefix will be used for all message mappings bound with `@MessageMapping`, e.g. `/app/request`
- In `registerStompEndpoints()` the `/websocket` endpoint is registered with SockJS fallback enabled
  - This allows alternate transports to be used if WebSockets are not available
???t

---

## Implementing STOMP messages

- Think about the messages you want to exchange

```json
{
    "name": "Fred"
}
```

```json
{
    "content": "Hello, Fred!"
}
```

![](images/fred-flintstone-computers.jpg#no-border)

---

## Implementing STOMP messages

- Next, create objects to represent the messages

```java
public class RequestMessage {
  private String name;
  public RequestMessage() {
  }
  public RequestMessage(String name) {
    this.name = name;
  }
  // Getters and setters
}
```

```java
public class ResponseMessage {
  private String content;
  public ResponseMessage() {
  }
  public ResponseMessage(String content) {
    this.content = content;
  }
  // Getters and setters
}
```

???trainer
The service will accept messages containing a name in a STOMP message whose body is a JSON object. Spring will use the Jackson JSON library to automatically marshal instances of type Greeting into JSON.
???t

---

## Implementing the message-handling controller

- STOMP messages can be routed to `@Controller` classes

```java
@Controller
public class MessageController {
  @MessageMapping("/request")
  @SendTo("/topic/responses")
  public ResponseMessage answer(RequestMessage message) throws Exception {
    return new ResponseMessage("Hello, " + message.getName() + "!");
  }
}
```

???trainer
Notice:
- `@MessageMapping` maps to the `/request` destination
- The `answer()` method is called
- The payload of the STOMP message is bound to `RequestMessage`
- A `ResponseMessage` is created and broadcast to all subscribers to `/topic/responses`
???t

---

## Creating a WebSockets client

- Let's create a HTML page so we can send and receive messages
  - Put in in `src/main/resources/static/`

```html
<!DOCTYPE html>
<html>
<head>
  <title>Hello WebSocket</title>
  <link href="/webjars/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/main.css" rel="stylesheet">
  <script src="/webjars/jquery/jquery.min.js"></script>
  <script src="/webjars/bootstrap/bootstrap.min.js"></script>
  <script src="/webjars/sockjs-client/sockjs.min.js"></script>
  <script src="/webjars/stomp-websocket/stomp.min.js"></script>
  <script src="/app.js"></script>
</head>
<body>
  <!-- more code here -->
</body>
</html>
```

---

## Creating a WebSockets client

```html
<div id="main-content" class="container">
  <div class="row">
    <form class="form-inline">
      <div class="form-group">
        <label for="connect">WebSocket connection:</label>
        <button id="connect" class="btn btn-primary" type="submit">Connect</button>
        <button id="disconnect" class="btn btn-danger" type="submit"
          disabled="disabled">Disconnect</button>
      </div>
    </form><br/>
    <form id="name-form" class="form-inline">
      <div class="form-group">
        <label for="name">What is your name?</label>
        <input type="text" id="name" class="form-control" placeholder="Your name here..."/>
      </div>
      <button id="send" class="btn btn-primary" type="submit">Send</button>
    </form>
  </div>
```

---

## Creating a WebSockets client

```html
    <div class="row">
      <div class="col-md-12">
        <table id="conversation" class="table table-striped">
          <thead>
            <tr>
              <th>Messages</th>
            </tr>
          </thead>
          <tbody id="messages"></tbody>
        </table>
      </div>
    </div>
  </form>
</div>
```

???trainer
Now that was a big block of code! In this code, we set up the client page. We imported all the necessary scripts. The `app.js` script will contain the client logic for this page.
???t

---

## Creating a WebSockets client

- Now the JavaScript client code
  - Put it in `src/main/resources/static/app.js`

```javascript
var stompClient = null;

$(function () {
  $("form").on('submit', function (e) {
    e.preventDefault();
  });
  $( "#connect" ).click(function() { connect(); });
  $( "#disconnect" ).click(function() { disconnect(); });
  $( "#send" ).click(function() { send(); });
  setConnected(false);
});
```

- The code above sets up the button functions

---

## Creating a WebSockets client

- Next, the `connect()` and `disconnect()`

```javascript
function connect() {
  var socket = new SockJS('/websocket');
  stompClient = Stomp.over(socket);
  stompClient.connect({}, function (frame) {
    setConnected(true);
    console.log('Connected: ' + frame);
    stompClient.subscribe('/topic/responses', function (response) {
      receive(JSON.parse(response.body).content);
    });
  });
}

function disconnect() {
  if (stompClient != null) {
    stompClient.disconnect();
  }
  setConnected(false);
  console.log("Disconnected");
}
```

???trainer
The `connect()` method will use `SockJS` and `stomp.js` to open connections. When the clients connects, we subscribe to the `/topic/responses` where the server will publish messages. When messages arrive, the `receive()` method is called, which will display them.
???t

---

## Creating a WebSockets client

- Finally, `send()`, `receive()` and `setConnected()`

```javascript
function send() {
  stompClient.send("/app/request", {}, JSON.stringify({'name': $("#name").val()}));
}

function receive(message) {
  $("#messages").append("<tr><td>" + message + "</td></tr>");
}

function setConnected(connected) {
  $("#connect").prop("disabled", connected);
  $("#disconnect").prop("disabled", !connected);
  if (connected) {
    $("#conversation").show();
    $("#name-form").show();
  }
  else {
    $("#conversation").hide();
    $("#name-form").hide();
  }
  $("#messages").html("");
}
```

???trainer
The `send()` method retrieves the input from the user, and uses the STOMP client to send it to the `/app/request` destination. The `answer()` method will receive it and create a response.
???t

---

## Exercise: WebSockets

- Exercise one: importing the "websockets" project
  - In this exercise, we will import the starter project
- Steps:
  1. Import the "websockets" project into your IDE as a Maven project
  2. Check out the code
  3. Run the `ApplicationConfiguration` class
  4. Open a browser at http://localhost:8080

---

## Exercise: WebSockets

- Exercise two: adding the configuration
  - Let's configure the WebSockets application
- Steps:
  1. Add the WebSockets Spring Boot dependency in the `pom.xml` file
  2. Add the STOMP and SockJS WebJars dependencies in the `pom.xml` file
  3. Add a `js` directory with an `app.js` file
  4. Add the SockJS, STOMP and App JavaScript files to the bottom of the `index.html` file

---

## Exercise: WebSockets

- Exercise three: implementing WebSockets
  - Now that the configuration is ready, let's implement!
- What to do?
  1. Add a `WebSocketConfig` class to the `config` package
  2. Add a `websockets` package, containing the `RequestMessage` and `ResponseMessage`
  3. Add a `MessageController` that takes a `RequestMessage` as parameter and returns a `ResponseMessage`
  4. Add the WebSocket JavaScript client code to send and receive messages from the `index.html` file
