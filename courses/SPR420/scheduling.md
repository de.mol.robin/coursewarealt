# Task scheduling

---

## Task runners

- Many applications have the need to run a task in the background
  - Sometimes this needs to be done immediately
      - Spring offers the `TaskExecutor` interface
  - Sometimes this needs to be done at some point in the future
      - Spring offers the `TaskScheduler` interface
- Examples
  - Running a background operation
      - Long running operations (batch jobs, monitoring, …)
  - Executing an operation asynchronously
      - JMS listeners, `@Async` methods, …
  - Running a business method periodically
      - Scheduled tasks

---

## TaskExecutor

- The `TaskExecutor` is an implementation of the Java 5 `Executor` interface
  - It provides a simple interface to offer a `Runnable` instance for immediate execution

```java
public interface TaskExecutor extends Executor {
* void execute(Runnable task);
}
```

- The default implementation delegates execution to a backing thread pool
  - This means that the `Runnable` is executed on a different `Thread`

---

## Configuring task executors

- Configuring a `TaskExecutor` can be done in serveral ways
- Using the classic `<bean>` wiring mechanism

```xml
<bean id="taskExecutor" class="o.s.s.c.ThreadPoolTaskExecutor">
  <property name="poolSize" value="20"/>
</bean>
```

- Using the custom task namespace

```xml
<task:executor id="taskExecutor" pool-size="20"/>
```

- Using Java config

```java
@Bean
public TaskExecutor taskExecutor() {
* ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
  executor.setMaxPoolSize(20);
  return executor;
}
```

---

## TaskScheduler

- The `TaskScheduler` is similar to the `TaskExecutor` but allows offering a task for future execution
  - It will still be backed by an underlying thread pool

```java
public interface TaskScheduler {
* ScheduledFuture<?> schedule(Runnable task, Trigger trigger);
  ScheduledFuture<?> schedule(Runnable task, Date startTime);
* ScheduledFuture<?> scheduleAtFixedRate(Runnable task, Date dt, long p);
  ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period);
* ScheduledFuture<?> scheduleWithFixedDelay(Runnable t, Date dt, long d);
  ScheduledFuture<?> scheduleWithFixedDelay(Runnable task, long delay);
}
```

- It returns a Java 5 `Future` instance

---

## Configuring task schedulers

- Configuring a TaskScheduler can be done in serveral ways
- Using the classic <bean> wiring mechanism

```xml
<bean id="taskScheduler" class="o.s.s.c.ThreadPoolTaskScheduler">
  <property name="poolSize" value="20"/>
</bean>
```

- Using the custom task namespace

```xml
<task:scheduler id="taskScheduler" pool-size="20"/>
```

- Using Java config

```java
@Bean
public TaskScheduler taskScheduler() {  
* ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();  
  scheduler.setPoolSize(20);  
  return scheduler;
}
```

---

## Using task runners

- When configured in Spring, you can inject and use the `TaskExecutor` and `TaskScheduler` like any other component

```java
@Component
public class Service {
* @Autowired private TaskScheduler scheduler;

  public void schedulePeriodicEventBusinessMethod() {
*   scheduler.scheduleAtFixedRate(new Runnable() {
      @Override
      public void run() {
        System.out.println("Executing task!");
      }
    }, 1000);
  }
}
```

---

## Trigger

- The `TaskScheduler` has an interesting method receiving a `Trigger` instance
  - The `Trigger` interface is an abstraction to determine the next execution time for the paired `Runnable`

```java
ScheduledFuture<?> schedule(Runnable task, Trigger trigger);
```

- Two implementations exist
  - `CronTrigger`
      - Allows setting the next execution time as a CRON pattern
  - `PeriodicTrigger`
      - Allows setting the next execution time as a scalar with configurable time unit (usually milliseconds)

---

## CronTrigger

- The most useful `Trigger` implementation is `CronTrigger`
  - It allows you to specify a future periodic execution using the compact and well established CRON syntax

```java
scheduler.schedule(task, new CronTrigger("0/20 * 9-17 * * MON-FRI"));
```

- It also has an advantage over the simpler `PeriodicTrigger`
  - `PeriodicTrigger` does not take into account the time it takes to execute the task itself
      - Causing a progressive “shift” of the time to execute
  - You should prefer the use of `CronTrigger` if possible

---

## CronTrigger

- CRON is a well known way of defining scheduled events in the Unix world
  - You define the time to execute by means of a sequence of 6 or 7 fields

![](images/scheduling-cron-syntax.png#no-border)

- Note:
  - Different implementations of CRON may have small variations of this syntax

---

## CronTrigger

- You can also use wild cards and special tokens

Token     | Description
:-------- | :-------------------------------------------------------
`*`       | Any unit
`?`       | Irrelevant unit in context of the rest of the expression
`0/20`    | Every 20 units beginning from the 0th unit
`0, 5, 8` | Only the 0th, 5th and 8th unit
`10-20`   | Range from the 10th to the 20th unit

- Some examples

```sh
 * * * * * *			        Every second
 0/20 * 9-17 * * MON-FRI		Every 20 seconds during business hours
 0 0 0 1 1 *			        Happy new year!
```

---

## Declarative scheduling

- Next to the programmatic execution of tasks, you can also declare them
  - Advantage: simple to configure and understandable, no need for programming
  - Disadvantage: not dynamic (you can not “calculate” the time to execute at runtime)
- Declarative scheduling needs to be enabled first
- Using the task namespace

```xml
<task:annotation-driven/>
```

- Using Java config

```java
@Configuration
*@EnableScheduling
public class ApplicationConfiguration {}
```

- You still need to configure a `TaskScheduler` or use Spring Boot

---

## Declarative scheduling

- You can then simply annotate business methods to be periodically executed
  - The annotated method must belong to a `bean` in the Spring context

```java
@Component
public class Service {
* @Scheduled(cron = "0/20 * 9-17 * * MON-FRI")
  public void scheduleOne() { ... }

* @Scheduled(fixedRate = 1000)
  public void scheduleTwo() { ... }
}
```

---

## Asynchronous execution

- Methods can be marked to run asynchronously
  - They will be executed on a separate background thread
      - i.e. using a `TaskExecutor`
- This can be useful if the executed method may take a long time and is not critical to the continuation of the current business flow
  - i.e. when you don’t need its result or confirmation to continue

---

## Asynchronous execution

- First, enable support for asynchronous execution
  - Alternatively, the `<task:annotation-driven/>` will enable both `@Scheduled` and `@Async`

```java
@Configuration
*@EnableAsync
public class ApplicationConfiguration {}
```

- Then, mark a method of a bean in the context for asynchronous execution
  - Any exceptions or return values can not be captured unless you return a `Future` instance (usually `AsyncResult`)

```java
@Component
public class Service {
* @Async
  public void scheduleOne() { ... }
}
```

---

## Exercise: Task runners

- Open exercise “scheduling”
  1. Run the unit tests
  2. You will see they fail... so your job is to fix them!
  3. You should not change any of the unit tests themselves
- Complete the `SchedulerService` so that all unit tests are successful
  1. Add a `TaskScheduler` to the application context
  2. Enable support for scheduling and async
  3. Make sure the `executeBookProcessingSchedule()` method runs periodically
  4. Add a CRON schedule for “every two seconds during business hours”
  5. Make sure the `createBookAsynchronously()` method runs in async mode
