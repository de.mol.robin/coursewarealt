package com.realdolmen.spring_ws.web.socket;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.realdolmen.spring_ws.domain.Animal;
import com.realdolmen.spring_ws.repository.AnimalRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Random;

@Component
public class AnimalWebSocketHandler extends TextWebSocketHandler {
    Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Direct use of Jackson, provided as a bean by Spring Boot.
     */
    @Autowired
    ObjectMapper jsonMapper;

    @Autowired
    AnimalRepository animalRepository;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        logger.info("Connection established with websocket");
        Random random = new Random();
        while(true) {
            List<Animal> animals = animalRepository.findAll();
            Animal randomAnimal = animals.get(random.nextInt(animals.size()));
            logger.info("Sending random animal '" + randomAnimal.getName() + "'");
            sendObject(session, randomAnimal);
            Thread.sleep(2000);
        }
    }

    private <T> void sendObject(WebSocketSession session, T t) throws IOException {
        StringWriter writer = new StringWriter();
        jsonMapper.writeValue(writer, t);
        session.sendMessage(new TextMessage(writer.getBuffer()));
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        logger.info("Received websocket message: " + message);
    }
}
