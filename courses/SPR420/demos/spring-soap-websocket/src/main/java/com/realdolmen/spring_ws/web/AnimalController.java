package com.realdolmen.spring_ws.web;

import com.realdolmen.spring_ws.repository.AnimalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class AnimalController {
    @Autowired
    AnimalRepository animalRepository;

    @RequestMapping("animals")
    public String animalList(Map<String, Object> model) {
        model.put("myAnimalList", animalRepository.findAll());
        return "animal/list";
    }
}
