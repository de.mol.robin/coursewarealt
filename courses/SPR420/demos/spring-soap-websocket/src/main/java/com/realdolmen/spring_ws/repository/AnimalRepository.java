package com.realdolmen.spring_ws.repository;


import com.realdolmen.spring_ws.domain.Animal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnimalRepository extends JpaRepository<Animal, Integer> {
    List<Animal> findByNameLikeOrderByName(String nameLike);
}
