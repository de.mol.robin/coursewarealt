package com.realdolmen.spring_ws.service.soap;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@Configuration
@EnableWs
public class WebServiceConfig {
    /**
     * Sets up a servlet for capturing SOAP POST requests and routing them to @Endpoint classes.
     */
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/api/soap/*");
    }

    /**
     * To support SOAP 1.2 (default is 1.1)
     */
    @Bean
    public SaajSoapMessageFactory messageFactory() {
        SaajSoapMessageFactory messageFactory = new SaajSoapMessageFactory();
        messageFactory.setSoapVersion(SoapVersion.SOAP_12);
        return messageFactory;
    }

    /**
     * Generates WSDL based on XSD and some other input.
     * Publishes WSDL at .../animals.wsdl
     * Note: assumes operations to be mapped to <...Request> and <...Response> by default {@link DefaultWsdl11Definition#setRequestSuffix(String)}
     */
    @Bean(name = "animals")
    public Wsdl11Definition defaultWsdl11Definition(XsdSchema schema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("AnimalsPortType");
        wsdl11Definition.setLocationUri("http://localhost:8080/api/soap");
        wsdl11Definition.setTargetNamespace("http://www.realdolmen.com/animals/wsdl");
        wsdl11Definition.setSchema(schema);
        wsdl11Definition.setCreateSoap12Binding(true);
        wsdl11Definition.setCreateSoap11Binding(false);
        return wsdl11Definition;
    }

    /**
     * Path to our custom written XSD file. Can also use {@link org.springframework.xml.xsd.XsdSchemaCollection}
     */
    @Bean
    public XsdSchema schema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/animals.xsd"));
    }
}
