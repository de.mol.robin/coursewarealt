package com.realdolmen.spring_ws.service.soap;

import com.realdolmen.spring_ws.domain.Animal;
import com.realdolmen.spring_ws.repository.AnimalRepository;
import com.realdolmen.spring_ws.service.soap.jaxb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.*;

import java.util.List;

/**
 * Note the conversion activity. Annoying but often necessary to isolate internal representation to external representation so that
 * the two can vary independently (i.e. versioning of WS, refactoring of internal domain structure w/o affecting SOAP interface, ...)
 */
@Endpoint
public class AnimalSoapService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public static final String NAMESPACE = "http://www.realdolmen.com/animals";

    @Autowired
    AnimalRepository animalRepository;

    @PayloadRoot(localPart = "SearchAnimalsRequest", namespace = NAMESPACE)
    public @ResponsePayload SearchAnimalsResponse search(@RequestPayload SearchAnimalsRequest request) {
        String searchFilter = "%" + request.getNameLikeFilter() + "%";
        logger.info("Searching all animals by name like '" + searchFilter + "'");

        List<Animal> animals = animalRepository.findByNameLikeOrderByName(searchFilter);
        SearchAnimalsResponse response = new SearchAnimalsResponse();
        for (Animal animal : animals) {
            SearchAnimalsResponse.Animal xmlAnimal = new SearchAnimalsResponse.Animal();
            xmlAnimal.setId(animal.getId());
            xmlAnimal.setName(animal.getName());
            xmlAnimal.setAge(animal.getAge());
            xmlAnimal.setType(animal.getType().name());
            response.getAnimal().add(xmlAnimal);
        }
        return response;
    }

    @PayloadRoot(localPart = "CreateAnimalRequest", namespace = NAMESPACE)
    public @ResponsePayload CreateAnimalResponse create(@RequestPayload CreateAnimalRequest request) {
        CreateAnimalResponse response = new CreateAnimalResponse();
        Animal domainAnimal = new Animal(
            request.getAnimal().getName(),
            request.getAnimal().getAge(),
            Animal.Type.valueOf(request.getAnimal().getType())
        );
        animalRepository.save(domainAnimal);
        logger.info("Creating new animal with id " + domainAnimal.getId());
        response.setId(domainAnimal.getId());
        return response;
    }

    @PayloadRoot(localPart = "RemoveAnimalRequest", namespace = NAMESPACE)
    public @ResponsePayload RemoveAnimalResponse remove(@RequestPayload RemoveAnimalRequest request) {
        logger.info("Removing animal by id " + request.getId());

        RemoveAnimalResponse response = new RemoveAnimalResponse();
        boolean exists = animalRepository.exists(request.getId());
        response.setRemoved(exists);
        if(exists) {
            animalRepository.delete(request.getId());
        }
        return response;
    }
}
