package com.realdolmen.spring_ws.domain;

import javax.persistence.*;

@Entity
public class Animal {
    public enum Type {
        TIGER,
        ELEPHANT,
        BEAR,
        PANTHER,
        SNAKE,
        ORANGUTAN
    }

    @Id
    @GeneratedValue
    private Integer id;

    private String name;
    private int age;

    @Enumerated(EnumType.STRING)
    private Type type;

    /**
     * Used by JPA.
     */
    protected Animal() {
    }

    public Animal(String name, int age, Type type) {
        this.name = name;
        this.age = age;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
