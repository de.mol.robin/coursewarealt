insert into animal(id, name, age, type) values(1000, 'Dumbo', 1, 'ELEPHANT');
insert into animal(id, name, age, type) values(2000, 'Shere-Khan', 29, 'TIGER');
insert into animal(id, name, age, type) values(3000, 'Balou', 37, 'BEAR');
insert into animal(id, name, age, type) values(4000, 'Bagheera', 32, 'PANTHER');
insert into animal(id, name, age, type) values(5000, 'King Louwie', 47, 'ORANGUTAN');
insert into animal(id, name, age, type) values(6000, 'Kaa', 28, 'SNAKE');