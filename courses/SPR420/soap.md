# SOAP Web Services

---

## Web Services definition

- A software system designed to support interoperable machine to machine interaction over a network (W3C)
  - Self-contained and self-describing using WSDL
  - Clients and servers (endpoints) communicate via XML messages that follow the SOAP standard
  - Programs become accessible over a network and are executed on a remote system hosting them

---

## XML Schema

- SOAP (Simple Object Access Protocol)
  - XML-based communication protocol
  - Message-wrapping format
  - Platform and language independent
- WSDL (Web Services Description Language)
  - An XML format used to describe web service interfaces and protocol bindings details in a platform independent fashion
  - Also used for configuration and for generating client and server code
- UDDI (Universal Description, Discovery and Integration)
  - A platform-independent, XML-based registry for businesses to list themselves on the internet
  - Each business can publish a service listing
  - Designed to be interrogated by SOAP messages and to provide access to WSDL documents

???trainer
- An UDDI business registration is composed by:
  - White Pages (address, contact and known identifiers)
  - Yellow Pages (industrial categorizations)
  - Green Pages (technical info about exposed services)
???t

---

## Web Services architecture

- Web Services are found using UDDI
- WSDL files are exchanged to specify the protocol bindings
- SOAP is used for message exchange

![](images/soap-architecture.png#no-border)

---

## Web Services styles

- RPC (Remote Procedure Calls)
  - Present a distributed function call interface
  - Tightly coupled to language-specific functions
- SOA (Service Oriented Architecture)
  - A message is the basic unit of communication
  - Focus on WSDL contract
- RESTful (REpresentational State Transfer)
  - Constrain the interface to a set of standard HTTP operations
  - Focus is on interacting with stateful resources

---

## JAX-WS

- JAX-WS (Java API for XML Web Services)
  - Provides a standard way for accessing and exposing RPC-style SOAP Web Services
- How does it work?
  - Stubs are generated based on the WSDL contract
  - Java programs invoke methods on those stubs
  - The stub invokes routines in the JAX-WS Runtime System
  - The JAX-WS Runtime System converts the remote method invocation into a SOAP message
  - The JAX-WS Runtime System transmits the message as an HTTP request
- Web Services can be implemented on the server side as a Servlet or EJB container

---

## Trade-offs

- Be aware of the trade-offs!
  - Web Services provide interoperability between different web applications running on different platforms
  - Represent a possible solution for remoting
  - Choose interoperability and extensibility over ease-of-use and performance
- Other remoting strategies can offer better performance, but lose interoperability instead...
  - Such as Spring's `HTTPInvoker`, which is for Java to Java communication

---

## Spring Web Services

- Is available as a separate Spring project
  - Product of the Spring community
  - Focused on creating document-driven Web Services
  - Facilitates contract-first SOAP service development
  - Based on Spring itself
      - You can use the Spring concepts such as dependency injection as an integral part of your Web Service
  - Makes the best practice an easy practice
      - Especially when finding alternative SOAP stacks lacking

---

## Spring Web Services features

- Powerful mappings
  - You can distribute incoming XML requests to any object, depending on message payload, SOAP Action header, or an XPath expression
- XML API support
  - DOM, SAX, and StAX, but also JDOM, dom4j, XOM, or even marshalling technologies
- Flexible XML Marshalling
  - Supports JAXB 1 and 2, Castor, XMLBeans, JiBX, and XStream
- Reuses your Spring expertise
  - Spring Web Services uses Spring application contexts for its configuration
  - The architecture of Spring Web Services resembles that of Spring MVC

---

## Spring Web Services features

- Supports WS-Security
  - WS-Security allows you to sign SOAP messages, encrypt and decrypt and authenticate against them
- Integrates with Spring Security
- Built by Maven
- Apache licence
  - You can confidently use Spring Web Services in your project

---

## Architecture of a Spring Web Services application

![](images/soap-app-architecture.png#no-border)

---

## Writing contract-first Web Services

- There are two main approaches to developing web services
  - Contract-first
      - The service contract (WSDL) is defined first, and code is then written to fulfill this contract
  - Contract-last
      - The service contract (WSDL) is generated from an existing bean
- Most developers prefer a contract-last approach... but this will lead to many future issues

---

## Problems with contract-last Web Services

- Object/XML impedance mismatch
  - Difficult to model an object graph in an hierarchical XML (XSD)
- Fragility
  - Not all SOAP stacks generate the same contract
- Performance
  - No guarantee of what is sent over the wire (deep, unwanted dependencies)
- Reusability and versioning
  - Contract-first lets you reuse the contract in different scenarios
  - A start from code means a new contract for every code change

---

## Focus on the contract!

- When writing contract-first web services
  - Think in terms of XML!
  - Java-language concepts are of lesser importance
  - It is the XML that is sent across the wire, and you should focus on that
- Example Web service created by a Human Resources department
  - Clients can send holiday request forms to this service to book a holiday

---

## Defining the messages

- A `Holiday`
  - Consists of a start and end date
  - ISO-8601 standard for dates saves parsing problems
  - The added namespace makes sure the elements can be used in other XML documents

```xml
<Holiday xmlns="http://com/realdolmen/spr420/ws/domain">
  <StartDate>20XX-07-01</StartDate>
  <EndDate>20XX-07-31</EndDate>
</Holiday>
```

---

## Defining the messages

- An `Employee`
  - Has an employee number, a first and last name
  - A different namespace might be needed if employees are used in other scenarios

```xml
<Employee xmlns="http://com/realdolmen/spr420/ws/domain">
  <Number>42</Number>
  <FirstName>Claire</FirstName>
  <LastName>Farron</LastName>
</Employee>
```

---

## Defining the messages

- A `HolidayRequest`
  - Consists of both the `Holiday` and `Employee`
  - The order is not important, only that the data is there!

```xml
<HolidayRequest xmlns="http://com/realdolmen/spr420/ws/domain">
	<Holiday>
		<StartDate>20XX-07-01</StartDate>
		<EndDate>20XX-07-31</EndDate>
	</Holiday>
	<Employee>
		<Number>42</Number>
		<FirstName>Claire</FirstName>
		<LastName>Farron</LastName>
	</Employee>
</HolidayRequest>
```

- A `HolidayResponse`

```xml
<HolidayResponse xmlns="http://com/realdolmen/spr420/ws/domain">
	<Result>Approved!</Result>
</HolidayResponse>
```

---

## The message contract

- The contract will let you agree on and validate the message exchange format
  - It makes sense to formalize this in a schema
  - Multiple schema solutions exist for XML
      - DTD, XML Schema, RelaxNG, Schematron
  - XML Schema is not the easiest, but widely supported
- XML Schema files can be generated from example XML files, but would still have to be polished up afterwards
  - They can be used as a starting point

---

## The message contract

- `HolidayService.xsd`

```xml
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:hr="http://com/realdolmen/spr420/ws/domain"
   elementFormDefault="qualified" targetNamespace="http://com/realdolmen/spr420/ws/domain">
  <xs:element name="HolidayRequest">
    <xs:complexType>
      <xs:all>
        <xs:element name="Holiday" type="hr:Holiday"/>
        <xs:element name="Employee" type="hr:Employee"/>
      </xs:all>
    </xs:complexType>
  </xs:element>
  <xs:element name="HolidayResponse">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Result" type="xs:string"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <!-- more -->
```

---

## The message contract

```xml
<!-- continued -->
<xs:complexType name="Holiday">
  <xs:sequence>
    <xs:element name="StartDate" type="xs:date"/>
    <xs:element name="EndDate" type="xs:date"/>
  </xs:sequence>
</xs:complexType>
<xs:complexType name="Employee">
  <xs:sequence>
    <xs:element name="Number" type="xs:integer"/>
    <xs:element name="FirstName" type="xs:string"/>
    <xs:element name="LastName" type="xs:string"/>
  </xs:sequence>
</xs:complexType>
</xs:schema>
```

- The Web Service contract is usually expressed with WSDL
  - With Spring Web Services, writing the WSDL by hand is not required
  - Using the XSD and some conventions, Spring Web Services can create the WSDL for you

---

## Creating the project

- Start from a simple Spring Boot web project
  - Add the XSD file to a folder such as `src/main/resources/schemas`
- We will use JAXB as OXM
  - It can generate Java classes from types in the XSD
  - You will need the JAXB plugin in the `pom.xml` file

---

## Creating the project

```xml
<plugin>
  <groupId>org.codehaus.mojo</groupId>
  <artifactId>jaxb2-maven-plugin</artifactId>
  <version>1.6</version>
  <executions>
    <execution>
      <goals>
        <goal>xjc</goal>
      </goals>
      <phase>generate-sources</phase>
    </execution>
  </executions>
  <configuration>
    <packageName>com.realdolmen.spr420.ws.domain</packageName>
    <schemaDirectory>${project.basedir}/src/main/resources/schemas</schemaDirectory>
  </configuration>
</plugin>
```

- Generated classes are placed in `target/generated-sources/jaxb/` directory
  - Make sure to add this folder in you IDE as source root

---

## Adding libraries

- You will need `spring-ws-core` and `wsdl4j` dependencies
  - From an existing Spring Boot web project, add the following

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-ws</artifactId>
</dependency>
<dependency>
  <groupId>wsdl4j</groupId>
  <artifactId>wsdl4j</artifactId>
</dependency>
```

---

## Configuring Spring Web Services

- Using XML configuration, add the `ws` namespace and the following element

```xml
<ws:annotation-driven />
```

- Using Java configuration, extend `WsConfigurerAdapter` and annotations

```java
*@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
  // more beans here
}
```

---

## Configuring Spring Web Services

- To expose Spring Web Service endpoints over HTTP, we need to configure a Servlet
  - This can be done with a dedicated Spring Web Services `MessageDispatcherServlet`
  - You can also integrate with an existing Spring MVC application
- The `MessageDispatcherServlet` configuration

```java
@Bean
public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
  MessageDispatcherServlet servlet = new MessageDispatcherServlet();
  servlet.setApplicationContext(applicationContext);
  servlet.setTransformWsdlLocations(true);
  return new ServletRegistrationBean(servlet, "/ws/*");
}
```

???trainer
- It is important to inject and set `ApplicationContext` to `MessageDispatcherServlet`. Without that, Spring WS will not detect Spring beans automatically
- By naming this bean `messageDispatcherServlet`, it does not replace Spring Boot’s default `DispatcherServlet` bean
???t

---

## Configuring Spring Web Services

- We also need to expose the WSDL file as the contract to back up our web service
  - The only contract you really need is the XSD
  - Spring can generate the WSDL for you based on the created XSD

```java
@Bean(name = "holiday")
public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema holidaySchema) {
  DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
  wsdl11Definition.setPortTypeName("HolidayPort");
  wsdl11Definition.setLocationUri("http://localhost:8080/ws");
  wsdl11Definition.setTargetNamespace("http://com/realdolmen/spr420/ws/domain");
  wsdl11Definition.setSchema(holidaySchema);
  return wsdl11Definition;
}

@Bean
public XsdSchema holidaySchema() {
  return new SimpleXsdSchema(new ClassPathResource("schemas/HolidayService.xsd"));
}
```

???trainer
Note that you need to specify the names of the servlet and beans. These names will determine the URL under which the web service and generated WSDL file will be available. In this case: `http://<host>:<port>/ws/holiday.wsdl`
???t

---

## Creating the Web Service endpoint

- A service endpoint is the actual web service that is called using SOAP
  - It will usually delegate to internal business services
  - This provides a more robust way of controlling versioning and interface changes
- Mark your implementation class with `@Endpoint`

```java
*@Endpoint
public class HolidayServiceEndpoint {
  @Autowired
  private HolidayService holidayService; // Internal business service

  // Operations come here
}
```

???trainer
Make sure this bean gets picked up by your Spring configuration. You will need to enable component scanning!
???t

---

## Defining the service

- The interface

```java
public interface HolidayService {
  public HolidayResponse requestHoliday(Holiday holiday, Employee employee);
}
```

- The implementation

```java
@Service
public class HolidayServiceImpl implements HolidayService {
  public String requestHoliday(Holiday holiday, Employee employee){
    // do something meaningful
    return "Succeeded!";
  }
}
```

???trainer
The `Holiday` and `Employee` arguments are objects generated by the JAXB Maven plugin. Remember you could use internal business domain objects instead.
???t

---

## Defining Web Service operations

- Endpoints handle incoming XML messages
- There are two kinds of endpoints
  - Message endpoints
      - Give access to the entire XML message, including SOAP
  - Payload endpoints
      - Only have access to the contents of the SOAP body
      - This is the most typical use

---

## Defining Web Service operations

- Mark the Java method to be bound with `@PayloadRoot`

```java
@Endpoint
public class HolidayServiceEndpoint {
  @Autowired
  private HolidayService holidayService;

  @PayloadRoot(localPart="HolidayRequest", namespace="http://com/realdolmen/spr420/ws/domain")
  public @ResponsePayload HolidayResponse requestHoliday(@RequestPayload HolidayRequest request) {
    String response = holidayService.requestHoliday(request.getHoliday(), request.getEmployee());
    return new HolidayResponse(response);
  }
}
```

???trainer
The above example captures the entire payload XML as a single parameter. `HolidayRequest` is a JAXB object.

It is possible to capture only a part of the payload XML. By adding a namespace declaration and XPath instructions, you can make selections from the request.

```java
@PayloadRoot(localPart = "HolidayRequest", namespace = "http://com/realdolmen/spr020/ws/domain")
@Namespace(prefix="h", uri= "http://com/realdolmen/spr020/ws/domain")
public @ResponsePayload HolidayResponse requestHoliday(@XPathParam("//h:Employee/h:firstName") String firstName){
  // ...
}
```

The `@ResponsePayload` marks the return value as SOAP response. The returned object should also be a JAXB object, that will be serialized as the complete payload (body) of the SOAP envelope.
???t

---

## Publishing the Web Service

- Deploy the war file on you web server and point a browser to the WSDL location
- With Spring Boot you can start from a `main` method

```java
@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
```

- The WSDL is ready for use by clients!

---

## Creating Web Service clients with WebServiceTemplate

- Spring Web Services supports web service clients using the `WebServiceTemplate`
  - This is similar to the other template classes of Spring core
      - `JdbcTemplate`, `JmsTemplate`, ...
- Using `WebServiceTemplate`, you can send and receive the XML messages as JAXB objects
- Any other XML marshalling framework supported by the `spring-oxm` module is also supported

???trainer
The Spring Web Services approach using a `WebServiceTemplate` is different to Java EE's approach. With Java EE you generate a Service Provider Interface from a tool like `wsimport` and work with a proxy to simulate a transparent RPC.
???t

---

## Creating Web Service clients with WebServiceTemplate

- Set up a `WebServiceTemplate` as a Spring bean
  - Note that this can also be done in XML configuration

```java
@Bean
public WebServiceTemplate template(Jaxb2Marshaller marshaller) {
  WebServiceTemplate template = new WebServiceTemplate();
  template.setMarshaller(marshaller);
  template.setUnmarshaller(marshaller);
  template.setDefaultUri("http://localhost:8080/ws/holiday");
  return template;
}

@Bean
public Jaxb2Marshaller marshaller() {
  Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
  marshaller.setPackagesToScan(new String[]{"com.realdolmen.spr420.ws.domain"});
  return marshaller;
}
```

---

## Creating Web Service clients with WebServiceTemplate

- You can now inject the template into you client service and call it
  - This is a typical scenario, to encapsulate the template

```java
@Service
public class HolidayServiceConsumer {
  @Autowired
  private WebServiceTemplate template;

  public void requestHoliday() {
    HolidayRequest request = new HolidayRequest();
    request.setEmployee(new Employee());
    request.setHoliday(new Holiday());
    HolidayResponse response = (HolidayResponse) template.marshalSendAndReceive(request);
    System.out.println(response.getResult());
  }
}
```

---

## Exercise: Spring Web Services
- Exercise one: Explore the starter application
  - Import the starter application into your IDE
  - Explore its contents:
      1. Notice the `service` package, containing the `HolidayService`
      2. Open the `HolidayService.xsd` file in `resources/schemas`
      3. Notice the `targetNamespace` declaration
      3. Open the `pom.xml` file and notice the Spring Boot dependencies
      4. Take a look at the `jaxb2-maven-plugin` configuration
  - Compile the code using Maven, which should succeed
      - It should generate an `xml` package containing the JAXB generated classes and successfully compile

---

## Exercise: Spring Web Services
- Exercise two: Configuring and creating Web Services
  - In this exercise, we will configure Spring Web Services and
  - Your tasks:
      1. Add annotations to `WebServiceConfig`
      2. Create a `messageDispatcherServlet` method
      3. Create a `holidaySchema` method
      4. Create a `defaultWsdl11Definition` method
      5. Open the `HolidayServiceEndpoint` and complete it with annotations, injections and call to the `HolidayService`
      6. Run the `Application` and browse to `http://localhost:8080/ws/holiday.wsdl`

---

## Exercise: Spring Web Services
- Exercise three: Calling the Web Service
  - Now that the service runs, we can call it from unit tests
  - You will have to:
      1. Configure the `TestConfig` class
      2. Create a `Jaxb2Marshaller` and a `WebServiceTemplate`
      3. Make sure the unit tests return all green!
