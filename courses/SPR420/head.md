# Spring 4 Advanced Topics

---

## Goal

> Learn more advanced Spring 4 topics that will help you build and enhance more complex enterprise Java applications

---

## Agenda

- Migrating from earlier versions of Spring
- Caching data
- Spring Security
- Spring Web Flow
- SOAP Web Services
- RESTful Web Services
- Spring Data REST
- WebSockets
- Task scheduling
- Messaging in Spring
- Sending email
