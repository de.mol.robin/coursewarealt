# Spring Boot with Groovy

---

## Groovy and Spring Boot CLI
- Groovy is a popular alternative JVM language
    - It is fully JVM and Java compatible
        - Very easy to learn when you already know Java (superset)
    - It works with any existing Java code
        - All your existing Java code still works
    - It has much reduced ceremony compared to Java
        - Optional semicolon, default public, convenient operators, dynamic typing, …
- Spring Boot CLI supports creating Spring applications using Groovy
    - The command-line interface allows you to run a Groovy script without any hassle
        - No bootstrapping logic (main)
        - No initial context configuration
        - No Maven / Gradle build configuration

---

## Installing the CLI
- Spring Boot CLI needs to be installed first
    - Download and extract it from the Spring webpages
        - http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#getting-started-installing-the-cli
    - Add the /bin folder to the `PATH` environment variable
    - Or using Windows’ environment configuration settings
    - Test it by running the `spring` command from a terminal

```sh
export SPRING_BOOT_CLI_HOME=<pathtospring boot cli>
export PATH=$SPRING_BOOT_CLI/bin:$PATH
```

```sh
$ spring --version
```

---

## Groovy domain classes
- In Groovy, beans are much easier to write
    - Public is the default
    - Properties have getters and setters by default
    - Semicolons are optional
- The previous `Contact` domain class can be written in Groovy as follows
    - Name it `Contact.groovy` in the root of your project (__not__ `src/main/java`)

```groovy
class Contact {
  long id
  String firstName
  String lastName
  String phoneNumber
  String emailAddress
}
```

---

## Auto-importing
- Groovy applications using Spring Boot CLI make use of auto-importing, as well as auto-configuration
    - You do not need to write any `import` statements for Spring dependencies
    - Groovy does not need imports for many Java packages
        - `java.lang`, `java.math`, `java.util`, `java.io`, `groovy.lang`, `groovy.util`, …
- Dependencies will be automatically downloaded and imported at runtime
    - Running the code the first time will fail (`NoClassDefFoundError`)
    - Spring Boot will automatically download missing dependencies and load the classes
    - The code is executed a second time automatically

---

## Grabbing
- Some dependencies can not be auto-imported
    - They are _opt in_: there is no explicit compile-time dependency to them
    - The auto-configuration mechanism guesses bean configuration based on them existing on the classpath
    - These are the libraries we had to explicitly add using Maven before as well
        - Examples: Thymeleaf, H2, …
- You can add these dependencies to your Spring Boot CLI application using the `@Grab` annotation
  - Simply add this to the top of any .groovy file that needs it

```java
@Grab("h2")
@Grab("thymeleaf-spring4")
```

---

## Groovy controllers
- The controller rewritten in Groovy looks like this
    - Name it `ContactController.groovy` in the root of the project

```groovy
@Grab("thymeleaf-spring4") // No imports!
@Controller
@RequestMapping("/")
class ContactController{
  @Autowired ContactRepository contactRepo
  @RequestMapping(method=RequestMethod.GET)
  String home(Map<String, Object> model) {
    List<Contact> contacts = contactRepo.findAll()
    model.putAll([contacts: contacts]) // Ultra cool map literals!
    "home" // Implicit return!
  }
  @RequestMapping(method=RequestMethod.POST)
  String submit(Contact contact) {
    contactRepo.save(contact)
    "redirect:/"
  }
}
```

---

## Groovy views and static resources
- Views and static resources can be reused as-is
    - They simply need to be placed directly in the root of the project

![](images/Groovy.png#no-border)

---

## Groovy persistence
- The repository can be rewritten in Groovy as follows
    - Name it `ContactRepository.groovy` in the root of the project

```groovy
@Grab("h2")
import java.sql.ResultSet // Some non-Spring imports still necessary :(
class ContactRepository {
  @Autowired JdbcTemplate jdbc
  List<Contact> findAll() {
    jdbc.query("select id, firstName, lastName, phoneNumber, emailAddress from contacts" +
    "order bylastName", (RowMapper<Contact>){ ResultSetrs, int rowNum ->
      new Contact(id: rs.getLong(1),firstName: rs.getString(2),lastName:
      rs.getString(3),phoneNumber: rs.getString(4),emailAddress: rs.getString(5))})
  }
  void save(Contact c) {
    jdbc.update("insert into contacts(firstName, lastName, phoneNumber, emailAddress)"
    + "values(?, ?, ?, ?)", c.firstName, c.lastName, c.phoneNumber, c.emailAddress)
  }
}
```

---

## Running from the CLI
- A Groovy based Spring Boot application can be run from the command-line simply by running the `spring run` command
  - All dependencies will be auto-imported or grabbed
  - Spring context is auto-configured
  - Application is launched with the auto-configured `ApplicationContext`
  - Embedded servlet engine is launched at http://localhost:8080

```sh
$ spring run **/*.groovy
```

- You can still create and run it as a regular stand-alone application as follows

```sh
$ spring jar myapp.jar **/*.groovy
$ java –jar myapp.jar
```

---

## Enabling the Actuator

- Enable the Actuator with Groovy by grabbing it:

```groovy
@Grab("spring-boot-starter-actuator")
```

- Auto-configuration will do the REST!

---

## Exercise: Spring Boot with Groovy
- Optional exercise: using Groovy and the Spring Boot CLI
    - The goal of this exercise is to convert the Spring Boot application to Groovy and run it on the command-line interface
    - The following tasks are needed for this:
        1. Convert your Java classes to Groovy
        2. "Grab" the Thymeleaf and H2 dependencies
        3. Move the groovy and resource files to the project’s root
        4. Remove the Maven `pom.xml`, `src/main/java` and `src/main/resources`
        5. Launch the program using the Spring Boot CLI
    - Some hints to convert classes from Java to Groovy:
        - Syntactically almost 100% Java compatible, optional semicolons, map literals: [key: value], no need to import typical Java packages (`java.util`, `java.io`, `java.lang`, `java.math`, …), public is default, getters and setters are implicit!
        - Yes it’s really cool!
