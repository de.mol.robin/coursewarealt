# Spring JPA

---

## JPA pros and cons
- JPA extends on JDBC by adding an extra layer of abstraction on top of it
    - This abstraction is called __Object Relational Mapping__ (ORM)
        - Other ORM frameworks are: Hibernate, iBATIS, …
- Advantages
    - Can simplify tedious work with JDBC
        - Lazy and eager loading
        - Cascading rules and transparent joins
    - Offers an object perspective to relational data
- Disadvantages
    - An extra layer of abstraction to learn
    - Less control over tweaking and performance

---

## Spring ORM
- Spring ORM supports multiple ORM frameworks
    - We will use the official standard Java Persistence API (JPA)
        - Starting from Spring 4, a JPA 2 runtime library is required
- The ORM module supports features such as
    - Transactions
    - Translation of framework exceptions to `DataAccessException`
    - Template classes
    - DAO Support classes

---

## Dependencies
- In order to use JPA, you will need to add some dependencies

```xml
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-orm</artifactId>
  <version>4.x.x.RELEASE</version> <!-- Select latest version -->
</dependency>
```

- You also need a JPA implementation and a JDBC driver

```xml
<dependency>
    <groupId>org.hibernate</groupId> <!-- Hibernate is a JPA implementation -->
    <artifactId>hibernate-entitymanager</artifactId>
    <version>x.x.x.Final</version> <!-- Select latest version -->
</dependency>
<dependency>
    <groupId>mysql</groupId> <!-- Replace with your own database driver! -->
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.xx</version>
</dependency>
```

---

## POJO repositories
- JPA provides the `EntityManager` class as the gateway to the persistence world
    - It is like a "JDBC connection on steroids"; most database interactions go through this interface
- JPA has much less boilerplate logic to deal with, so there is no need for a template class like `JdbcTemplate`
- Instead, Spring JPA will use a pure POJO approach to inject an `EntityManager` into a repository
    - This makes a Spring repository identical to an EJB or CDI bean
        - The only difference is some annotation metadata

---

## EntityManagerFactory
- All JPA applications need to have an `EntityManagerFactory` configured
    - Responsible for creating `EntityManager` instances injected into the repositories
- There are two types of `EntityManagerFactory`
    - Application managed (`RESOURCE_LOCAL`)
        - Used outside a Java EE application server
    - Container managed (`JTA`)
        - Used on a Java EE application server like WildFly or WebLogic
- There are two ways to configure each of them using Spring
    - Using JPA’s own `/META-INF/persistence.xml`
    - Using Spring’s Java configuration

---

## Spring based persistence configuration
- Spring 4 provides a no-xml configuration style for the `EntityManagerFactory`
    - This way you do not need a `persistence.xml` at all

```java
@Bean
public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(DataSource dataSource,
  JpaVendorAdapter jpaVendorAdapter) {
  LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
  emfb.setDataSource(dataSource);
  emfb.setJpaVendorAdapter(jpaVendorAdapter);
  emfb.setPackagesToScan("com.realdolmen");
  return emfb;
}
```

- Notes:
  - You can specify the `DataSource` bean to use (including one obtained using JNDI)
  - You can specify which packages to scan for JPA entities
  - You can specify JPA implementation configuration specifics using a `JpaVendorAdapter`

---

## Spring based persistence configuration
- The `JpaVendorAdapter` allows configuration of vendor properties
  - Several are available to choose from:
      - `HibernateJpaVendorAdapter`, `EclipseLinkJpaVendorAdapter`, `OpenJpaVendorAdapter`
  - The database dialect can be selected with the `database` property
      - `DEFAULT`, `DB2`, `DERBY`, `H2`, `HSQL`, `INFORMIX`, `MYSQL`, `ORACLE`, `POSTGRESQL`, `SQL_SERVER`, `SYBASE`

```java
@Bean
public JpaVendorAdapter jpaVendorAdapter() {
  HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
  adapter.setDatabase(Database.MYSQL);
  adapter.setGenerateDdl(true);
  adapter.setShowSql(true);
  return adapter;
}
```

---

## JPA based persistence configuration
- Spring can still be configured using a classic JPA `persistence.xml` configuration
- With the persistence configuration fixed in `/META-INF/persistence.xml`

```java
@Bean
public LocalEntityManagerFactoryBean entityManagerFactoryBean() {
  return new LocalEntityManagerFactoryBean();
}
```

- With the persistence configuration in an arbitrary location

```java
@Bean
public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
  LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
  emfb.setPersistenceXmlLocation("/my/strange/location/for/persistence.xml");
  return emfb;
}
```

???trainer
The `persistence.xml` not shown here is specific for JPA.
???t

---

##  Transaction manager
- JPA does not have transactions set to "auto commit" by default
    - This means that in order to modify any data, we need to setup a Spring transaction manager, and mark repositories as `@Transactional`

```java
@Bean
public JpaTransactionManager transactionManager (EntityManagerFactory entityManagerFactory) {
  return new JpaTransactionManager(entityManagerFactory);
}
```

- You also need to enable transactions with `@EnableTransactionManagement`

```java
@Configuration
@EnableTransactionManagement
public class PersistenceConfig { /* ... */ }
```

---

## JPA repositories
- When the `EntityManagerFactory` is configured, we can inject `EntityManager` instances into a repository using JPA’s `@PersistenceContext` annotation

```java
@Repository
@Transactional
public class JpaSpitterRepository implements SpitterRepository {
  @PersistenceContext private EntityManager em;

  public void addSpitter(Spitter spitter) {
    em.persist(spitter);
  }

  public Spitter getSpitterById(Long id) {
    return em.find(Spitter.class, id);
  }

  public void saveSpitter(Spitter spitter) {
    em.merge(spitter);
  }
}
```

---

## JPA repositories
- The `Spitter` class in the previous example must be a valid JPA entity

```java
@Entity
public class Spitter {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String username;

  private String fullname;

  private String email;

  private String password;

  // Add constructors, getters and setters as well
}
```

---

## Exercise: Spring JPA
- Open the "data-access-jpa" exercise provided by the teacher

![](images/data-access-jpa.png#no-border)

---

## Exercise: Spring JPA
- Exercise one: configuring an `EntityManagerFactory`
    - The goal of this exercise is to configure Spring to support the use of JPA
    - The following steps are needed for this:
        1. Configure a DBCP based MySQL `DataSource` for the production profile
        2. Configure an embedded H2 `DataSource` for the test profile
        3. Configure an `EntityManagerFactoryBean` for Hibernate, using only Java configuration
        4. Make sure the production profile uses the `MYSQL` database dialect
        5. Make sure the test profile uses the `H2` database dialect

---

## Exercise: Spring JPA
- Exercise two: implementing the `JpaAnimalRepository`
    - A domain class `Animal` already exists, but is not usable with JPA yet
    - The goal of this exercise is to fix all the unit tests in `AnimalRepositoryTest`
        - Do not modify the unit tests!
    - To do this, the following steps are required:
        1. Enhance the `Animal` class so it can be used as a JPA entity
        2. Implement `AnimalRepository` as a transactional repository that injects a persistence context
        3. Implement `AnimalRepository` contract using JPA

---

## Exercise: Spring JPA
- Exercise three: implementing the `JpaFoodRepository`
    - The domain classes for `Food`, `VegiFood`, `MeatyFood` and `DietEntry` have already been configured for JPA, take a look at the annotations used!
    - The goal of this exercise is to fix all the unit tests for `FoodRepositoryTest`
        - Do not modify any of the unit tests!
    - To do this, the following steps are needed:
        1. Implement `FoodRepository` as a new transactional repository that injects a persistence context
        2. Implement all the CRUD methods defined by the interface using JPA
    - Note: this exercise requires some more knowledge about JPA
