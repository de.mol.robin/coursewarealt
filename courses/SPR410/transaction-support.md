# Transaction Support

---

## Understanding transactions
- A transaction is a unit of work performed against a database
- Transactions generally represent changes in the database
    - They provide recovery from failure and keep databases consistent
    - They provide isolation between programs accessing the database concurrently
- Transactions are ACID
    - **A** tomic: all or nothing
    - **C** onsistent: leave the database in a valid state
    - **I** solated: determines the visibility to other transactions
    - **D** urable: once committed, the results are stored permanently

![](images/Transactions-are-ACID.png#no-border)

---

## Understanding transactions
- An example transaction: ATM money withdrawal
    - Verify account details
    - Accept withdrawal request
    - Check balance
    - Update balance
    - Dispense money
- If any of these steps fail, we should rollback any changes made
- If all steps succeed, we can commit the transaction

![](images/ATM-money-withrawal.png#no-border)

---

## Transaction support
- Transaction support is one of the main reasons to use Spring
    - Consistent across different APIs (JDBC, Hibernate, JPA, JTA, …)
    - Support for declarative transaction management
    - Simpler API for programmatic transaction management
    - Easy to integrate with Spring data access abstractions
- Spring resolves the disadvantages of local and global transactions
    - Local: resource-specific, invasive, cannot work across multiple resources
    - Global: only work with JTA and JNDI, cumbersome (`Exception`s), need an application server
- With Spring, you write code once, and you can benefit from different management strategies in different environments

---

## Spring framework transaction strategy
- The transaction strategy is defined by the `PlatformTransactionManager`

```java
public interface PlatformTransactionManager {
  TransactionStatus getTransaction(TransactionDefinition definition) throws TransactionException;
  void commit(TransactionStatus status) throws TransactionException;
  void rollback(TransactionStatus status) throws TransactionException;
}
```

- As an interface, it can be easily mocked or stubbed
    - It is not tied to a lookup strategy such as JNDI
    - Transactional code can be tested much more easily than if using JTA directly
- Spring's implementations are defined like any other bean
- `TransactionException` is unchecked, so you are not forced to catch it


---

## Choosing a platform

- You need to define a `PlatformTransactionManager` implementation
    - This implementation requires knowledge of the environment in which it will work
- Implementations
    - JDBC: `DataSourceTransactionManager`
    - JTA: `JtaTransactionManager`
    - Hibernate: `HibernateTransactionManager`
    - JPA: `JpaTransactionManager`
- Whatever you choose, configuration is similar
    - In all cases, application code does not need to change

---

## Defining transaction managers
- Add the bean to your configuration depending on the chosen persistence
- JDBC

```java
@Bean
public PlatformTransactionManager transactionManager(DataSource dataSource) {
  return new DataSourceTransactionManager(dataSource);
}
```

- JTA
    - This bean will look the same regardless the chosen technology, due to the fact that JTA transactions are global transactions

```java
@Bean
public PlatformTransactionManager transactionManager() {
  return new JtaTransactionManager();
}
```

---

## Defining transaction managers
- Hibernate

```java
@Bean
public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {
  return new HibernateTransactionManager(sessionFactory);
}
```

- JPA

```java
@Bean
public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
  return new JpaTransactionManager(entityManagerFactory);
}
```

---

## Declarative transaction management
- This is used by most Spring users
    - This option has the least impact on application code and is the most consistent with the ideals of a non-invasive lightweight container
- Declarative transactions are made possible with Spring AOP
- You can even declaratively specify for which `Exception`s to rollback
    - This is part of the configuration
    - The advantage is that business objects do not depend on the transaction infrastructure

---

## Declarative transaction management

![](images/Declarative-transact-mana.png#no-border)

---

## Using @Transactional
- Consider the following interface and implementation
    - With the `@Transactional` annotation, this bean can be made transactional

```java
public interface FooService {
  Foo getFoo(String fooName);
  void insertFoo(Foo foo);
  void updateFoo(Foo foo);
}
```

```java
@Service
@Transactional
public class DefaultFooService implements FooService {
  public Foo getFoo(String fooName) {
    throw new UnsupportedOperationException();
  }
  public void insertFoo(Foo foo) {
    throw new UnsupportedOperationException();
  }
  public void updateFoo(Foo foo) {
    throw new UnsupportedOperationException();
  }
}
```

---

## Using @Transactional
- You still need to enable transactions in JavaConfig, or they won't happen

```java
@Configuration
@EnableTransactionManagement
public class PersistenceConfig{
  @Bean
  public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory){
    return new JpaTransactionManager(entityManagerFactory);
  }
  // ...
}
```

- `@Transactional` can be placed
    - before an interface definition
    - on a method on an interface
    - on a class definition (preferred)
    - on a public method on a class

---

## Using @Transactional

- The most derived location takes precedence when evaluating the transactional settings for a method

```java
@Service
@Transactional(readOnly = true)
public class DefaultFooService implements FooService {

  public Foo getFoo(String fooName) {
    // do something
  }

  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public void updateFoo(Foo foo) {
    // do something
  }
}
```

---

## @Transactional settings
- Defaults
    - Propagation: `PROPAGATION_REQUIRED`
    - Isolation: `ISOLATION_DEFAULT`
    - Transaction is read/write
    - Default timeout of the underlying transaction system is used
    - `RuntimeException`s trigger rollback, checked `Exception`s do not
- All these defaults can be changed with the properties of `@Transactional`

---

## @Transactional properties

| Property  | Description  |
| :--  | :--  |
| `value`  | Optional qualifier specifying the transaction manager to be used  |
| `propagation`  | Optional propagation setting  |
| `isolation`  | Optional isolation level  |
| `readOnly`  | Read/write vs. read-only transaction  |
| `timeout`  | Transaction timeout  |
| `rollbackFor`  | Optional array of exception classes that must cause rollback  |
| `rollbackForClassName`  | Optional array of names of exception classes that must cause rollback  |
| `noRollbackFor`  | Optional array of exception classes that must not cause rollback  |
| `noRollbackForClassName`  | Optional array of names of exception classes that must not cause rollback  |

---

## Transaction propagation

| Propagation  | Description  |
| :-- | :-- |
| `PROPAGATION_MANDATORY`  | Method must run within transaction. If no transaction in progress, throw exception.  |
| `PROPAGATION_NESTED`  | Method must run in nested transaction. Not all resource manager support this!  |
| `PROPAGATION_NEVER`  | Method should never run in a transaction. If transaction in progress, throw exception.  |
| `PROPAGATION_NOT_SUPPORTED`  | Method should not run in a transaction. If a transaction in progress, transaction is suspended.  |
| `PROPAGATION_REQUIRED`  | Method must run in a transaction. If transaction in progress, run within that transaction; otherwise start new transaction (default).  |
| `PROPAGATION_REQUIRES_NEW`  | Method must run in its own transaction. If transaction in progress, it will be suspended for the duration of the method.  |
| `PROPAGATION_SUPPORTS`  | Method does not require a transactional context but can run in transaction if already in progress.  |

---

## Transaction isolation

| Isolation Level  | Description  |
| :-- | :-- |
| `ISOLATION_DEFAULT`  | Use default isolation level of the underlying database.  |
| `ISOLATION_READ_UNCOMMITTED`  | Allows to read changes that have not yet been committed. May result in dirty reads, non-repeatable reads and phantom reads. |
| `ISOLATION_READ_COMMITTED`  | Allows to read changes that have been committed. May result in non-repeatable reads and phantom reads.  |
| `ISOLATION_REPEATABLE_READ`  | Multiple reads of the same fields will yield the same results unless changed by the transaction itself. Phantom reads might still occur.  |
| `ISOLATION_SERIALIZABLE`  | Fully ACID-compliant isolation level. Slowest of all isolation levels because it is typically doing full table locks on tables used in the transaction.  |

---

## Programmatic transaction management
- The Spring Framework provides two means of programmatic transaction management
    - Using the `TransactionTemplate` (recommended)
    - Using a `PlatformTransactionManager` implementation directly
- The `TransactionTemplate` works in the same way as other templates
    - It uses a callback approach
    - It will couple your code into Spring's transaction infrastructure

---

## Programmatic transaction management
- Using a `TransactionTemplate` will be similar to this

```java
@Service
public class SimpleService implements Service {
  private final TransactionTemplate transactionTemplate;

  @Autowired
  public SimpleService(PlatformTransactionManager transactionManager) {
    this.transactionTemplate = new TransactionTemplate(transactionManager);
  }

  public Object someServiceMethod() {
    // using a TransactionCallback as functionalinterface
    return transactionTemplate.execute(ts -> {
      Object result = null;
      // do stuff in a transaction
      return result;
    });
  }
}
```

---

## Programmatic transaction management
- From within the callback, you can rollback the transaction

```java
return transactionTemplate.execute(ts -> {
  try {
    // do stuff in a transaction
  } catch(SomeBusinessException e) {
    ts.setRollbackOnly();
  }
});
```

---

## Exercise: Transaction Support
- Open exercise "transactions" provided by the teacher
    - In this exercise we will program a `Bank` that performs a number of transactions on `Account`s
    - Unfortunately there may be some `Bug`s into the money transfer system
    - We need to make sure that even when there are bugs, the `Account` balances are always correct

![](images/exercise-transactions.png#no-border)

---

## Exercise: Transaction Support
- Exercise one: configuring a `PlatformTransactionManager`
    - The goal of this exercise is to configure transaction management
    - The following steps are required for this:
        1. Add a `PlatformTransactionManager` bean for JPA
        2. Make sure transaction management is enabled in the Spring context
    - Do you think we are safe now?
        - Continue to exercise two to check!

---

## Exercise: Transaction Support
- Exercise two: making the `Bank` transactional
    1. Explore and run the Main class
        - It will execute some financial transactions for our Bank
    2. Check if the Bank seems to work correctly
        - Do the account balances printed on screen add up?
    3. Now let’s do some work on the Bank system, by calling the "maintenance()" method
        - Let’s hope we did not break anything!
    4. Check if the Bank still works properly
        - Do the account balances printed on screen still add up?
    5. Make the Bank resilient to errors by making it transactional
    6. Check again if the numbers are correct!
