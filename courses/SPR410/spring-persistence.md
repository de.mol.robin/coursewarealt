# Spring Persistence
---

## Persistence overview

- Persistence is traditionally done using a Relational Database Management System (RDBMS)
- Spring has built-in support for the typical ways to interface with a RDBMS
    - Using low-level __JDBC__ access
    - Using an ORM framework such as __JPA__
- Alternative persistence mechanisms are also supported by Spring
    - These are known as "NoSQL" or "schema-less" persistence solutions
        - MongoDB, Redis, Neo4j, ...

---

## Repositories

- Repositories or Data Access Objects (DAO) are specialized objects that deal exclusively with persistence related logic
    - They are used to insulate the rest of the application from persistence layer details
- It is common practice to decouple repository logic from the rest of the application using interfaces

![](images/Repository-interface.png#no-border)

---

## Repositories

- Decoupling persistence logic through interfaces offers some interesting advantages
    - The application layers become more __loosely-coupled__
        - Reducing interdependencies, and making the code more reusable
    - It makes the code __easier to unit test__
        - You can create mocks to "swap out" the real repository with a "fake"
    - The repository interfaces can be made __technology agnostic__
        - Makes it possible to switch strategies later (for example: switch from JDBC to JPA)
- Spring highly recommends designing to interfaces this way
    - This is by no means enforced though: you can still decide to take a different approach
        - The architect (you!) is still in control of the architecture, not Spring

---

## Repositories

- Repository interfaces typically have "CRUD"-like methods
    - Create, Read, Update, Delete

```java
public interface KnightRepository {
  void create(Knight knight);
  Knight findById(int id);
  List<Knight> findByQuestName(String questName);
  List<Knight> findAll();
  void update(Knight knight);
  void remove(Knight knight);
}
```

- You can create a repository bean using the `@Repository` annotation

```java
@Repository
public class JdbcKnightRepository implements KnightRepository {
  // Implement methods here
}
```

---

## DataAccessException

- Dealing with exceptions in the persistence layer can be challenging
    - There is often no reasonable way to recover from them
        - Unable to connect to database?
        - Invalid query syntax?
        - Constraint violation?
    - They are handled differently by different frameworks
        - JDBC throws a single checked `SQLException`
        - JPA throws an entire range of unchecked `PersistenceException` subclasses
    - They should and should not be handled by the repository layer
        - Not doing so would defeat the purpose of "isolating" the persistence layer
        - But often the services need to at least know that there is a problem

---

## DataAccessException

- Spring translates all persistence layer exception types into its own exception hierarchy
- This hierarchy solves the previous challenges
    - They are __unchecked__
        - Requiring only catch blocks in case you know what to do with them
    - They are framework __agnostic__
        - Not leaking any framework specific exceptions, thus keeping the persistence layer insulated
    - They are __semantically rich__
        - Allowing you to differentiate between different types of exceptions (catch only what you want to fish for)

---

## DataAccessException

- Spring’s exception hierarchy roots from `DataAccessException` and has many subclasses

|            |  |
| ----------  | :---------- |  
| `BadSqlGrammarException`            | `PessimisticLockingFailureException`   |
| **`DataIntegrityViolationException`**    | `QueryTimeoutException`    |
| `DataRetrievalFailureException`     | `RecoverableDataAccessException`    |
| `DataSourceLookupApiUsageException`  | `SQLWarningException`    |
| **`DeadlockLoserDataAccessException`**   | `SqlXmlFeatureNotImplementedException`  |
| **`DuplicateKeyException`**     | **`TransientDataAccessException`**    |
| `EmptyResultDataAccessException`     | `TransientDataAccessResourceException`  |
| **`InvalidDataAccessApiUsageException`**   | `TypeMismatchDataAccessException`    |
| **`OptimisticLockingFailureException`**    | `UncategorizedDataAccessException`    |
| `PermissionDeniedDataAccessException`  | `UncategorizedSQLException`    |

---

## DataAccessException
- Example without `DataAccessException`

```java
public interface KnightRepository {
  void create(Knight knight) throws SQLException; // Persistence layer not insulated!
}
```

```java
try { repository.create(knight); } catch(SQLException e) {
  // Must catch! Leaking persistence layer details (JDBC) to the service layer
}
```

- Example with `DataAccessException`

```java
public interface KnightRepository {
  void create(Knight knight); // Insulated, yet still able to catch when needed
}
```

```java
try { repository.create(knight); } catch(OptimisticLockingFailureException e) {
  // Handle specific exception, in case you need to!
}
```

---

## Template classes
- Spring integrates with most persistence technologies using "template" classes, providing many convenient features
    - Reducing boilerplate code
    - Simplifying typical usage scenarios to "one liners"
    - Taking care of exception translation to `DataAccessException`
    - Still allowing access to the full power of the underlying framework using callbacks
- Various template classes exist, each for integration with a specific framework
    - `JdbcTemplate`
    - `JpaTemplate`
    - `HibernateTemplate`

---

## Template classes
- Spring’s templates are based on the "template method" design pattern
    - This pattern allows a bigger problem to be split in a sequence of smaller sub-problems
    - Each smaller problem can optionally be customized (overridden) using a callback

![](images/Template-classes.png#no-border)

- Note:
    - This is frequently implemented using polymorphism or anonymous inner classes, but since Java 8 you can conveniently use lambdas

---

## DataSource
- Every application that connects to a RDBMS needs a `DataSource`
    - `DataSource` is an abstraction around the JDBC connection properties needed to connect to the database
        - Required: URI, username, password, driver class
        - Optional: connection pooling parameters, …
- Through the `DataSource`, applications can obtain database connections

```java
public interface DataSource { // Simplified for conceptual idea
  Connection getConnection() throws SQLException;
}
```

- The bottom line is: any Java persistence technology that connects to a RDBMS will need to have a `DataSource`

---

## DataSource
- In a Spring application, the `DataSource` is configured as a bean in the `ApplicationContext`
    - All we need to do is make sure an implementation of `DataSource` is in the context
- There are several ways you can obtain a `DataSource`
    - Using JDBC’s `DriverManager`
    - Using a connection pooled configuration
    - Using a JNDI lookup
    - Using an embedded `DataSource`

---

## DriverManagerDataSource
- Simple applications can create a `DataSource` by using JDBC’s `DriverManager` class underneath
  - Advantage: extremely simple
  - Disadvantage: not considered production grade (no concurrency support)
      - Still useful in unit tests for example

```java
@Bean
public DataSource dataSource() {
  DriverManagerDataSource ds = new DriverManagerDataSource();
  ds.setDriverClassName("org.h2.Driver");
  ds.setUrl("jdbc:h2:tcp://localhost/spitter");
  ds.setUsername("sa");
  ds.setPassword("");
  return ds;
}
```

---

## Pooled DataSource
- Almost all Spring applications benefit from a connection pooled `DataSource`
    - A separate Connection pooling library is required for this: C3P0, Commons DBCP, …
    - Advantage: almost as simple to configure as a `DriverManagerDataSource`, but without the disadvantages
    - Disadvantage: not externalized, requires rebuilding upon changes

```java
@Bean
public BasicDataSource dataSource() {
  BasicDataSource ds = new BasicDataSource(); // From Commons DBCP
  ds.setDriverClassName("org.h2.Driver");
  ds.setUrl("jdbc:h2:tcp://localhost/spitter");
  ds.setUsername("sa");
  ds.setPassword("");
  ds.setInitialSize(5);
  ds.setMaxActive(10);
  return ds;
}
```

---

## JNDI DataSource
- When a Java EE application server, database connection settings are usually managed by the server
    - The `DataSource` can be obtained by performing a JNDI lookup
    - Advantage: managed by the server, and can be changed without recompiling
    - Disadvantage: a Java EE application server is required for this

```xml
<jee:jndi-lookup id="dataSource" jndi-name="java:comp/jdbc/SpitterDS" />
```

```java
@Bean
public JndiObjectFactoryBean dataSource() {
  JndiObjectFactoryBean jndiObjectFB = new JndiObjectFactoryBean();
  jndiObjectFB.setJndiName("java:comp/jdbc/SpitterDS");
  jndiObjectFB.setProxyInterface(javax.sql.DataSource.class);
  return jndiObjectFB;
}
```

---

## Embedded DataSource
- Some simple applications ship with their own embedded database
    - A popular example is H2, which can run in "in-memory" or "file-based" mode

```java
@Bean
public DataSource dataSource() {
  return new EmbeddedDatabaseBuilder()
    .setType(EmbeddedDatabaseType.H2)
    .addScript("classpath:schema.sql")  // Executed when initialized
    .addScript("classpath:test-data.sql")
    .build();
}
```

- These are often used to run unit tests
  - Extremely fast
  - Simple to reinitialize upon each test run

---

## Using profiles
- It would be interesting to conditionally select the `DataSource` to use depending on the environment
    - This is where Spring’s profiles come in handy

```java
@Profile("development")
@Bean
public DataSource embeddedDataSource() {
  return new EmbeddedDatabaseBuilder()
    .setType(EmbeddedDatabaseType.H2).addScript("classpath:test-data.sql").build();
}

@Profile("production")
@Bean
public DataSource dataSource() {
  JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
  jndiObjectFactoryBean.setJndiName("jdbc/SpittrDS");
  jndiObjectFactoryBean.setResourceRef(true);
  jndiObjectFactoryBean.setProxyInterface(javax.sql.DataSource.class);
  return (DataSource) jndiObjectFactoryBean.getObject();
}
```
