# Wiring Beans

---

## Wiring beans
- Any application is made up of several objects that must work together to meet some business goal
    - These objects must be aware of one another and communicate with one another to get their jobs done
- This requires coupling
    - Objects need to create associations via constructor or lookup
    - This leads to complicated code and makes objects highly coupled
    - Highly coupled objects are hard to reuse and hard to test
- In Spring, the container gives an object the references it needs
    - The creation of associations between objects is called _dependency injection_ and is commonly referred to as _wiring_
    - Dependency injection is the most elemental thing Spring does

---

## Spring configuration options
- While Spring does dependency injection, developers need to tell Spring which beans to create and how to wire them
    - This requires configuration
- Spring configuration has several options
    - Explicit configuration in XML
    - Explicit configuration in Java
    - Implicit bean discovery and automatic wiring
- There is some overlap in what each configuration technique offers
    - Choice is simply a matter of personal taste, mix-and-match is possible
    - Recommendation: use automatic configuration as much as possible, then JavaConfig, fall back to XML when there is no JavaConfig option available
- During this course, we will focus on automatic and JavaConfig

---

## Automatically wiring beans
- Spring's automatic configuration focuses on ease of use
  - Component scanning: automatically discover beans in the application context
  - Autowiring: automatically satisfy bean dependencies

<img src="images/Cassette.png#no-border" style="float:right; padding: 10px; height: 10em">

- This helps keep configuration to a minimum
- Example: `Walkman` and `Cassette`
  - Using an interface, you keep the coupling to a minimum
  - It still requires an implementation

```java
public interface Cassette {
  void play();
}
```

---

## Automatically wiring beans
- Implementing the interface

```java
@Component
public class AwesomeMixVolume1 implements Cassette {
  private String title = "Awesome Mix Volume 1";
  private String artist = "Various Artists";
  public void play() {
    System.out.println("Playing " + title + " by " + artist);
  }
}
```

- The `@Component` identifies the class as a Spring bean
- Enabling auto-discovery of beans requires some configuration

```java
@Configuration
@ComponentScan
public class WalkmanConfig {
}
```

---

## Automatically wiring beans

- `@ComponentScan` annotation enables component scanning
    - By default, `@ComponentScan` scans the same package as the configuration class, including sub-packages
    - It will find the `Cassette` implementation and create a bean for it in Spring
- To test the configuration, we can write a simple JUnit test

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=WalkmanConfig.class)
public class WalkmanTest{

  @Autowired
  private Cassette cassette;

  @Test
  public void cassetteShouldNotBeNull() {
    assertNotNull(cassette);
  }
}
```

---

## Automatically wiring beans
- Explaining the test
    - The `SpringJUnit4ClassRunner` creates a Spring `ApplicationContext` as the test starts
    - `@ContextConfiguration` informs the loader which configuration to load
    - Because of the `@ComponentScan`, the `Cassette` bean will be created
    - To prove it, the bean gets `@Autowire`-d into the test
    - With a simple test method, we assert the cassette property is not null
- Any classes with `@Component` will be created as beans
    - This scales well from a few beans to many!

---

## Component scanning
- Beans get an `id`
    - The default `id` is derived from the name of the class, starting with a lowercase
- If you want to give the bean another `id`, pass it to the `@Component`

```java
@Component("guardiansOfTheGalaxySoundtrack")
public class AwesomeMixVolume1 implements Cassette {
  // ...
}
```

- You can also use the `@Named` annotation from CDI
    - In most common cases, these annotations are interchangeable


```java
@Named("guardiansOfTheGalaxySoundtrack")
public class AwesomeMixVolume1 implements Cassette {
  // ...
}
```

---

## Component scanning
- Spring has several specialization annotations to indicate components
- They are called stereotypes
    - `@Component`: indicates a bean for component scanning
    - `@Controller`: indicates a bean that serves as controller in Spring MVC
    - `@Repository`: indicates a bean is used for storage, retrieval and search
    - `@Service`: indicates a standalone, stateless bean that offers operations
- These annotations are all similar, but differentiating can be useful
    - They demarcate application layers
    - You can write aspects based on the presence of these annotations
    - `@Repository` beans get `Exception` translation (more on that later)

---

## Component scanning
- You can set the base package in the `@ComponentScan`
    - This enables you to keep all the configuration in a package of its own
    - `basepackages` is plural, so you can add multiple strings surrounded with braces

```java
@Configuration
@ComponentScan("musicplayer") // or @ComponentScan(basePackages={"musicplayer"})
public class WalkmanConfig{}
```

- Setting package names as strings is not very type-safe
    - You can also specify the packages to scan by using classes in the packages
    - Whatever packages those classes are in, will be used as base package
    - It is recommended to consider empty marker interfaces in the packages to be scanned

```java
@Configuration
@ComponentScan(basePackageClasses={Walkman.class, MovieBox.class})
public class WalkmanConfig{}
```

---

## Autowiring with annotations
- Let Spring automatically satisfy a bean's dependencies by finding other beans in the application context that match
    - Use the `@Autowired` annotation

<img src="images/Walkman.png#no-border" style="float:right; padding: 10px; height: 10em">

```java
@Component
public class Walkman implements MediaPlayer {
  private Cassette cassette;

  @Autowired
  public Walkman(Cassette cassette) {
    this.cassette = cassette;
  }

  public void play() {
    cassette.play();
  }
}
```


---

## Autowiring with annotations
- `@Autowired` can be put on constructors and setters, or any other method
  - Spring will attempt to satisfy the dependency expressed in the method's parameters
  - Assuming that one and only one bean matches, that bean will be wired in

```java
@Autowired
public void setCassette(Cassette cassette) {
  this.cassette = cassette;
}
```

```java
@Autowired
public void insertCassette(Cassette cassette) {
  this.cassette = cassette;
}
```

- If there are no matching beans, Spring will throw an exception as the application context is created

---

## Autowiring with annotations
- You can set the required attribute to false
    - Spring will then perform autowiring, but if there are no matching beans, it will leave the bean unwired
    - Gotcha! Unwired properties can easily lead to `NullPointerException`s…

```java
@Autowired(required=false)
public Walkman(Cassette cassette) {
  this.cassette = cassette;
}
```

- If there are multiple matching beans, Spring will also throw an exception indicating ambiguity

---

## Autowiring with annotations
- You can replace `@Autowired` with `@Inject` from CDI
    - In most common cases, these annotations are interchangeable

```java
@Named
public class Walkman {
  //...
  @Inject
  public Walkman(Cassette cassette) {
    this.cassette = cassette;
  }
  //...
}
```

---

## Autowiring with annotations
- A small unit test to check the automatic configuration

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=WalkmanConfig.class)
public class WalkmanTest {
  @Rule // see http://stefanbirkner.github.io/system-rules/index.html
  public final StandardOutputStreamLog log = new StandardOutputStreamLog();

  @Autowired
  private MediaPlayer player;

  @Autowired
  private Cassette cassette;

  //...

  @Test
  public void play() {
    player.play();
    assertEquals("Playing Awesome Mix Volume 1 by Various Artists\n", log.getLog());
  }
}
```

---

## Wiring beans with Java
- There are times when you need to configure Spring explicitly
    - E.g. wiring components from third party libraries
- JavaConfig is the preferred way for explicit configuration
    - It is type-safe and refactor-friendly
    - But, JavaConfig is not just any Java code, it is configuration code
    - Avoid putting any business logic in Java configurations
    - Avoid doing any Java configuration in your business logic

---

## Wiring beans with Java
- Use `@Configuration` to create a JavaConfig class
  - This annotation identifies it as a class containing bean details to be created in Spring

```java
@Configuration
public class WalkmanConfig {
}
```

- We can replace the `@ComponentScan` with explicit configuration
    - There is no reason not to combine them, but serves as an example
    - The `@Bean` annotation tells Spring this method returns a bean that should be created and added to the application context
    - The name of the bean will be the name of the method

```java
@Bean
public Cassette awesomeMix() {
  return new AwesomeMixVolume1();
}
```

---

## Wiring beans with Java
- You can change the name of the bean with the name attribute

<img src="images/Wiring-beans.png#no-border" style="float:right; padding-left: 10px">

```java
@Bean(name="guardiansOfTheGalaxySoundtrack")
public Cassette awesomeMix() {
  return new AwesomeMixVolume1();
}
```

- Since the configuration is in Java, you could do whatever you want to create a bean, as you have the whole Java power behind it

---

## Wiring beans with Java
- The easiest way to wire beans together, is by using the referenced bean method

```java
@Bean
public Walkman walkman() {
  return new Walkman(awesomeMix());
}
```

- While it looks like the `awesomeMix()` method will be called, this is not true
    - Because of `@Bean`, Spring will intercept the calls to the method, and return the bean produced by the method
    - By default, all Spring beans are _singletons_

---

## Wiring beans with Java
- Another way to refer to a bean, is by using a parameter, with autowiring
  - This does not depend on the `Cassette` being configured in the same configuration class
  - Spring can also wire in the `Cassette` from XML or component scanning

```java
@Bean
public Walkman walkman(Cassette cassette) {
  return new Walkman(cassette);
}
```

- Nothing stops you from using setters, as any Java code is allowed

```java
@Bean
public Walkman walkman(Cassette cassette) {
  Walkman walkman = new Walkman();
  walkman.setCassette(cassette);
  return walkman;
}
```

---

## Wiring with XML
- XML is not the only Spring configuration option
    - XML should not be your first choice, with JavaConfig and automatic configuration available
- XML was the primary way of expressing configuration
    - Lots of configuration files were already written for Spring
    - It is still important to understand how to use XML in existing applications

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
                http://www.springframework.org/schema/beans/spring-beans.xsd">  
  <!--configuration details go here -->
</beans>
```

---

## Wiring with XML
- Declare a bean with the `<bean>` element

```xml
<bean class="musicplayer.AwesomeMixVolume1" />
```

- It is a good `id`(ea) to give your bean a name for referring to it later
    - The default name of the bean would be `musicplayer.AwesomeMixVolume1#0`

```xml
<bean id="awesomeMix" class="musicplayer.AwesomeMixVolume1" />
```

- Spring will create the bean using its default constructor
    - The XML is a more passive configuration
    - XML is less powerful, as in JavaConfig you can do almost anything to arrive at a bean instance
    - The XML also does not have compile-time verification

---

## Wiring with XML
- Constructor injection

```xml
<bean id="walkman" class="musicplayer.Walkman">
  <constructor-arg ref="awesomeMix" />
</bean>
```

- A shorter notation is provided from Spring 3 onwards

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:c="http://www.springframework.org/schema/c"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
                http://www.springframework.org/schema/beans/spring-beans.xsd">
  <bean id="awesomeMix" class="musicplayer.AwesomeMixVolume1" />
  <bean id="walkman" class="musicplayer.Walkman" c:cassette-ref="awesomeMix"/>
  <!--bean id="walkman" class="musicplayer.Walkman" c:_0-ref="awesomeMix" -->
  <!--bean id="walkman" class="musicplayer.Walkman" c:_-ref="awesomeMix"-->
</beans>
```

---

## Wiring with XML
- Injecting literal values

```java
public class BlankCassette implements Cassette {   
  private String title;
  private String artist;

  public BlankCassette(String title, String artist) {
    this.title = title;
    this.artist = artist;
  }

  public void play() {
    System.out.println("Playing " + title + " by " + artist);
  }
}
```

```xml
<bean id="awesomeMix" class="musicplayer.BlankCassette">
  <constructor-arg value="Awesome Mix Volume 1" />
  <constructor-arg value="Various Artists" />
</bean>
```

---

## Wiring with XML
- This can also be shortened out with the `c`-namespace attributes

```xml
<bean id="awesomeMix" class="musicplayer.BlankCassette" c:_title="Awesome Mix Volume 1"
  c:_artist="Various Artists"/>
```

- Or, using the numbered shorthands

```xml
<bean id="awesomeMix" class="musicplayer.BlankCassette" c:_0="Awesome Mix Volume 1"
  c:_1="Various Artists"/>
```

---

## Wiring with XML
- The constructor-arg element lets you configure collections

```java
public class BlankCassette implements Cassette {
  // ...
  private List<String> tracks;

  public BlankCassette(String title, String artist, List<String> tracks) {
    // ...
    this.tracks = tracks;
  }
  // ...
}
```

- You could configure it as empty, using the `null` element

```xml
<bean id="awesomeMix" class="musicplayer.BlankCassette">
  <constructor-arg value="Awesome Mix Volume 1" />
  <constructor-arg value="Various Artists" />
  <constructor-arg><null/></constructor-arg>
</bean>
```

---

## Wiring with XML

```xml
<bean id="awesomeMix" class="musicplayer.BlankCassette">
  <constructor-arg value="Awesome Mix Volume 1" />
  <constructor-arg value="Various Artists" />
  <constructor-arg>
    <list> <!-- wiring a java.util.List -->
      <value>Hooked on a Feeling</value>
      <value>Go All the Way</value>
      <value>Spirit in the Sky</value>
      <value>Moonage Daydream</value>
      <value>Fooled Around and Fell in Love</value>
      <!-- ...other tracks omitted for brevity... -->
    </list>
  </constructor-arg>
</bean>
```

- Using `<ref>` you can reference other beans
- With `<set>` you can wire in a `java.util.Set`

---

## Wiring with XML
- Setting properties
    - The `property` element will use the setter to wire in the dependency

```xml
<bean id="walkman" class="musicplayer.Walkman">
  <property name="cassette" ref="awesomeMix" />
</bean>
```

- Constructor injection or setter injection
    - Constructor injection implements hard dependencies
        - The bean cannot exist without its dependencies being satisfied
    - Setter injection can denote optional dependencies
        - Imagine the bean being able to function without having the dependency satisfied
    - Spring offers you the choice!

---

## Wiring with XML
- A shorter notation with the `p`-namespace

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:p="http://www.springframework.org/schema/p"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
          http://www.springframework.org/schema/beans/spring-beans.xsd">
  <bean id="walkman" class="musicplayer.Walkman" p:cassette-ref="awesomeMix"/>
</bean>
```

- Setting values

```xml
<bean id="awesomeMix" class="musicplayer.BlankCassette">
  <property name="title" value="Awesome Mix Volume 1" />
  <property name="artist" value="Various Artists" />
  <property name="tracks">
    <list>
      <value>Hooked on a feeling</value>
      <!-- ...other tracks omitted for brevity... -->
    </list>
  </property>
</bean>
```

---

## Wiring with XML
- There is no convenient way to wire collections with the `p`-namespace
    - So you can use the `util`-namespace instead

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:p="http://www.springframework.org/schema/p"
  xmlns:util="http://www.springframework.org/schema/util"
  xsi:schemaLocation="http://www.springframework.org/schema/beans
  http://www.springframework.org/schema/beans/spring-beans.xsd
  http://www.springframework.org/schema/util
  http://www.springframework.org/schema/util/spring-util.xsd">
  <!-- ... -->
</beans>
```

---

## Wiring with XML
- The `util`-namespace offers the `<util:list>` element, that lets you create the list as a bean of its own

```xml
<util:list id="trackList">
  <value>Hooked on a Feeling</value>
  <value>Go All the Way</value>
  <value>Spirit in the Sky</value>
  <value>Moonage Daydream</value>
  <value>Fooled Around and Fell in Love</value>
  <!-- ...other tracks omitted for brevity... -->
</util:list>
```

- You can now use the `p`-namespace to declare the bean

```xml
<bean id="awesomeMix" class="musicplayer.BlankCassette"
  p:title="Awesome Mix Volume 1"
  p:artist="Various Artists"
  p:tracks-ref="trackList" />
```

---

## Wiring with XML
- Other elements in the `util`-namespace

|Element         |Description   |
| :------------- | :------------- |
| `<util:constant>` | References a public static field on a type and exposes it as a bean |
| `<util:list>`     | Creates a bean that is a `java.util.List` of values or references|
| `<util:map>`     | Creates a bean that is a `java.util.Map` of values or references |
| `<util:properties>` | Creates a bean that is a `java.util.Properties` |
| `<util:property-path>` | References a bean property (or nested property) and exposes it as a bean |
| `<util:set>`     | Creates a bean that is a `java.util.Set` of values or references |

---

## Importing and mixing configurations
- None of the Spring configuration options are mutually exclusive
    - You are free to mix component scanning and autowiring with JavaConfig and/or XML
- Remember it does not matter where the bean comes from!
- Importing beans
    - The `@Import` annotation will let you bring the two configurations together

```java
@Configuration
public class CassetteConfig{
  @Bean public Cassette cassette() {
    return new AwesomeMixVolume1();
  }
}
```

```java
@Configuration
@Import(CassetteConfig.class)
public class WalkmanConfig{
  @Bean public Walkman walkman(Cassette cassette) {
    return new Walkman(cassette);
  }
}
```

---

## Importing and mixing configurations
- When the beans come from an XML file, you can import it with `@ImportResource` into a JavaConfig

```java
@Configuration
@Import(CassetteConfig.class)
@ImportResource("classpath:cassette-config.xml")
public class WalkmanConfig{
}
```

- You can also break XML files apart, then import them in one another

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"...>
  <import resource="cassette-config.xml" />
  <bean id="walkman" class="musicplayer.Walkman" c:cassette-ref="cassette" />
</beans>
```

---

## Importing and mixing configurations
- To import a JavaConfig class in XML, you declare it as a bean in the XML file

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"...>
  <bean class="musicplayer.CassetteConfig" />
  <bean id="walkman" class="musicplayer.Walkman" c:cassette-ref="cassette"/>
</beans>
```

- Best practice
    - Create a root configuration that brings JavaConfig and XML files together (be it in XML or Java)
    - Activate component scanning in this root configuration for automatic configuration

---

## Exercise: Wiring
- Add a `FoodRepository` interface and implementation
    - `addFoodForAnimalType(Class<? extends Animal> clazz, Food food)`
    - `findFoodForAnimalType(Class<? extends Animal> clazz)`
- Add a `FoodDistributionService` interface and implementation
    - `feedAnimalsByType(List<Animal> animals)`
- Add the following method to the Zoo
    - `feedAnimals()`
- Wire the `FoodRepository` to the `FoodDistributionService`
- Wire the `FoodDistributionService` to the `ZooImpl`
- Note: Configure the wiring in any way you like, Java or XML or both!

---

## Exercise: Wiring

![](images/Exercises-Food.png#no-border)
