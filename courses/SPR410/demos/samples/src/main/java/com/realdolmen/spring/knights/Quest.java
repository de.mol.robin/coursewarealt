package com.realdolmen.spring.knights;

/**
 * Created by cda5732 on 18/03/2015.
 */
public interface Quest {
    void embark();
}
