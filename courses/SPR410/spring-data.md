# Spring Data

---

## Spring Data Motivation
- Creating repositories can become repetitive
  - The method below will probably look the same for any entity
  - The domain type may be different, but the method is fairly common across repositories

```java
public void addSpitter(Spitter spitter) {
  entityManager.persist(spitter);
}
```

- Spring Data JPA brings an end to boilerplate repository methods
    - Rather than write the same repository implementations again and again, you can stop at writing the repository interface
    - You then get access to 18 methods for performing common persistence operations

```java
public interface SpitterRepository extends JpaRepository<Spitter, Long> {}
```

---

## Spring Data Configuration
- The repository class itself will be created by Spring Data at startup time

```java
@Configuration
@EnableJpaRepositories(basePackages="com.acme.spittr.db")
public class JpaConfiguration {
  //...
}
```

- The implementation includes the 18 methods from
    - `JpaRepository`
    - `PagingAndSortingRepository`
    - `CrudRepository`
- But what if you need more than these 18 methods?

---

## Defining query methods
- You only need to add a method to the _interface_

```java
public interface SpitterRepository extends JpaRepository<Spitter, Long> {
  Spitter findByUsername(String username);
}
```

- Spring Data will examine the method and attempt to understand its purpose
    - Spring Data defines its own DSL where persistence details are expressed in the method signatures
- Repository methods are composed of a __verb__, and optional __subject__, the word __`By`__, and a __predicate__
    - E.g. `readSpitterByFirstnameOrLastname()`
    - `get`, `read` and `find` are synonyms
    - There is also a `count` verb to retrieve a count of matching objects
    - When the subject begins with `Distinct`, distinct results will be returned

---

## Defining query methods
- With the predicate, you can constrain the results
    - Each condition references a property and may specify a comparison operation
    - If the comparison operator is left out, it implied to be an equals operations
- Other predicates
    - `IsAfter`, `After`, `IsBetween`, `Between`
    - `IsGreaterThan`, `GreaterThan`, `IsGreaterThanEqual`, `GreaterThanEqual`, `IsBefore`, `Before`, `IsLessThan`, `LessThan`, `IsLessThanEqual`, `LessThanEqual`
    - `IsNull`, `Null`, `IsNotNull`, `NotNull`
    - `IsIn`, `In`, `IsNotIn`, `NotIn`
    - `IsStartingWith`, `StartingWith`, `StartsWith`, `IsEndingWith`, `EndingWith`, `EndsWith`,
    - `IsContaining`, `Containing`, `Contains`
    - `IsLike`, `Like`, `IsNotLike`, `NotLike`
    - `IsTrue`, `True`, `IsFalse`, `False`, `Is`, `Equals`, `IsNot`, `Not`

---

## Defining query methods
- The values that the properties will be compared against are the parameters of the method

```java
List<Spitter> readByFirstnameOrLastname(String first, String last);
```

- Case-insensitive search are done with `IgnoringCase` or `IgnoresCase`

```java
List<Spitter> readByFirstnameIgnoringCaseOrLastnameIgnoresCase(String first, String last);
```

```java
List<Spitter> readByFirstnameOrLastnameAllIgnoresCase(String first, String last);
```

- Sorting is done using `OrderBy` and `Asc` or `Desc`

```java
List<Spitter> readByFirstnameOrLastnameOrderByLastnameAscFirstnameDesc(String first, String last);
```

- By carefully constructing a repository method signature, you can let Spring Data JPA generate an implementation to query almost anything!

---

## Declaring custom queries
- Imagine the following method
    - This searches for all `Spitter`s whose email address is a Gmail address
    - This method does not adhere to the Spring Data method naming conventions

```java
List<Spitter> findAllGmailSpitters();
```
- In these situations, you can give Spring Data the query that should be performed
  - This is also useful in situations when the method name would become too long

```java
@Query("select s from Spitters where s.email like '%gmail.com'")
List<Spitter> findAllGmailSpitters();
```

```java
// Should replace this method with a @Query
List<Order> findByCustomerAddressZipCodeOrCustomerNameAndCustomerAddressState();
```
---

## Mixing in custom functionality
- When you need any custom functionality that Spring Data does not offer…
    - … just use the `EntityManager` at lower level on the methods that need it
    - You can still keep using Spring Data for the things it knows how to do
- Create a class with the same name as the interface, suffixed with `Impl`
    - If the class exists, Spring Data JPA merges its methods with the generated methods

```java
public class SpitterRepositoryImpl implements SpitterSweeper {
  @PersistenceContext
  private EntityManager em;
  public int eliteSweep() {
    String update = "UPDATE Spitter spitter SET spitter.status= 'Elite' " +
    "WHERE spitter.status= 'Newbie' AND spitter.id IN (" +
    "SELECT s FROM Spitters WHERE (SELECT COUNT(spittles) FROM s.spittles spittles) >" + "10000)";
    return em.createQuery(update).executeUpdate();
  }
}
```

---

## Mixing in custom functionality
- Notice the `SpitterRepositoryImpl` does not implement `SpitterRepository`
    - Spring Data JPA is still responsible for that interface
    - The only thing that ties your implementation to Spring Data is its name
- Make sure that the interface you implement is declared in `SpitterRepository`
  - The easiest way is by making the `SpitterRepository` interface extend it

```java
public interface SpitterSweeper {
  int eliteSweep();
}
```

```java
public interface SpitterRepository extends JpaRepository<Spitter, Long>, SpitterSweeper {
  // ...
}
```

---

## Mixing in custom functionality
- If you prefer using another suffix than `Impl`, you can configure it

```java
@EnableJpaRepositories(
  basePackages="com.acme.spittr.db",
  repositoryImplementationPostfix="Helper"
)
```

- Spring Data will now search for a class named `SpitterRepositoryHelper`
- Spring Data JPA is just the tip of the iceberg!
    - It also supports NoSQL databases!

---

## Exercise: Spring Data
- Create a domain for a Blog with following JPA entity classes:
    - `Blog`
    - `Author`
    - `BlogPost`
    - `Comment`
- Create repositories using Spring Data
- Add methods to
    - Find all blog posts ordered by creation date descending (with paging)
    - Find the first 50 comments by blog post id ordered by creation date descending
- Create a "test" and "production" profile, one with embedded database, the other for MySQL
- Test the repositories with JUnit tests

---

## Exercise: Spring Data

![](images/Exercise-spring-data.png#no-border)
