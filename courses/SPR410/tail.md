# References

---

## References
- Books

![](images/References.png#no-border)

---

## References
- Online
    - http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/
    - http://docs.spring.io/spring-boot/docs/
    - http://docs.spring.io/spring-data/jpa/docs/
    - http://www.thymeleaf.org/documentation.html
    - and many more Google searches …
