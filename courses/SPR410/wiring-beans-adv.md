# Wiring Beans Advanced

---

## Environments and profiles

- Transitioning an application from one environment to another is one of the most challenging aspects in software development
  - From development to production, many things can change: database configuration, encryption algorithms, integration and external systems, …
- Consider following `DataSource`
  - This `DataSource` is useful in development, but horrible in production!

<img src="images/DataSource.png#no-border" style="float:right; padding: 10px; height: 5em">


```java
@Bean(destroyMethod="shutdown")
public DataSource dataSource() {
  return new EmbeddedDatabaseBuilder()
    .setType(EmbeddedDatabaseType.H2)
    .addScript("classpath:schema.sql")
    .addScript("classpath:test-data.sql")
    .build();
}
```

---

## Environments and profiles
- In production we would rather use a `DataSource` from our container using JNDI
  - This is more fitting for production, but unnecessarily complicated for a simple developer test

```java
@Bean
public DataSource dataSource() {
  JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
  jndiObjectFactoryBean.setJndiName("jdbc/myDS");
  jndiObjectFactoryBean.setResourceRef(true);
  jndiObjectFactoryBean.setProxyInterface(javax.sql.DataSource.class);
  return (DataSource) jndiObjectFactoryBean.getObject();
}
```

---

## Environments and profiles
- And for QA testing, you would perhaps prefer another `DataSource` created with a Commons DBCP connection

```java
@Bean(destroyMethod="close")
public DataSource dataSource() {
  BasicDataSource dataSource = new BasicDataSource();
  dataSource.setUrl("jdbc:h2:tcp://dbserver/~/test");
  dataSource.setDriverClassName("org.h2.Driver");
  dataSource.setUsername("sa");
  dataSource.setPassword("password");
  dataSource.setInitialSize(20);
  dataSource.setMaxActive(30);
  return dataSource;
}
```

- All three configurations are different, but valid
  - We need a way to chose the most appropriate configuration for each environment

---

## Environments and profiles
- Spring introduces bean profiles
    - You can configure beans into one or more profiles
    - At runtime, Spring makes the decision which beans need to be created
    - The same deployment unit will work in all environments without being rebuilt
- Use the `@Profile` annotation

```java
@Configuration
@Profile("dev")
public class DevelopmentProfileConfig{
  @Bean(destroyMethod="shutdown")
  public DataSource dataSource() {
    return new EmbeddedDatabaseBuilder()
    .setType(EmbeddedDatabaseType.H2)
    .addScript("classpath:schema.sql")
    .addScript("classpath:test-data.sql")
    .build();
  }
}
```

---

## Environments and profiles
- Another JavaConfig may have another profile
    - The bean will not be created unless the corresponding profile is active
    - Any bean with no profile will always be created, regardless which profile is active

```java
@Configuration
@Profile("prod")
public class ProductionProfileConfig{
  @Bean
  public DataSource dataSource() {
    JndiObjectFactoryBean jndiObjectFactoryBean= new JndiObjectFactoryBean();
    jndiObjectFactoryBean.setJndiName("jdbc/myDS");
    jndiObjectFactoryBean.setResourceRef(true);
    jndiObjectFactoryBean.setProxyInterface(
    javax.sql.DataSource.class);
    return (DataSource) jndiObjectFactoryBean.getObject();
  }
}
```

---

## Environments and profiles
- You can add the profile on the method level
    - This makes it possible to combine both bean declarations into a single configuration

```java
@Configuration
public class DataSourceConfig{
  @Bean(destroyMethod="shutdown")
  @Profile("dev")
  public DataSource embeddedDataSource() {
    return new EmbeddedDatabaseBuilder()
    //...
    .build();
  }
  @Bean
  @Profile("prod")
  public DataSource jndiDataSource() {
    JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
    //...
    return (DataSource) jndiObjectFactoryBean.getObject();
  }
}
```

---

## Environments and profiles
- To activate a profile, you can use two properties
    - `spring.profiles.active`
    - `spring.profiles.default`
- These values can be set in several ways
    - As initialization parameters on `DispatcherServlet`
    - As context parameters of a web application
    - As JNDI entries
    - As environment variables
    - As JVM system properties
    - Using the `@ActiveProfiles` annotation on an integration test class
---

## Environments and profiles
- A possible scenario
    - Set the `spring.profiles.default` on `"dev"`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app version="3.0" ...>
  <!-- ... -->
  <context-param>
    <param-name>spring.profiles.default</param-name>
    <param-value>dev</param-value>
  </context-param>
  <!-- ... -->
  <servlet>
    <servlet-name>appServlet</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
      <param-name>spring.profiles.default</param-name>
      <param-value>dev</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
  </servlet>
</web-app>
```

---

## Environments and profiles
- A possible scenario
    - When the application is deployed to QA or production, set `spring.profiles.active` using system properties, environment variables, JNDI or other
    - These will then take precedence in the corresponding environment
- You can make several profiles active at the same time by listing the profile names, separated by commas
- In testing, you can use the `@ActiveProfiles` annotation

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={PersistenceTestConfig.class})
@ActiveProfiles("dev")
public class PersistenceTest {
  // ...
}
```

---

## Conditional beans
<img src="images/Gargamel.png#no-border" style="float:right; padding: 10px; height: 10em">

- Suppose
    - You only want a bean if and only if some library is available
    - Or if a certain other bean is also declared
    - Or if a specific environment variable is set
- Spring 4 introduces a new `@Conditional` annotation you can add on `@Bean` methods
    - If the condition evaluates to true, the bean is created, otherwise it is ignored
    - The `@Profile` annotation has been refactored to be based on `@Conditional`

```java
@Bean
@Conditional(SmurfsExistsCondition.class)
public MagicBean magicBean() {
  return new MagicBean();
}
```

---

## Conditional beans
- The `@Conditional` comes with a `Condition` interface

```java
public interface Condition {
  boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata);
}
```

- Example, you could check for the existence of a property in the environment

```java
public class SmurfsExistsCondition implements Condition {
  public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
    Environment env = context.getEnvironment();
    return env.containsProperty("smurfs");
  }
}
```

- There are much more that can be considered, from the `ConditionContext` and `AnnotatedTypeMetadata` interfaces

---

## Addressing ambiguity in autowiring

- Autowiring only works when exactly one bean matches the desired result
    - When there is more than one bean, ambiguity prevents Spring from autowiring
- Illustration

<img src="images/Cake.png#no-border" style="float:right; padding: 10px; height: 7em">

```java
@Autowired
public void setDessert(Dessert dessert) {
  this.dessert = dessert;
}
```

```java
@Component
public class Cake implements Dessert { ... }
```

```java
@Component
public class Cookies implements Dessert { ... }
```

```java
@Component
public class IceCream implements Dessert { ... }
```


---

## Addressing ambiguity in autowiring
- When Spring is unable to choose, it will fail with an exception
    - `NoUniqueBeanDefinitionException`

```dos
nested exception is org.springframework.beans.factory.NoUniqueBeanDefinitionException:
No qualifying bean of type [com.desserteater.Dessert] is defined: expected single matching bean but found 3: cake,cookies,iceCream
```

- Remark
    - In reality, autowiring ambiguity is rare
    - More often than not, there is only one implementation of a given type
- Solution
    - Designate one bean as primary choice, or use qualifiers

---

## Addressing ambiguity in autowiring
- Using `@Primary`, Spring will choose this bean over any other candidate beans

```java
@Component
@Primary
public class IceCream implements Dessert { ... }
```


```java
@Bean
@Primary
public Dessert iceCream() {
  return new IceCream();
}
```

- But you could also designate more than one primary bean… which does not solve the problem…

---

## Addressing ambiguity in autowiring
- Use `@Qualifier` to narrow down to a single bean
    - If ambiguity still exists after applying all qualifiers, you can always add more to narrow the choices further

<img src="images/iceCream.png#no-border" style="float:right; padding: 10px; height: 10em">

```java
@Autowired
@Qualifier("iceCream")
public void setDessert(Dessert dessert) {
  this.dessert = dessert;
}
```

- The parameter given to qualifier is the id of the bean
    - By default, all beans are given a qualifier that is the same as their bean id
    - This can be problematic when refactoring however…

---

## Addressing ambiguity in autowiring
- Instead of relying on the bean id, you can assign your own qualifier
  - It is recommended to use a trait or descriptive term for the bean, rather than an arbitrary name

```java
@Component
@Qualifier("cold")
public class IceCream implements Dessert { ... }
```

```java
@Autowired
@Qualifier("cold")
public void setDessert(Dessert dessert) {
  this.dessert = dessert;
}
```

---

## Addressing ambiguity in autowiring
- But using qualifiers with traits can still get you into trouble

```java
@Component
@Qualifier("cold")
public class Popsicle implements Dessert { ... }
```

- You can then use additional qualifiers to narrow down the selection
    - But… this is not allowed in Java however… and `@Qualifier` is not `@Repeatable`

<img src="images/ColdCreamy.png#no-border" style="float:right; padding: 10px; height: 10em">

```java
@Component
@Qualifier("cold")
@Qualifier("creamy")
public class IceCream implements Dessert { ... }
```

```java
@Autowired
@Qualifier("cold")
@Qualifier("creamy")
public void setDessert(Dessert dessert) {
  this.dessert = dessert;
}
```

---

## Addressing ambiguity in autowiring
- You can then create your own qualifier annotations

```java
@Target({ElementType.CONSTRUCTOR, ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Cold { }
```

```java
@Target({ElementType.CONSTRUCTOR, ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Creamy { }
```

```java
@Component
@Cold
@Creamy
public class IceCream implements Dessert { ... }
```

```java
@Autowired
@Cold
@Creamy
public void setDessert(Dessert dessert) { this.dessert = dessert; }
```

---

## Scoping beans
- By default, all beans are created as singletons
    - This is ideal most of the time: the cost of instantiating and garbage collection cannot be justified for stateless objects that can be reused over and over again
- But sometimes, you have classes with mutable state, that are not safe for reuse…
- Spring defines several scopes under which a bean can be created
    - Singleton—One instance for the entire application
    - Prototype—One instance every time the bean is injected or retrieved
    - Session—One instance for each session in a web application
    - Request—One instance for each request in a web application
- To select an alternative scope, use the `@Scope` annotation

---

## Scoping beans
- Setting the prototype scope

<img src="images/Notepad.png#no-border" style="float:right; padding: 10px; height: 10em">

```java
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Notepad { ... }
```

```java
@Bean
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public Notepad notepad() {
  return new Notepad();
}
```

- An instance of the bean will be created each and every time it is injected into or retrieved from the Spring application context

---

## Scoping beans
- In web applications, it is useful to instantiate a bean in session scope
  - E.g. a shopping cart

<img src="images/Cart.png#no-border" style="float:left; padding: 10px; height: 5em">

```java
@Bean
@Scope(value=WebApplicationContext.SCOPE_SESSION,
  proxyMode=ScopedProxyMode.INTERFACES)
public ShoppingCart cart() { ... }
```

- The `proxyMode` attribute is required in this case
  - This allows the reference to resolve to the actual session-scoped shopping cart

```java
@Component // singleton
public class StoreService{
  @Autowired // which one? not a singleton, but a proxy
  public void setShoppingCart(ShoppingCart shoppingCart) {
    this.shoppingCart = shoppingCart;
  }
  // ...
}
```

---

## Scoping beans
- Setting the `proxyMode` to `ScopedProxyMode.INTERFACES`
    - This indicates that the proxy should implement the `ShoppingCart` interface and delegate to the implementation bean
- Setting the `proxyMode` to `ScopedProxyMode.TARGET_CLASS`
    - This indicates that the proxy should be generated as subclass of the target class

<img src="images/Scoping-beans.png#no-border" style="height: 16em">

---

## Runtime value injection
- Wiring is not only about object references
    - Sometimes you want to inject values into a bean

```java
@Bean
public Cassette awesomeMix() {
  return new BlankCassette("Awesome Mix Volume 1", "Various Artists");
}
```

- This will hardcode the values into the configuration class
- Spring offers two solutions to evaluate values at runtime
    - Property placeholders
    - The Spring Expression Language (SpEL)

---

## Runtime value injection

- The simplest way to inject external values is by declaring a property source

```java
@Configuration
@PropertySource("classpath:/com/musicplayer/app.properties")
public class ExpressiveConfig{
  @Autowired
  Environment env;

  @Bean
  public BlackCassette cassette() {
    return new BlankCassette(env.getProperty("cass.title"), env.getProperty("cass.artist"));
  }
}
```

- The `@PropertySource` references a file with properties

```java
cass.title=Awesome Mix Volume 1
cass.artist=Various Artists
```
---

## Runtime value injection
- The `Environment` has overloaded `getProperty()` methods
    - `String getProperty(String key)`
    - `String getProperty(String key, String defaultValue)`
    - `T getProperty(String key, Class<T> type)`
    - `T getProperty(String key, Class<T> type, T defaultValue)`
- You can then specify a default value if the properties do not exist

```java
@Bean
public BlankCassette cassette() {
  return new BlankCassette(env.getProperty("cass.title", "The Very Best Of"),
    env.getProperty("cass.artist", "Bob Marley"));
}
```

- If the property is required, you can also use `getRequiredProperty()`
- From the `Environment` you can retrieve the active profiles

---

## Runtime value injection
- Properties can be injected using property placeholders in code
  - Use `@Value` in the same way as `@Autowired` but for values

```java
public BlankCassette(@Value("${cass.title}") String title, @Value("${cass.artist}") String artist) {
  this.title = title;
  this.artist = artist;
}
```

- This requires a `PropertySourcesPlaceholderConfigurer`
  - This bean will resolve using the Spring Environment and its set of property sources

```java
@Bean
public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
  return new PropertySourcesPlaceholderConfigurer();
}
```

---

## Wiring with Spring Expression Language
- SpEL is a powerful but compact way of wiring values into bean properties or arguments using expressions that are evaluated at runtime
    - Use `#{...}` instead of `${...}`
- SpEL has lots of capabilities
    - Reference beans by their identifiers
    - Invoking methods and accessing properties on objects
    - Mathematical, relational, and logical operations on values
    - Regular expression matching
    - Collection manipulation
- SpEL can also be used in combination with Thymeleaf, MVC, Security, Web Flow and more…

---

## Wiring with Spring Expression Language
- Examples

```java
#{1} // returns 1
```

```java
#{T(System).currentTimeMillis()} // working with Types
```

```java
#{awesomeMix.artist} // refer to bean properties
```

```java
#{systemProperties['cass.title']} // refer to system properties
```

```java
#{3.14159} #{'Hello'} #{false} // literals
```

```java
#{artistSelector.selectArtist().toUpperCase()} // calling bean methods
```

```java
#{artistSelector.selectArtist()?.toUpperCase()} // type-safe operator, protects against null
```

---

## Wiring with Spring Expression Language
- You can also use operators in SpEL

| Operator type  | Operators |
| :-------------  | :---------- |
| Arithmetic  | `+`, `-`, `*`, `/`, `%`, `^` |
| Comparison  | `<`, `lt`, `>`, `gt`, `==`, `eq`, `<=`, `le`, `>=`, `ge` |
| Logical  | `and`, `or`, `not` |
| Conditional  | `?:` (ternary), `?:` (Elvis) |
| Regularexpression  | `matches`  |

---

## Wiring with Spring Expression Language
- Examples

```java
#{T(java.lang.Math).PI * circle.radius ^ 2} // a circle's area
```

```java
#{cass.title + ' by ' + cass.artist} // String concatenation
```

```java
#{scoreboard.score > 1000 ? "Winner!" : "Loser"} // ternary operator
```

```java
#{cass.title ?: 'The Very Best of'} // Elvis operator
```

```java
#{admin.email matches '[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.com'} // regular expression
```

---

## Wiring with Spring Expression Language
- A few tricks of SpEL on collections and arrays

```java
#{jukebox.songs[4].title} // referencing a value from an array or list
```

```java
#{jukebox.songs[T(java.lang.Math).random() * jukebox.songs.size()].title} // random select
```

```java
#{jukebox.songs.?[artist eq 'Bob Marley']} // filter the collection on the artist
```

```java
#{jukebox.songs.^[artist eq 'Bob Marley']} // selecting the first occurrence
```

```java
#{jukebox.songs.$[artist eq 'Bob Marley']} // selecting the last occurrence
```

```java
#{jukebox.songs.![title]} // projection on the titles
```

- Warning: do not be too clever with SpEL expressions, because they can become difficult to test…

---

## Exercise: Wiring Beans Advanced

- Add the following properties to the `ZooImpl`
    - `maxAnimalCount`
    - `ownerName`
    - `ticketPrice`
- Configure these properties and the Zoo `name` by using a properties file
- Create an alternate configuration for the Zoo, for use in testing
    - Use default testing values for the Zoo properties
- Use profiles to select the testing configuration in a JUnit test

<img src="images/kibble.png#no-border" style="float:right; padding: 10px;">

- Create an alternate `FoodRepository`, giving all animals special "kibble"
- Select this new `FoodRepository` using your own `Qualifier`

---

## Exercise: Wiring Beans Advanced

![](images/Exercise-animal.png#no-border)
