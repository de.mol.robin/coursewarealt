# Introduction

---

## What is Spring?
- Spring's fundamental mission: Simplify Java Development
- Spring is an open source framework
    - Described in by Rod Johnson's book "Expert One-on-One: J2EE Design and Development"
- Spring was created to address the complexity of enterprise application development
    - Use plain-vanilla JavaBeans to achieve things previously only possible with EJB
    - Any Java application can benefit from Spring: simplicity, testability and loose coupling

---

## History of Spring
- Spring had a tremendous impact on enterprise application development
    - It has become a de facto standard for many Java projects
    - It had an impact on the evolution on the standard specifications and frameworks
- Spring continues to evolve and improve upon itself
    - Making difficult development tasks simpler
    - Empower Java developers with new and innovative features
    - Paving new trails in Java application development
    - Spring progresses into areas where JEE is just starting or isn't innovating at all

---

## Exciting new features in Spring 4
- Emphasis on Java-based configuration
- Conditional configuration and profiles
- Enhancements and improvements to Spring MVC, especially for REST support
- Using Thymeleaf with Spring web applications as alternative to JSP
- Enabling Spring Security with Java-based configuration
- Using Spring Data to automatically generate repository implementations at runtime (for JPA, MongoDB and Neo4j)
- New declarative caching support
- Asynchronous web messaging with WebSocket and STOMP
- Spring Boot, a new approach to working with Spring

---

## Spring strategies
- Everything Spring does boils down to:
    - Lightweight and minimally invasive development with POJOs
    - Loose coupling through dependency injection and interface orientation
    - Declarative programming through aspects and common conventions
    - Eliminating boilerplate code with aspects and templates
- As introduction, let us see some examples of the above strategies

---

## The power of POJOs
- Spring avoids (as much as possible) littering your application code with its API
    - Spring almost never forces you to implement Spring-specific interfaces or extend a Spring-specific class
    - Classes in a Spring-based application often have no indication that they're being used by Spring
    - At most, a class may be annotated with one of Spring's annotations, but it's a POJO

```java
public class HelloWorldBean { // this is all you need for Spring
  public String sayHello() {
    return "Hello World";
  }   
}
```

---

## Dependency injection
- Makes your code simpler, easier to understand and easier to test
- Consider following example

```java
public class DamselRescuingKnight implements Knight {
  private RescueDamselQuest quest;

  public DamselRescuingKnight() {
    this.quest = new RescueDamselQuest(); // this is a dependency
  }

  public void embarkOnQuest() {
    quest.embark();
  }
}
```

---

## Dependency injection
- The previous class is tightly coupled
    - If you need any other quest… the knight will have to sit it out
- This code is also very difficult to test, to reuse and to understand
    - If anything goes wrong when embarking on a quest… which class is responsible?
    - How can you test the `DamselRescuingKnight` in isolation?
- We need coupling or else nothing would happen in our code
- Coupling needs to be carefully managed!

---

## Dependency injection
- Definition of *dependency injection*
    - Objects are given their dependencies at creation time by some third party that coordinates each object in the system
    - Objects are not expected to create or obtain their dependencies themselves

![](images/dependency-injection.png#no-border)

---

## Dependency injection
- In the example below, the knight receives a quest at construction time
    - This is called _constructor injection_
    - Since the given quest implements the `Quest` interface, the knight can embark on any quest: `RescueDamselQuest`, `SlayDragonQuest`, …

```java
public class BraveKnight implements Knight {
  private Quest quest;

  public BraveKnight(Quest quest) { // Quest is injected
    this.quest = quest;
  }
  public void embarkOnQuest() {
    quest.embark();
  }
}
```

---

## Dependency injection
- The key benefit is _loose coupling_
    - If an object only knows about its dependencies by their interface, then the dependency can be swapped out with a different implementation
    - The depending object would not even know the difference
- Loose coupling can then make code easier to test
    - One common reason to swap out a dependency, is to introduce a mock implementation
    - This allows you to test the code in isolation, in a true unit test!

---

## Dependency injection
- Example using JUnit and Mockito

```java
public class BraveKnightTest {
  @Test
  public void knightShouldEmbarkOnQuest() {
    Quest mockQuest = mock(Quest.class); // create mock
    BraveKnight knight = new BraveKnight(mockQuest); // inject mock
    knight.embarkOnQuest();
    verify(mockQuest, times(1)).embark();
  }
}
```

---

## Wiring
- Suppose you want the knight to embark on a quest to slay a dragon

<img src="images/slaydragonquest.png#no-border" style="float:right; padding-left: 10px;">

```java
public class SlayDragonQuest implements Quest {
  private PrintStream stream;
  public SlayDragonQuest(PrintStream stream) {
    this.stream = stream;
  }
  public void embark() {
    stream.println("Going to slay the dragon!");
  }
}
```

- How do you give this quest to the knight? How do you give the `PrintStream` to the `SlayDragonQuest`?

---

## Wiring
- _Wiring_ is the act of creating associations between application components
- In Spring, there are many ways of to wire components together


```html
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.springframework.org/schema/beans
http://www.springframework.org/schema/beans/spring-beans.xsd">
  <bean id="knight" class="com.acme.knights.BraveKnight">
    <constructor-arg ref="quest" /> <!--injecting the quest bean -->
  </bean>
  <bean id="quest" class="com.acme.knights.SlayDragonQuest">
    <constructor-arg value="#{T(System).out}" /> <!--using Spring EL -->
  </bean>
</beans>
```

---

## Wiring
- An equivalent in Java configuration

```java
@Configuration
public class KnightConfig {
  @Bean
  public Knight knight() {
    return new BraveKnight(quest()); // injecting the quest bean
  }

  @Bean
  public Quest quest() {
    return new SlayDragonQuest(System.out);
  }
}
```

- Whether you use XML or Java-based configuration, the benefits are the same

---

## Seeing it work
- To start Spring with configuration, we need to start an _application context_
    - This is a framework object responsible to load bean definitions and wire them

```java
public class KnightMain {
  public static void main(String[] args) throws Exception {
    // load the Spring context
    ClassPathXmlApplicationContext context =
      new ClassPathXmlApplicationContext("/META-INF/spring/knight.xml");
    Knight knight = context.getBean(Knight.class); // get the knight bean
    knight.embarkOnQuest(); // use knight
    context.close();
  }
}
```

---

## Seeing it work
- For use with a Java configuration and Spring Boot, use another application context implementation

```java
@SpringBootApplication
public class KnightMain{
  public static void main(String[] args) throws Exception {
    // load the Spring context
    ConfigurableApplicationContext context = SpringApplication.run(KnightMain.class);
    Knight knight = context.getBean(Knight.class); // get the knight bean
    knight.embarkOnQuest(); // use knight
    context.close();
  }
}
```

---

## Applying aspects
- _Aspect Oriented Programming_ (AOP) separates _cross-cutting concerns_
    - It enables you to capture functionality that is used throughout your application in reusable components
- System services are usually cross-cutting
    - Logging, transaction management, security
    - They often find their way into components whose core responsibility is elsewhere
    - These services cut across multiple components in the system
- Cross-cutting introduces complexity
    - Code to implement these services is duplicated across multiple components
        - Even if you abstract the concern in a separate module, the method call is still duplicated
    - Your components are littered with code that is not aligned with their core functionality

---

## Applying aspects
- Illustration
    - In the below example, the business objects on the left are too intimately involved with the system services on the right

![](images/applying-aspects.png#no-border)

---

## Applying aspects
- AOP makes it possible to modularize these services and then apply them declaratively to the components they should affect
    - The core application does not even know these services exist

![](images/applying-aspects-aop.png#no-border)

---

## Applying aspects
- Adding a minstrel to the knight

```java
public class Minstrel {
  private PrintStream stream;

  public Minstrel(PrintStream stream) {
    this.stream = stream;
  }

  public void singBeforeQuest() {
    stream.println("Fa la la, the knight is so brave!");
  }

  public void singAfterQuest() {
    stream.println("Tee heehee, the brave knight did embark on a quest!");
  }
}
```

- Should you inject the minstrel into the knight?

---

## Applying aspects
- It is not the knight's concern to manage the minstrel and his state
    - The minstrel should do his job without having to be asked to
    - The knight should not have to remind the minstrel
- Using AOP you can separate the minstrel code
    - The minstrel can then sing about the knight's quest, while the knight is freed of the minstrel management

<img src="images/minstrel.png#no-border" style="float:right; padding-left: 10px;">

- We will keep minstrel as a POJO, but declare it as aspect
    - We configure when the minstrel should sing…

---

## Applying aspects

```java
@Aspect // an aspect
public class Minstrel {
  private PrintStream stream;

  public Minstrel(PrintStream stream) {
    this.stream = stream;
  }

  @Pointcut("execution(* *.embarkOnQuest(..))") // a pointcut
  private void embarksOnQuest(){      
  }

  @Before("embarksOnQuest()") // a before advice
  public void singBeforeQuest() {
    stream.println("Fa la la, the knight is so brave!");
  }

  @After("embarksOnQuest()") // an after advice
  public void singAfterQuest() {
    stream.println("Tee hee hee, the brave knight did embark on a quest!");
  }
}
```

---

## Applying aspects

- We just need a few extra lines of configuration to enable AOP … and done!

```java
@Configuration
@EnableAspectJAutoProxy // enabling AOP
public class KnightConfig {

  // assume knight and quest beans still present...

  @Bean
  public Minstrel minstrel() { // adding the minstrel
    return new Minstrel(System.out);
  }
}
```

- Of course you can also use an XML file to configure AOP instead

---

## Templating
- There are a lot of places where the Java API involves _boilerplate code_
    - This is code that you often have to write over and over again to accomplish common and otherwise simple tasks
    - Examples: JDBC, JNDI, consuming REST Services, …
- Spring seeks to eliminate boilerplate code by encapsulating it in _templates_
    - As developer you can focus on business instead of technology to make the code work
    - Templates eliminate much of the ceremony required when using APIs

---

## Spring containers
- Application objects live in a Spring _container_
    - The container creates the objects, wires them together, configures them and manages their lifecycle
- Spring comes with several container implementations

![](images/Spring-containers.png#no-border)

---

## Spring containers
- Bean factories
    - Are the simplest containers, providing basic support for dependency injection
    - `org.springframework.beans.factory.BeanFactory` interface
- Application contexts
    - Provide additional application-framework services
    - `org.springframework.context.ApplicationContext` interface
- Application contexts are preferred, as bean factories are too low-level for most applications

---

## Spring containers
- Application context implementations
    - `AnnotationConfigApplicationContext`
        - Loads Spring from one or more Java-based configuration classes
    - `AnnotationConfigWebApplicationContext`
        - As above, but for web applications
    - `ClassPathXmlApplicationContext`
        - Loads a context definition from one or more XML files located in the classpath
    - `FileSystemXmlApplicationContext`
        - As above, but the files are located on the file system
    - `XmlWebApplicationContext`
        - As above, but contained in a web application
    - `ConfigurableApplicationContext`
        - A context returned by Spring Boot

---

## Spring containers
- Examples

```java
ApplicationContext context = new
    FileSystemXmlApplicationContext("c:/knight.xml");
```

```java
ApplicationContext context = new
    ClassPathXmlApplicationContext("knight.xml");
```

```java
ApplicationContext context = new
    AnnotationConfigApplicationContext(KnightConfig.class);
```

```java
ConfigurableApplicationContext context =
    SpringApplication.run(KnightConfig.class, args);
```

---

## Bean lifecycle
- A bean goes through several steps between creation and destruction in the Spring container

![](images/Bean-lifecycle.png#no-border)

---

## Spring framework modules

![](images/Spring-framework-modules.png#no-border)

---

## Spring framework modules
- Core Spring container
    - The centerpiece of the Spring framework
    - Contains the bean factory and application context implementations
- Spring AOP module
    - Provides rich support for AOP in Spring
- Data access and integration
    - Support for JDBC, ORM, JMS, OXM and transactions
- Web and remoting
    - Includes MVC support, remoting (RMI, Hessian, Burlap, JAX-WS, HTTPInvoker) and REST

---

## Spring framework modules
- Instrumentation
    - Provides support for adding agents to the JVM (weaving agent for Tomcat bytecode instrumentation)
- Testing
    - Module dedicated to testing Spring applications

---

## Spring portfolio
- Next to the default modules, Spring includes several frameworks and libraries build on top of core Spring
- Spring Web Flow
    - Supports building conversational, flow-based web applications, build on Spring MVC
- Spring Web Services
    - Offers a contract-first web services model
- Spring Security
    - Declarative security mechanism for Spring applications, implemented with AOP
- Spring Integration
    - Implementation of several common integration patterns
- Spring Batch
    - For bulk operations on data

---

## Spring portfolio
- Spring Data
    - Makes it easy to work with all kinds of databases in Spring
    - Adds support for NoSQL databases
- Spring Social
    - Helps to connect Spring applications with REST APIs, social or not
- Spring Mobile
    - An extension to Spring MVC to support development of mobile web applications
- Spring for Android
    - Brings the simplicity of Spring to the development of native Android applications
- Spring Boot
    - Makes developing with Spring easier using automatic configuration techniques

---

## What's new in Spring 4?
- Support for JSR-356: Java API for WebSocket
- Support for a higher level message-oriented programming model on top of WebSockets(using SockJSand STOMP protocol)
- Support for Java 8 features, including lambda's
- Support for JSR-310: Date and Time API
- Smoother programming model for applications developed in Groovy
- Generalized support for conditional bean creation
- Asynchronous implementation of the Spring RestTemplate
- Support for many JEE specs: JMS 2.0, JTA 1.2, JPA 2.1 and Bean Validation 1.1

---


## Exercise: Introduction
- Open the "introduction" exercise provided by the Teacher
    - Explore the project to familiarize yourself with the code
- Create an interface for a Zoo in the package service
    - `void addAnimal(Animal animal)`
    - `void releaseAnimal(Animal animal)`
    - `boolean accept(Visitor visitor)`
    - `String getName()`
    - `int countAnimals()`
- Add `Animal` classes in a `domain` package
    - Animals have a `name` property (add getters and setters)
    - `Animal` is `abstract`
    - Add some concrete animal classes to the `domain` package

---

## Exercise: Introduction
- Create a new `ZooImpl` implementation class that implements the `Zoo` interface
    - Add a collection of animals and implement the interface methods
- Create a class `ZooConfig` in a `config` package
    - Configure the `ZooImpl` as a bean, give it a name with its constructor and add some "exotic" animals
- Start Spring using the `main` method
    - Fetch the configured `Zoo`
    - Print out its name and number of animals

---

## Exercise: Introduction

![](images/Exercise-intro.png#no-border)
