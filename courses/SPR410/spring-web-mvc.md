# Spring Web MVC

---

## Spring Web MVC
- For many Java developers, web-based applications are their primary focus
- Web applications quickly grow in complexity
- Challenges
    - State management
    - Workflow
    - Validation
    - HTTP is stateless
- Spring Web MVC helps to build web-based applications that are as flexible and loosely coupled as the Spring framework itself

---

## Getting started with Spring Web MVC

<img src="images/Request-7-steps.png#no-border" style="float:right; padding: 10px; height: 15em">

- The Request goes through 7 steps
    1. Front controller `DispatcherServlet`
    2. `HandlerMapping` selects the controller based on the URL of the `Request`
    3. The controller processes the `Request` by delegating to services
    4. Model data and view name is returned
    5. The `DispatcherServlet` consults a `ViewResolver` to find the `View`
    6. The `View` uses the model data to render the output
    7. The `Response` is carried back to the client

---

## Configuring the DispatcherServlet
- The easiest way is to use Java to configure the `DispatcherServlet`

```java
public class SpittrWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
  @Override
  protected String[] getServletMappings() {
    return new String[] { "/" }; // map the DispatcherServlet to '/'
  }
  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[] { RootConfig.class };
  }
  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class<?>[] { WebConfig.class }; // specify a configuration class
  }
}
```

- This requires the Servlet 3.0 specification
  - You do not have an excuse to not have it since it was released in 2009, and is available in Tomcat 7 or higher

---

## A tale of two application contexts…
- A Spring web application usually has two application contexts
    - The `WebConfig` will define beans for the `DispatcherServlet` (`Controller`s, `ViewResolver`s, `HandlerMapping`s)
    - The `RootConfig` will configure the other beans of the application (middle-and data-tier components that drive the backend)
- The `DispatcherServlet` loads its own configuration
- The backend configuration is loaded by a `ContextLoaderListener`
- The simplest configuration looks as follows

```java
@Configuration
@EnableWebMvc
public class WebConfig {
}
```

---

## Configuring Spring Web MVC
- The previous example is too simple
    - There is no view resolver, component scanning is not enabled and the `DispatcherServlet` will also need to handle static resources …

```java
@Configuration
@EnableWebMvc
@ComponentScan("spitter.web") // enable component scanning
public class WebConfig extends WebMvcConfigurerAdapter {
  @Bean
  public ViewResolver viewResolver() { // configure a view resolver
    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
    resolver.setPrefix("/WEB-INF/views/");
    resolver.setSuffix(".jsp");
    resolver.setExposeContextBeansAsAttributes(true);
    return resolver;
  }

  @Override
  public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
    configurer.enable(); // configures static resources
  }
}
```

---

## Configuring the backend application context

- Configure the backend in `RootConfig` as usual
    - Notice we filter out the `WebConfig` configuration by filtering on the `@EnableWebMvc` annotation

```java
@Configuration
@ComponentScan(
  basePackages={"spitter"},
  excludeFilters={@Filter(type=FilterType.ANNOTATION, value=EnableWebMvc.class)}
)
public class RootConfig {
}
```

---

## Writing a simple controller
- Use `@Controller` as stereotype for component scanning
    - Using `@Component` would have the same effect, but it would not be so expressive…

```java
@Controller // declare as a controller
public class HomeController {
  @RequestMapping(value="/", method=RequestMethod.GET) // handle GET requests for '/'
  public String home() {
    return "home"; // the view name is "home"
  }
}
```

- `@RequestMapping` specifies the request path and HTTP method to handle
- The return value will be interpreted as the name of the view to be rendered
    - With the previous `InternalResourceViewResolver`, this is `/WEB-INF/views/home.jsp`

???trainer
In Spring 4.3 (and with Spring Boot 1.4), new types of `@RequestMapping` annotations have been added to map HTTP methods. So instead of the following example...

```java
@RestController
@RequestMapping("/api/books")
public class BookAPIController {
  @RequestMapping
  public ResponseEntity<?> getBooks(){
  }
  @RequestMapping("/{book_id}")
  public ResponseEntity<?> getBook(@PathVariable("book_id") String bookId){
  }
  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<?> addNewBook(@RequestBody Map<String, Object> requestBody){
  }
  @RequestMapping(method = RequestMethod.POST, value="/{book_id}")
  public ResponseEntity<?> editBook(@PathVariable("book_id") String bookId){
  }
  @RequestMapping(method = RequestMethod.DELETE, value="/{book_id}")
  public ResponseEntity<?> deleteBook(@PathVariable("book_id") String bookId){
  }
}
```

...we can now use the alternative annotations `@GetMapping`, `@PostMapping`, `@PutMapping`, `@PatchMapping` and `@DeleteMapping`. This makes the below code a lot easier to read:

```java
@RestController
@RequestMapping("/api/books")
public class BookAPIController {
  @GetMapping
  public ResponseEntity<?> getBooks(){
  }
  @GetMapping("/{book_id}")
  public ResponseEntity<?> getBook(@PathVariable("book_id") String bookId){
  }    
  @PostMapping
  public ResponseEntity<?> addNewBook(@RequestBody Map<String, Object> requestBody){
  }
  @PostMapping("/{book_id}")
  public ResponseEntity<?> editBook(@PathVariable("book_id") String bookId){
  }
  @DeleteMapping("/{book_id}")
  public ResponseEntity<?> deleteBook(@PathVariable("book_id") String bookId){
  }        
}
```
???t

---

## Creating the view
- The home page JSP

```html
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
  <head>
    <title>Spittr</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/style.css" />" >
  </head>
  <body>
    <h1>Welcome to Spittr</h1>
    <a href="<c:url value="/spittles" />">Spittles</a> |
    <a href="<c:url value="/spitter/register" />">Register</a>
  </body>
</html>
```

---

## Testing the controller
- Since the controller is a POJO, it is easy to test

```java
public class HomeControllerTest {
  @Test
  public void testHomePage() throws Exception {
    HomeController controller = new HomeController();
    assertEquals("home", controller.home());
  }
}
```

- But this is not a good test…
    - What makes this class a `Controller`?
    - Will the `home()` method be called when a `GET` request for `"/"` comes in?
    - Is `"home"` a `View`?

---

## Testing the controller
- Much better!

```java
public class HomeControllerTest{
  @Test
  public void testHomePage() throws Exception {
    HomeController controller = new HomeController();
    MockMvc mockMvc = standaloneSetup(controller).build();
    mockMvc.perform(get("/")).andExpect(view().name("home"));
  }
}
```

- This time, we issue a `GET` request and test the resulting view as `"home"`
- We created a `MockMvc` container to test controllers
- No need to fire up a web server or web browser!

---

## Defining class-level request handling
- You can split up the `@RequestMapping` annotations on class and method
    - The class-level `@RequestMapping` applies to all handler methods of the controller
    - Any `@RequestMapping` annotation on the methods complement the class-level one
    - You can add multiple values as an array of Strings to handle multiple paths with the same controller/method

```java
@Controller
@RequestMapping({"/", "/homepage"})
public class HomeController{
  @RequestMapping(method=GET)
  public String home() {
    return "home";
  }
}
```

---

## Passing model data to the view
- To get model data, you will need a repository

```java
public interface SpittleRepository {
  List<Spittle> findSpittles(long max, int count);
}
```

- You will need an implementation of this interface that fetches data from a database
- The model bean is just a POJO data object

```java
public class Spittle {
  private final Long id;
  private final String message;
  private final Date time;
  private Double latitude;
  private Double longitude;
  // constructors, getters, setters, equals and hashCode
}
```

---

## Passing model data to the view
- Creating the controller

```java
@Controller
@RequestMapping("/spittles")
public class SpittleController {
  private SpittleRepository spittleRepository;

  @Autowired
  public SpittleController(SpittleRepository spittleRepository) { // inject repository
    this.spittleRepository = spittleRepository;
  }

  @RequestMapping(method=RequestMethod.GET)
  public String spittles(Model model) {
    // adding to the model
    model.addAttribute(spittleRepository.findSpittles(Long.MAX_VALUE, 20));
    return "spittles"; // returning the view name
  }
}
```

- Notice the `Model` parameter…

---

## Passing model data to the view
- Testing the controller

```java
@Test
public void shouldShowRecentSpittles() throws Exception {
  List<Spittle> expectedSpittles = createSpittleList(20);
  SpittleRepository mockRepository= mock(SpittleRepository.class);
  when(mockRepository.findSpittles(Long.MAX_VALUE, 20)).thenReturn(expectedSpittles);
  SpittleController controller = new SpittleController(mockRepository);
  // setSingleView() is necessary to not confuse the view path with the request path
  MockMvc mockMvc = standaloneSetup(controller)
    .setSingleView(new InternalResourceView("/WEB-INF/views/spittles.jsp"))
    .build();
  // see next block
}
```

---

## Passing model data to the view
- Testing the controller (continued)

```java
// insert in previous block
mockMvc.perform(get("/spittles"))
  .andExpect(view().name("spittles"))
  .andExpect(model().attributeExists("spittleList"))
  .andExpect(model().attribute("spittleList", hasItems(expectedSpittles.toArray())));
```

```java
// ...
private List<Spittle> createSpittleList(int count) {
  List<Spittle> spittles = new ArrayList<Spittle>();
  for (int i = 0; i < count; i++) {
    spittles.add(new Spittle("Spittle " + i, new Date()));
  }
  return spittles;
}
```

---

## Passing model data to the view
- The `Model` parameter is essentially a map
    - It will be handed over to the `View` so the data can be rendered to the client
    - The name of the key is inferred from the type of object being set as value
        - In case of a `List<Spittle>`, the key will be inferred as `"spittleList"`
    - You can specify the key name explicitly, and you can replace `Model` with `Map` if you prefer to work with a non-Spring type
- Spring can make the controller method shorter
    - This method returns the model data, which is put into the model
    - The model key is inferred from the type, in this case `"spittleList"`
    - The logical view name is derived from the path, so it becomes `"spittles"`

```java
@RequestMapping(method=RequestMethod.GET)
public List<Spittle> spittles() {
  return spittleRepository.findSpittles(Long.MAX_VALUE, 20));
}
```

---

## Passing model data to the view
- The model data is copied into the request as request attributes
- In `/WEB-INF/views/spittles.jsp`, we can access the model data with JSTL

```html
<c:forEach items="${spittleList}" var="spittle">
  <li id="spittle_<c:out value="spittle.id"/>">
    <div class="spittleMessage">
      <c:out value="${spittle.message}" />
    </div>
    <div>
      <span class="spittleTime"><c:out value="${spittle.time}" /></span>
      <span class="spittleLocation">
        (<c:out value="${spittle.latitude}" />,<c:out value="${spittle.longitude}"/>)
      </span>
    </div>
  </li>
</c:forEach>
```

---

## Accepting request input
- Clients can send data back to the server
- Without this feature, the web would be read-only
- Spring MVC accepts
    - Query parameters
    - Form parameters
    - Path variables

---

## Accepting request input
- Taking query parameters

```java
@RequestMapping(method=RequestMethod.GET)
public List<Spittle> spittles(
  @RequestParam("max") long max,
  @RequestParam("count") int count) {
  return spittleRepository.findSpittles(max, count);
}
```

- You can add default values for the case when the parameters are not present

```java
private static final String MAX_LONG_AS_STRING = Long.toString(Long.MAX_VALUE);
@RequestMapping(method=RequestMethod.GET)
public List<Spittle> spittles(
    @RequestParam(value="max", defaultValue=MAX_LONG_AS_STRING) long max,
    @RequestParam(value="count", defaultValue="20") int count) {
  return spittleRepository.findSpittles(max, count);
}
```

---

## Accepting request input
- Taking path variables
    - Instead of `/spittles/show?spittle_id=12345`, prefer `/spittles/12345`
    - This identifies a resource, following REST principles
    - Notice that you can omit the value parameter in `@PathVariable` because the method parameter name is the same as the placeholder name

```java
@RequestMapping(value="/{spittleId}", method=RequestMethod.GET)
public String spittle(@PathVariable("spittleId")long spittleId, Model model) {
  model.addAttribute(spittleRepository.findOne(spittleId));
  return "spittle";
}
```

```html
<div class="spittleView">
  <div class="spittleMessage"><c:out value="${spittle.message}" /></div>
  <div>
    <span class="spittleTime"><c:out value="${spittle.time}" /></span>
  </div>
</div>
```

---

## Accepting request input
- Processing forms
    - There are two sides to working with forms: displaying the form and processing the data the user submits from the form
- Displaying the form
    - Renders `/WEB-INF/views/registerForm.jsp` when requesting `/spitter/register`

```java
@Controller
@RequestMapping("/spitter")
public class SpitterController {
  @RequestMapping(value="/register", method=GET)
  public String showRegistrationForm() {
    return "registerForm";
  }
}
```

---

## Accepting request input
- Displaying the form

```html
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
  <head>
    <title>Spittr</title>
    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/style.css' />" >
  </head>
  <body>
    <h1>Register</h1>
    <form method="POST"> <!--no action parameter posts back to /spitter/register -->
      First Name: <input type="text" name="firstName" /><br/>
      Last Name: <input type="text" name="lastName" /><br/>
      Username: <input type="text" name="username" /><br/>
      Password: <input type="password" name="password" /><br/>
      <input type="submit" value="Register" />
    </form>
  </body>
</html>
```

---

## Accepting request input
- Writing form handling

```java
@Controller
@RequestMapping("/spitter")
public class SpitterController{
  private SpitterRepository spitterRepository;

  @Autowired
  public SpitterController(SpitterRepository spitterRepository) {
    this.spitterRepository = spitterRepository;
  }

  @RequestMapping(value="/register", method=GET)
  public String showRegistrationForm() {
    return "registerForm";
  }

  @RequestMapping(value="/register", method=POST)
  public String processRegistration(Spitter spitter) {
    spitterRepository.save(spitter);
    return "redirect:/spitter/" + spitter.getUsername();
  }
}
```

---

## Accepting request input
- Writing form handling
    - Notice the `processRegistration()` method is given an `Spitter` object
        - This object will be populated from the request parameters of the same name
    - Rather than returning the `View`, we return a `redirect` specification
        - Instead of a view name, the browser will receive a redirect for the user's profile page
    - Next to `redirect` you can also use `forward` which will forward the request to the given URL
- Adding a handler for the redirected page

```java
@RequestMapping(value="/{username}", method=GET)
public String showSpitterProfile(@PathVariable String username, Model model) {
  Spitter spitter = spitterRepository.findByUsername(username);
  model.addAttribute(spitter);
  return "profile";
}
```

---

## Validating forms
- Spring supports the Java Validation API (JSR-303)
    - No extra configuration is necessary, just add Hibernate Validator on the classpath
- The Java Validation API specifies several annotations you can put on properties, but you can add your own!
    - `@AssertFalse`, `@AssertTrue`
    - `@DecimalMin`, `@DecimalMax`
    - `@Digits`
    - `@Future`, `@Past`
    - `@Min`, `@Max`
    - `@Null`, `@NotNull`
    - `@Pattern`
    - `@Size`

---

## Validating forms
- Adding validation annotations to the `Spitter` bean

```java
public class Spitter {
  private Long id;
  @NotNull
  @Size(min=5, max=16)
  private String username;
  @NotNull
  @Size(min=5, max=25)
  private String password;
  @NotNull
  @Size(min=2, max=30)
  private String firstName;
  @NotNull
  @Size(min=2, max=30)
  private String lastName;
  // ...
}
```

---

## Validating forms
- We can now add a validation check to the controller method
  - Note the `Errors` parameter must follow the `@Valid`-annotated parameter
  - If the form has any errors, we take the user back to the form so they can correct any problems and try again

```java
@RequestMapping(value="/register", method=POST)
public String processRegistration(@Valid Spitter spitter, Errors errors) {
  if (errors.hasErrors()) {
    return "registerForm";
  }
  spitterRepository.save(spitter);
  return "redirect:/spitter/" + spitter.getUsername();
}
```

---

## Rendering web views
- None of the controller methods produce HTML that is rendered in the browser
    - They populate the model with some data
    - They pass the model off to a view for rendering
    - The methods return a String value that is the logical name of the view
    - `Controller`s are not aware of the views and technology used for rendering
- Spring uses `ViewResolver`s to determine the actual view to render
    - Next to the `InternalResourceViewResolver`, Spring provides several out-of-the-box implementations

---

## Rendering web views
- In most applications, you only need a handful of these view resolvers
    - For the most part, each resolver corresponds to a specific view technology available for Java web applications

| View Resolver  | View Resolver  |
| :-- | :-- |
| `BeanNameViewResolver`  | `UrlBasedViewResolver`  |
| `ContentNegotiatingViewResolver`  | `VelocityLayoutViewResolver`  |
| `FreeMarkerViewResolver`  | `VelocityViewResolver`  |
| `InternalResourceViewResolver`  | `XmlViewResolver`  |
| `JasperReportsViewResolver`  | `XsltViewResolver`  |
| `ResourceBundleViewResolver`  | `TilesViewResolver`  |

---

## Creating JSP views
- Spring supports JSP pages in two ways
    - `InternalResourceViewResolver` and JSTL `Locale` / `ResourceBundle` support
    - Two JSP tag libraries with form-to-model binding and general utilities
- `InternalResourceViewResolver` uses a convention to determine the path to your JSP pages
  - Placing your pages in `WEB-INF` is a common practice to prevent direct access
  - By adding a prefix and a suffix, the physical view path can be derived

```java
@Bean
public ViewResolver viewResolver() {
  InternalResourceViewResolver resolver = new InternalResourceViewResolver();
  resolver.setPrefix("/WEB-INF/views/");
  resolver.setSuffix(".jsp");
  return resolver;
}
```

---

## Resolving JSTL views
- If your JSP pages are using JSTL, you may want to configure the `InternalResourceViewResolver` to resolve a `JstlView`
    - JSTL tags will then be given a `Locale` and a message source configured in Spring
    - This is useful for properly formatting dates and money

```java
@Bean
public ViewResolver viewResolver() {
  InternalResourceViewResolver resolver = new InternalResourceViewResolver();
  resolver.setPrefix("/WEB-INF/views/");
  resolver.setSuffix(".jsp");
  resolver.setViewClass(org.springframework.web.servlet.view.JstlView.class);
  return resolver;
}
```

---

## Binding forms to the model
- Tag libraries bring functionality to a JSP template without needing Java code directly in scriptlet blocks
- The Spring form-binding JSP tag library contains 14 tags
    - These are used to render HTML `form` tags
    - These tags can be bound to an object in the model and can be populated with values from the model's object properties
- To use the form-binding tag library, you will need to declare it
  - Notice the prefix `sf` for Spring forms
  - It is common to find a prefix of `form`

```html
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
```

---

## Binding forms to the model
- These are the 14 tags of the form-binding tag library

| JSP Tag  | JSP Tag  |
| :-- | :-- |
| `<sf:checkbox>`  | `<sf:option>`  |
| `<sf:checkboxes>`  | `<sf:options>`  |
| `<sf:errors>`  | `<sf:password>`  |
| `<sf:form>`  | `<sf:radiobutton>`  |
| `<sf:hidden>`  | `<sf:radiobuttons>`  |
| `<sf:input>`  | `<sf:select>`  |
| `<sf:label>`  | `<sf:textarea>`  |

---

## Binding forms to the model
- Applying those tags on the registration form

```html
<sf:form method="POST" commandName="spitter">
  First Name: <sf:input path="firstName" /><br/>
  Last Name: <sf:input path="lastName" /><br/>
  Email: <sf:input path="email" /><br/>
  Username: <sf:input path="username" /><br/>
  Password: <sf:password path="password" /><br/>
  <input type="submit" value="Register" />
</sf:form>
```

- Notice the `commandName` attribute set to `spitter`

```java
@RequestMapping(value="/register", method=GET)
public String showRegistrationForm(Model model) {
  model.addAttribute(new Spitter());
  return "registerForm";
}
```

---

## Binding forms to the model
- The `<sf:input>` tag allows you to specify a `type` attribute so that you can declare HTML5 specific text fields, such as `date`, `range`, `email` …

```html
Email: <sf:input path="email" type="email" /><br/>
```

- To guide the user when they make errors, we need to add the `<sf:errors>` tag

```html
<sf:form method="POST" commandName="spitter">
  First Name: <sf:input path="firstName" />
  <sf:errors path="firstName" cssClass="error"/><br/>
  <!--- ... -->
</sf:form>
```

- If there is a validation error for the property, the `<sf:errors>` will render the error message in a HTML `<span>` tag
    - You can use the `cssClass` attribute to make the error stand out

---

## Binding forms to the model
- Another way to show errors, is by displaying them all together

```html
<sf:form method="POST" commandName="spitter" >
  <sf:errors path="*" element="div" cssClass="errors" />
  <!-- ... -->
</sf:form>
```

- On the `<sf:label>` and `<sf:input>` you can also add a `cssErrorClass` to highlight the fields to be corrected

```html
<sf:form method="POST" commandName="spitter" >
  <sf:label path="firstName" cssErrorClass="error">First Name</sf:label>:
  <sf:input path="firstName" cssErrorClass="error" /><br/>
  <!-- ... -->
</sf:form>
```

---

## Binding forms to the model
- You can set the `message` attribute on validation annotations, to make errors friendlier to read and to internationalize them
    - The message will be defined in a properties file named `ValidationMessages.properties`

```java
@NotNull
@Size(min=5, max=16, message="{username.size}")
private String username;
@NotNull
@Size(min=5, max=25, message="{password.size}")
private String password;
@NotNull
@Size(min=2, max=30, message="{firstName.size}")
private String firstName;
@NotNull
@Size(min=2, max=30, message="{lastName.size}")
private String lastName;
@NotNull
@Email(message="{email.valid}")
private String email;
```
---

## Spring's general tag library
- Next to the form-binding tags, Spring offers a more general JSP library
    - Notice the prefix `s` is usually set to `spring`

```html
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
```

- This gives you 10 JSP tags

| JSP Tag  | JSP Tag  |
| :-- | :-- |
| `<s:bind>`  | `<s:nestedPath>`  |
| `<s:escapeBody>`  | `<s:theme>`  |
| `<s:hasBindErrors>`  | `<s:transform>`  |
| `<s:htmlEscape>`  | `<s:url>`  |
| `<s:message>` | `<s:eval>`  |

---

## Spring's general tag library
- Most of these are no longer necessary due to the Spring form-binding tags
    - The `<s:bind>` was Spring's original form-binding tag, but it was much more complex to use
- You can use the `<s:message>` tags for internationalization
- Instead of …

```html
<h1>Welcome to Spittr!</h1>
```

- … you should use

```html
<h1><s:message code="spittr.welcome" /></h1>
```

---

## Spring's general tag library
- Use a `ResourceBundleMessageSource` to load the properties files

```java
@Bean
public MessageSource messageSource() {
  ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
  messageSource.setBasename("messages");
  return messageSource;
}
```

- Optionally, a `ReloadableResourceBundleMessageSource` can be configured that allows to reload message properties without restarting the application

```java
@Bean
public MessageSource messageSource() {
  ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
  messageSource.setBasename("file:///etc/spittr/messages"); // looks in the file system
  messageSource.setCacheSeconds(10);
  return messageSource;
}
```

---

## Spring's general tag library
- `<s:url>` creates servlet-context specific URLs

```html
<s:url href="/spitter/register" var="registerUrl" /> <!--/spittr/spitter/register -->
<a href="${registerUrl}">Register</a>
```

- With `<s:param>` parameters can be added
    - You can optionally escape the URL for HTML and JavaScript use

```html
<s:url href="/spittles" var="spittlesUrl" htmlEscape="true" javaScriptEscape="true">
  <s:param name="max" value="60" />
  <s:param name="count" value="20" />
</s:url>
```
- The `<s:escapeBody>` is a general purpose escaping tag

```html
<s:escapeBody htmlEscape="true">
  <h1>Hello</h1> <!--rendered as &lt;h1&gt;Hello&lt;/h1&gt; -->
</s:escapeBody>
```

---

## Working with Thymeleaf
- Thymeleaf is a templating library
- Thymeleaf templates
    - Are natural, easy to write, and do not rely on tag libraries
    - Can be edited and rendered anywhere raw HTML is welcome
    - Are not tied to the Servlet specification
- To configure Thymeleaf, you need three beans
    - `ThymeleafViewResolver`
    - `SpringTemplateEngine`
    - `TemplateResolver`

---

## Working with Thymeleaf

```java
@Bean
public ViewResolver viewResolver(SpringTemplateEngine templateEngine) {
  ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
  viewResolver.setTemplateEngine(templateEngine);
  return viewResolver;
}

@Bean
public TemplateEngine templateEngine(TemplateResolver templateResolver) {
  SpringTemplateEngine templateEngine = new SpringTemplateEngine();
  templateEngine.setTemplateResolver(templateResolver);
  return templateEngine;
}

@Bean
public TemplateResolver templateResolver() {
  TemplateResolver templateResolver = new ServletContextTemplateResolver();
  templateResolver.setPrefix("/WEB-INF/templates/");
  templateResolver.setSuffix(".html");
  templateResolver.setTemplateMode("HTML5");
  return templateResolver;
}
```

---

## Defining Thymeleaf templates
- Thymeleaf templates are essentially just HTML files
    - It adds its attributes to the standard set of HTML tags via a custom namespace
    - The `th:href` attribute value can contain Thymeleaf expressions to evaluate dynamic values

```html
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
  <head>
    <title>Spittr</title>
    <link rel="stylesheet" type="text/css" th:href="@{/resources/style.css}"></link>
  </head>
  <body>
    <h1>Welcome to Spittr</h1>
    <a th:href="@{/spittles}">Spittles</a> |
    <a th:href="@{/spitter/register}">Register</a>
  </body>
</html>
```

- As the template is just HTML, it renders nicely in browsers/tools

---

## Form binding with Thymeleaf
- Consider the following snippet

```html
<label th:class="${#fields.hasErrors('firstName')} ? 'error'">First Name</label>:
<input type="text" th:field="*{firstName}" th:class="${#fields.hasErrors('firstName')} ? 'error'" />
```

- `<th:class>` renders a class attribute
- `<th:field>` references the field from the backing object
    - This gives you both a `name` and `value` attribute set to `firstName`
- The `${}` are variable expressions; in Spring they're SpEL expressions
- The `*{}` are selection expressions, evaluated on the selected object
    - In this case the `spitter` object

---

## Form binding with Thymeleaf
- The complete form

```html
<form method="POST" th:object="${spitter}">
  <div class="errors" th:if="${#fields.hasErrors('*')}">
    <ul>
      <li th:each="err : ${#fields.errors('*')}" th:text="${err}">Input is incorrect</li>
    </ul>
  </div>
  <label th:class="${#fields.hasErrors('firstName')}? 'error'"> First Name</label>:
  <input type="text" th:field="*{firstName}"
    th:class="${#fields.hasErrors('firstName')}?'error'" /><br/>
  <label th:class="${#fields.hasErrors('lastName')}? 'error'"> Last Name</label>:
  <input type="text" th:field="*{lastName}" th:class="${#fields.hasErrors('lastName')}?'error'" /><br/>
  <label th:class="${#fields.hasErrors('email')}? 'error'"> Email</label>:
  <input type="text" th:field="*{email}" th:class="${#fields.hasErrors('email')}? 'error'"/><br/>
  <!-- ... -->
```

---

## Form binding with Thymeleaf
- The complete form (continued)

```html
  <!-- ... -->
  <label th:class="${#fields.hasErrors('username')}? 'error'">Username</label>:
  <input type="text" th:field="*{username}" th:class="${#fields.hasErrors('username')}?'error'" /><br/>
  <label th:class="${#fields.hasErrors('password')}? 'error'"> Password</label>:
  <input type="password" th:field="*{password}"
    th:class="${#fields.hasErrors('password')}?'error'" /><br/>
  <input type="submit" value="Register" />
</form>
```

- Notice
    - Error rendering at the top of the form
    - The `th:each` that allows to loop, `th:if` that specifies conditional output
    - The `th:object` attribute referring to the `Spitter` object from the model
    - The use of `${}` and `*{}`

---

## Exercise: Spring Web MVC

- Explore the MVC project offered by the trainer
- Notice the Thymeleaf pages, the `domain`, `dao` and `config` packages
- Create `Controller`s to make the web applications work completely
    - Add a `HomeController` that refers to the index page
    - Add an `AuthorController` that lists out the list of authors
    - Add a `RegistrationController` that lets you register new `Author`s with a form
- Edit the Thymeleaf pages to add the missing elements and links
- Test your MVC exercise in the browser, so that everything works!
