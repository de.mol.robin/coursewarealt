# Spring Boot

---

## What is Spring Boot?
- Spring Boot is a framework that helps you get up-and-running with a Spring project quickly
    - Takes an opinionated view of building production-ready applications
    - Favors convention over configuration to minimize configuration
- Features
    - Create stand-alone Spring applications
        - Optionally embedding web container or enterprise features when necessary
    - Provides simplified Maven and Gradle build support
    - Tries to automatically configure the `ApplicationContext`
    - Supports enterprise features such as runtime health checks and other metrics
    - Does not rely on any code generation

---

## Spring Boot rationale
- Spring is an extremely flexible framework
    - Scalable
        - It scales from the simplest command-line apps to large mission critical enterprise services
    - Extensible
        - It is almost infinitely extensible, allowing you to customize, and fine-tune to your specific needs
- This flexibility does come with a price, however
    - Spring applications do tend to need a high amount of configuration
        - With Spring 4, configuration is already a lot simpler
- Spring Boot is built to allow projects to get started quickly
    - Reducing the initial configuration overhead to an absolute minimum

---

## Main components
- Spring Boot starters
    - Group typical-use-case dependencies together as a single artifact
        - Supports both Maven and Gradle build systems
- Auto configuration
    - Automatically enable and configure Spring features based on project dependencies
- Command-line interface (CLI)
    - Provide a groovy-based console environment to quickly perform common tasks
- Actuator
    - Add management features typically needed for production grade applications

---

## Starter dependencies
- Dependency management of a typical Spring app easily becomes unwieldy
    - Gathering the right dependencies
    - Selecting the correct versions to make sure they play together
    - Maintaining (upgrading, adding, removing, …) these dependencies
- Both Maven and Gradle have this problem
    - Gradle is a lot less verbose than Maven though
- Spring Boot provides a large number of predefined combinations of libraries that eliminate this dependency hell
    - When your project grows larger and more specific dependencies become necessary, they can still be added the usual way

---

## Starter dependencies
- Consider the Maven dependencies for a __simple__ Spring based web app

```xml
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-web</artifactId>
  <version>4.x.x.RELEASE</version>
</dependency>
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-webmvc</artifactId>
  <version>4.x.x.RELEASE</version>
</dependency>
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-jdbc</artifactId>
  <version>4.x.x.RELEASE</version>
</dependency>
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-tx</artifactId>
  <version>4.x.x.RELEASE</version>
</dependency>
```

---

## Starter dependencies

```xml
<dependency>
  <groupId>c.fasterxml.jackson.core</groupId>
  <artifactId>jackson-databind</artifactId>
  <version>2.x.x</version>
</dependency>
<dependency>
  <groupId>com.h2database</groupId>
  <artifactId>h2</artifactId>
  <version>1.4.x</version>
</dependency>
<dependency>
  <groupId>org.thymeleaf</groupId>
  <artifactId>thymeleaf-spring4</artifactId>
  <version>2.x.x.RELEASE</version>
</dependency>
```

---

## Starter dependencies
- Using Spring Boot’s aggregated dependencies, we can shorten this by an order of magnitude
  - Starter dependencies group many other dependencies
  - Framework versions are selected automatically

```xml
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-parent</artifactId>
  <version>1.5.6.RELEASE</version>
</parent>
```  

---

## Starter dependencies

```xml
<dependency>
  <groupId>o.s.boot</groupId>
  <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
  <groupId>o.s.boot</groupId>
  <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
<dependency>
  <groupId>com.h2database</groupId>
  <artifactId>h2</artifactId>
</dependency>
<dependency>
  <groupId>org.thymeleaf</groupId>
  <artifactId>thymeleaf-spring4</artifactId>
</dependency>
```

---

## Starter dependencies
- The list of available starter dependencies is huge
    - Here is a list of some of the most interesting ones


| ArtifactId  | ArtifactId  | ArtifactId  |
| :-- | :-- | :-- |
| `spring-boot-starter-aop`  | `spring-boot-starter-security`  | `spring-boot-starter-data-mongodb`  |
| `spring-boot-starter-batch`  | `spring-boot-starter`  | `spring-boot-starter-amqp`  |
| `spring-boot-starter-data-jpa`  | `spring-boot-starter-test`  | `spring-boot-starter-thymeleaf`  |
| `spring-boot-starter-data-rest`  | `spring-boot-starter-web`  | `spring-boot-starter-redis`  |
| `spring-boot-starter-integration`  | `spring-boot-starter-websocket`  | `spring-boot-starter-social-twitter`  |
| `spring-boot-starter-jdbc`  | `spring-boot-starter-ws`  | `spring-boot-starter-social-linkedin`  |

- GroupId: `org.springframework.boot`

---

## Automatic configuration
- Automatic configuration simplifies the initial Spring configuration overhead
    - Using classpath scanning, Spring Boot detects which libraries are present
    - When a well-known library is present, Spring Boot automatically configures it using the typical (opinionated) way
- This allows you to start working with the libraries without having to do tedious `ApplicationContext` configuration
- This mechanism uses a "reasonable guess"
    - When its guess is wrong, you can still configure the `ApplicationContext` the usual way
    - You can also configure the default guesses with `application.properties`

---

## Command-line interface
- Spring Boot uses the power of Groovy to minimize application logic
    - Groovy is a dynamic JVM programming language that gets rid of some of the verbosity of Java
- Any Groovy code can interface with Java, by using some boilerplate logic
    - This boilerplate logic is eliminated by Spring Boot’s CLI feature
- Consider the following Groovy script

```groovy
@RestController class Hi { @RequestMapping("/") String hi() { "Hi!" } }
```

- This is a __fully__ functional Spring application! You can run it as follows:

```dos
C:\project>spring run Hi.groovy
```

---

## Actuator
- Big enterprise servers typically have a lot of management features
    - Runtime metrics, server health, etc…
- Spring Boot promotes a stand-alone way of running applications
    - How can we make sure these stand-alone apps are equally production-grade as the big enterprise suites?
- The Actuator enhances stand-alone applications so that they also offer these features
    - It adds REST endpoints that allow you to query various runtime statistics on your live stand-alone application

---

## Building a web application
- Using Spring Boot, let us create a simple but representative web application using the following (typical) technologies
    - Maven
    - Spring MVC
    - Thymeleaf (templating)
    - JdbcTemplate
    - H2 (RDBMS)

<img src="images/Spring-Boot.png#no-border" style="float:right; padding: 10px; height: 10em;">

- As you’ll see, as long as you stick to the defaults, configuration is reduced to an absolute minimum
    - You can always change this later, when your requirements become more specific

---

## Project setup
- First we need to setup the Maven `pom.xml`
    - The project should inherit from `spring-boot-starter-parent`

```xml
<project>
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.realdolmen.spring</groupId>
  <artifactId>contacts</artifactId>
  <version>1.0</version>
  <packaging>jar</packaging>

  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>1.5.6.RELEASE</version>
  </parent>
</project>
```

---

## Project setup
- The `pom.xml` should also have the `spring-boot-maven-plugin` plugin
    - This plugin makes sure your application is runnable as a stand-alone jar by repackaging the dependencies into a single "über-jar"
        - Put this under `<build><plugins>`

```xml
<plugin>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-maven-plugin</artifactId>
</plugin>
```

- Since this will be a web application, also add the `spring-boot-starter-web` dependency
    - Put this under `<dependencies>`

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

---

## Controllers
- Since you have added the web starter dependency, Spring Boot will automatically configure all Spring beans required to enable MVC
    - Adding a Tomcat Servlet engine
    - Setting up the `DispatcherServlet`
    - …
- This means you can simply start programming the Spring MVC controllers the usual way without any special bean configuration

---

## Controllers
- Our controller might look like this

```java
@Controller
@RequestMapping("/")
public class ContactController {
  @Autowired
  private ContactRepository repository;

  @RequestMapping(method = RequestMethod.GET)
  public String home(Map<String, Object> model) {
    List<Contact> contacts = repository.findAll();
    model.put("contacts", contacts);
    return "home";
  }

  @RequestMapping(method=RequestMethod.POST)
  public String submit(Contact contact) {
    repository.save(contact);
    return "redirect:/";
  }
}
```

---

## Domain classes
- The domain class is a simple POJO called `Contact`
  - No special Spring Boot mechanics, just a POJO!
      - If you want to use JPA, this domain class can be upgraded to a JPA entity

```java
public class Contact {
  private Integer id;
  private String firstName;
  private String lastName;
  private String phoneNumber;
  private String emailAddress;

  // Getters and setters
}
```

---

## Views
- When using Thymeleaf, we will have to add an extra dependency

```xml
<dependency>
  <groupId>org.thymeleaf</groupId>
  <artifactId>thymeleaf-spring4</artifactId>
</dependency>
```

- Using the automatic configuration, Spring Boot will automatically enable all beans for Thymeleaf
    - `ThymeleafViewResolver`, `SpringTemplateEngine`, `TemplateResolver`, …
- Simply put the template files in the default location
    - `/src/main/resources/templates`

---

## Views
- The Thymeleaf template might look like this
  - This is a regular template, nothing up the sleeves!

```html
<h2>Spring Boot Contacts</h2>
<form method="POST">
  <label for="firstName">First Name:</label><input type="text" name="firstName"/><br/>
  <label for="lastName">Last Name:</label><input type="text" name="lastName"/><br/>
  <label for="phoneNumber">Phone #:</label><input type="text" name="phoneNumber"/><br/>
  <label for="emailAddress">Email:</label><input type="text" name="emailAddress"/><br/>
  <input type="submit"/>
</form>
<ul th:each="contact : ${contacts}">
  <li>
    <span th:text="${contact.firstName}">First</span>
    <span th:text="${contact.lastName}">Last</span> :
    <span th:text="${contact.phoneNumber}">phoneNumber</span>,
    <span th:text="${contact.emailAddress}">emailAddress</span>
  </li>
</ul>
```

---

## Static resources
- Static resources like JavaScript, CSS and images can be added quickly by sticking to the default locations
    - `src/main/resources/META-INF/resources/`
    - `src/main/resources/resources/`
    - `src/main/resources/static/`
    - `src/main/resources/public/`
- Spring Boot automatically configures a resource handler for these locations
    - For example, we can add a file style.css to any of the above directories

```css
body { background-color: #eeeeee; font-family: sans-serif; }
label { display: inline-block; width: 120px; text-align: right; }
```

---

## Persistence
- To add persistence, we need two dependencies:
    - `spring-boot-starter-jdbc`
        - Adds all the necessary jars for persistence (spring-jdbc, …)
    - A JDBC driver

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
<dependency>
  <groupId>com.h2database</groupId>
  <artifactId>h2</artifactId>
</dependency>
```

- Again, Spring Boot will automatically configure the required Spring beans
    - `DataSource`, `JdbcTemplate`, `TransactionManager`, …

---

## Persistence
- Exporting the data schema can be done automatically as well
    - Simply add a file called `schema.sql` under `/src/main/resources`

```sql
create table contacts (
  id identity,
  firstName varchar(30) not null,
  lastName varchar(50) not null,
  phoneNumber varchar(13),
  emailAddress varchar(30)
);
```

- Spring Boot will detect this file and execute it at boot time using the automatically configured database settings
    - Very useful if you are using an embedded database like H2 during development

---

## Persistence
- We can now start creating a regular repository implementation using `JdbcTemplate`

```java
@Repository
public class ContactRepository{
  @Autowired private JdbcTemplate jdbcTemplate;
  public List<Contact> findAll() {
    return jdbcTemplate.query("select id, firstName, lastName, phoneNumber, " +
    "emailAddress from contacts order by lastName", (rs, rowNum) -> {
      Contact contact = new Contact(rs.getInt(1), rs.getString(2), rs.getString(3),
      rs.getString(4), rs.getString(5));
      return contact;
    });
  }
  public void save(Contact contact) {
    jdbcTemplate.update("insert into contacts(firstName, lastName, phoneNumber, " +
    "emailAddress) values(?, ?, ?, ?)", contact.getFirstName(),
    contact.getLastName(), contact.getPhoneNumber(), contact.getEmailAddress());
  }
}
```

---

## Application configuration
- One final piece remains: bootstrapping the application
    - Spring Boot applications can run simply as a `main()` application
    - `SpringApplication.run()` instantiates `Main` and treats it as a `JavaBean` configuration
    - `@SpringBootApplication` is a shorthand for
        - `@EnableAutoconfiguration` (which triggers the automatic bean configurations)
        - `@Configuration`
        - `@ComponentScan` (from the current package and subpackages)
    - No need to configure the `DispatcherServlet`, add a `web.xml`, …

```java
@SpringBootApplication
public class Main{
  public static void main(String[] args) {
    SpringApplication.run(Main.class, args);
  }
}
```

---

## Running the application
- The application can be launched from the command line
  - This will launch an embedded web server
      - http://locahost:8080/
  - During development this is extremely useful

```dos
C:\project>mvn package
C:\project>java –jar target/contacts-1.0.jar
```

- Production environments usually still need to deploy the application as a WAR though
    - To do this, simply change the `pom.xml` packaging from `jar` to `war`
        - The application will then be both executable from the command-line as deployable as a WAR

---

## Enabling the Actuator
- Spring Boot allows you to add a number of REST endpoints to enable server monitoring
- This must be enabled by adding the actuator dependency
    - Using Maven by adding it as a `<dependency>`

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

- Automatic configuration will do the REST!

---

## Actuator endpoints

| Endpoint  | Description  |
| :-- | :-- |
| `GET /autoconfig`  | Displays auto-configuration guesses  |
| `GET /beans`  | Shows the beans in the `ApplicationContext`  |
| `GET /dump`  | Shows the application threads with their current stack  |
| `GET /env`  | Shows environment properties  |
| `GET /health`  | Displays health of the application  |
| `GET /info`  | Shows some application info  |
| `GET /metrics`  | Shows application statistics such as requests per controller  |
| `POST /shutdown`  | Shuts down the server  |
| `GET /trace`  | Shows HTTP details about the most recent requests  |

---

## Actuator endpoints
- The actuator endpoints show their information in JSON
    - This can be used to write a client against

```json
[{
  "context": "application",
  "parent": null,
  "beans": [{
    "bean": "contactController",
    "scope": "singleton",
    "type": "ContactController",
    "resource": "null",
    "dependencies": ["contactRepository"]
    }, {
    "bean": "contact",
    "scope": "singleton",
    "type": "Contact",
    "resource": "null",
    "dependencies": []
    }]
}]
```

---

## Exercise: Spring Boot
- Open the "boot" exercise provided by the teacher
    - We will use Spring Boot to build an administration web app for adding and removing animals from the "RealDolmen Zoo"

![](images/exercise-boot.png#no-border)

---

## Exercise: Spring Boot
- Exercise one: setting up a Spring Boot project
    - The goal of this exercise is to configure a Maven Spring Boot project
    - The following tasks are needed for this:
        1. Add the Spring Boot parent to the `pom.xml`
        2. Add the Spring starter dependency for a web application
        3. Add the Spring starter dependency for JDBC
        4. Add Thymeleaf as an explicit dependency
        5. Add H2 as an explicit dependency
        6. Add the Spring Boot plugin
    - This should give us a working project setup!

---

## Exercise: Spring Boot
- Exercise two: configuring the application
    - The purpose of this exercise is to activate the existing code using Spring Boot only
        - A number of classes are already provided; writing controllers and repositories is not the purpose of this exercise
    - The following tasks are needed for this:
        1. Create a `main()` that bootstraps the application using Spring Boot annotations and utilities
        2. Put the provided resources in the correct location
        3. Configure the provided controller and repository as Spring Beans
        4. Wire in the `JdbcTemplate` in the repository (where does it come from?)
        5. Launch the application and make sure everything works!
    - During this exercise, do not use any of the following annotations
        - `@EnableMvc`, `@Configuration`, `@ComponentScan`, `@Import`, `@Bean`

---

## Exercise: Spring Boot
- Exercise three: adding the Actuator
    - The goal of this exercise is to enable the Actuator and try it
    - The following tasks are needed for this:
        1. Enable the Actuator in the project
        2. Boot the application and verify in the logs if the actuator endpoints are activated
        3. Try out the `/health`, `/info` and `/beans` endpoints
