# Spring 4 Workshop

---

## Goal

> Learn how to use the Spring Framework to simplify Java Enterprise development

---

## Agenda
- Introduction
- Wiring Beans
- Aspect Oriented Programming
- Spring Persistence (JDBC / JPA)
- Spring Data
- Transaction Support
- Spring Web MVC
- Spring Test
- Spring Boot
- Spring Boot with Groovy
