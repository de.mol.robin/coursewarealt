# Spring JDBC

---

## JDBC pros and cons
- JDBC is a low-level data access API
    - It offers a way to execute SQL from Java
        - It does not make abstraction of the SQL statements
- Advantages
    - No extra data abstraction frameworks to learn
    - You can access the database’s proprietary features
    - Full control and tweaking of performance
- Disadvantages
    - No convenience features
    - Lots of error handling and boilerplate code

---

## Dependencies
- To use `JdbcTemplate` you need to add a dependency to your project

```xml
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-jdbc</artifactId>
  <version>4.1.5.RELEASE</version>
</dependency>
```

- You may also need a JDBC Driver for the corresponding database
    - This depends whether you use a Java EE server or not

```xml
<dependency>
  <groupId>mysql</groupId>
  <artifactId>mysql-connector-java</artifactId>
  <version>5.1.33</version>
</dependency>
```

---

## JDBC boilerplate code
- Traditional JDBC code requires lots of boilerplate logic
    - The error handling can not be skipped, as this would lead to resource leaks!

```java
public void addSpitter(Spitter spitter) {
  Connection conn = null; PreparedStatement s = null;
  try {
    conn = dataSource.getConnection();
    s = conn.prepareStatement("insert into spitter (username,password,fullname) values (?,?,?)");
    s.setString(1, spitter.getUsername());
    s.setString(2, spitter.getPassword());
    s.setString(3, spitter.getFullname());
    s.execute();
  } catch (SQLException e) { // Handle exception
  } finally {
    try {
      if (conn != null) { // Clean up resources
        conn.close();
      }
    } catch (SQLException e) { // Handle critical exception
    }
  }
}
```

---

## JDBC boilerplate code
- Another example to prove the point
    - Error handling can not be skipped, but is nearly identical to the previous example

```java
public Spitter findOne(long id) {
  Connection conn = null; PreparedStatement s = null; ResultSet rs = null;
  try {
    conn = dataSource.getConnection();
    s = conn.prepareStatement("select id, username, fullname from spitter where id=?");
    s.setLong(1, id)
    rs = s.executeQuery();
    if (rs.next()) {
      Spitter spitter= new Spitter();
      spitter.setId(rs.getLong("id"));
      spitter.setUsername(rs.getString("username"));
      spitter.setFullname(rs.getString("fullname"));
      return spitter;
    }
  } catch (SQLException e) { /* Handle exception */ }
  finally {
    try { if(conn != null) conn.close(); } catch(SQLException e) { /* Handle critical exception */ }
  }
  return null;
}
```

---

## JdbcTemplate
- Spring provides the `JdbcTemplate` class to facilitate working with JDBC
    - Uses the "template method" design pattern
- `JdbcTemplate` cleans up the nasty parts of JDBC, leaving only the advantages
    - Abstracts away boilerplate logic
        - Exception handling, closing connections, …
    - Provides easy to reuse convenience features
- This way applications can focus more on business value, and less on implementation technicalities

---

## Configuring JdbcTemplate
- Using `JdbcTemplate` is a matter of injecting a reference to a `JdbcTemplate` bean into your repository
    - `JdbcTemplate` itself depends on a `DataSource`

```java
@Bean
public JdbcTemplate jdbcTemplate(DataSource dataSource) {
  return new JdbcTemplate(dataSource);
}

@Bean
public DataSource dataSource() {
  return new DriverManagerDataSource("jdbc:mysql:/localhost/spitter", "user", "pass");
}
```

```java
@Repository
public class JdbcSpitterRepository implements SpitterRepository {
  @Autowired JdbcTemplate jdbcTemplate;
}
```

---

## Eliminating boilerplate code
- The two previous examples can now be rewritten using `JdbcTemplate`
    - Spring handles and converts `SQLException` to `DataAccessException` automatically

```java
public void addSpitter(Spitter spitter) {
  jdbcTemplate.update("insert into spitter(username, password, fullname) values(?, ?, ?)",
  spitter.getUsername(),
  spitter.getPassword(),
  spitter.getFullname());
}
```

```java
public Spitter findOne(long id) {
  return jdbcTemplate.queryForObject(
  "select id, username, fullname from spitter where id=?", (rs, rowNum) -> {
    return new Spitter(
    rs.getLong("id"),
    rs.getString("username"),
    rs.getString("fullname"));
  }, id);
}
```

---

## RowMappers
- `RowMapper`s are used by `JdbcTemplate` to convert a record to an object
    - They describe how `JdbcTemplate` should convert a JDBC `ResultSet` at a predefined position (record) into an arbitrary object
    - They can be reused if defined as a named class or variable

```java
jdbcTemplate.queryForObject("select * from spitter where id = ?", new RowMapper<Spitter>() {
  public Spitter mapRow(ResultSet rs, int rowNum) throws SQLException {
    Spitter spitter = new Spitter();
    spitter.setId(rs.getInt("id"));
    spitter.setUsername(rs.getString("username"));
    spitter.setPassword(rs.getString("password"));
    spitter.setEmail(rs.getString("email"));
    spitter.setFullname(rs.getString("fullname"));
    return spitter;
  }
}, id);
```

---

## RowMappers
- When using Java 8, you can reduce amount of the code by using lambdas

```java
RowMapper<Spitter> mapper = (rs, rowNum) -> {
  Spitter spitter = new Spitter();
  spitter.setId(rs.getInt("id"));
  spitter.setUsername(rs.getString("username"));
  spitter.setPassword(rs.getString("password"));
  spitter.setEmail(rs.getString("email"));
  spitter.setFullname(rs.getString("fullname"));
  return spitter;
}
jdbcTemplate.queryForObject("select * from spitter where id=1", mapper);
```

- Or even shorter when using a method reference to an existing method

```java
jdbcTemplate.queryForObject("select * from spitter where id=1", SpitterRepository::map);
```

```java
static Spitter map(ResultSet rs, int rowNum){ ... }
```

---

## JdbcTemplate API
- `JdbcTemplate` has many convenient methods for various SQL statements
    - Most have a number of overloaded variants that allow extra arguments to be passed

| Method | Description |
| :----- | :---------- |
| `execute()`  | For generic queries that do not return anything, such as `CREATE`, `ALTER`,…  |
| `query()`  | For queries that return lists of composites such as `SELECT * FROM spitter`  |
| `queryForList()` |  For queries that return lists of simple types such as `SELECT birth_date FROM spitter` |
| `queryForObject()` | For queries that return a single record such as `SELECT * FROM spitter WHERE id=1` |
| `queryForMap()` | For queries that return "flat" records for which there is no convenient class to map to  |
| `batchUpdate()` | For efficient execution of batch statements |
| `update()` | For `INSERT`, `DELETE` and `UPDATE` statements (that return an "affected row count") |

---

## Named parameters
- JDBC only supports indexed parameters natively

```sql
INSERT INTO spitter(username, password, fullname) VALUES(?, ?, ?)
```

- Spring extends this by adding named parameter support
    - Very convenient when a query requires a lot of parameters to be filled

```sql
INSERT INTO spitter(username, password, fullname) VALUES(:username, :password, :fullname)
```

- To use this, you should inject a `NamedParameterJdbcTemplate`

```java
Map<String, Object> paramMap = new HashMap<String, Object>();
paramMap.put("username", spitter.getUsername());
paramMap.put("password", spitter.getPassword());
paramMap.put("fullname", spitter.getFullname());
namedParameterJdbcTemplate.update(SQL, paramMap);
```

---

## Exercise: Spring JDBC

- Open the "data-access-jdbc" exercise provided by the teacher
    - Explore the project to familiarize yourself with the code and the database structure

![](images/data-access-jdbc-project.png#no-border)

![](images/data-access-jdbc-DB-Structure.png#no-border)

---

## Exercise: Spring JDBC
- Exercise one: configuring a `DataSource` and `JdbcTemplate`
    - We will need two `DataSource` configurations, one for "test" and one for "production"
        1. Add an Apache DBCP `Datasource` to the "production" profile
        2. Add an embedded H2 `Datasource` to the "test" profile
    - Next, add a `JdbcTemplate` that used (one of) the previously configured `DataSource`s

---
## Exercise: Spring JDBC
- Exercise two: implement the `FoodRepository` for JDBC
    - The goal of this exercise is to fix the unit tests and the `Main` application
    - The following steps are needed for this:
        1. Create a repository bean that implements the provided `FoodRepository`
        2. Inject a `JdbcTemplate` into your `FoodRepository`
        3. Implement `findAllFoods()`, and create a `RowMapper` to help you
        4. Implement `findFoodById()`, and reuse your `RowMapper`
        5. Implement `findFoodForAnimalType()`, and reuse your `RowMapper`
        6. Implement `removeFood()`
    - The unit tests will guide your way
        - Do not modify them!
    - When done, run the `Main` application and see what happens!
