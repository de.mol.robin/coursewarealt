# Aspect Orientation

---

## Cross-cutting concerns
- In software development, some functions need to be invoked in multiple places, but it is undesirable to invoke them explicitly every time
    - Examples: logging, security, transaction management, caching, …
- These are called "cross-cutting concerns"
- To keep the software manageable, cross-cutting concerns are often put in different compilation units
    - The Aspect Oriented Programming (AOP) allows us to do this
        - Allows us to decouple cross-cutting concerns from the business objects they affect

---

## Aspect Oriented Programming
- Cross-cutting concerns are needed across several regular business services

![](images/business-services.png#no-border)

- AOP allows us to modularize them as "aspects"
    - Offers a cleaner alternative to OO principles such as inheritance or delegation
    - Centralizes the logic concerned in a single location, without having a dependency to them in each business service

---

## AOP terminology
<img src="images/AOP.png#no-border" style="float:right; padding: 10px; height: 10em">

- Just like OO, the AOP paradigm has its own jargon
- Aspects are described in terms of:
    - "advices", "join points" and "pointcuts"
- We say that:
    - "Advices are woven into the program’s execution at specific join points selected by a pointcut"

---

## Advices
- An advice represents an aspect’s purpose
    - It controls two things: "what" to do and "when" to do it
- What:
    - The aspect’s functionality or "job", as a fragment of (Java) programming logic
- When:
    - An aspect is triggered upon an event in the program execution

| Advice   | Description  |
| :-------- | :----------- |
| `@Before`   | Functionality should trigger before execution of a business method   |
| `@After`, `@AfterReturning`, `@AfterThrowing`  | Functionality should trigger after execution of a business method |
| `@Around`   | The advice is wrapped around the business method (before and after)   |

---

## Join points
- A join point is a point in the normal program execution where an aspect can be plugged in
- Can be a variety of things
    - A method call
    - An exception being thrown
    - An object being instantiated
    - A field being assigned
- Your aspects will be inserted alongside these join points to "enhance" the normal business logic

---

## Pointcuts
- Aspects can be assigned to join points selectively
    - Not all aspects need to be inserted on all join points
- A pointcut allows selection of "where" to plug an aspect in
    - In other words: "on which join points"
- Often this is done by using some regular expression like syntax to select one or more join points for inclusion

---

## Aspects
- An aspect is simply the combination of a pointcut and an advice
    - This defines everything there is to know about: "what", "when" and "where"
- The name "aspect" allows us to talk about the concept as we do in natural language
    - For example:
        - "The security aspect of the system"
        - "Our application has many aspects to consider"
        - "Logging is only one aspect we need to deal with"

---

## Introduction
- An introduction allows us to dynamically add new methods or state to a previously defined business class
- Since we want to do something with this new method, we need to know about it at compile-time
    - For this reason, introductions are usually done by dynamically implementing an additional interface on an existing class
        - The interface can be typed against in client logic

---

## Weaving
- Weaving is the process of wrapping a proxy around a target object, so that the advices can intercept selected join points
    - A target object is a Java object that has a join point which is selected as the subject of an aspect
- Depending on the technology used, weaving can be done in multiple ways
    - At compile time
        - Requires a special compiler (AspectJ) that weaves aspects in using special syntax (not Java)
    - At class load time
        - Uses a special `ClassLoader` to weave aspects into a class when it’s being loaded in the JVM
    - At runtime
        - Dynamically weaves in aspects at runtime by using a container or engine (like Spring or Java EE)

---

## AOP frameworks
- There are many implementations of AOP systems
    - They differ in the join points they support and their weaving model
- JBoss AOP
    - Supports more join points than Spring AOP
- AspectJ
    - Supports both runtime and compile time weaving
    - Extends Java syntax, but required more complex build configuration
    - Very fine grained AOP support
- Spring AOP

---

## Spring AOP
- Spring's AOP engine can be used in multiple ways
    - Using annotations (preferred)
        - By borrowing the AspectJ runtime annotations, you can configure aspects on business objects by adding a few simple annotations
    - Using XML and pure POJOs
        - Using only XML configuration, you can configure aspects for business objects without modifying their source files
    - Low level using interfaces and classes to weave proxies around target objects
        - The outdated, but internally still used to implement the previous two
    - Using AspectJ compiled classes
        - The most powerful solution, requiring you to program aspects in AspectJ’s special language which is then compiled to JVM compatible bytecode

---

## Spring AOP
- Spring uses Java to write your advices
    - Allows you to reuse all existing Java knowledge at the price of some expressiveness
- Spring uses runtime weaving
    - Target objects will be proxied using the proxy design pattern to allow applying advices before, after or around business methods
    - These proxy objects are lazily created at runtime by the `ApplicationContext`
- Spring supports only method join points
    - Suffices for most scenarios, while keeping the complexity in check

---

## AOP proxies
- Spring AOP proxies are based on the proxy design pattern

![](images/AOP proxies.png#no-border)

---

## Configuring aspects using annotations
- Annotations is the preferred way to configure aspects in Spring 4
    - We will primarily focus on this style
- Spring borrows the AspectJ annotations and expression language for this
    - Only supports a subset of the full set of AspectJ designators
- The full documentation on AspectJ can be found here:
    - https://eclipse.org/aspectj/
- We will explore the most useful expressions on the next couple of slides

---

## Dependencies
- Since Spring is a modular framework, we need to add the AOP module as a dependency to the project
- When using Maven, this can be done by adding the following dependencies
    - `org.springframework:spring-aop`
    - `org.aspectj:aspectjrt`
    - `org.aspectj:aspectjweaver`
- When using Spring Boot, this is reduced to a single dependency

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```

---

## Enabling AOP
- To enable the Spring AOP engine, the `ApplicationContext` configuration must be annotated with `@EnableAspectJAutoProxy`

```java
@Configuration
@ComponentScan
@EnableAspectJAutoProxy
public class MyConfiguration {
  @Bean
  public Performer performer() {
    return new Concert(...);
  }
}
```

---

## Pointcut designators

- Pointcuts use designators to select a range of joinpoints on which their advice must act
    - Most of them have some sort of regexp-style syntax
- Each of these designators has its own specific expression syntax
    - We will explore this syntax by taking some examples from the book and the Spring reference manual
- Each expression can be chained using the logical operators or their word equivalents
    - `&&` (`and`), `||` (`or`), `!` (`not`)

---

## Pointcut designators
- Here is a list of AspectJ expressions supported by Spring

| Designator                    | Description |
| ---------------------------  | :---------- |
| `args()`, `@args()` | Limits join-point matches to the execution of methods whose arguments are instances of the given types, or are annotated with the given annotation types. |
| `execution()` | Matches join points that are method executions. |
| `this()`  | Limits join-point matches to those where the bean reference of the AOP proxy is of a given type. |
| `target()`, `@target()`   | Limits join-point matches to those where the target object is of a given type or is annotated with the given annotation type. Matched at runtime and can be used to bind the target type. |
| `within()`, `@within()`   | Static version of target() and @target(), which is faster but does not support binding. |
| `@annotation()` | Limits join-point matches to those where the subject of the join point has the given annotation. |

---

## Execution
- The `execution` designator uses the following syntax

```aspectj
execution(modifiers-pattern? ret-type-pattern declaring-type-pattern? name-pattern(param-pattern) throws-pattern?)
```

- You can use the `*` wildcard anywhere

| Pattern | Explanation |
| :------ | :---------- |
| modifiers-pattern | Visibility modifiers such as "public". Optional. |
| ret-type-pattern | FQN for the return type. Required. |
| declaring-type-pattern | FQN of the class that owns the joinpoint. Optional.|
| name-pattern | The joinpoint (method) to intercept. Required.|
| param-pattern | Comma separated list of FQN parameter names. May be empty. |
| throws-pattern | FQN of the throwing exception. Optional. |

---

## Execution
- Some examples using the Performance interface

<img src="images/Performance_interface1.png#no-border" style="float:right; padding: 10px; height: 6em">

```java
package concert;

public interface Performance {
  public String perform();
}
```

- Some examples using the Performance interface

<img src="images/Performance_interface2.png#no-border" style="float:right; padding: 10px; height: 6em">

```java
package concert;

public class Concert implements Performance {
  public String perform() {
    System.out.println("Keep on rocking in the free world!");
    return "Neil Young";
  }
}
```

---

## Execution
- The execution of any public method

```aspectj
execution(public * *(..))
```

- The execution of any method with a name beginning with "set"

```aspectj
execution(* set*(..))
```

- The execution of any method defined by the `AccountService` interface

```aspectj
execution(* com.xyz.service.AccountService.*(..))
```

- The execution of any method defined in the `service` package

```aspectj
execution(* com.xyz.service.*.*(..))
```

- The execution of any method defined in the `service`- or sub-package

```aspectj
execution(* com.xyz.service..*.*(..))
```

---

## Within, this and target
- Any method `within` the service package

```aspectj
within(com.xyz.service.*)
```

- Any method within the service package or a sub-package

```aspectj
within(com.xyz.service..*)
```
- Any method where the type of the target object has an `@Transactional` annotation

```aspectj
@within(org.springframework.transaction.annotation.Transactional)
```  
- Any method where the proxy implements the `AccountService` interface

```aspectj
this(com.xyz.service.AccountService)
```

---

## Within, this and target
- Any method where the target object implements the `AccountService` interface

```aspectj
target(com.xyz.service.AccountService)
```

- Any method where the target object has an `@Transactional` annotation:

```aspectj
@target(org.springframework.transaction.annotation.Transactional)
```

---

## Args
- Any method which takes a single parameter, and where the runtime type of the argument passed has the `@Classified` annotation

```aspectj
@args(com.xyz.security.Classified)
```
- Any method which takes a single parameter, and where the argument passed at runtime is `Serializable`

```aspectj
args(java.io.Serializable)
```

---

## Annotations
- Any method where the executing method has an `@Transactional` annotation

```aspectj
@annotation(org.springframework.transaction.annotation.Transactional)
```

---

## Bean
- Any method on a Spring bean named "tradeService"

```aspectj
bean(tradeService)
```

- Any method on Spring beans having names that match the wildcard expression `*Service`

```aspectj
bean(*Service)
```

- Only the `perform()` method on instances of `Performance` that are known in the Spring context with name "woodstock"

```aspectj
execution(* concert.Performance.perform()) and bean(woodstock)
```

---

## Aspect beans
- Aspects are created in Spring AOP as regular Java classes
    - These classes need to be registered in the `ApplicationContext` using __any__ of the available methods
- Using `@Bean` and `@Configuration`

```java
@Bean public Audience audience() { return new Audience(); }
```

- *__Or__* using `@Component` and `@ComponentScan`

```java
@Component public class Audience { }
```

- *__Or__* using XML and `<bean>` elements

---

## Aspect beans

- On top of that, they need to be marked as an aspect using the `@Aspect` annotation

```java
@Component // <--in case you chose the component scanning method
@Aspect
public class Audience {
  // Advices will be put inside the aspect
}
```

---

## Advices
- Advices are created by adding a method to an aspect bean that is marked with an Advice annotation
  - The method body will control the "what"
  - The annotation type will control the "when"
  - The embedded pointcut expression will control the "where"

```java
@Aspect
public class Audience {
  @Before("execution(* concert.Performance.perform(..))")
  public void takeSeats() { System.out.println("Taking seats"); }
}
```

---

## Advices
- There are five advice types

| Annotation | Advice |
| :-------- | :------- |
| `@Before` | The advice method is called before the advised method.|
| `@After ` | The advice method is called after the advised method.|
| `@AfterReturning` | The advice method is called only after the advised method returns gracefully by returning normally. |
| `@AfterThrowing` | The advice method is called only after the advised method returns ungracefully by throwing an exception. |
| `@Around` | The advice method is called before and after the advised method. This is the most powerful advice type, but also the most dangerous. |

---

## Before advice
- The `@Before` advice is the simplest of the advice types

```java
@Before("execution(* concert.Performance.perform(..))")
public void silenceCellPhones() {
  System.out.println("Silencing cell phones");
}

@Before("execution(* concert.Performance.perform(..))")
public void takeSeats() {
  System.out.println("Taking seats");
}
```

- It allows the advised method to be intercepted before being called itself
    - This can be used to "prepare" context for the advised method, such as opening a connection to a resource, so that the advised method doesn’t need to worry about it anymore

---

## After advice
- The `@After` advice has three shapes
    - `@After` (any), `@AfterReturning` and `@AfterThrowing`

```java
@AfterReturning("execution(* concert.Performance.perform(..))")
public void applause() {
  System.out.println("CLAP CLAP CLAP!!!");
}

@AfterThrowing("execution(* concert.Performance.perform(..))")
public void demandRefund() {
  System.out.println("Boo! Demanding a refund.");
}
```

- It allows the advised method to be "post-processed" depending on the exit condition

---

## After advice capturing
- `@AfterReturning` and `@AfterThrowing` are triggered after the advised method has been called
    - This means the advised method’s return or exception values are already "known"
- These returned / thrown object can be captured by the advice

```java
@AfterReturning(value="execution(* concert.Performance.perform(..))", returning="artist")
public void returnCapturingAdvice(String artist) {
  System.out.println("Great performance by" + artist);
}

@AfterThrowing(value="execution(* concert.Performance.perform(..))", throwing="myException")
public void exceptionCapturingAdvice(IllegalArgumentException myException) {
  System.out.println("Bad concert due to" + myException.getMessage());
}
```

---

## Around advice
- The `@Around` advice is somewhat special, because it is very powerful
    - It wraps the advised method completely, effectively combining a `@Before` and `@After`
- Because of this, it can exert full control over the advised method
    - Control whether or not the advised method is invoked at all
    - Modify or __change__ the return value
    - Modify or __change__ the thrown value
- This can be used for advanced cross-cutting concerns
    - Security: controlling access to an advised method
    - Transactions: opening before and commit/rollback after the advised method

---

## Around advice
- An `@Around` advice is used like this
  - They __must__ receive an instance of `ProceedingJoinPoint` as their first parameter
      - Use it to control when and if to call the advised method
  - If the advised method returns or throws something you wish to propagate, you must also return or throw it, otherwise it will be "swallowed"

```java
@Around("execution(* concert.Performance.perform(..))")
public String watchPerformance(ProceedingJoinPoint jp) throws Throwable{
  String artist = null;
  try{
    System.out.println("Silencing cell phones");
    artist = jp.proceed();
    System.out.println("CLAP CLAP CLAP!!!");
  } catch (Throwable e) {
    System.out.println("Demanding a refund");
  }
  return artist;
}
```

---

## JoinPoint reflection
- Any advice can introspect the `JoinPoint` that it is triggered by
    - Simply add a parameter of type `JoinPoint` __as the first__ of the advice method
    - This reference contains reflection-like introspection methods such as `getSignature()`

<img src="images/JoinPoint.png#no-border" style="float:right; padding: 10px; height: 10em">

```java
@Before("execution(* concert.Performance.perform(..))")
public void beforeAdvice(JoinPoint jp) {
  System.out.println("Called from" + jp.getSignature());
}
@AfterReturning(pointcut="execution(* concert.Performance.perform(..))",
returning="artist")
public void afterAdvice(JoinPoint jp, String artist) {
  System.out.println("Called on " + jp.getTarget());
}
@Around("execution(* concert.Performance.perform(..))")
public Object aroundAdvice(ProceedingJoinPoint jp) throwsThrowable{
  return jp.proceed();
}
```

---

## Named pointcuts
- Previously, we have used the same pointcut expression multiple times
    - This is not efficient: a modification of this pointcut would require multiple changes
- To reuse pointcut expressions, you can use the `@Pointcut` annotation on an empty method
    - This results in a reusable pointcut named `performance()`

```java
@Pointcut("execution(* concert.Performance.perform(..))") // pointcut expression
public void performance() {} // pointcut name
```

- You can use them in advice annotations

```java
@Before("performance()")
public void silenceCellPhones() {
  System.out.println("Silencing cell phones");
}
```

---

## Argument capturing
- All advice types can capture and bind the parameters from the advised method to a parameter of the advice method
    - Offers a similar feature as the returning and throwing attribute of the `@After`X advices
- Binding advised parameters is done using the `args()` designator
    - The name x of the advice method parameter must match the name used in `args(x)`

```java
public class CompactDisk{
  public void playTrack(int trackNr) { ... } // <--The method being adviced
}
```

```java
@Before("execution(* CompactDisk.playTrack(int)) && args(trackNumber)")
public void countTrack(int trackNumber) {
  System.out.println("Logging track played: " + trackNumber);
}
```

---

## Introductions
- Java does not have the concept of an "open class"
    - These are classes that allow new methods to be (dynamically) added without modifying the class itself
        - Other languages do have this: Groovy, C#, Ruby, JavaScript …
- The AOP concept of an "Introduction" allows us to emulate such a feature
    - We know that Spring implements AOP by creating a proxy that implements the same interface as the targeted class, and that this happens dynamically at runtime
    - When we let this proxy also implement another interface simultaneously, we have essentially emulated an "open class"
- The process of introducing behavior to existing classes is called a "mix-in"
    - They can be seen as a dynamic version of a trait or multiple-inheritance

---

## Introductions

```java
public interface Performance {
  void perform();
}
```

```java
public class Concert implements Performance {
  @Override
  public void perform() {
    System.out.println("Performing");
  }
}
```

```java
public interface Encoreable{
  void performEncore();
}
```

```java
public class DefaultEncore implements Encoreable{
  @Override
  public void performEncore() {
    System.out.println("Encoring");
  }
}
```

---

## Introductions
- The previous classes can be mixed together using an introduction aspect

```java
@Aspect
public class EncoreableIntroducer {
    @DeclareParents(value = "concert.Performance+", defaultImpl = DefaultEncore.class)
    public Encoreable encoreable;
}
```

???trainer
The `+` sign here is not a typo. It specifies any subtype of `Performance`, as opposed to `Performance` itself.
???t

---

## Introductions
- The configuration, usage and proxy structure looks like this

<img src="images/Structure.png#no-border" style="float:right; padding: 10px; height: 15em">

```java
@Bean
public Concert performance() {
  return new Concert();
}
@Bean
public EncoreableIntroducer aspect() {
  return new EncoreableIntroducer();
}
```

```java
Encoreable e = context.getBean("performance", Encoreable.class);
e.performEncore();

Performance p = context.getBean("performance", Performance.class);
p.perform();
```


---

## XML configuration
- The preferred way to configure AOP in Spring 4 is using annotations
- If desired you can also configure everything using only XML
    - This way, your classes become pure POJOs (if you consider classes "polluted" with annotations as not being POJOs)
- For this, the `aop`-namespace is available

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd
    http://www.springframework.org/schema/aop
    http://www.springframework.org/schema/aop/spring-aop.xsd">
    <!--bean definitions here-->
</beans>
```

---

## XML configuration
- You can use XML configuration that is fully equivalent to the annotations
  - All annotations can be removed from the source files

```xml
<aop:config>
  <aop:aspect ref="audience">
    <aop:pointcut id="performance"
      expression="execution(* concert.Performance.perform(..))" />
    <aop:before pointcut-ref="performance" method="silenceCellPhones"/>
    <aop:after-returning pointcut-ref="performance" method="applause"/>
    <aop:after-throwing pointcut="execution(* concert.Performance.perform(..))"
      method="demandRefund" throwing="myException"/>
  </aop:aspect>
</aop:config>
<bean id="audience" class="concert.Audience"/>
<bean id="concert" class="concert.Concert"/>
```

---

## Exercise: Aspect Orientation

- Open the "aspects" exercise provided by the Teacher
    - Explore the project to familiarize yourself with the code

![](images/Exercise-aspects.png#no-border)

---

## Exercise: Aspect Orientation
- Exercise one: counting visitors
    - The goal of this exercise is to fix a unit test
        - `zooKeepsCountOfAllVisitors()`
    - The following tasks are needed for this:
        1. Enable the Spring AOP Engine
        2. Create class `BookKeeping` as an aspect and a Spring bean
        3. Add an advice that advices the `Zoo.accept(Visitor)` method
        4. Make sure the advice uses the return value of the advised method to keep track of the number of happy and unhappy visitors
    - Do not change any of the unit tests or `Zoo` classes
        - You should rely solely on aspects to achieve your goal

---

## Exercise: Aspect Orientation
- Exercise two: launching a marketing campaign
    - The goal of this exercise is to fix a unit test:
        - `zooLaunchesMarketingCampaignWhenNewAnimalArrives()`
    - The following tasks are needed for this:
        1. Create a new aspect: `Marketing`
        2. Add an advice that advices the `Zoo` before new animals are added
        3. Trigger the `launchMarketingCampaign()` method with the right parameters. You should figure out where to retrieve the parameters from!
    - Do not change any of the unit tests or `Zoo` classes
        - You should rely solely on Aspects to achieve your goal

---

## Exercise: Aspect Orientation
- Exercise three: securing the zoo from escaping animals
    - The goal of this exercise is to fix two unit tests:
        - `zooIsAlertedWhenAnimalsEscape()`
        - `zooDoesNotAllowTigersToEscape()`
    - The following tasks are needed for this:
        1. Create a new aspect: `Security`
        2. Create a new advice that __prevents__ all animals except `Chimp`s to escape
        3. When any animal but `Chimp` tries to escape, prevent it by calling `preventEscapeOf()`
        4. When a `Chimp` escapes, sound the alert by throwing an `EscapedAnimalException`
    - Do not change any of the unit tests or `Zoo` classes
        - You should rely solely on Aspects to achieve your goal

---

## Exercise: Aspect Orientation
- Exercise four: adding `Restaurant` facilities
    - The goal of this exercise is to a unit test:
        - `zooAddsRestaurantFeaturesNextToCoreBusiness()`
    - The following tasks are needed for this:
        1. Create an implementation of `Restaurant`: `DefaultRestaurant`
        2. Create a new Aspect: `Facilities`
        3. Introduce the `Restaurant` behavior into the exiting `Zoo`
    - Do not change any of the unit tests or `Zoo` classes
        - You should rely solely on Aspects to achieve your goal
        - Take special note that neither `Zoo` nor `Restaurant` are explicitly combined at compile time!
