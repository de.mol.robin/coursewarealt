# Spring Test

---

## Test-driven development with Spring
- Because Spring takes Dependency Injection to heart, your code becomes suitable for unit testing
    - You can easily create mock dependencies and set them in the object you need to test
    - This allows you to focus on the class or method under test
    - You can remove any environment dependency
- Unit testing is not sufficient
    - At some point, you need to bring all the parts of the application together to see if they will work
    - This is integration testing
- Spring provides first-class testing support
    - Spring will help you write integration tests without deploying and running the whole system

---

## Test-driven development with Spring
- The Spring `TestContext` Framework is independent of the test framework of choice
  - You can use it while running tests in a standalone environment
  - You can use either JUnit or TestNG to run tests
- Goals
    - Ease of configuration and creation of the Spring Container
    - Injecting dependencies in beans as well as test suites
    - Help with testing database interactions and ORM code in transaction context
    - Test web functionality without deploying the application code in a web container

---

## Configuring the ApplicationContext
- You can use XML, JavaConfig and automatic wiring with unit tests
- Make sure you have the dependencies for Spring Test in your pom file

```html
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-context</artifactId>
  <version>4.x.x.RELEASE</version> <!-- Select latest release -->
</dependency>
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-test</artifactId>
  <version>4.x.x.RELEASE</version> <!-- Select latest release -->
  <scope>test</scope>
</dependency>
<dependency>
  <groupId>junit</groupId>
  <artifactId>junit</artifactId>
  <version>4.12</version>
  <scope>test</scope>
</dependency>
```

---

## Configuring the ApplicationContext
- Start the Spring `TestContext` with the JUnit annotation `@RunWith`
- Use `@ContextConfiguration` to load the configuration

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class AccountIntegrationTests {
  @Autowired
  private AccountService accountService;

  @Test
  public void accountServiceShouldBeInjected() {
    Assert.assertNotNull(accountService);
  }
}
```

- You can specify multiple configuration files to load
    - The default is to search for `<TestClassName>`‐context.xml in the same package as the test

---

## Configuring the ApplicationContext
- Loading JavaConfig classes

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={TestConfig.class})
public class AccountIntegrationTestsWithJavaConfig {
  // ...
}
```

- Behind the scenes, different classes handle the work
    - `TestContext`, `TestContextManager`, `TestExecutionListener`, `ContextLoader`, `SmartContextLoader`
    - You don't interact with these classes directly

---

## Configuring the ApplicationContext
- You can also combine multiple configurations, XML and JavaConfig, but not on the same class

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={TestConfig.class, ServiceConfig.class})
@ActiveProfiles(profiles={"test","c3p0"})
public class AccountIntegrationTestsWithJavaConfig {
  @Configuration
  @ImportResource("classpath:/applicationContext.xml")
  static class Config {
  }
  // ...
}
```

- Activate the profiles you need with `@ActiveProfiles`

---

## Inheriting context configuration
- Spring supports inheriting from base test classes

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={BaseTestConfig.class})
public class BaseTest {
  @Autowired
  protected Foo foo;
}
```

```java
@ContextConfiguration(classes={ChildTestConfig.class}) // context from BaseTest is inherited
public class ChildTest extends BaseTest {
  @Autowired
  private Bar bar;

  @Test
  public void dependenciesShouldBeAvailable() {
    Assert.assertNotNull(foo);
    Assert.assertNotNull(bar);
  }
}
```

---

## ApplicationContext caching
- The exact same configuration classes and XML locations can be specified on several test classes
    - In that case Spring creates the `ApplicationContext` only once and shares it among those test classes at runtime
    - The cache is kept in a static variable
- Gotcha!
    - For this to work, test suites need to run in the same process
    - If you run tests, you must make sure the build tool does not fork between tests
- If you need to discard the context, use `@DirtiesContext`

---

## Injecting dependencies in tests
- You can use `@Autowired`, `@Qualifier`, `@Resource`, `@Inject` and `@Named`

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DependencyInjectionTestConfig.class)
public class DependencyInjectionTests {
  @Autowired
  @Qualifier("foo1")
  private Foo foo1;

  @Resource
  private Foo foo2;

  @Resource
  private Bar bar;

  @Test
  public void testInjections() {
    Assert.assertNotNull(foo1);
    Assert.assertNotNull(foo2);
    Assert.assertNotNull(bar);
  }
}
```

---

## Injecting dependencies in tests
- If you need programmatic access to the `ApplicationContext`, you can `@Autowire` it in

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class DependencyInjectionTests {
  @Autowired
  private ApplicationContext applicationContext;
  // ...
}
```

---

## Using transaction management in tests
- You can execute tests within a transactional context
    - You will need a `transactionManager` of type `PlatformTransactionManager`
    - You need to place `@Transactional` on either the class or method

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TransactionalTestConfig.class)
@Transactional
public class TransactionalTests {
  @Test
  public void transactionalTestMethod1() {
    // ...
  }

  @Test
  public void transactionalTestMethod2() {
    // ...
  }
}
```

---

## Using transaction management in tests
- The main philosophy of unit tests is:

> Expect a clean environment before you run and leave that environment clean after you finish execution

- In accordance with this philosophy, Spring will rollback the transaction at the end of the test method instead of committing
    - That way, changes in the database will not cause side effects in later tests that also use the database
- If you need the transaction to commit, use `@Rollback(false)`

---

## Using transaction management in tests
- JUnit provides `@Before` and `@After` annotations to create fixtures
    - These let you run code snippets before and after the execution of each test method
    - If the method is `@Transactional`, these methods will run within the transaction
- If you want to execute some code outside the transaction, you can use
    - `@BeforeTransaction`
    - `@AfterTransaction`
- Spring will try to find a bean with the exact name `transactionManager`
    - Configure the behavior with `@TransactionConfiguration`

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TransactionalTestConfig.class)
@Transactional
@TransactionConfiguration(transactionManager="myTxMgr",defaultRollback=false)
public class TransactionalTests {
  // ...
}
```

---

## Testing with ORM frameworks
- Be careful for caching with ORM frameworks!
    - ORM frameworks accumulate persistence operations in their internal state
    - Those operations are usually executed at transaction commit time
    - Since the Spring framework rolls back the transaction, the persistence operations may not even be executed!
- You will need to flush the `EntityManager` at the end of the test method
    - This will ensure interaction with the database, so you are certain any database constraints are also triggered when needed
- For fast database tests, you can use an embedded database

---

## Testing web applications
- With Spring `TestContextFramework` you can load a `WebApplicationContext` in an integration test

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=WebApplicationTestConfig.class)
@WebAppConfiguration
public class WebApplicationTests {
  @Autowired
  private WebApplicationContext applicationContext;

  @Autowired
  private MockServletContext servletContext;

  @Test
  public void testWebApp() {
    Assert.assertNotNull(applicationContext);
    Assert.assertNotNull(servletContext);
  }
}
```

---

## Testing web applications
- Behind the scenes, Spring creates a `ServletContext` of type `MockServletContext`
    - By default, its base resource path is the `src/main/webappfolder`
    - Change the path with `@WebApplicationConfiguration`
- Spring also creates the following
    - `MockHttpServletRequest`
    - `MockHttpServletResponse`
- These are created per test method in a test suite and put into the Spring Web's thread local `RequestContextHolder`
    - This is cleared after the test method completes

---

## Testing request- and session-scoped beans
- `LoginAction` is request-scoped, `UserPreferences` is session-scoped

```java
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=ScopedBeanTestConfig.class)
public class ScopedBeanTests {
  @Autowired
  private UserService userService;

  @Autowired
  private MockHttpServletRequest httpServletRequest;

  @Autowired
  private MockHttpSession httpSession;

  @Test
  public void testScopedBeans() {
    httpServletRequest.setParameter("username", "jdoe");
    httpServletRequest.setParameter("password", "secret");
    httpSession.setAttribute("theme", "blue");
    Assert.assertEquals("jdoe", userService.getLoginAction().getUsername());
    Assert.assertEquals("secret", userService.getLoginAction().getPassword());
    Assert.assertEquals("blue", httpSession.getAttribute("theme"));
  }
}
```

???trainer
In the code above, the `MockHttpServletRequest` and `MockHttpSession` are created by Spring. Any beans with the corresponding scope will be created and be available for injection. In the test method, we can expect these beans to be available in the `UserService`.
???t

---

## Testing Spring MVC projects
- You can invoke the `DispatcherServlet` from your test code
    - This enables you to run integration tests without the servlet container!
- Imagine the following controller

<img src="images/Hello-Kitty.png#no-border" style="float:right; padding: 10px; height: 10em">

```java
@Controller
public class HelloController{
  @RequestMapping(value = "/hello")
  public ModelAndView sayHello() {
    ModelAndView mv = new ModelAndView();
    mv.addObject("message", "Hello Kitty!");
    mv.setViewName("hello");
    return mv;
  }
}
```

???trainer
Notice how we return a `ModelAndView` object from this controller. This corresponds to step 4 of the Spring MVC architecture.
???t

---

## Testing Spring MVC projects

```java
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=WebAppConfig.class)
public class HelloControllerTests{
  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  @Before
  public void setup() {
    this.mockMvc= MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @Test
  public void helloControllerWorksOk() throws Exception {
    mockMvc.perform(get("/hello"))
      .andExpect(status().isOk())
      .andExpect(model().attribute("message", "Hello Kitty!"))
      .andExpect(view().name("hello"));
  }
}
```

---

## Testing form submission
- Imagine the following controller

```java
@Controller
public class UserController{
  @RequestMapping(value = "/form")
  public ModelAndView user() {
    ModelAndView modelAndView = new ModelAndView("userForm", "user", new User());
    return modelAndView;
  }

  @RequestMapping(value = "/result", method=POST)
  public ModelAndView processUser(@Valid User user, Errors errors) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("user", user);
    if (errors.hasErrors()) {
      modelAndView.setViewName("userForm");
    } else {
      modelAndView.setViewName("userResult");
    }
    return modelAndView;
  }
}
```

---

## Testing form submission
- First test the happy flow of the form

```java
@Test
public void formSubmittedSuccessfully() throws Exception {
  this.mockMvc.perform(post("/result")
      .param("username", "johndoe")
      .param("email", "john@doe.com"))
    .andExpect(status().isOk())
    .andExpect(view().name("userResult"))
    .andExpect(model().hasNoErrors())
    .andExpect(model().attribute("user", hasProperty("username", is("johndoe"))))
    .andExpect(model().attribute("user", hasProperty("email", is("john@doe.com"))));
}
```

- You will need a dependency to `hamcrest-all` to use the `hasProperty()` matcher

---

## Testing form submission
- What if a validation error occurs?

```java
@Test
public void formSubmittedSuccessfullyButContainsValidationErrors() throws Exception {
  this.mockMvc.perform(post("/result")
      .param("email", "john.doe.com")) // not a valid email and username is missing
    .andDo(print())
    .andExpect(status().isOk())
    .andExpect(view().name("userForm"))
    .andExpect(model().hasErrors());
}
```

- The operation `andDo(print())` makes it possible to print the contents of `MockHttpServletRequest` and `MockHttpServletResponse`

---

## Testing form submission
- You may need the following dependencies

```xml
<dependency>
  <groupId>javax.el</groupId>
  <artifactId>javax.el-api</artifactId>
  <version>3.0.0</version>
  <scope>test</scope>
</dependency>
<dependency>
  <groupId>org.glassfish</groupId>
  <artifactId>javax.el</artifactId>
  <version>3.0.0</version>
  <scope>test</scope>
</dependency>
<dependency>
  <groupId>javax.servlet</groupId>
  <artifactId>javax.servlet-api</artifactId>
  <version>3.1.0</version>
  <scope>provided</scope>
</dependency>
```

---

## Testing exception handlers
- What if the controller throws exceptions?

```java
@Controller
public class UserController {
  // ...
  @RequestMapping(value = "/result")
  public ModelAndView processUser(String name) throws Exception {
    ModelAndView modelAndView = new ModelAndView();
    User user = users.get(name);
    if (user == null) { throw new UserNotFoundException(name);}
    modelAndView.addObject("user", user);
    modelAndView.setViewName("userResult");
    return modelAndView;
  }
  @ExceptionHandler
  public ModelAndView handleException(UserNotFoundException e) {
    ModelAndView modelAndView = new ModelAndView("errorUser");
    modelAndView.addObject("errorMessage", e.getMessage());
    return modelAndView;
  }
}
```

---

## Testing exception handlers
- You can then check if the `ExceptionHandler` caught the exception correctly

```java
@Test
public void userNotFoundExceptionHandledSuccessfully() throws Exception {
  this.mockMvc.perform(get("/result").param("name", "johndoe"))
  .andExpect(status().isOk())
  .andExpect(view().name("errorUser"))
  .andExpect(model().attribute("errorMessage","User not found with name: johndoe"));
}
```

---

## Spring-provided mock objects and other utilities
- The `org.springframework.mock` package and subpackages contain more mock object implementations
    - `MockEnvironment`
    - `MockPropertySource`
    - `SimpleNamingContextBuilder`
- Next to these Spring adds several utilities useful for testing
    - `ReflectionTestUtils`
    - `JdbcTestUtils`
- Some annotations can also help specific testing scenarios
    - `@Timed`
    - `@Repeat`
    - `@IfProfileValue`

???trainer
`@Repeat` can be used to quickly load test an application by performing the same test multiple times. Combine it with JUnit parameterized tests to perform the test multiple times on different data sets or with some random function. The scope of the `@Repeat` also includes any _set up_ and _tear down_ of the test fixture.
???t

---

## Exercise: Spring Test
- Open exercise "test" provided by the teacher
    - In this exercise we will add some unit tests for a small web application
    - Explore the code and run the `Blog` class
        - Navigate to http://localhost:8080

![](images/exercise-test.png#no-border)

---

## Exercise: Spring Test
- Exercise one: testing the `HomeController`
    - Our goal is to write a unit test that:
        1. Simulates a `GET` to `/index`
        2. Asserts that it returns status code `200 OK`
        3. Asserts that it yields view name `"index"`
        4. Asserts that the content of the page contains `"This is the homepage of the Realdolmen Blog."`
    - You will need to perform multiple tasks for this:
        1. Enable JUnit to run with Spring’s Runner
        2. Spring uses `BlogApplication` as the configuration set with `"test"` as the active profile
        3. A `MockMvc` is created for the entire web app context

---

## Exercise: Spring Test
- Exercise two: configuring the `TestContext` framework
    - The goal of this exercise is to create a reusable configuration for testing controllers
    - The following steps are needed for this:
        1. Configure `AbstractControllerTest` as a typical Spring test that loads the `BlogApplication` context with `"test"` as the active profile
        2. Create a field for a Mockito `Mock` for `AuthorRepository`
        3. Create a `@Before` fixture to setup a standalone `MockMvc` for the `AuthorController` and `RegistrationController`
        4. Configure both controllers to use the `AuthorRepository` mock

---

## Exercise: Spring Test
- Exercise three: testing the `Controller`s
    - The goal of this exercise is to implement `RegistrationControllerTest` and `AuthorControllerTest`
    - The following steps are needed for this:
        1. Write a test to verify that invalid input for registration form is not accepted
        2. Write a test to verify that valid input for registration form triggers a database insert
        3. Write a test to verify that `/authors` puts a list of authors on the model
        4. Write a test to verify that `/authors/1/remove` calls the repository’s `delete()` method correctly
