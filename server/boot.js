const child = require('child_process');

exports.config = () => {
    return new Promise((resolve, reject) => {
        let config = {
            versions: {
                node: child.execSync('node --version').toString().trim().substr(1),
                npm: child.execSync('npm --version').toString().trim(),
                phantomjs: child.execSync('phantomjs.cmd --version').toString().trim()
            },
            httpPort: 8090
        };

        console.log("  _____                                                            ___  ");
        console.log(" / ____|                                                          |__ \\ ");
        console.log("| |      ___   _   _  _ __  ___   ___ __      __ __ _  _ __  ___     ) |");
        console.log("| |     / _ \\ | | | || '__|/ __| / _ \\\\ \\ /\\ / // _` || '__|/ _ \\   / / ");
        console.log("| |____| (_) || |_| || |   \\__ \\|  __/ \\ V  V /| (_| || |  |  __/  / /_ ");
        console.log(" \\_____|\\___/  \\__,_||_|   |___/ \\___|  \\_/\\_/  \\__,_||_|   \\___| |____|");
        console.log("                                                  Where awesome thrives!");

        console.log('------------------------------------------------------------------------');
        console.log(`Node.js version:\t${config.versions.node}`);
        console.log(`NPM version:\t\t${config.versions.npm}`);
        console.log(`Phantom.js version:\t${config.versions.phantomjs}`);
        console.log(`Landing page:\t\thttp://localhost:${config.httpPort}`);
        console.log('------------------------------------------------------------------------');
        resolve(config);
    });
};