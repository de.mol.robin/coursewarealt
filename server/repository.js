const fs = require('fs');
const async = require('async');
const marked = require('marked');

const COURSES_BASE_FOLDER = "./courses/";

exports.findAllCourses = () => {
    return new Promise((resolve, reject) => {
        fs.readdir(COURSES_BASE_FOLDER, (error, courseCodes) => {
            if (error) {
                reject(error);
            } else {
                async.map(courseCodes, (courseCode, callback) => {
                    exports.findCourse(courseCode).then((course) => {
                        callback(null, course);
                    }, reject);
                }, (errors, courses) => {
                    console.log(`Courses found: ${courseCodes.join(', ')}`);
                    resolve(courses);
                });
            }
        });
    });
};

exports.findCourse = (courseCode) => {
    return new Promise((resolve, reject) => {
        fs.readFile(COURSES_BASE_FOLDER + courseCode + '/course.json', (error, data) => {
            if (error) {
                reject(new Error(`Unable to find course with code: -->${courseCode}<-- `));
            } else {
                let course = JSON.parse(data.toString());
                course.code = courseCode;
                console.log(`Retrieved course with code '${course.code}' and title '${course.title}'`);
                resolve(course);
            }
        });
    });
};

/**
 * Retrieves a course's table of contents going two levels deep, using the following layout:
 * <code>
 *  {
 *      "Chapter 1": ["Topio 1A", "Topic 1B"],
 *      "Chapter 2": ["Topio 2A", "Topic 2B"]
 *  }
 * </code>
 * @param courseCode The course code to retrieve table of contents for.
 */
exports.findCourseTableOfContents = courseCode => {
    return new Promise((resolve, reject) => {
        exports.findAllModules(courseCode).then((text) => {
            const tocEntry = /^<h(\d) id=".*">(.*)<\/h\1>$/;

            let toc = Object.create(null);
            let current = null;
            for (let line of marked(text).split('\n')) {
                let match = tocEntry.exec(line);
                if (match) {
                    let [, depth, title] = match;
                    if (depth == '1') {
                        toc[current = title] = [];
                    } else if (depth == '2') {
                        if (toc[current].indexOf(title) == -1) {
                            toc[current].push(title);
                        }
                    }
                }
            }
            return toc;
        }).then(resolve, reject);
    });
};

exports.findModule = (courseCode, moduleName) => {
    return new Promise((resolve, reject) => {
        fs.readFile(COURSES_BASE_FOLDER + courseCode + '/' + moduleName + '.md', (error, data) => {
            if(error) {
                reject(new Error(`Unable to find contents for module ${courseCode}#${moduleName}`));
            } else {
                console.log(`Retrieved module ${courseCode}#${moduleName}`);
                resolve(data.toString());
            }
        });
    });
};

exports.findAllModules = (courseCode) => {
    return new Promise((resolve, reject) => {
        exports.findCourse(courseCode).then((course) => {
            Promise.all(course.modules.map((moduleName) => {
                return exports.findModule(courseCode, moduleName);
            })).then((moduleContentsArray) => {
                let allModuleContentsText = '';
                for (let i = 0; i < moduleContentsArray.length; i++) {
                    allModuleContentsText += moduleContentsArray[i];

                    // Add markdown separator --- (<hr>) after all but the last module.
                    if (i < moduleContentsArray.length - 1) {
                        allModuleContentsText += '\n\n---\n\n';
                    }
                }
                resolve(allModuleContentsText);
            }, reject);
        }, reject);
    });
};