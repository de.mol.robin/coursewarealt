/**
 * @author Kevin Van Robbroeck
 */

const express = require('express');
const fs = require('fs');
const async = require('async');

const repository = require('./repository.js');
const handouts = require('./handouts.js');
const api = require('./api.js');

exports.start = (config) => {
    return new Promise((resolve, reject) => {
        console.log('Starting server...');
        let app = express();

        app.set('view engine', 'jade');
        app.set('views', './server/views');

        app.use(express.static(__dirname + '/public'));

        /**
         * Landing page.
         */
        app.get('/', (request, response, next) => {
            repository.findAllCourses().then((courses) => {
                response.render('courses', {courses});
            }, next);
        });

        /**
         * Transform TOC format to interop JSON for Education Admin Website (https://education.realdolmen.com/admin/)
         */
        app.get('/:courseCode/interop/admin-tool/table-of-contents.json', (request, response, next) => {
            repository.findCourseTableOfContents(request.params.courseCode).then(toc => {
                let output = {
                    chapters: []
                };
                for(let chapterName in toc) {
                    let chapter = {
                        name: chapterName,
                        topics: []
                    };
                    for(let topic of toc[chapterName]) {
                        chapter.topics.push({
                            title: topic
                        });
                    }
                    output.chapters.push(chapter);
                }
                response.json(output);
            }, next);
        });

        app.use('/api', api);

        /**
         * PDF rendering route. Renders handouts using Phantomjs.
         */
        app.get('/:courseCode.pdf', (request, response, next) => {
            let passParameters = (url, parameters) => {
                for(var parameter in parameters) {
                    if(parameters.hasOwnProperty(parameter)) {
                        url += '&' + parameter + '=' + parameters[parameter];
                    }
                }
                return url;
            };
            var url = 'http://localhost:' + config.httpPort + '/course.html?mode=handouts&course=' + request.params.courseCode;
            url = passParameters(url, request.query);
            console.log(`Requesting PDF rendering of handouts for course '${request.params.courseCode}'`);
            handouts.render(url).then(fileName => {
                console.log(`Received PDF location from PDF renderer at '${fileName}'`);
                response.sendFile(fileName, {root: '.'});
            }).catch(next);           
        });

        app.use(express.static('.'));

        app.use((error, request, response, next) => {
            response.status(500).render('error', {error});
            console.error(error);
        });

        app.listen(config.httpPort, function() {
            console.log(`Listening on port ${config.httpPort}`);
            resolve();
        });
    });
};