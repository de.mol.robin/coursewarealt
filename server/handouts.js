const child = require('child_process');

const phantomjsBinary = 'phantomjs.cmd';
const handoutsPdfOutputFile = './output/handouts.pdf';
const renderScript = './server/render.js';

exports.render = (url) => {
    return new Promise((resolve, reject) => {
        console.log(`Delegating rendering of '${url}' to PhantomJS... (normally takes a few seconds to complete)`);
        var process = child.spawn(phantomjsBinary, [renderScript, encodeURIComponent(url), handoutsPdfOutputFile]);
        var buffer = "";

        var captureOutput = (data) => {
            console.log('PhantomJS says:', data.toString().trim());
            buffer += data;
        };

        process.stdout.on('data', captureOutput);
        process.stderr.on('data', captureOutput);

        process.on('error', (error) => {
            reject(error);
        });

        process.on('close', (code) => {
            console.log('Phantom finished with code ' + code);
            if(code !== 0) {
                reject(new Error(buffer));
            } else {
                resolve(handoutsPdfOutputFile);
            }
        });
    });
};