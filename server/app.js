/**
 * @author Kevin Van Robbroeck
 */
const server = require('./server.js');
const boot = require('./boot.js');

boot.config().then(server.start).then(() => {
    console.log('Ready for work!');
});