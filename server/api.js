const marked = require('marked');
const router = require('express').Router();
const repository = require('./repository.js');


router.get('/courses', (request, response, next) => {
    repository.findAllCourses().then((courses) => {
        response.json(courses);
    }, next);
});

router.get('/course/:courseCode', (request, response, next) => {
    repository.findCourse(request.params.courseCode).then((course) => {
        response.json(course);
    }, next);
});

router.get('/course/:courseCode/module/:moduleName', (request, response, next) => {
    repository.findModule(request.params.courseCode, request.params.moduleName).then((text) => {
        response.format({
            'text/plain': () => {
                response.end(text);
            },

            'text/html': () => {
                response.end(marked(text));
            }
        });
    }, next);
});

router.get('/course/:courseCode/modules', (request, response, next) => {
    repository.findAllModules(request.params.courseCode).then((text) => {
        response.format({
            'text/html': () => {
                response.end(marked(text));
            },

            'text/plain': () => {
                response.end(text);
            }
        });
    }, next);
});

router.get('/course/:courseCode/table-of-contents', (request, response, next) => {
    repository.findCourseTableOfContents(request.params.courseCode).then(toc => {
        response.json(toc);
    }, next);
});

router.use((error, request, response, next) => {
    response.status(404).end(error.message);
});

module.exports = router;