/**
 * PhantomJS rendering script.
 * Used with phantomjs executable, not as a Node module.
 */

var page = require('webpage').create();
var system = require('system');

var url = decodeURIComponent(system.args[1]);
var outputFile = system.args[2];
console.log('URL: ' + url);
console.log('Output file: ' + outputFile);
if(!url || !outputFile) {
    url || console.log('Missing URL');
    outputFile || console.log('Missing output file');
    phantom.exit(1);
}

page.paperSize = {
  width: '21cm',
  height: '29.7cm',
  orientation: 'portrait',
  header: {
    height: '2.5cm',
    contents: phantom.callback(function(pageNum, numPages) {
      if (pageNum == 1) { // no header on first page
        return '';
      }
      var title = page.evaluate(function() {
        return $('title').text();
      });
      var direction = pageNum % 2 == 0 ? 'left' : 'right'; // header left on even page, right on odd page
      return '<div style="margin: 0.5cm 1cm 0 1cm; font-family: Arial, sans-serif; float:' + direction + '; font-style:italic; font-size:0.75em">' + title + '</div><hr style="clear:both;border: 0;height: 0;border-bottom: 1px solid black; margin: 0 1cm;"/>';
    })
  },
  footer: {
    height: '2cm',
    contents: phantom.callback(function(pageNum, numPages) {
      // return "<span class="footer">" + pageNum + " / " + numPages + "</span>";
      if (pageNum == 1) { // no footer on first page
        return '';
      }
      var direction = pageNum % 2 == 0 ? 'left' : 'right'; // footer left on even page, right on odd page
      return '<hr style="border: 0;height: 0;border-bottom: 1px solid black; margin: 0 1cm 0.5cm 1cm;"/><div style="margin: 0 1cm 1cm 1cm; font-family: Arial, sans-serif;float:' + direction + '; font-size:0.75em; background-color: #FFFFFF;">  &#169; <script>document.write(new Date().getFullYear())</script> Realdolmen  - '
          + pageNum + ' / ' + numPages + '</div>';

    })
  }
};

console.log('Opening page "' + url + '"');
page.open(url, function(status) {
    console.log(status);
    var delay = 10000;
    console.log('Allowing the page to settle (delay ' + delay + 'ms)');
    setTimeout(function () {
        console.log('Rendering page to file "' + outputFile + '"');
        page.render(outputFile);
        console.log('Rendered.');
        phantom.exit(status === 'success' ? 0 : 1);
    }, delay); // this timeout is required to make sure the page is rendered completely before printing (longer may be necessary)
});
