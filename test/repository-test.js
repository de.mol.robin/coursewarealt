const assert = require('chai').assert;

const repository = require('../server/repository.js');

describe('Repository', () => {
    describe('findCourse()', () => {
        it('resolves to a course when correct course code is passed', () => {
            return repository.findCourse('HTM050').then((course) => {
                assert.equal('Upgrading Skills to HTML5', course.title);                
            });
        });
        
        it('rejects when an invalid course code is passed', () => {
            return repository.findCourse('NO_EXIST').then(() => {
                assert.fail('Must be rejected');
            }).catch((error) => {
                assert.equal('Unable to find course with code NO_EXIST', error.message)
            });
        });
    });

    describe('findAllModules()', () => {
        it('resolves to concatenated contents of all modules', () => {
            return repository.findAllModules('GIT010').then((text) => {
                assert.isString(text);
                assert(text.indexOf('# Introduction') !== -1);  // String 'contains()'
                assert(text.indexOf('# Basic Commands') !== -1);
                assert(text.indexOf('# Merging') !== -1);
                assert(text.indexOf('# Branches') !== -1);
                assert(text.indexOf('# Remotes') !== -1);
                assert(text.indexOf('# Security') !== -1);
                assert(text.indexOf('# Extras') !== -1);
                assert(text.indexOf('# References') !== -1);
            });
        });

        it('does not end with --- separator', () => {
            return repository.findAllModules('GIT010').then((text) => {
                assert(!text.trim().endsWith('---'));
            });
        });

        it('adds --- separator between modules', () => {
            return repository.findAllModules('GIT010').then((text) => {
                console.log(text);
                assert(!text.indexOf('\n\n---\n\n#Basic commands') !== -1);
            });
        });
    });

    describe('findAllCourses()', () => {
        it('resolves to an array of courses', () => {
            return repository.findAllCourses().then((courses) => {
                assert.isArray(courses);
                assert.equal('Android Unit Testing', courses[0].title); //Assumes the first course is always AND610. Fragile.
            });
        });
    });

    describe('findModule()', () => {
        it('resolves contents of a module', () => {
            return repository.findModule('HTM050', 'css3-flex').then((moduleContents) => {
                assert(moduleContents.startsWith('# Flex Layout'));
            });
        });

        it('rejects when module can not be found', () => {
            return repository.findModule('HTM050', 'myUnexistingModuleName').then(assert.fail).catch((error) => {
                assert.equal('Unable to find contents for module HTM050#myUnexistingModuleName', error.message);
            });
        })
    });

    describe('findCourseTableOfContents()', () => {
        it('resolves table of contents', () => {
            return repository.findCourseTableOfContents('GIT010').then(toc => {
                assert.equal(toc['Getting Started'][0], 'Course Objectives');
                assert.equal(toc['Getting Started'][1], 'Agenda');
                assert.equal(toc['Introduction'][0], 'What is a Version Control System?');
                assert.equal(toc['Introduction'][1], 'Revisions');
                assert.equal(toc['Security'][0], 'Remote Access Using SSH');
                assert.equal(toc['Security'][1], 'Signing Commits With GPG Keys');
            });
        });

        it('rejects when course code is invalid', () => {
            return repository.findCourseTableOfContents('BLA').then(assert.fail).catch((error) => {
                assert.equal('Unable to find course with code BLA', error.message);
            });
        });
    })
});