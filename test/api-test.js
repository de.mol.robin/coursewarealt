const assert = require('chai').assert;
const request = require('supertest');
const express = require('express');

const sinon = require('sinon');
require('sinon-as-promised');   // Wraps sinon with extra promise-oriented methods. Why is this not part of sinon by default?

const apiRouter = require('../server/api.js');
const repository = require('../server/repository.js');

describe('Api', () => {
    let api = express();
    api.use(apiRouter);

    let sandbox = null;

    /**
     * Sets up a sandbox for sinon. Everything that is stubbed or mocked using sinon can be restored using the afterEach()
     * Otherwise all future tests would continue running with the same stubs of each of the stubbed modules.
     */
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('GET /course/:courseCode', () => {
        it('should succeed with 200 and json', (done) => {
            let findCourse = sandbox.stub(repository, 'findCourse').resolves({title: 'Stub Course'});

            request(api).get('/course/HTM050').end((error, response) => {
                assert.equal(response.status, 200);
                assert.equal(response.body.title, 'Stub Course');
                assert(response.headers['content-type'].startsWith('application/json'));

                assert.isTrue(findCourse.calledWith('HTM050'));
                done();
            });
        });

        it('should fail with 404 if unexisting course code is given', (done) => {
            let findCourse = sandbox.stub(repository, 'findCourse').resolves({title: 'Stub Course'}).rejects(new Error('Stub Rejection'));

            request(api).get('/course/UNEXISTING').end((error, response) => {
                assert.equal(response.status, 404);
                assert.equal(response.text, 'Stub Rejection');
                done();
            });
        });
    });

    describe('GET /courses', () => {
        it('should succeed with 200 and json', (done) => {
            let findAllCourses = sandbox.stub(repository, 'findAllCourses').resolves([{title: 'Stub Course A'}, {title: 'Stub Course B'}]);

            request(api).get('/courses').end((error, response) => {
                assert.equal(response.status, 200);
                assert.equal(response.body.length, 2);
                assert.isTrue(response.headers['content-type'].startsWith('application/json'));
                done();
            });
        });
    });

    describe('GET /course/:courseCode/modules', () => {
        it('should succeed with 200 and markdown when accept header is `text/plain`', (done) => {
            let findAllModules = sandbox.stub(repository, 'findAllModules').resolves('# Chapter 1\n\n---\n\n# Chapter 2');

            request(api).get('/course/GIT010/modules')
                .set('Accept', 'text/plain')
                .end((error, response) => {
                    assert.equal(response.status, 200);
                    assert.equal(response.text, '# Chapter 1\n\n---\n\n# Chapter 2');
                    assert.isTrue(findAllModules.calledWith('GIT010'));
                    done();
                });
        });

        it('should succeed with 200 and html when accept header is `text/html`', (done) => {
            let findAllModules = sandbox.stub(repository, 'findAllModules').resolves('# Chapter 1\n\n---\n\n# Chapter 2');

            request(api).get('/course/GIT010/modules')
                .set('Accept', 'text/html')
                .end((error, response) => {
                    assert.equal(response.status, 200);
                    assert.equal(response.text, '<h1 id="chapter-1">Chapter 1</h1>\n<hr>\n<h1 id="chapter-2">Chapter 2</h1>\n');
                    assert.isTrue(findAllModules.calledWith('GIT010'));
                    done();
                });
        });

        it('should fail with 404 if unexisting course code is passed', (done) => {
            let findAllModules = sandbox.stub(repository, 'findAllModules').rejects(new Error('Stub Rejection'));

            request(api).get('/course/UNEXISTING/modules').end((error, response) => {
                assert.equal(response.status, 404);
                assert.equal(response.text, 'Stub Rejection');
                done();
            });
        });
    });

    describe('GET /course/:courseCode/table-of-contents', () => {
        it('succeeds with 200 OK with table of contents if course code exists', (done) => {
            let findAllModules = sandbox.stub(repository, 'findAllModules').resolves('# Chapter 1\n\n---\n\n## Topic 1A\n\n---\n\n## Topic 1B\n\n---\n\n# Chapter 2\n\n---\n\n## Topic 2A\n\n---\n\n## Topic 2B');

            request(api).get('/course/GIT010/table-of-contents')
                .end((error, response) => {
                    assert.equal(response.status, 200);
                    var data = JSON.parse(response.text);
                    assert.equal(data['Chapter 1'][0], 'Topic 1A');
                    assert.equal(data['Chapter 1'][1], 'Topic 1B');
                    assert.equal(data['Chapter 2'][0], 'Topic 2A');
                    assert.equal(data['Chapter 2'][1], 'Topic 2B');
                    assert.isTrue(findAllModules.calledWith('GIT010'));
                    done();
                });
        });

        it('fails with 404 if course code is invalid', (done) => {
            let findAllModules = sandbox.stub(repository, 'findAllModules').rejects(new Error('Does not compute'));
            request(api).get('/course/UNEXISTING/table-of-contents')
                .end((error, response) => {
                    assert.equal(response.status, 404);
                    assert.equal(response.text, 'Does not compute');
                    assert.isTrue(findAllModules.calledWith('UNEXISTING'));
                    done();
                });
        });
    });

    describe('GET /course/:courseCode/module/:moduleName', () => {
        it('should succeed with 200 and markdown when accept header is `text/plain`', (done) => {
            let findModule = sandbox.stub(repository, 'findModule').resolves("# Stub Module");

            request(api).get('/course/HTM050/module/head')
                .set('Accept', 'text/plain')
                .end((error, response) => {
                    assert.equal(response.status, 200);
                    assert.equal(response.text, "# Stub Module");
                    assert.isTrue(response.headers['content-type'].startsWith('text/plain'));
                    assert.isTrue(findModule.calledWith('HTM050', 'head'));
                    done();
                });
        });

        it('should succeed with 200 and html when accept header is `text/html`', (done) => {
            let findModule = sandbox.stub(repository, 'findModule').resolves("# Stub Module");

            request(api).get('/course/HTM050/module/head')
                .set('Accept', 'text/html')
                .end((error, response) => {
                    assert.equal(response.status, 200);
                    assert.equal(response.text, '<h1 id="stub-module">Stub Module</h1>\n');
                    assert.isTrue(response.headers['content-type'].startsWith('text/html'));
                    assert.isTrue(findModule.calledWith('HTM050', 'head'));
                    done();
                });
        });

        it('should fail with 404 if unexisting module name is given', (done) => {
            let findModule = sandbox.stub(repository, 'findModule').rejects(new Error("Stub Rejection"));

            request(api).get('/course/HTM050/module/unexisting').end((error, response) => {
                assert.equal(response.status, 404);
                assert.equal(response.text, 'Stub Rejection');
                done();
            });
        });
    });
});
